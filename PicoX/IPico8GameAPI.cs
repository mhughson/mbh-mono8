﻿using System;

namespace PicoX
{
    /// <summary>
    /// Stores information about a tile beyond just the final ID used to draw.
    /// </summary>
    public struct TileData
    {
        public int ID;
        public bool FlipX;
        public bool FlipY;
        public int Bank;
    }

    /// <summary>
    /// The public Pico 8 API exposed to Pico8 games.
    /// </summary>
    /// <remarks>
    /// Uses float for anything that could likely be interpolated (position, width, etc), and assumes
    /// that implementation will case down for pixel perfect rendering.
    /// </remarks>
    public interface IPico8GameAPI
    {
        // DRAWING
        //

        void cls(int c = 0);

        void pset(float x, float y, int c);

        int pget(int x, int y);

        void sspr(int sx, int sy, float sw, float sh, float dx, float dy, float dw = float.MaxValue, float dh = float.MaxValue, bool flip_x = false, bool flip_y = false);

        void spr(int n, float x, float y, float w = 1, float h = 1, bool flip_x = false, bool flip_y = false);

        void sprfxset(int fx, bool enabled);

        void sprfxadd(Func<int, int, int, int> fx_func, int id);

        // Sets the sprite bank to use for all rendering calls after this point.
        void bset(int n);

        // Gets the color value of a pixel on the sprite sheet.
        int sget(int x, int y);

        int mget(int celx, int cely, int map_layer = 0);

        TileData? mget_tiledata(int celx, int cely, int map_layer = 0);

        bool mfget(int celx, int cely, int flag, int map_layer = 0);

        byte mfgetflags(int celx, int cely, int map_layer = 0);

        void mset(int celx, int cely, int snum, int map_layer = 0);

        void msetbank(int celx, int cely, int snum, int bank, int map_layer = 0);

        bool fget(int n, int f);

        byte fget(int n);

        bool fget(TileData? tile_data, int flags);

        byte fget(TileData? tile_data);

        void fset(int n, int f, bool v);

        void fset(int n, byte f);

        void palt(int col, bool t);

        void pal(int c0, int c1, int p = 0);

        void pal();

        void line(float x0, float y0, float x1, float y1, int col);
        
        void circfill(float x, float y, float r, int col);

        void circ(float x, float y, float r, int col);

        void rect(float x0, float y0, float x1, float y1, int col);

        void rectfill(float x0, float y0, float x1, float y1, int col);

        void print(string str, float x, float y, int col);

        void camera(float x, float y);

        void map(int celx, int cely, float sx, float sy, int celw, int celh, int layer = 0, int map_layer = 0);

        void set_render_scale(float new_scale = 1.0f);

        // AUDIO
        //

        void sfx(int sfx, int channel = 0, int offset = 0);

        void music(int song, int fadems = 0, int channelmask = 0);

        // INPUT
        //

        bool btn(int id, int player = 0);

        bool btnp(int id, int player = 0);

        // p8 extension to support checking if a button was "released" this frame.
        bool btnr(int id, int player = 0);

        // MATH
        //

        void srand(int val);

        float rnd(float max);

        float sin(float a);

        float cos(float a);

        float min(float a, float b);

        float max(float a, float b);

        float mid(float a, float b, float c);

        float sqrt(float a);

        int flr(float a);

        float abs(float a);

        // FILE IO
        //

        void printh(string str, string filename = "", bool overwrite = false);

        int dget(uint index);

        void dset(uint index, int value );

        void cartdata(string id);

        void reloadmap(string map_string);

        void menuitem(int index, string label, Action callback);
        void menuitem(int index);
    }
}
