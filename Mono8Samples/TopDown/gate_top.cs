﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class gate_top : gate_base
    {
        public gate_top(string door_type_num, PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            w = 8;
            h = 16;
            open_anim = "top_door_" + door_type_num + "_opening";
            set_anim("top_door_" + door_type_num + "_closed");
        }

        public override void on_door_open_complete(string anim_name)
        {
            if (anim_name == open_anim)
            {
                Game.game_world.cur_area.fset((int)(x / 8), (int)((y) / 8), area.collision_flag.none);
                Game.game_world.cur_area.fset((int)(x / 8), (int)((y) / 8) - 1, area.collision_flag.none);
            }
        }
    }
}
