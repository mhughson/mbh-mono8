﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    public class melee_swipe : bullet
    {
        public melee_swipe(float _x, float _y, float _dx, float _dy, obj _source, type_flags _collision_mask, PicoXGame owner, area owning_area = null) 
            : base(_x, _y, _dx, _dy, _source, _collision_mask, owner, owning_area)
        {
            anims = new Dictionary<string, anim>()
                {
                    {
                        "idle",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= new int[][] { new int[] { 0 } },//what frames are shown.
                        }
                    }
                };

            set_anim("idle");
        }

        public override rect col_box()
        {
            return new rect(x, y + 4, 8, 8);
        }
    }
}
