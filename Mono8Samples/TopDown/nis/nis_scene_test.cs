﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.nis
{
    public class nis_scene_test : obj
    {
        public nis_scene_test(PicoXGame owner) : base(owner)
        {
            // This is a behavior tree, where each "node" has a number of conditions, and if they all pass,
            // a series of "tracks" will play. Each track type does something different (spawn objects, move objects,
            // open some ui, etc). Each track instance has an optional start and end time. Each track can be either
            // a slave or master. If it is a master, then that node will not be considered completed until that track
            // reports itself done. In the case of an animation, that might be when the animation finishes playing.
            // For an NPC dialog box, that might be after the user presses a button to dismiss it.
            motion_tree_root = new fight_tree.fight_branch
            (
                conditions: null,
                name: "root",
                owner: Game,
                children: new List<fight_tree.fight_branch>()
                {
                    new fight_tree.fight_node
                    (
                        name:"intro",
                        owner:Game,
                        conditions: new List<fight_tree.fight_condition>()
                        {
                            // This is minor hack to make cutscenes behavior trees play out in
                            // order.
                            new fight_tree.fight_condition_play_once(Game),
                        },
                        tracks: new List<fight_tree.fight_track>()
                        {
                            // A simple track that just waits 60 ticks.
                            // It is a master track, so this node will not exit until this track is done.
                            new fight_tree.fight_track_wait(60, this, Game) { is_master = true },
                        },
                        children:null
                    ),
                    new fight_tree.fight_node
                    (
                        name:"shot 1",
                        owner:Game,
                        conditions: new List<fight_tree.fight_condition>()
                        {
                            new fight_tree.fight_condition_play_once(Game),
                        },
                        tracks: new List<fight_tree.fight_track>()
                        {
                            // spawn an object with the name "nis_object".
                            // position and stuff are hard coded right now in the track itself.
                            new fight_tree.fight_track_spawn_object("nis_object", this, Game),
                            // spawn some explosion effects at the position of the nis_object we just spawned.
                            new fight_tree.fight_track_custom(null, new Func<bool>(() =>
                            {
                                obj o = Game.game_world.cur_area.find_obj_by_id_name("nis_object");
                                new explosion_series(1, 0, 0, 0, Game) { x = o.x, y = o.y }.activate();
                                return true;
                            }), this, Game ),
                            // play the walking down animation on the object we just spawned.
                            new fight_tree.fight_track_play_anim("soldier_walk_down", 1, "nis_object", Game),
                            // move the object down for 60 ticks (speed and stuff is hard coded atm).
                            // note: is_master so this node won't exit until the object is moved for 60 ticks.
                            new fight_tree.fight_track_move_in_dir(0, 1, "nis_object", Game) { is_master = true, length = 60 },
                            // open an npc dialog box.
                            // note: is_master so this node won't exit until the user dismisses this dialog box.
                            new fight_tree.fight_track_npc_dialog(new ui_dialog("good bye...", "portrait_0", Game, Game.game_world.ui_area), this, Game) { start = 30, is_master = true },
                            // after 60 ticks, play the idle animation (incase the player doesn't dismiss the dialog box right away).
                            new fight_tree.fight_track_play_anim("soldier_idle_down_armed", 1, "nis_object", Game) { start = 60 },
                        },
                        children:null
                    ),
                    new fight_tree.fight_node
                    (
                        name:"shot 2",
                        owner:Game,
                        conditions: new List<fight_tree.fight_condition>()
                        {
                            new fight_tree.fight_condition_play_once(Game),
                        },
                        tracks: new List<fight_tree.fight_track>()
                        {
                            // play the idle animation
                            new fight_tree.fight_track_play_anim("soldier_idle_down_armed", 1, "nis_object", Game),
                            // wait for 10 ticks.
                            // note: same thing could have been accomplished by giving track above a length of 10, and making it a master.
                            new fight_tree.fight_track_wait(10, this, Game) { is_master = true },
                        },
                        children:null
                    ),
                    new fight_tree.fight_node
                    (
                        name:"shot 3",
                        owner:Game,
                        conditions: new List<fight_tree.fight_condition>()
                        {
                            new fight_tree.fight_condition_play_once(Game),
                        },
                        tracks: new List<fight_tree.fight_track>()
                        {
                            new fight_tree.fight_track_play_anim("soldier_walk_left", 1, "nis_object", Game),
                            new fight_tree.fight_track_move_in_dir(-1, 0, "nis_object", Game),
                            new fight_tree.fight_track_wait(120, this, Game) { is_master = true },
                        },
                        children:null
                    ),
                    new fight_tree.fight_node
                    (
                        name:"cleanup",
                        owner:Game,
                        conditions: new List<fight_tree.fight_condition>()
                        {
                            new fight_tree.fight_condition_play_once(Game),
                        },
                        tracks: new List<fight_tree.fight_track>()
                        {
                            // spawn a big explosion.
                            new fight_tree.fight_track_custom(null, new Func<bool>(() => 
                            {
                                obj o = Game.game_world.cur_area.find_obj_by_id_name("nis_object");
                                new explosion_series(20, 0, 8, 16, Game) { x = o.x, y = o.y }.activate();
                                return true;
                            }), this, Game ),
                            // destroy the object we spawned.
                            new fight_tree.fight_track_destroy_object("nis_object", Game),
                            // destory this cutscene itself!
                            new fight_tree.fight_track_destroy_object(this, Game),
                        },
                        children:null
                    )
                }
            );
        }
    }
}
