﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    class fight_track_wait : fight_track
    {
        int frames_remaining;

        public fight_track_wait(int frames_to_wait, obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {
            frames_remaining = frames_to_wait;
        }

        public override bool advance(int frames)
        {
            frames_remaining--;
            if (frames_remaining <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
