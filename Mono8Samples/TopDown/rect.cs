﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class rect
    {
        public float x { get; private set; }
        public float y { get; private set; }
        public float w { get; private set; }
        public float h { get; private set; }

        public rect(float center_x, float center_y, float width, float height)
        {
            x = center_x;
            y = center_y;
            w = width;
            h = height;
        }

        public float top { get { return y - h * 0.5f; } }
        public float left { get { return x - w * 0.5f; } }
        public float bottom { get { return y + h * 0.5f; } } // -1 so that we are IN the rectangle.
        public float right { get { return x + w * 0.5f; } } // -1 so that we are IN the rectangle.

        public bool intersects(rect other)
        {
            var xd = x - other.x;

            var xs = w * 0.5f + other.w * 0.5f;

            if (Math.Abs(xd) >= xs)
            {
                return false;
            }

            var yd = y - other.y;

            var ys = h * 0.5f + other.h * 0.5f;

            if (Math.Abs(yd) >= ys)
            {
                return false;
            }

            return true;
        }

        public bool contains(float x, float y)
        {
            return x >= left && x <= right && y >= top && y <= bottom;
        }
    }
}
