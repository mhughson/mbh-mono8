﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PicoX;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using System.Threading;
using MoonSharp.Interpreter;
using System.IO;
using System.Diagnostics;

namespace Mono8
{
    /// <summary>
    /// Helper struct for detecting single key presses.
    /// </summary>
    public struct BufferedKey
    {
        private class KeyGroup
        {
            public Keys[] RequiredKeys;
            public bool IsPressed;
        }
        /// <summary>
        /// The keys that must ALL be pressed to trigger this.
        /// </summary>
        private List<KeyGroup> Keys;

        /// <summary>
        /// Checks if any of the possible key combinations are active.
        /// </summary>
        public bool IsPressed { get { return Keys.Exists(x => x.IsPressed == true); } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Key">A key which needs to be pressed.</param>
        public BufferedKey(Keys Key) : this(new Keys[] { Key })
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Key">An array of keys which all need to be pressed.</param>
        public BufferedKey(Keys[] Key) : this(new Keys[][] { Key })
        {
        }

        public BufferedKey(Keys[][] KeyGroups)

        {
            Keys = new List<KeyGroup>();

            foreach(Keys[] kgroup in KeyGroups)
            {
                Keys.Add(new KeyGroup() { RequiredKeys = kgroup });
            }
        }

        /// <summary>
        /// Call this at least once per frame.
        /// </summary>
        /// <returns>True if the key was pressed THIS frame.</returns>
        public bool Update()
        {
            for (int i = 0; i < Keys.Count; i++)
            {
                KeyGroup kgroup = Keys[i];
                bool WasPressed = kgroup.IsPressed;
                foreach (Keys K in kgroup.RequiredKeys)
                {
                    if (Keyboard.GetState().IsKeyDown(K))
                    {
                        kgroup.IsPressed = true;
                    }
                    else
                    {
                        kgroup.IsPressed = false;
                        break;
                    }
                }

                if (kgroup.IsPressed && !WasPressed)
                {
                    return true;
                }
            }

            return false;
        }
    }

    /// <summary>
    /// The class from which any Pico8 games should derive from. By deriving from Mono8Game, the 
    /// game will have access to the Pico8 API as implemented in Mono8API.
    /// This class implemented IPico8GameAPI directly, so that game code does not need to be 
    /// modified to call into P8API object manually (eg. cls(0) vs P8API.cls(0)). This means
    /// porting code from Pico8 Lua to C# is simplified.
    /// It does not implement IPico8InteralAPI, as that functionality should not be exposed to
    /// the game.
    /// </summary>

    public class Mono8Game<PicoXGameType> : Game
        where PicoXGameType : PicoXGame, new()
    {
        /// <summary>
        /// The main interface with the graphics hardware.
        /// </summary>
        GraphicsDeviceManager Graphics;

        /// <summary>
        /// Used to render sprites to the screen.
        /// </summary>
        SpriteBatch SpriteBatch;

        /// <summary>
        /// It is the job of Mono8Game to manage this PicoX game; providing it an API implementation
        /// and triggering events as expected (_init, _draw, etc).
        /// </summary>
        public PicoXGame PXGame { get; private set; }

        /// <summary>
        /// Provides the implementation of the Pico8 API for this particular framework/platform.
        /// </summary>
        public Mono8API P8API { get; private set; }

        /// <summary>
        /// Is the console paused right now.
        /// </summary>
        bool Paused;

        bool Scripting;

        Script LuaScript = new Script();

        /// <summary>
        /// The different menu items that appear in the pause menu.
        /// </summary>
        string[] PauseMenuEntries =
        {
            "continue",
            "reset cart",
            "quit"
        };

        /// <summary>
        /// Helpers for detected key presses for the OS.
        /// </summary>
        BufferedKey ResetCartButton;
        BufferedKey ReloadContentButton;
        BufferedKey GifCaptureButton;
        BufferedKey GifResetButton;
        BufferedKey ToggleFullscreenButton;
        BufferedKey ToggleScripting;

        public Mono8Game()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.AllowUserResizing = true;
        }

        /// <summary>
        /// Restarts the Mono8 framework as if the game was loaded from scratch.
        /// </summary>
        private void Restart(bool reload_content)
        {
            if (P8API != null && !reload_content)
            {
                P8API.Shutdown();
                P8API = null;
            }

#if !WINDOWS_UWP
            if (reload_content)
            {
                Content.Unload();

                //string command = @"/k msbuild C:\Users\Matt\Documents\Dev\Game\mono8\Mono8Samples\Mono8Samples.csproj";
                //ProcessStartInfo cmdsi = new ProcessStartInfo(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Visual Studio 2015\Visual Studio Tools\cmd.exe");
                //cmdsi.Arguments = command;
                //Process cmd = Process.Start(cmdsi);
                //cmd.WaitForExit();
                
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory() + @"msbuild.exe");
                string projectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
//#if OPEN_GL
                p.StartInfo.Arguments = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName + @"\" + projectName + @".csproj";
//#else
//                p.StartInfo.Arguments = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName + @"\Mono8Samples.csproj";
//#endif
                p.Start();
                p.WaitForExit();

                return;
            }
#endif // !WINDOWS_UWP

            // Allocate the game itself so that we can get its map, sprite, etc, data.
            PXGame = new PicoXGameType();

            // Gather all the sound effects.
            Dictionary<int, string> SoundEffectPaths = PXGame.GetSoundEffectPaths();
            Dictionary<int, SoundEffect> SoundEffects = new Dictionary<int, SoundEffect>();
            foreach (var Item in SoundEffectPaths)
            {
                SoundEffects.Add(Item.Key, Content.Load<SoundEffect>(Item.Value));
            }

            // Gather all the music files.
            Dictionary<int, string> MusicPaths = PXGame.GetMusicPaths();
            Dictionary<int, SoundEffect> Music = new Dictionary<int, SoundEffect>();
            foreach (KeyValuePair<int, string> Item in MusicPaths)
            {
                Music.Add(Item.Key, Content.Load<SoundEffect>(Item.Value));
            }

            List<Texture2D> Sheets = new List<Texture2D>();
            List<string> SheetPaths = PXGame.GetSheetPath();
            foreach(string s in SheetPaths)
            {
                Sheets.Add(Content.Load<Texture2D>(s));
            }

            Texture2D PalTexture = null;
            if (!string.IsNullOrWhiteSpace(PXGame.GetPalTextureString()))
            {
                PalTexture = Content.Load<Texture2D>(PXGame.GetPalTextureString());
            }

            Point Res = new Point(PXGame.GetResolution().Item1, PXGame.GetResolution().Item2);

            // Create the Mono8 engine. We create it again on Restart because there appear to be a frame
            // delay issue with audio, where music played from _init will get stopped if API is shutdown
            // on the same frame.
            P8API = new Mono8API(Graphics, SpriteBatch, Sheets, Content.Load<Texture2D>("raw/pico8_font"), PXGame.GetMapString(), SoundEffects, Music, Res, PalTexture)
            {
                GifScale = PXGame.GetGifScale(),
                GifCaptureEnabled = PXGame.GetGifCaptureEnabled(),
            };

            // Make sure to assign this before doing any real Pico 8 calls.
            PXGame.P8API = P8API;

            LuaScript.Globals["load"] = (Action<string>)LoadLuaFile;

            Dictionary<string, object> ScriptFunctions = PXGame.GetScriptFunctions();
            foreach (KeyValuePair<string, object> Entry in ScriptFunctions)
            {
                LuaScript.Globals[Entry.Key] = Entry.Value;
            }

            P8API.RegisterLuaFunctions(LuaScript);

            TextBuffer = "";

            P8API.ResetGifCapture();

            // Update the resolution now that the game is ready to provie it.
            int WindowScale = 2;
            if (!Graphics.IsFullScreen)
            {
                Graphics.PreferredBackBufferWidth = P8API.Resolution.X * WindowScale;
                Graphics.PreferredBackBufferHeight = P8API.Resolution.Y * WindowScale;
            }
            else
            {
                Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            }
            Graphics.ApplyChanges();

            event_post_init?.Invoke(this);

            // Calling init here rather than from Initialize(), as it seems to make more
            // sense that _init would be called AFTER everything is loaded.
            PXGame._init();
        }

        public delegate void on_post_init(Mono8Game<PicoXGameType> game);
        public on_post_init event_post_init;
        public delegate void on_post_update();
        public on_post_update event_post_update;

        public object GetAction() { return (Action<int>)P8API.cls;  }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Should this kind of initialization be left up to the derived 
            // game classes? What should be handled at a low level? 
            // Perhaps a Mono8GameExt could be created, where some extended functionality
            // is implemented for the user (resizing, time step, etc) where as core 
            // functionality is left here (ticking _update60(), _draw(), getting map data, etc).
            if (true)
            {
                // TODO: Resize after initializing game.
                //int WindowScale = 4;
                //Graphics.PreferredBackBufferWidth = 160 * WindowScale;
                //Graphics.PreferredBackBufferHeight = 128 * WindowScale;
            }
            //else
            //{
            //    Graphics.PreferredBackBufferWidth = 1920;
            //    Graphics.PreferredBackBufferHeight = 1080;
            //    Graphics.IsFullScreen = true;
            //}

            IsFixedTimeStep = true;
            Graphics.ApplyChanges();

            GifCaptureButton = new BufferedKey(Keys.F9);
            GifResetButton = new BufferedKey(Keys.F8);
            ResetCartButton = new BufferedKey(new Keys[] { Keys.LeftControl, Keys.R });
            ReloadContentButton = new BufferedKey(new Keys[] { Keys.LeftShift, Keys.R });
            ToggleFullscreenButton = new BufferedKey(new Keys[][] { new Keys[] { Keys.RightAlt, Keys.Enter }, new Keys[] { Keys.LeftAlt, Keys.Enter } });
            ToggleScripting = new BufferedKey(Keys.OemTilde);

            Window.TextInput += HandleInput;

            //  string scriptCode = @"    
            //-- defines a factorial function
            //function fact (n)
            // if (n == 0) then
            //  return 1
            // else
            //  return n*fact(n - 1)
            // end
            //end

//      function sqr(n)
//          return n*n
//      end

//return sqrt(mynumber)";

//  script.Globals["mynumber"] = 25;
//  script.Globals["sqrt"] = (Func<float, float>)sqrt;
//  script.Globals["line"] = (Action<float, float, float, float, int>)line;

//  DynValue res = script.DoString(scriptCode);
//  printh(res.Number.ToString());

//#if WINDOWS_UWP
//            LuaScript.Options.ScriptLoader = new LuaScriptLoaderUWP();
//#endif

//#if WINDOWS_UWP
//            Windows.UI.Core.SystemNavigationManager.GetForCurrentView().BackRequested += SystemNavigationManager_BackRequested;
//#endif
            base.Initialize();
        }

//#if WINDOWS_UWP
//        private void SystemNavigationManager_BackRequested(object sender, Windows.UI.Core.BackRequestedEventArgs e)
//        {
//            if (!e.Handled)
//            {
//                e.Handled = true;
//            }
//        }
//#endif

        string TextBuffer = "";
        List<string> TextBufferHistory = new List<string>();
        int TextBufferHistoryIndex = 0;

        private void HandleInput(object sender, TextInputEventArgs e)
        {
            if (Scripting)
            {
                char charEntered = e.Character;

                if (charEntered == '\b')
                {
                    if (TextBuffer.Length > 0)
                    {
                        TextBuffer = TextBuffer.Remove(TextBuffer.Length - 1);
                    }
                }
                else if (charEntered == '\r')
                {
                    string scriptCode = TextBuffer;
                    try
                    {
                        DynValue res = LuaScript.DoString(scriptCode);
                        P8API.print(res.CastToString(), 1, 0, 0);
                        P8API.print(res.CastToString(), 0, 1, 0);
                        P8API.print(res.CastToString(), 0, 0, 7);
                    }
                    catch (InterpreterException e2)
                    {
                        P8API.printh(e2.DecoratedMessage);
                        P8API.print(e2.Message, 1, 0, 0);
                        P8API.print(e2.Message, 0, 1, 0);
                        P8API.print(e2.Message, 0, 0, 7);
                    }
                    TextBufferHistory.Add(TextBuffer);
                    TextBuffer = "";
                    TextBufferHistoryIndex = 0;
                    // Don't close here because we want to see the results before the
                    // screen gets cleared again.
                    //Scripting = false;
                }
                else if (charEntered != '`' && charEntered != '~') // ignore the key that opens and closes the console.
                {
                    TextBuffer += charEntered;
                }
            }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            //Texture2D temp  = Content.Load<Texture2D>(@"raw\sheet");
            ////Texture2D temp2 = Content.Load<Texture2D>(@"raw\sheet_source");

            //string filepath = Directory.GetCurrentDirectory() + "/Content/raw/sheet_source.png";

            //Texture2D fileTexture;
            //using (FileStream fileStream = new FileStream(filepath, FileMode.Open))
            //{
            //    fileTexture = Texture2D.FromStream(GraphicsDevice, fileStream);
            //}

            Restart(false);
        }

//        protected async void LoadTest()
//        {
//#if WINDOWS_UWP
//            Windows.Storage.StorageFolder storageFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;// Windows.Storage.ApplicationData.Current.LocalFolder;
//            Windows.Storage.StorageFile sampleFile = await storageFolder.GetFileAsync(@"Content\raw\lua_test.lua");

//            string FileContent = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);
//            P8API.printh("Lua File: " + FileContent);

//            LuaScript.DoString(FileContent);
//#endif
//        }

        protected void LoadLuaFile(string FileName)
        {
#if WINDOWS_UWP
            if (String.IsNullOrEmpty(FileName))
            {
                FileName = "adv_platformer_lua";
            }
            P8API.printh("Loading: " + FileName);
            LuaLuaFileAsync(FileName);
#endif
        }

#if WINDOWS_UWP
        protected async void LuaLuaFileAsync(string FileName)
        {
            Windows.Storage.StorageFolder storageFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;// Windows.Storage.ApplicationData.Current.LocalFolder;
            string Path = @"Content\raw\" + FileName + @".lua";
            P8API.printh("Loading: " + Path);
            Windows.Storage.StorageFile sampleFile = await storageFolder.GetFileAsync(Path);

            TextBuffer = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);

            try
            {
                LuaScript.DoString(TextBuffer);

                if (!LuaScript.Globals.Get("_init").IsNil())
                {
                    try
                    {
                        LuaScript.Call(LuaScript.Globals["_init"]);
                    }
                    catch (InterpreterException e2)
                    {
                        P8API.printh(e2.DecoratedMessage);
                    }
                }
            }
            catch (InterpreterException e2)
            {
                P8API.printh(e2.DecoratedMessage);
            }
    }
#endif

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //P8API.printh("update");

            ResetCartButton.Update();
            if (ReloadContentButton.Update())
            {
                Restart(true);
            }
            else if (ResetCartButton.IsPressed)
            {
                Restart(false);
            }

            if (GifResetButton.Update())
            {
                P8API.ResetGifCapture();
            }

            if (GifCaptureButton.Update())
            {
#if !WINDOWS_UWP
                // Start a thread that calls a parameterized static method.
                Thread newThread = new Thread(P8API.CaptureGif);
                newThread.Start();
#endif
            }

            if (ToggleScripting.Update())
            {
                Scripting = !Scripting;
                TextBuffer = "";
                TextBufferHistoryIndex = 0;

                if (Scripting)
                {
                    //LoadTest();

                    //try
                    //{
                    //    LuaScript.DoFileAsync("Content/raw/lua_test.lua");
                    //}
                    //catch (InterpreterException e2)
                    //{
                    //    P8API.printh(e2.DecoratedMessage);
                    //}
                }
            }

            if (Scripting && TextBufferHistory.Count > 0)
            {

                int StartIndex = TextBufferHistoryIndex;
                if (P8API.btnp(2))
                {
                    TextBufferHistoryIndex++;
                }
                else if (P8API.btnp(3))
                {
                    TextBufferHistoryIndex--;
                }

                TextBufferHistoryIndex = MathHelper.Clamp(TextBufferHistoryIndex, 0, TextBufferHistory.Count - 1);

                if (StartIndex != TextBufferHistoryIndex)
                {
                    TextBuffer = TextBufferHistory[TextBufferHistory.Count - TextBufferHistoryIndex - 1];
                }
            }

            if (ToggleFullscreenButton.Update())
            {
                int WindowScale = 4;
                if (Graphics.IsFullScreen)
                {
                    Graphics.PreferredBackBufferWidth = P8API.Resolution.X * WindowScale;
                    Graphics.PreferredBackBufferHeight = P8API.Resolution.Y * WindowScale;
                    Graphics.IsFullScreen = false;
                }
                else
                {
                    Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                    Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                    Graphics.IsFullScreen = true;

                }
                //                Graphics.ToggleFullScreen();
                Graphics.ApplyChanges();

#if !WINDOWS_UWP
                // Center window in case of going back to windowed mode.
                Window.Position = new Point(
                    GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2 - (P8API.Resolution.X * WindowScale / 2),
                    GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2 - (P8API.Resolution.Y * WindowScale / 2));
#endif
            }
            else if (!Scripting)
            {
                if (PXGame.GetPauseMenuEnabled())
                {
                    if (Paused)
                    {
                        if (PXGame.btnp(6) || PXGame.btnp(5) || PXGame.btnp(4))
                        {
                            if (FocusedIndex == 1)
                            {
                                Restart(false);
                            }
                            else if (FocusedIndex == 2)
                            {
                                Exit();
                                return;
                            }
                            else if (FocusedIndex > 2)
                            {
                                // TODO: This is not right. It will crash if the game skips any entries (eg. calling menuitem(2,...) without calling menuitem(1,...).
                                //       For now I am just not going to do that.
                                P8API.AdditionalPauseMenuEntries[FocusedIndex - PauseMenuEntries.Length].Item2();
                            }
                            FocusedIndex = 0;
                            Paused = false;
                        }
                    }
                    else
                    {
                        if (PXGame.btnp(6))
                        {
                            Paused = true;
                        }
                    }
                }
            }

            if (!Scripting)
            {
                if (!Paused)
                {
                    // Tick the game.
                    PXGame._update60();

                    if (!LuaScript.Globals.Get("_update60").IsNil())
                    {
                        try
                        {
                            LuaScript.Call(LuaScript.Globals["_update60"]);
                        }
                        catch (InterpreterException e2)
                        {
                            P8API.printh(e2.DecoratedMessage);
                        }
                    }
                }
                else
                {
                    if (PXGame.btnp(2))
                    {
                        FocusedIndex -= 1;
                    }
                    if (PXGame.btnp(3))
                    {
                        FocusedIndex += 1;
                    }
                    FocusedIndex = Math.Min(Math.Max(FocusedIndex, 0), PauseMenuEntries.Length + P8API.AdditionalPauseMenuEntries.Count - 1);
                }
            }

            base.Update(gameTime);

            // Do this in update because we can get multiple update calls between draw calls.
            P8API.CacheInputStates();

            event_post_update?.Invoke();
        }

        int FocusedIndex = 0;

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //P8API.printh("draw");
            if (Scripting)
            {
                float CharSize = 4;
                int MaxChars = (int)(P8API.Resolution.X / CharSize);
                string VisbleString = TextBuffer.Substring(Math.Max(0, TextBuffer.Length - MaxChars));
                P8API.rectfill(0, P8API.Resolution.Y - 7, P8API.Resolution.X, P8API.Resolution.Y, 6);
                P8API.rectfill(0, P8API.Resolution.Y - 6, P8API.Resolution.X, P8API.Resolution.Y, 5);
                P8API.print(VisbleString, 0, P8API.Resolution.Y - 5, 0);
            }
            else
            {
                // Draw the game.
                PXGame._draw();

                if (!LuaScript.Globals.Get("_draw").IsNil())
                {
                    try
                    {
                        LuaScript.Call(LuaScript.Globals["_draw"]);
                    }
                    catch (InterpreterException e2)
                    {
                        P8API.printh(e2.DecoratedMessage);
                    }
                }

                if (Paused)
                {
                    PXGame.camera(0, 0);

                    // Pause menu.
                    float yinc = 8;
                    float xw = 96.0f;
                    int TotalEntries = (PauseMenuEntries.Length + P8API.AdditionalPauseMenuEntries.Count);
                    float xh = (TotalEntries) * yinc + 8;
                    float x = (P8API.Resolution.X / 2) - (xw / 2);
                    float y = (P8API.Resolution.Y / 2) - (xh / 2);
                    PXGame.rectfill(x, y, x + xw, y + xh, 0);
                    PXGame.rect(x + 1, y + 1, x + 1 + xw - 2, y + 1 + xh - 2, 7);

                    y = (P8API.Resolution.Y / 2) - ((TotalEntries / 2.0f) * 8.0f);

                    int counter = 0;
                    for (int i = 0; i < PauseMenuEntries.Length; i++)
                    {
                        float fx = x + 16;
                        if (counter == FocusedIndex)
                        {
                            PXGame.print(">", x + 8, y + 2, 7);
                            fx += 1;
                        }
                        PXGame.print(PauseMenuEntries[i], fx, y + 2, 7);
                        y += yinc;
                        counter++;
                    }
                    foreach(var entry in P8API.AdditionalPauseMenuEntries)
                    {
                        float fx = x + 16;
                        if (counter == FocusedIndex)
                        {
                            PXGame.print(">", x + 8, y + 2, 7);
                            fx += 1;
                        }
                        PXGame.print(entry.Value.Item1, fx, y + 2, 7);
                        y += yinc;
                        counter++;
                    }
                }
            }

            //PXGame.print((1 / gameTime.ElapsedGameTime.TotalSeconds).ToString(), 2, 16, 7);

            // Present what has been draw to the screen.
            P8API.EndDraw();

            base.Draw(gameTime);
        }
    }
}