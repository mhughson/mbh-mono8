﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    public class pickup : obj
    {
        protected int pickup_delay;

        protected agent owner;

        protected Dictionary<int, int> pal_overrides = new Dictionary<int, int>();

        public pickup(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
        }

        public override void update()
        {
            pickup_delay = (int)Game.max(pickup_delay - 1, 0);
            if (owner == null && pickup_delay == 0)
            {
                for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    obj o = Game.game_world.cur_area.objs_active[i];
                    if (o != this && o is agent)
                    {
                        if (o.col_box().intersects(col_box()))
                        {
                            if (pick_up(o as agent))
                            {
                                new floating_text(display_name, Game, Game.game_world.ui_area).activate();
                                break;
                            }
                        }
                    }
                }
            }

            dx *= 0.9f;
            dy *= 0.9f;
            x += dx;
            y += dy;

            // Simple collision. Could probably go back to previous x,y for slightly better results.
            if (Game.hit_left(this) || Game.hit_right(this))
            {
                dx = 0;
            }
            if (Game.hit_top(this) || Game.hit_bottom(this))
            {
                dy = 0;
            }

            base.update();
        }

        public override void draw()
        {
            if (owner != null)
            {
                return;
            }

            int[] p = Game.get_cur_pal();

            Game.apply_pal(p);

            for (int i = 0; i < 32; i++)
            {
                Game.pal(i, p[31]);
            }
            base.draw();
            Game.apply_pal(p);

            foreach (KeyValuePair<int, int> c in pal_overrides)
            {
                Game.pal(c.Key, c.Value);
            }

            float _y = y;
            y += Game.sin((Game.tick * 0.005f)) * 4.0f - 4.0f;
            base.draw();
            y = _y;
            Game.pal();
        }

        public virtual bool pick_up(agent new_owner)
        {
            if (new_owner.add_to_inventory(this))
            {
                owner = new_owner;
                // TODO: This is causing ALL held weapons to "travel" with the player. It should be more like,
                //       when an object travels (eg the player) it should be able to bring some stuff with it.
                //       Quick fix may be to just only mark pickups as persistent if the owning is a player.
                is_persistent = true;

                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual void drop()
        {
            if (owner != null)
            {
                x = owner.x;
                y = owner.y;
                dx = -owner.fx * 2;
                dy = -owner.fy * 2;
                owner.remove_from_inventory(this);
                owner = null;
                pickup_delay = 30;
                is_persistent = false;
            }

        }
    }
}
