﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    public class base_ai : agent
    {
        public bool can_cloak = false;

        public base_ai(PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
        }

        public override void draw()
        {
            // Turn on cloak vfx if needed.
            if (can_cloak && !Game.ir_vision_enabled)
            {
                Game.sprfxset(0, true);
            }
            else
            {
                // Don't push the pal if we are cloaked because it will screw
                // with the effect (drawing Green instead of Dark grey for instance).
                push_pal();
            }
            base.draw();

            pop_pal();
            Game.sprfxset(0, false);
        }

        public virtual void push_pal()
        {
            //Game.pal(30, 21);
            //Game.pal(7, 20);

            if (Game.ir_vision_enabled)
            {
                // Make the character a solid color.
                for (int i = 0; i < 32; i++)
                {
                    Game.pal(i, 17);
                }
                return;
            }

            int[] p = Game.get_cur_pal();

            Game.apply_pal(p);
            Game.pal(30, p[21]);
            Game.pal(7, p[20]);
        }

        public virtual void pop_pal()
        {

            Game.pal();
        }
    }
}
