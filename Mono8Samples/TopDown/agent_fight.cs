﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    public class agent_fight : obj
    {
        public agent_fight(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
        }

        public override void update()
        {
            x += dx;
            y += dy;

            float dcc = 0.8f;
            dy *= dcc;
            dx *= dcc;

            base.update();
        }
    }
}
