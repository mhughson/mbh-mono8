﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class npc_ai : base_ai
    {
        public npc_ai(PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
        }

        public override bool can_interact(obj target)
        {
            return target.type_flag == type_flags.Player;
        }

        public override void on_interact()
        {
            base.on_interact();

            string text = "hello! this is a really long piece of text, that i will try to fit into the window. if it doesn't all fit, it should scroll when the player presses a button.";
            new ui_dialog(text, "portrait_0", Game, Game.game_world.ui_area).activate();
        }



        public override rect col_box()
        {
            return new rect(x, y + 8, 8, 16);
        }
    }
}
