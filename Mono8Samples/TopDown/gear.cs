﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    public class gear : pickup
    {
        /// <summary>
        /// The different types of gear in the game.
        /// </summary>
        public enum gear_types
        {
            night_vis,
            ir_vis,
            key_card,
        }

        /// <summary>
        /// The type of gear this instance is.
        /// </summary>
        protected gear_types gear_type;

        /// <summary>
        /// A function to execute when this gear is toggled in the inventory screen.
        /// </summary>
        protected Action on_toggle;

        /// <summary>
        /// Is this gear enabled?
        /// </summary>
        public bool enabled { get; protected set; }

        public gear(gear_types t, float pos_x, float pos_y, PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            x = pos_x;
            y = pos_y;
            w = 8;
            h = 8;

            switch (t)
            {
                case gear_types.night_vis:
                    {


                        anims = new Dictionary<string, anim>()
                              {
                                  {
                                      "on_ground",
                                      new anim()
                                      {
                                          ticks=1,//how long is each frame shown.
                                          frames= new int[][] { new int[] { 59 } },//what frames are shown.
                                      }
                                  },
                              };
                        set_anim("on_ground");
                        display_name = "n.vis";
                        on_toggle = () => { Game.night_vision_enabled = enabled; };
                        pal_overrides.Add(15, 19);
                        break;
                    }


                case gear_types.ir_vis:
                    {
                        {
                            anims = new Dictionary<string, anim>()
                              {
                                  {
                                      "on_ground",
                                      new anim()
                                      {
                                          ticks=1,//how long is each frame shown.
                                          frames= new int[][] { new int[] { 59 } },//what frames are shown.
                                      }
                                  },
                              };
                            set_anim("on_ground");
                            display_name = "thermal vis";
                            on_toggle = () => { Game.ir_vision_enabled = enabled; };
                            break;
                        }
                    }
            }

            gear_type = t;
        }

        public void toggle()
        {
            enabled = !enabled;

            // Invoke action if defined.
            on_toggle?.Invoke();
        }
    }
}
