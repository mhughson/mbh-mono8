﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;

namespace Mono8Samples.TopDown
{
    class explosion_series : obj
    {
        int ticks_since_last;

        int num;
        int cur_num;
        int delay;
        int damage_radius;
        int spawn_radius;

        public explosion_series(int num, int delay, int damage_radius, int spawn_radius, PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            cur_num = 0;
            this.num = num;
            this.delay = delay;
            this.damage_radius = damage_radius;
            this.spawn_radius = spawn_radius;
            this.ticks_since_last = delay;
        }

        public override rect col_box()
        {
            return new rect(x, y, damage_radius, damage_radius);
        }

        public override void update()
        {
            base.update();

            ticks_since_last++;

            if (ticks_since_last >= delay)
            {
                bool is_primary = cur_num == 0 ? true : false;

                new explosion(spawn_radius, Game) { x = x + Game.rnd(spawn_radius * 2) - spawn_radius, y = y + Game.rnd(spawn_radius * 2) - spawn_radius }.activate();

                //if (is_primary)
                //{
                //    //Vector2 pos_a = new Vector2(col_box().x, col_box().y);
                //    for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                //    {
                //        obj o = Game.game_world.cur_area.objs_active[i];
                //        if (o != this && o.is_killable_now() /*&& o.type_flag != type_flags.None && collision_mask.HasFlag(o.type_flag)*/)
                //        {
                //            //Vector2 pos_b = new Vector2(o.col_box().x, o.col_box().y);

                //            //if (Vector2.DistanceSquared(pos_a, pos_b) <= damage_radius * damage_radius + o.col_box().w * o.col_box().w) // assumes width>=height
                //            if(col_box().intersects(o.col_box()))
                //            {
                //                if (o.on_take_damage(this, 10))
                //                {
                //                    o.on_death(this);
                //                }
                //            }
                //        }
                //    }
                //}
                cur_num++;
            }

            if (cur_num >= num)
            {
                //Vector2 pos_a = new Vector2(col_box().x, col_box().y);
                for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    obj o = Game.game_world.cur_area.objs_active[i];
                    if (o != this && o.is_killable_now() /*&& o.type_flag != type_flags.None && collision_mask.HasFlag(o.type_flag)*/)
                    {
                        //Vector2 pos_b = new Vector2(o.col_box().x, o.col_box().y);

                        //if (Vector2.DistanceSquared(pos_a, pos_b) <= damage_radius * damage_radius + o.col_box().w * o.col_box().w) // assumes width>=height
                        if (col_box().intersects(o.col_box()))
                        {
                            if (o.on_take_damage(this, 10))
                            {
                                o.on_death(this);
                            }
                        }
                    }
                }

                destroy();
            }
        }
    }
}
