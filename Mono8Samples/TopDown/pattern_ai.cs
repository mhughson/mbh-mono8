﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;

namespace Mono8Samples.TopDown
{
    class pattern_ai : base_ai
    {
        public enum pattern_types
        {
            charge,
            sentry,
            gunner,
        };

        int lifetime = 0;
        int run_time;
        pattern_types pattern_type;
        bool leave = false;
        int fire_rate = 120;
        int fire_frame = 0;

        public pattern_ai(pattern_types pattern_type, PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            type_flag = type_flags.Enemy;
            attack_target_types = type_flags.Player;
            
            acc = 1;
            dcc = 0;
            walk_speed = 0.25f;
            run_speed = 0.5f;
            cur_movement_type = movement_types.run;
            // Move 6 tiles.
            run_time = (int)Math.Round((6.0f * 8.0f) / run_speed); // (int)Game.rnd(60) + 30;
            this.pattern_type = pattern_type;
            fire_frame = (int)Math.Round(Game.rnd(fire_rate));
        }

        public override void update()
        {
            lifetime++;

            switch (pattern_type)
            {
                case pattern_types.charge:
                    {
                        clear_btns();
                        if (lifetime < run_time)
                        {
                            btn_down = true;
                        }
                        //else if (lifetime == run_time)
                        //{
                        //    btn_dodge = true;
                        //}
                        else if (lifetime < run_time + 60)
                        {
                            face_player();
                        }
                        else if (!leave)
                        {
                            face_player();
                            if (lifetime % 120 == fire_frame)
                            {
                                btn_attack = true;
                            }

                            player target = Game.game_world.get_player();
                            if (Game.abs(target.y - y) < 16)
                            {
                                leave = true;
                            }
                        }
                        else if (leave)
                        {
                            if (x < Game.game_cam.x)
                            {
                                btn_left = true;
                            }
                            else
                            {
                                btn_right = true;
                            }
                        }
                        break;
                    }

                case pattern_types.sentry:
                    {
                        clear_btns();

                        face_player();

                        if (lifetime % 120 == fire_frame)
                        {
                            btn_attack = true;
                        }
                        break;
                    }

                case pattern_types.gunner:
                    {
                        clear_btns();

                        face_player(true);

                        if (lifetime % 120 < 30)
                        {
                            btn_attack = true;
                        }
                        break;
                    }
            }
            base.update();
        }

        void face_player(bool unlock_axis = false)
        {
            player target = Game.game_world.get_player();

            Vector2 to_other = new Vector2(target.x - x, target.y - y);
            to_other.Normalize();
            wx = (int)Math.Round(to_other.X);
            wy = (int)Math.Round(to_other.Y);
            if (unlock_axis)
            {
                fx = to_other.X;
                fy = to_other.Y;
            }
            else
            {
                fx = wx;
                fy = wy;
            }
        }
    }
}
