﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class ui_inventory_item : ui_element
    {
        public ui_inventory_item(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
        }

        public override void draw()
        {
            base.draw();
            
            player p = Game.game_world.get_player();
            if (p != null)
            {
                weap w = p.cur_weap;
                if (w != null)
                {
                    w.draw_inventory_view((int)x, (int)y);
                }
            }
        }
    }
}
