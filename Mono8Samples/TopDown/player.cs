﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class player : agent
    {
        int push_x_time = 0;
        int push_y_time = 0;

        public int trans_dir_x = 0;
        public int trans_dir_y = 0;

        public player(PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            type_flag = type_flags.Player;
            attack_target_types = type_flags.Enemy | type_flags.Destructable;
            //cur_weap.bullet_speed = 3;
            is_persistent = true;

            crawl_speed = 0.25f;
            walk_speed = 0.25f;
            run_speed = 0.75f;

            cur_movement_type = movement_types.run;
        }

        public override void update()
        {
            clear_btns();

            if (Game.get_game_state() == TopDown.game_states.door_trans_out || Game.get_game_state() == TopDown.game_states.door_trans_in)
            {
                //if (Game.tick % 4 == 0)
                {
                    if (trans_dir_x < 0) btn_left = true;
                    if (trans_dir_x > 0) btn_right = true;
                    if (trans_dir_y < 0) btn_up = true;
                    if (trans_dir_y > 0) btn_down = true;
                }
            }
            else
            {
                if (Game.is_gameplay_input_enabled())
                {
                    btn_left = Game.btn(0);
                    btn_right = Game.btn(1);
                    btn_up = Game.btn(2);
                    btn_down = Game.btn(3);
                    btn_dodge = Game.btn(4);
                    if (cur_weap != null)
                    {
                        btn_attack = cur_weap.automatic ? Game.btn(5) : Game.btnp(5);
                    }
                    else
                    {
                        btn_attack = false;
                    }
                }
            }

            base.update();
        }

        public override void draw()
        {
            if (Game.ir_vision_enabled)
            {
                // Make the character a solid color.
                for (int i = 0; i < 32; i++)
                {
                    Game.pal(i, 16);
                }
            }
            else if (Game.night_vision_enabled || Game.game_world.cur_area.light_status == area.lighting_status.on)
            {
                int[] p = Game.get_cur_pal();
                Game.apply_pal(p);
            }
            base.draw();
            Game.pal();
        }

        protected override void post_resolve_wall_collisions(bool hit_left, bool hit_right, bool hit_top, bool hit_bottom)
        {
            base.post_resolve_wall_collisions(hit_left, hit_right, hit_top, hit_bottom);

            float max_off = 64.0f;
            float cam_speed = 1.0f;
            int hold_time = 30;

            if (cur_cover != agent.cover_dir.none && hit_left && btn_left && !btn_up && !btn_down)
            {
                push_x_time++;
                if (push_x_time > hold_time)
                {
                    Game.game_cam.offset.X = Game.max(-max_off, Game.game_cam.offset.X - cam_speed);
                }
            }
            else if (cur_cover != agent.cover_dir.none && hit_right && btn_right && !btn_up && !btn_down)
            {
                push_x_time++;
                if (push_x_time > hold_time)
                {
                    Game.game_cam.offset.X = Game.min(max_off, Game.game_cam.offset.X + cam_speed);
                }
            }
            else
            {
                push_x_time = 0;
                Game.game_cam.offset.X += Math.Sign(Game.game_cam.offset.X) * -1 * cam_speed;
            }

            if (cur_cover != agent.cover_dir.none && hit_top && btn_up && !btn_left && !btn_right)
            {
                push_y_time++;
                if (push_y_time > hold_time)
                {
                    Game.game_cam.offset.Y = Game.max(-max_off, Game.game_cam.offset.Y - cam_speed);
                }
            }
            else if (cur_cover != agent.cover_dir.none && hit_bottom && btn_down && !btn_left && !btn_right)
            {
                push_y_time++;
                if (push_y_time > hold_time)
                {
                    Game.game_cam.offset.Y = Game.min(max_off, Game.game_cam.offset.Y + cam_speed);
                }
            }
            else
            {
                push_y_time = 0;
                Game.game_cam.offset.Y += Math.Sign(Game.game_cam.offset.Y) * -1 * cam_speed;
            }
        }

        public override kill_fx generate_kill_fx(obj source)
        {
            kill_fx fx = base.generate_kill_fx(source);
            fx.pal = new List<Tuple<int, int>>(); // clear the ai pal

            return fx;
        }

        public override void on_death(obj source)
        {
            if (!Game.god_mode_enabled)
            {
                base.on_death(source);

                Game.set_game_state(TopDown.game_states.game_over);
            }
        }
    }
}
