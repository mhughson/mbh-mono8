﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoonSharp;
using MoonSharp.Interpreter.Loaders;
using MoonSharp.Interpreter;

namespace Mono8
{
    class LuaScriptLoaderUWP : ScriptLoaderBase
    {
        public override object LoadFile(string file, Table globalContext)
        {
            //return string.Format("print ([[A request to load '{0}' has been made]])", file);
            return LoadFileAsync(file, globalContext);
        }

        private async Task<DynValue> LoadFileAsync(string file, Table globalContext)
        {
            Windows.Storage.StorageFolder storageFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;// Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.GetFileAsync(@"Content\raw\lua_test.lua");

            string Result = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);
            return DynValue.NewString(Result);
        }

        public override bool ScriptFileExists(string name)
        {
            return true;
        }
    }
}
