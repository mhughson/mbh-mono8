﻿using Microsoft.Xna.Framework;
using System;
using PicoX;
using System.Collections.Generic;

namespace Mono8Samples
{
    class Wolf : PicoXGame
    {
        bool debug = false;
        float posx = 22.0f;
        float posy = 12.0f;

        float dirx = 1.0f;
        float diry = 0.0f;

        float planex = 0.0f;
        float planey = 0.66f;

        bool dump = false;

        int light = 1;

        float jump = 0;
        float yvel = 0;
        float shake_x, shake_y = 0;
        float shake_time = 0;
        bool grounded = true;

        int[][] fadetable ={
         new int[] {0,0,0,0,0,0,0},
         new int[] {1,1,1,0,0,0,0},
         new int[] {2,2,2,1,0,0,0},
         new int[] {3,3,3,1,0,0,0},
         new int[] {4,2,2,2,1,0,0},
         new int[] {5,5,1,1,1,0,0},
         new int[] {6,13,13,5,5,1,0},
         new int[] {6,6,13,13,5,1,0},
         new int[] {8,8,2,2,2,0,0},
         new int[] {9,4,4,4,5,0,0},
         new int[] {10,9,4,4,5,5,0},
         new int[] {11,3,3,3,3,0,0},
         new int[] {12,12,3,1,1,1,0},
         new int[] {13,5,5,1,1,1,0},
         new int[] {14,13,4,2,2,1,0},
         new int[] {15,13,13,5,5,1,0}
        };

        public Wolf() : base()
        {
        }

        public override void _init()
        {
            
        }

        public override void _update60()
        {
            if(btnp(0, 1))
            {
                dump = true;
                shake_time = 10;
            }


            if (btnp(4))
            {
                light = Math.Min(fadetable[0].Length,light+1);
            }
            else if (btnp(5)) {

                light = Math.Max(1, light - 1);

            }
	
	
	        if(false && btnp(4)) {

                yvel = 5;

                grounded = false;

            }
	
	        if(!grounded) {

                yvel -= 0.3f;

                jump += yvel;

            }
	
	        if(jump < 0) {

                jump = 0;

                yvel = 0;

		        if(!grounded) {

                    shake_time = 10;

                }

                grounded = true;

            }


            shake_time = Math.Max(0, shake_time - 1);
            if (shake_time > 0) {

                shake_x = rnd(4) - 2;

                shake_y = rnd(4) - 2;
            }
            else
            { 
                shake_x = 0;

                shake_y = 0;

            }

            if (btn(2)) {

                posx += 0.1f * dirx;

                posy += 0.1f * diry;
            }
            else if (btn(3)) {

                posx -= 0.1f * dirx;

                posy -= 0.1f * diry;

            }


            float rot = 0;
            if (btn(0)) {

                rot = 0.01f;
            }
            else if (btn(1)) {

                rot = -0.01f;

            }

            Vector2 Res = rot_vec(dirx, diry, rot);
            dirx = Res.X;
            diry = Res.Y;

            Res = rot_vec(planex, planey, rot);
            planex = Res.X;
            planey = Res.Y;

        }

        public Vector2 rot_vec(float x, float y, float rot)
        {
            float _x = x * (float)cos(rot) + y * -(float)sin(rot);

            float _y = x * (float)sin(rot) + y * (float)cos(rot);

            return new Vector2(_x, _y);
        }

        public override void _draw()
        {
            //  //printh("//////////////-")
            //  cls()

            camera(shake_x, shake_y);

            for (int i = 0; i <= 15; i++)
            { 
                // TODO
                //pal(i, fadetable[i][light])
            }


            rectfill(0, 0, 127, 63 + (int)jump, 6);


            rectfill(0, 64 + (int)jump, 127, 127, 5);
	
	        for (int x = 0; x <= 127; x++)
            {

                //put camera into - 1 to 1 space

                float camerax = 2.0f * x / 128.0f - 1.0f;


                float rayposx = posx;

                float rayposy = posy;


                float raydirx = dirx + planex * camerax;
                //      raydirx = 1

                float raydiry = diry + planey * camerax;
                //      raydiry = 1


                int mapx = (int)Math.Floor(rayposx);

                int mapy = (int)Math.Floor(rayposy);


                float sidedistx = 0;

                float sidedisty = 0;


                float deltadistx = (float)Math.Sqrt(1 + ((raydiry * raydiry) / (raydirx * raydirx)));

                float deltadisty = (float)Math.Sqrt(1 + ((raydirx * raydirx) / (raydiry * raydiry)));

                //if(ray dir approaches zero,
                //it will have causes lua to
                //go over Math.Max num when calc
                //deltadist(divide by near 0).

                //to account for this, detect
                //&& treat as a very large
                //deltadist.
		        if(Math.Abs(raydirx) < 0.01) {

                    deltadistx = 9999;

                }
		        if(Math.Abs(raydiry) < 0.01) {

                    deltadisty = 9999;

                }

		        if(false && (Math.Abs(raydiry) < 0.006 || Math.Abs(raydirx) < 0.006)) {

                    //printh("error!!!!!!")

                    //printh("sqrt:"..Math.Sqrt(1 / 0.1))

                    //printh(raydirx..","..raydiry)

                    //printh(deltadistx..","..deltadisty)

                }


                float perpwalldist = 0;

                int stepx = 0;

                int stepy = 0;

                bool hit = false;

                float side = -1;

                if (raydirx < 0) {

                    stepx = -1;

                    sidedistx = (rayposx - mapx) * deltadistx;
                }
                else if (raydirx > 0) {

                    stepx = 1;

                    sidedistx = (mapx + 1 - rayposx) * deltadistx;

                }

                if (raydiry < 0) {

                    stepy = -1;

                    sidedisty = (rayposy - mapy) * deltadisty;
                }
                else if (raydiry > 0) {

                    stepy = 1;

                    sidedisty = (mapy + 1 - rayposy) * deltadisty;

                }


                int col = 0;
		        while (!hit)
                {

                    if (sidedistx < sidedisty) {

                        sidedistx += deltadistx;

                        mapx += stepx;

                        side = 0;
                    }
                    else
                    {
                        sidedisty += deltadisty;

                        mapy += stepy;

                        side = 1;

                    }

                    col = sget(mapx, mapy);
                    if (mapx >= 32 || mapy >= 32 || mapy < 0 || mapx < 0)
                        hit = true;
         
			        if(col > 0) {

                        hit = true;

                    }


                }

                if (side == 0) {

                    perpwalldist = (mapx - rayposx + (1 - stepx) / 2) / raydirx;
                }
                else
                {
                    perpwalldist = (mapy - rayposy + (1 - stepy) / 2) / raydiry;
                }


                int lineheight = (int)Math.Floor(128.0f / perpwalldist);

                float drawstart = Math.Max(0, -lineheight / 2 + 127 / 2);
                //      float draw} =
                //Math.Min(127, lineheight / 2 + 127 / 2)


                drawstart += jump;
        //      draw} += jump

                /*
		        if(side == 1) {
                    col = fadetable[col][2];
                }
                */


                float wallx = 0;
                if (side == 0) {
                    wallx = rayposy + perpwalldist * raydiry;
                }
                else
                {
                    wallx = rayposx + perpwalldist * raydirx;
                }
                /*
                //printh("!!!")

                //printh("side:"..side)

                //printh("rayposx:"..rayposx)

                //printh("perpwalldist:"..perpwalldist)

                //printh("raydirx:"..raydirx)

                //printh("wallx:"..wallx)

                //printh("wallx:"..rayposx + (perwalldist * raydirx))

                //printh("wallx:"..22 + 4.618 * 1)

                //printh("!!!")
                */
                wallx = wallx - (float)Math.Floor(wallx);

                //u coord of texture

                float texx = (float)Math.Floor((wallx * 8));

                if (side == 0 && raydirx > 0) {
                    texx = 8 - texx - 1;
                }
                else if (side == 1 && raydiry < 0) {
                    texx = 8 - texx - 1;
                }
		
		        //		if(lineheight==0) {
		        if(dump==true) {
                    //printh("//-")

                    //printh("x:"..x)

                    //printh("lineheight:"..lineheight)
			        if(lineheight==0) {
                        //printh("error error error")

                    }
                    //printh("map:["..mapx..","..mapy.."]")

                    //printh("perpwalldist:"..perpwalldist)

                    //printh("side:"..side)

                    //printh("rayposx:"..rayposx)

                    //printh("rayposy:"..rayposy)

                    //printh("raydirx:"..raydirx)

                    //printh("raydiry:"..raydiry)

                    //printh("deltadistx:"..deltadistx)

                    //printh("deltadisty:"..deltadisty)

                    //printh("sidedistx:"..sidedistx)

                    //printh("sidedisty:"..sidedisty)

                    //printh("wallx:"..wallx)

                    //printh("texx:"..texx)

                }

                /*
		        if(side==1) {
			        for (int i = 0; i <= 15; i++)
                    { 
                        // TODO:
                        //pal(i, fadetable[i + 1][light + 1])
                    }

                }
		        */
                //for y=drawstart,draw}-1 do
                float sx = col * 8 + texx;
                sspr((int)sx, (int)side * 8,
                    1, 8,
                    x, (int)drawstart,
                    1, lineheight,
                    false, false);

                //if (side == 1)
                //{
                //    P8.line(x, (int)drawstart, x, (int)(drawstart + lineheight), 11);
                //}
                //else
                //{
                //    P8.line(x, (int)drawstart, x, (int)(drawstart + lineheight), 3);
                //}
		        //}
		        /*
		        if(side==1) {
		        for i=0,15 do

                    pal(i, fadetable[i + 1][light])

                }

                }
		        */
		        //line(x, drawstart, x, draw}, col)

            }

            dump = false;
	        if(debug) {


                sspr(0, 0, 32, 32, 0, 0, 32, 32);


                line((int)posx, (int)posy,
                    (int)(posx + dirx), (int)(posy + diry),
                    7);


                pset((int)posx, (int)posy, 9);
                /*
		        for x=0,127 do

                    pset(x, 127, x % 2 + 1)

                }
		        */
		        //pal()

                //print("["..dirx..","..diry.."]",0,0)

                //print("["..planex..","..planey.."]",0,7)

                //print("cpu:"..stat(1),0,14)
		        //print("cpu:"..stat(1),0,8,7)


	        }

        }

        public override string GetMapString()
        {
            return
                "6c796c6c6c6c6c6c6c6c6c6c6c6c78796c6c6c6c6c6c6c6c786c6c6c6c6c78796c6c6c6c6c6c6c6c6c6c6c6c786c6a6b6c6c6c6c6c6c6c6c6c6c6c6c786c6c6c6c6c6c6c786c6c796c6c6c6c6c6c786c6c6c6c6c6c6c6c6c78796c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c7879686868686868786c" +
                "6c7969696969696969696969696978796969696969696969786c6c6c6c6c7879696969696969696969696969786c7a7b696969696969696969696969786c6c6c6c6c6c6c786c6c79696969696969786c6c6c6c6c6c6c6c6c7879696969696969696969696969696969696969696969696969696969697879696969696969786c" +
                "6c7900102c2d02034849004a001078794b1000106e006e4b786c6c6c6c6c78794b10006e00000010006e00106a6b04054c4d4a004e4f000000000000786c6c6c6c6c6c6c786c6c7900002c2d0000786c6c6c6c6c6c6c6c6c7879000000002c2d4e4f6a6b000000002c2d00000000000000002c2d00007879106e4a106e4a786c" +
                "6c7900003c3d12135859005a000078795b0000006e006e007a696969696978790000006e00000000006e00007a7b14155c5d5a005e5f00005b4b5b00786c6c6c6c6c6c6c7a69697b00003c3d6f007a6969696969696969697879000000003c3d5e5f7a7b000000003c3d00000000000000003c3d00007879006e5a00005a786a" +
                "6c7900006a6868686868686b4c4d787900004a006e006e4a000000004a10787900004a6e00004a00006e4a00786c6a6b00004e4f6a6b0000005b6a68686868686868686b000000004c4d00006f00786c6c6c6c7900005b4b78794c4d6a6b4c4d282900004a00000000006a68686b4c4d6a6b0000004a78794b6e28292829787a" +
                "6c7900007a6969696969697b5c5d787900005a0000006e5a004b00005a4b78796f6f5a0000005a4b106e5a00786c7a7b4a005e5f787900000000786c6c6c6c6c6c6c6c79000000005c5d000000007a696969697b0000000078795c5d7a7b5c5d383900005a00000000007a69697b5c5d7a7b0000005a78795b1138393839786c" +
                "6c794b0010004e4f005b4b004c4d78795b4b000000006e0028290000005b7879000000004e4f005b006e5b00786c78795a002829787900000000786c6c6c6c6c6c6c6c794c4d2829000000002c2d6f6f4e4f005b4a4b000078794c4d005b4b0028290000000000000000000000004c4d28290000000078794c4d6a6b4e4f786c" +
                "6c795b0000005e5f11004b005c5d7879005b00004b006e5b3839000000007879000000115e5f5b014b6e6f6f786c7879000038397879000000007a69696969696969697b5c5d3839000000003c3d00005e5f00005a00000078795c5d0000000038390000000000000000000000005c5d38390000000078795c5d7a7b5e5f786a" +
                "6c7900002c2d4c4d6a68686b4c4d78792c2d4e4f5b006e0048490000040578794c4d6a68686b004b5b6e0000786c78794c4d6a6b78794b0000002c2d0000786c6c6c6c794c4d6a6b00006a6b000000004e4f00000000282978796a6b4c4d6a6868686868686b4c4d6a68686b4b5b005b6a68686b4c4d78794c4d4e4f4e4f787a" +
                "6c7900003c3d5c5d7a69697b5c5d78793c3d5e5f0000000058590000141578795c5d7a69697b005b00004b5b786c78795c5d78797a7b5b0000003c3d00007a696969697b5c5d7a7b00007a7b000000005e5f00000000383978797a7b5c5d7a6969696969697b5c5d7a69697b000000007a69697b5c5d78795c5d5e5f5e5f786c" +
                "6c79282948494c4d4a0010004e4f7879020348494c4d0000000028296a6b78794c4d1000020300002829004b786c78794c4d78795b6e00004c4d2c2d00004e4f006e78794c4d0000000000002829000028294c4d4e4f6a68686b00004c4d6a6b4a4b000000004c4d0000004a00000000000000004c4d78794c4d02032829786c" +
                "6c79383958595c5d5a0000005e5f7879121358595c5d1100000038397a7b78795c5d000012134b0038391100786c78795c5d7879006e005b5c5d3c3d00005e5f000078795c5d0000000000003839000038395c5d5e5f786c6c7900005c5d7a7b5a00000000005c5d0000005a00000000000000005c5d78795c5d12133839786a" +
                "6c792c2d4a004c4d4e4f00002c2d78796868686868686a6b4b006a6b6868787968686868686b5b006a686868786c6a6b4c4d6a6b0000484900000000000028292829787900004c4d4e4f00006a6b4c4d6a6b00004e4f786c6c794c4d6a6b0000000000004e4f02034e4f0000000000004e4f4c4d6a68686b4c4d28294849787a" +
                "6c793c3d5a005c5d5e5f11003c3d78796c6c6c6c6c6c7a7b115b7a7b6c6c78796c6c6c6c6c791100786c6c6c786c7a7b5c5d7a7b00005859000000005b0038393839787900005c5d5e5f00007a7b5c5d7a7b00005e5f786c6c795c5d7a7b0000000000005e5f12135e5f0000000000005e5f5c5d786c6c795c5d38395859786c" +
                "6c79686b00004c4d6a686868686878796c6c6c6c6c6c6c6c6a6b6c6c6c6c6a686868686868686868686868686a6b02034c4d00004c4d6a6b00006a6b00006a6b6868686b4c4d4849484900006e004c4d006e00002829786c6c794c4d000000006a686868686b4c4d48496a68686b00006a6b4c4d786c6c79686868686868786c" +
                "6c796c7911005c5d786c6c6c6c6c787969696969696969697a7b69696969786c6c6c6c6c6c6c6c6c6c6c6c6c7a7b12135c5d00005c5d7a7b00007a7b00007a7b6c6c6c795c5d5859585900006e005c5d000000003839786c6c795c5d000000007a696969697b5c5d58597a69697b00007a7b5c5d786c6c79696969696969786c" +
                "6c79686868686868686868686868787928296a6b04056a686868686b2829786c6c6c6c6c6c6c6c6c6c6c6c6c6c796a6b6868686868686a6868686868686868686868686868686868686b4c4d0000020300004c4d6a686868686b4c4d282900005b4b5b4b000000000000005b4b00282900004c4d786c697b78794b6e78797a69" +
                "6c79696969696969696969696969787938397a7b14157a696969697b38397a69696969696969696969696969697b7a7b6969696969697a6969696969696969696969696969696969697b5c5d0000121300005c5d7a696969697b5c5d383900000000000000000000000000000000383900005c5d786c796978795b6e78796969" +
                "6c792c2d005b282910004e4f4b10787948496e004e4f005b6a6b2c2d4849005b4a4b0000282978794a4b2c2d02034e4f6e004e4f004b786c6c6c6c6c6c794e4f2829786c6c6c6c6c6c6c6a68686868686868686b6c6c6c6c6c794c4d282900000000000000006a68686b00006a6868686868686868686b4a78794e4f78794a78" +
                "6c793c3d0000383900005e5f005b7879585900005e5f00007a7b3c3d585900005a000000383978795a003c3d12135e5f00005e5f5b007a6969696969697b5e5f38397a696969696969697a69696969696969697b69696969697b5c5d38390000000000000000786c6c790000786c6c6c6c6c6c6c6c6c795a78795e5f7a7b5a78" +
                "6c794c4d4c4d4e4f48492c2d4c4d787900006a6b4e4f6a68686b282948494c4d6a6b28296a686868686b00004c4d4c4d4c4d2c2d4c4d78796e002c2d6e0048490203006e2c2d787928290000282978794e4f4e4f4e4f104a6a686868686b4a00282900002829786c6c796a6b786c6c6c6c6c6c6c6c6c796b78794e4f4e4f6a68" +
                "6c795c5d5c5d5e5f58593c3d5c5d78794a4b7a7b5e5f7a69697b383958595c5d7a7b38397a696969697b00005c5d5c5d5c5d3c3d5c5d787900003c3d6e0058591213006e3c3d787938394b00383978795e5f5e5f5e5f4b5a786c6c6c6c795a003839000038397a69697b7a7b7a6969696969696969697b7978795e5f5e5f7a69" +
                "6c794c4d00002829005b4e4f4c4d78795a000000484900005b4b6a6b28294c4d00004849005b78792c2d4849005b4b004c4d00004c4d78794c4d28296e006a68686b4c4d005b78794e4f5b004e4f78794e4f02034e4f5b6f6a686868686868686b6b68686868686878786c794e4f786c6c6c6c6c6c6c79797879282948490203" +
                "6c795c5d0000383900005e5f5c5d7879000000005859000000007a7b38395c5d00005859000078793c3d5859000000005c5d00005c5d78795c5d383900007a696c795c5d000078795e5f00005e5f78795e5f12135e5f6f6f7a696969696969697b79696969696969787a697b5e5f7a696969696969697b797a7b383958591213" +
                "6c794c4d4e4f4c4d4c4d4e4f000078794c4d4e4f6a68686b00006e006a68686b4c4d4a004c4d78794b5b4b004c4d6a6b000048494c4d787900006a6b00004e4f78794c4d6a68686b4e4f6a6b4e4f78796868686b2c2d4b00106e2c2d6e1078796c79104a4e4f0405787902034e4f4b104b4a5b102c2d787928294e4f6a6b4c4d" +
                "6c795c5d5e5f5c5d5c5d5e5f000078795c5d5e5f7a69697b00006e007a69697b5c5d5a005c5d7879000000005c5d7a7b000058595c5d78794a4b7a7b5b4a5e5f7a7b5c5d7a69697b5e5f7a7b5e5f78796969697b3c3d0000006e3c3d6e4b7879697b5b5a5e5f1415787912135e5f5b5b015a4b5b3c3d787938395e5f7a7b5c5d" +
                "6c794c4d48494b00020348494c4d78794c4d2c2d5b4b006e000000006e002c2d2c2d2c2d4c4d78796a6b484900006e004e4f0000282978795a002c2d005a28296e004c4d6e0078792829006e2829787910006e104b00004b00002c2d6e5b787928294b4b02032c2d78792c2d2829282928292829020378794849282902034c4d" +
                "6c795c5d58590000121358595c5d78795c5d3c3d4a000000005b4b006e003c3d3c3d3c3d5c5d78797a7b5859000000005e5f00003839787900003c3d000038396e005c5d006f787938390000383978796f4a6e0000004a5b00003c3d6e6f787938394a5b12133c3d78793c3d3839383938393839121378795859383912135c5d" +
                "6c794c4d00006a68686b00004c4d78794c4d02035a004c4d6a6b00006e00004b5b4b00004c4d7879282900006a686868686b005b6a6878796a6b6f004c4d484948496a6b006f78792829000028297879005a5b004c4d5a0002034b00282978796a6b5a5b6a6b4b5b786a6868686868686868686868686b79004a6a6868686868" +
                "6c795c5d115b7a69697b11005c5d78795c5d121300005c5d7a7b000000000000000000005c5d7879383900007a696969697b0000786c78797a7b00005c5d585958597a7b000078793839000038397879110011005c5d110012135b11383978797879111178795b117a786c6c6c6c6c6c6c6c6c6c6c6c797b4b5a7a6969696969" +
                "6c796a6b6a68686b6a68686b6a6b7879686868686868686868686868686868686a6b686b68686868686868686868686868686868686868686868686868686a6b6868686868686868686868686868686b686868686868686868686868686878797879686868686868686868686868786c6c796f6f6f1002035b006f6f6f6f786c" +
                "6c797a7b7a69697b7a69697b7a7b78796c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c78796c79696969696969696969696969696969696969696969696969696978796c6c6c6c6c6c6c6c6c6c6c6c6c6c6c7969696969696969696969696969697a7b78796969696969696969696969697a69697b110000001213000010110000786c" +

                "a6b6e60020300000e600e600e6008797e601c4d4c4d4c4d4c4d4c4d4c2d2a6868797c69782920000000000000000e4f40000c4d4c4d4c4d4c4d4c4d4c4d48797a6b6c6c6c6c6c6c6c6c6c6c6a6b6a6b6c4d4c4d4c4d401e6a6b6c4d4c4d4e60187972030e601e4f4e600e4f440508797c4d4c4d4e4f4c4d4e4f4c4d4e4f487c6" +
                "a7b7e600213100000000e600e6008797e600c5d5c5d5c5d5c5d5c5d5c3d387c68797c6978393b5a4000000000000e5f50000c5d5c5d5c5d5c5d5c5d5c5d58797a7b79696969696969696969687978797c5d5c5d5c5d500e6a7b7c5d5c5d50000879721310000e5f5e6b4e5f541518797c5d5c5d5e5f5c5d5e5f5c5d5e5f587c6" +
                "86b6e600e4f4c2d20000e60000008797e600c4d4000000000000b400000087c68797c697c2d200a50000000020308494000000000000000000000000c4d4879701001000c2d2a6b68292100087978797c4d400000000e4f42030c4d4e4f4c4d48797c4d4c2d2a6b6b4b5a6b6c4d4879701e6e4f48292e6018292e601829287c6" +
                "c697e600e5f5c3d30000000000008797e600c5d5b500b5b400000000b40087c68797c697c3d300000000000021318595000000000000000000000000c5d5879700000000c3d3a7b783930000a7b7a7b7c5d500000000e5f52131c5d5e5f5c5d58797c5d5c3d3a7b7b500a7b7c5d58797a4e6e5f58393e6a48393e6a4839387c6" +
                "c697000082928292a6b6a6b600008797e600c4d40000e4f4a4b4e4f4b50087c68797c69700b5a6b6c4d48494a6b6a6b6a6b6b400a6b6829200b5a4b4c4d487972030c4d4000082928494829287978797c4d4a4b48494a68686b6c4d48494c4d48797c4d40000000100b5a4b4c4d48797a5b4c2d2e4f4e6a5e4f4e6a5e4f487c6" +
                "c697000083938393a7b7a7b7000087970000c5d50000e5f5a5b5e5f5000087c68797c6970000a7b7c5d58595a7b7a7b7a7b70000879783930000a5b5c5d587972131c5d5000083938595839387978797c5d5a5b58595a79696b7c5d58595c5d58797c5d5b40010000000a510c5d5879711b5c3d3e5f5e6b4e5f500b4e5f587c6" +
                "96b7a6b6849484940000b400829287972030c4d400b5e4f4b4a4e4f4000087c68797c6970000000000000000e4f400b5b40000008797829200008292c4d48797a68686b684948494c4d4a6b687978797c4d4b410c2d2a68686b6a6b610f6f6f68797c4d4a4b40000e4f400b5c4d48797c4d4b400829200b58292b4b5829287c6" +
                "c6c687978595859500000000839387972131c5d500b4e5f5b5a5e5f500b4a7968797c6970000b5a400000000e5f5000000000000a7b7839300008393c5d58797a79696b785958595c5d5a7b787978797c5d5b500c3d3a79696b7a7b70000f6f68797c5d5a5b50011e5f51100c5d58797c5d5b500839300008393b500839387c6" +
                "c6c68797849484944050c4d4c4d4a68686b6c4d400008292a4b4829200b5e6018797c697000000a50000a6b68494c4d40000e4f4000082920000a6b6c4d48797c2d200b5c2d2e4f4000000a686b686b6c4d4a4b50000e4f401e6e4f4c2d200f68797c4d4b400c4d4e4f4c4d4c4d48797c4d484948494849484948494849487c6" +
                "86b6a7b7859585954151c5d5c5d5a79696b7c5d5b5008393a5b583930000e6008797c697000000000000a7b78595c5d50000e5f5000083930000a7b7c5d58797c3d30000c3d3e5f500000087c697c697c5d5a5b40000e5f50000e5f5c3d300008797c5d50000c5d5e5f5c5d5c5d58797c5d585958595859585958595859587c6" +
                "c697c2d2c2d200b5a6b6a6b6c4d4879700e6e4f40000a6b6c4d4a6b6000082928797c697c2d2849400008494c4d4c4d4a68686b60000849400000000c4d487972030c4d40000829200000087c697c697a6b6b500c2d2c4d4c4d4c4d40000c4d48797c4d4849482928292e600c2d2a686868686868686868686868686868686b6" +
                "c697c3d3c3d30000a7b7a7b7c5d58797b4e6e5f50000a7b7c5d5a7b7000083938797c697c3d3859500008595c5d5c5d5a79696b70000859500000000c5d587972131c5d50000839300001187c697c6978797b400c3d3c5d5c5d5c5d50000c5d58797c5d5859583938393e6b4c3d3a796969696969696969696969696969696b7" +
                "c697a4b48494000000e6e600c4d48797829282920000b5a4c4d4a4e6000082928797c69700000000c4d484940000000084948494000000008292c4d4a68686b6a6b6a6b6c2d2c2d20000a6b6c697c6978797a4b420300000000000000000c4d48797c4d4b4f6a68686b6e6a4b4f687c6c697203001e6e6a4a4b4e6e6405087c6" +
                "c697a500859500000000e600c5d587978393839300b4b4a5c5d5a5e6000083938797c69700000000c5d585950000000085958595000000008393c5d5a79696b7a7b7a7b7c3d3c3d300008797c697c6978797a5b521311100000011000011c5d58797c5d5f6f687c6c697e6a5f6f687c6c697213100e6e6a5a5b5e600415187c6" +
                "a6b600000000a6b68292829200b4a6868686868686b600b5829200e60000a686b697a6868686868686868686868686b6a68686868686868686868686868686b6a6b6a6b60000829200008797c697c6978797b500a68686868686868686868686879701a400f687c6c697e6f6f6f687c6c697c4d400e6e6b4b500e600c4d487c6" +
                "a7b7110000b5a7b783938393000087c6c6c6c6c6c697001183931100000087c69797a7969696969696969696969696b7a79696969696969696969696969696b7a7b7a7b7111183930000a7b796b796b7a7b71111a79696969696969696969696879700a5001187c6c6970000110087c6c697c5d5b5e6e6b50000e6b4c5d587c6" +
                "8686868686868686868686868686a7969696969696b78686868686868686a796b797000000000000000000a6868686c6c6c6c6c6c697e6e6e687c6c6c6879786868686868686868686868687c600000000000000000000000000000000000000a686868686868686868686868686868686b6c4d4a4b4e600f6f6b4a4c4d4a686" +
                "969696969696969696969696969696969696969696969696969696969696969696b7000000000000000000a7969696969696969696b7e6e6e6a7969696879796969696969696969696969687c600000000000000000000000000000000000000a796969696969696969696969696969696b7c5d5a5b5e6000000b5a5c5d5a796" +
                "c697c4d4b400a40000a4b501c2d287c697f6f68797e4f4879700f6f6f687c6000000000000000000000000e60187c6c697c6c6c697a4e6e6e6a487c6c687970100e601e601b4e600e6b40187c6000000000000000000000000000000000000000000c697010000e6e60000e6000001e68797c4d4b400e6f6f60000b5c4d487c6" +
                "c697c5d50011a5b400a50000c3d387c6970000a7b7e5f5a7b70000000087c6000000000000000000000000e60087c6c697c6c6c697a5e6e6e6a587c6c687970000e611e6b511e600e6b50087c6000000000000000000000000000000000000000000c697000000e6e600b4e6000000008797c5d5f60000000000f6f6c5d587c6" +
                "c697c4d4a6868686868686b6000087c697f6f600a79696b7e60000f6f687c6000000000000000000000000e60087c6c69786868686b6e6e6e6a686868687970000a6868686868686b6000087c6000000000000000000000000000000000000000000c69700a4b400e6a4b5e600a400008797c4d400a482920000a400c4d487c6" +
                "c697c5d5a7969696969696b7000087c697000000e60000e6000000000087c6000000000000000000001100000087c6c69796969696b7e6e6e6a796969687970000a6b696969696a6b6000087c6000000000000000000000000000000000000000000c69700a5b500e6a500e600a500008797c5d500a583930000a500c5d587c6" +
                "c697c4d4c4d4c4d4c4d4e600000087c697f6f6a4e6a40000a400a4f6f687c600000000000000000000e4f4000087c6c697c6c6c697a4e6e6e6a487c6c68797a4b4879701104050879700a487c6000000000000000000000000000000000000000000c69720308292e610829210b482928797c4d400f6e4f4e4f400f6c4d487c6" +
                "c697c5d5c5d5c5d5c5d5e600f6f687c6970000a500a50000a500a5000087c600000000000000000000e5f5110087c6c697c6c6c697a5e6e6e6a587c6c68797a5b58797b51141518797b4a587c6000000000000000000000000000000000000000000c69721318393e611839311b583938797c5d50000e5f5e5f50000c5d587c6" +
                "c69701e600b40000c4d4e600203087c697f6f60000a00000000000f6f687c600000000000000000000a68686868686c69786868686b6e600e6a686868687970000a7b786b6e4f48797b50087c6000000000000000000000000000000000000000000868686868686868686868686868686b6c4d4e4f48292e4f4e4f4c4d4a686" +
                "c69700e600b510b5c5d50000213187c69700000000000000000000000087c600000000000000000000a79696969696c69796969696b7e600e6a796969687970000a79696b7e5f5a7b7000087c6000000000000000000000000000000000000000000969696969696969696969696969696b7c5d5e5f58393e5f5e5f5c5d5a796" +
                "c69700a4c2d201a4c4d4a400a68687c697f6f6c4d4e4f4c4d40000f6f687c60000000000000000000001e601e687c6c6970000e6e601e600e601e600b58797000001e6c4d48494e600000087c60000000020300000405000000000a0000000000000c69701e600a4e601b4a400e601a48797c4d48292e4f4c2d28292c4d487c6" +
                "c69700a5c3d3b4a5c5d5a51187c687c6970000c5d5e5f5c5d500000000a7960000000000000000000000e600e6a796c69700b500e6a40000e6a4e600008797000000e6c5d58595e600000087c61100b0b02131b0b04151b0b01111b0b00000000000c69700e6b4a51000b5a5100000a58797c5d58393e5f5c3d38393c5d587c6" +
                "c69720300000b500c4d4a686868687c697c4d4a686868686b6c4d400002030203000000000e4f4000000e60000a000c697203000e6a500b1b4a500a0008797203000e6c4d4c4d4e600a00087c6a6b6c6c6a6b6c6c6a6b6c6c6a6b6c6c60000000000c6972030b50082920000829200b48797c4d4e4f4829284948494c4d487c6" +
                "c697213111000000c5d587c6c6c687c697c5d587c6c6c6c697c5d500002131213111110011e5f50000000000000000c697213100000011001100b500008797213111e6c5d5c5d5e6b4000087c68797c6c68797c6c68797c6c68797c6c60000000000c6972131110083930011839311b58797c5d5e5f5839385958595c5d587c6" +
                "8686868686868686868686868686868686868686868686868686868686868686868686868686b60000a686868686868686868686868686868686868686869786868686868686b6e6b5a68687c6868686868686868686868686868686860000000000868686868686868686868686868686868686868686868686868686868686" +
                "86c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c697000087c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c697c6c6c6c6c6c6c697111187c687c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c60000000000c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6c6";

        }


        public override List<string> GetSheetPath()
        {
            return new List<string>() { "raw/sheet_wolf" }; 
        }

        public override Dictionary<int, string> GetSoundEffectPaths()
        {
            return new Dictionary<int, string>();
        }

        public override Dictionary<int, string> GetMusicPaths()
        {
            return new Dictionary<int, string>();
        }

        public override Dictionary<string, object> GetScriptFunctions()
        {
            return new Dictionary<string, object>();
        }

        public override string GetPalTextureString()
        {
            return "";
        }
    }
}
