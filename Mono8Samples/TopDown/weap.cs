﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class weap : pickup
    {
        public struct offset
        {
            public float x;
            public float y;
            //public int layer;
        }

        public int bullet_delay = 0;
        public int min_bullet_delay = 5;
        public float bullet_speed = 3;
        public Dictionary<int, Dictionary<int, offset>> bullet_offsets;
        public int attack_range = 9999;

        public bool automatic = true;

        protected bool fired_last_frame = false;

        public weap_type weapon_type;

        public int ammo_remaining;
        public int ammo_max;
        public int magazine_size;
        public bool uses_ammo;
        public int cur_mag;

        protected string inventory_sprite_name;

        protected enum noise_level
        {
            none,
            high,
        }

        protected noise_level noise = noise_level.high;

        public weap(float _x, float _y, bool uses_ammo, int initial_ammo, int max_ammo, int magazine_size, PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            x = _x;
            y = _y;

            this.uses_ammo = uses_ammo;
            this.ammo_remaining = initial_ammo;
            this.ammo_max = max_ammo;
            this.magazine_size = magazine_size;
            cur_mag = (int)Game.min(magazine_size, initial_ammo);

            w = 16;

            pickup_delay = 0;

            bullet_offsets = new Dictionary<int, Dictionary<int, offset>>();

            bullet_offsets[-1] = new Dictionary<int, offset>()
            {
                { -1, new offset() { x = -5, y = -1 } },
                {  0, new offset() { x = -5, y = -1 } },
                {  1, new offset() { x = -4, y =  2 } },
            };
            bullet_offsets[0] = new Dictionary<int, offset>()
            {
                { -1, new offset() { x =  1, y = -2 } },
                {  0, new offset() { x =  0, y =  0 } }, // invalid
                {  1, new offset() { x = -1, y =  3 } },
            };
            bullet_offsets[1] = new Dictionary<int, offset>()
            {
                { -1, new offset() { x = 4, y = -1 } },
                {  0, new offset() { x = 4, y = -1 } },
                {  1, new offset() { x = 3, y =  2 } },
            };

            anims = new Dictionary<string, anim>()
                {
                    {
                        "on_ground",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= new int[][] { new int[] { 9, 10 } },//what frames are shown.
                        }
                    },
                };

            set_anim("on_ground");
        }

        public override void drop()
        {
            base.drop();
            fired_last_frame = false;
        }

        public bool start_fire(bool condition_to_try, obj shooter, float fx, float fy, type_flags attack_target_types)
        {
            bool fired = false;
            // TODO: This could probably be simplified to just use owner for most params, but I want to leave it open to
            //       shooting without an owner for now.
            if (condition_to_try && bullet_delay > min_bullet_delay && (!fired_last_frame || automatic))
            {
                if (!uses_ammo || ammo_remaining > 0)
                {
                    // SPAWN WAS HERE

                    bullet_delay = 0;
                    fired = true;

                    // If this uses ammo and we are not in an action area (where we want to give infinite ammo)...
                    if (uses_ammo && Game.game_world.cur_area.game_mode != area.game_modes.action)
                    {
                        ammo_remaining = (int)Game.max(0, ammo_remaining - 1);
                        cur_mag = (int)Game.max(0, cur_mag - 1);
                        if (cur_mag <= 0 && ammo_remaining > 0)
                        {
                            cur_mag = (int)Game.min(ammo_remaining, magazine_size);
                        }
                    }
                }
            }
            else
            {
                bullet_delay += 1;
                fired = false;
            }

            // We don't actually want to save whether a bullet was fired, but rather
            // whether or not the owner tried to fire.
            fired_last_frame = condition_to_try;

            return fired;
        }

        public void complete_fire(obj shooter, float fx, float fy, type_flags attack_target_types)
        {
            var o = bullet_offsets[(int)Math.Round(fx)][(int)Math.Round(fy)];
            if (shooter as agent != null)
            {
                agent a = shooter as agent;
                Vector2? fire_point = a.get_bullet_spawn_point();
                if (fire_point.HasValue)
                {
                    o = new offset()
                    {
                        x = fire_point.Value.X,
                        y = fire_point.Value.Y,
                    };
                }
            }

            if (weapon_type == weap_type.melee)
            {
                new melee_swipe(shooter.x + o.x, shooter.y + o.y, fx, fy, this, attack_target_types, Game) { speed = 0, life_time = 10 }.activate();
            }
            else
            {
                #region bullet_dir
                /*
                string anim = "bullet_";
                if (fx > 0 && fy > 0)
                {
                    anim += "down_right";
                }
                else if(fx == 0 && fy > 0)
                {
                    anim += "down";
                }
                else if (fx < 0 && fy > 0)
                {
                    anim += "down_left";
                }
                else if (fx < 0 && fy == 0)
                {
                    anim += "left";
                }
                else if (fx < 0 && fy < 0)
                {
                    anim += "up_left";
                }
                else if (fx == 0 && fy < 0)
                {
                    anim += "up";
                }
                else if (fx > 0 && fy < 0)
                {
                    anim += "up_right";
                }
                else if (fx > 0 && fy == 0)
                {
                    anim += "right";
                }
                */
                #endregion
                string anim = "bullet_dot";
                bullet b = new bullet((float)Math.Round(shooter.x + o.x), (float)Math.Round(shooter.y + o.y), fx, fy, this, attack_target_types, Game) { speed = bullet_speed, draw_layer = shooter.draw_layer };
                b.set_anim(anim);
                b.activate();
            }
            new muzzle_fire_fx((float)Math.Round(shooter.x + o.x), (float)Math.Round(shooter.y + o.y), Game).activate();

            if (noise != noise_level.none)
            {
                new sense_emit(Game)
                {
                    x = shooter.col_box().x,
                    y = shooter.col_box().y,
                    radius = 0,
                    radial_velocity = 5,
                    lifetime = 30,
                    creator = shooter,
                }.activate();
            }
        }



        // weapon factory

        public enum weap_type
        {
            pistol,
            machine_gun,
            melee,
        }
        static public weap create(weap_type t, float pos_x, float pos_y, PicoXGame owner, area owning_area = null)
        {
            weap new_weap = null;
            switch(t)
            {
                case weap_type.pistol:
                    {
                        weap pistol = new weap(pos_x, pos_y, true, 15, 999, 15, owner, owning_area);
                        pistol.anims = new Dictionary<string, anim>()
                                {
                                    {
                                        "on_ground",
                                        new anim()
                                        {
                                            ticks=1,//how long is each frame shown.
                                            frames= new int[][] { new int[] { 25 } },//what frames are shown.
                                        }
                                    },
                                };
                        pistol.w = 8;
                        pistol.min_bullet_delay = 0;
                        pistol.automatic = false;
                        pistol.set_anim("on_ground");
                        pistol.display_name = "pistol";
                        pistol.inventory_sprite_name = "/weapons/pistol";
                        new_weap = pistol;
                        break;
                    }

                case weap_type.machine_gun:
                    {
                        new_weap = new weap(pos_x + 4, pos_y, true, 30, 999, 30, owner, owning_area);
                        new_weap.display_name = "machine gun";
                        new_weap.inventory_sprite_name = "/weapons/machine_gun";
                        break;
                    }

                case weap_type.melee:
                    {
                        weap melee = new weap(pos_x, pos_y, false, 0, 0, 0, owner, owning_area);
                        melee.anims = new Dictionary<string, anim>()
                                {
                                    {
                                        "on_ground",
                                        new anim()
                                        {
                                            ticks=1,//how long is each frame shown.
                                            frames= new int[][] { new int[] { 15 } },//what frames are shown.
                                        }
                                    },
                                };
                        melee.w = 8;
                        melee.min_bullet_delay = 0;
                        melee.automatic = false;
                        melee.set_anim("on_ground");
                        melee.attack_range = 8;
                        melee.noise = noise_level.none;
                        melee.display_name = "unarmed";
                        melee.inventory_sprite_name = "/weapons/knife";
                        new_weap = melee;
                        break;
                    }
            }

            new_weap.weapon_type = t;
            return new_weap;
        }

        public void draw_inventory_view(int x, int y)
        {
            //Game.spr(9, x - 8, y, 2, 1);
            int initial_y = y;
            
            var s = Game.game_anims.get_sprite(inventory_sprite_name);
            if (s != null)
            {
                var final_w_half = Game.flr(s.w * 0.5f);
                var final_h_half = Game.flr(s.h * 0.5f);

                var start_x = x - (final_w_half);
                var start_y = y;// - (final_h_half);
                Game.sspr(s.x, s.y, s.w, s.h, start_x, start_y, s.w, s.h);
            }

            y += s.h + 1;

            if (uses_ammo)
            {
                int ticks_per_row = 15;
                for (int i = 0; i < magazine_size; i++)
                {
                    int col = 7;
                    int col2 = 30;
                    if (i < cur_mag)
                    {
                        if (cur_mag <= 5)
                        {
                            col = 15;
                            col2 = 14;
                        }
                        else
                        {
                            col = 17;
                            col2 = 27;
                        }
                    }
                    if (i % ticks_per_row == 0 && i != 0)
                    {
                        y += 5;
                    }
                    int start_x = x - 15 + (i % ticks_per_row) * 2;
                    Game.line(start_x, y, start_x, y + 3, col);
                    Game.pset(start_x, y, col2);
                }
            }

            y = initial_y + 10;
        }
    }
}
