﻿namespace PicoX
{
    /// <summary>
    /// Wraps the entire set of required interfaces into a single interface.
    /// When implementing PicoX for a new platform, this is what needs to
    /// be the base interface.
    /// </summary>
    public interface IPico8API : IPico8GameAPI
    {
        // TODO: Remove if no longer a point to this.
    }
}
