﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_track_move_in_dir : fight_track
    {
        int dir_x;
        int dir_y;

        public fight_track_move_in_dir(int dir_x, int dir_y, obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {
            this.dir_x = dir_x;
            this.dir_y = dir_y;
        }

        public fight_track_move_in_dir(int dir_x, int dir_y, string owning_obj_id, PicoXGame owner) : base(owning_obj_id, owner)
        {
            this.dir_x = dir_x;
            this.dir_y = dir_y;
        }

        public override bool advance(int frames)
        {
            //float max_speed = 0.75f;
            //float acc = 0.1f / 0.8f;
            //owning_obj.dy = Game.max(-max_speed, Game.min(max_speed, owning_obj.dy + (acc * dir_y)));
            //owning_obj.dx = Game.max(-max_speed, Game.min(max_speed, owning_obj.dx + (acc * dir_x)));

            float max_speed = 0.25f;
            owning_obj.x += dir_x * max_speed;
            owning_obj.y += dir_y * max_speed;
            return false;
        }

        public override string ToString()
        {
            return base.ToString() + "( )"; 
        }
    }
}
