﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class hummer_ai : destructable
    {
        public hummer_ai(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            w = 24;
            h = 40;

            hp = 20;

            collision_type = area.collision_flag.low;

            husk_anim = "hummer_damaged";

            set_anim("hummer");
        }

        public override void on_activated()
        {
            base.on_activated();

            //new pattern_ai(pattern_ai.pattern_types.sentry, Game) { x = x, y = y, }.activate();

            pattern_ai ai = new pattern_ai(pattern_ai.pattern_types.gunner, Game)
            {
                x = x,
                y = y - 12, // now using shape instead of tiles.
                h = 128, // make huge so that it draws over hummer.
                anim_prefix = "gunner",
                no_clip = true,
                draw_layer = 1, // hack to draw the gunner on top of the hummer.
            };
            //ai.set_col_box_pos(ai.x, ai.y - 8);
            weap gun = weap.create(weap.weap_type.machine_gun, 0, 0, Game);
            gun.pick_up(ai);
            gun.bullet_speed *= 0.25f; // 0.125f; or 0.25f

            ai.activate();
        }
    }
}
