﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    class fight_track_spawn_object : fight_track
    {
        string object_id;

        public fight_track_spawn_object(string object_id, obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {
            this.object_id = object_id;
        }

        public override bool advance(int frames)
        {
            base.advance(frames);

            new obj(Game, Game.game_world.cur_area)
            {
                x = Game.game_world.get_player().x,
                y = Game.game_world.get_player().y - 32,

                id_name = object_id,
            }.activate();

            return true;
        }
    }
}
