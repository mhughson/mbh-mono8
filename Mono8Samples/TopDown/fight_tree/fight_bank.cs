﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_bank : fight_branch
    {
        public fight_bank(List<fight_condition> conditions, List<fight_branch> children, string name, PicoXGame owner) : base(conditions, children, name, owner)
        {
        }
    }
}
