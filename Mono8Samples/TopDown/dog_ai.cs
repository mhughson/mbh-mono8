﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class dog_ai : simple_ai
    {
        public dog_ai(PicoXGame owner, area owning_area) : base(states.sleeping, owner, owning_area)
        {
            //acc = 0.3f;
            //dcc = 0.9f;
            walk_speed = 0.25f;
            run_speed = 0.75f;

            #region OLD_ANIMS
            /*
            anims = new Dictionary<string, anim>()
                {
                    // IDLE

                    {
                        "idle_up_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(512,1,1),//what frames are shown.
                        }
                    },
                    {
                        "idle_right_unarmed",
                        new anim()
                        {
                            w = 16,
                            ticks=1,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, -539, -538, },
                            },
                        }
                    },
                    {
                        "idle_down_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(515,1,1),//what frames are shown.
                        }
                    },
                    {
                        "idle_left_unarmed",
                        new anim()
                        {
                            w = 16,
                            ticks=1,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, 538, 539, },
                            },
                        }
                    },


                    {
                        "idle_up_right_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(524,1,1),//what frames are shown.
                        }
                    },
                    {
                        "idle_down_right_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(544,-1,1),//what frames are shown.
                        }
                    },
                    {
                        "idle_up_left_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(525,-1,1),//what frames are shown.
                        }
                    },
                    {
                        "idle_down_left_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(544,1,1),
                        }
                    },

                    // WALKING

                    {
                        "walk_up",
                        new anim()
                        {
                            h=24,
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(512,3,1),
                        }
                    },
                    {
                        "walk_right",
                        new anim()
                        {
                            w = 16,
                            ticks=10,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, -535, -534, },
                                new int[] { 0, 0, -537, -536, },
                                new int[] { 0, 0, -539, -538, },
                            },
                        }
                    },
                    {
                        "walk_down",
                        new anim()
                        {
                            h=24,
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(515,3,1),
                        }
                    },
                    {
                        "walk_left",
                        new anim()
                        {
                            w = 16,
                            ticks=10,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, 534, 535, },
                                new int[] { 0, 0, 536, 537, },
                                new int[] { 0, 0, 538, 539, },
                            },
                        }
                    },

                    {
                        "walk_up_right",
                        new anim()
                        {
                            h=24,
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(524,3,1),
                        }
                    },
                    {
                        "walk_down_right",
                        new anim()
                        {
                            h=24,
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(544,-3,1),
                        }
                    },
                    {
                        "walk_up_left",
                        new anim()
                        {
                            h=24,
                            ticks=10,//how long is each frame shown.
                            frames=  create_anim_sequence(524,-3,1),
                        }
                    },
                    {
                        "walk_down_left",
                        new anim()
                        {
                            h=24,
                            ticks=10,//how long is each frame shown.
                            frames=create_anim_sequence(544,3,1),
                        }
                    },


                    // RUNNING

                    {
                        "run_up_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(512,3,1),
                        }
                    },
                    {
                        "run_right_unarmed",
                        new anim()
                        {
                            w = 16,
                            ticks=5,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, -535, -534, },
                                new int[] { 0, 0, -537, -536, },
                                new int[] { 0, 0, -539, -538, },
                            },
                        }
                    },
                    {
                        "run_down_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(515,3,1),
                        }
                    },
                    {
                        "run_left_unarmed",
                        new anim()
                        {
                            w = 16,
                            ticks=5,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, 534, 535, },
                                new int[] { 0, 0, 536, 537, },
                                new int[] { 0, 0, 538, 539, },
                            },
                        }
                    },

                    {
                        "run_up_right_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(524,3,1),
                        }
                    },
                    {
                        "run_down_right_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(544,-3,1),
                        }
                    },
                    {
                        "run_up_left_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames=  create_anim_sequence(524,-3,1),
                        }
                    },
                    {
                        "run_down_left_unarmed",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(544,3,1),
                        }
                    },

                    // BITE
                    {
                        "idle_up_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(588,3),
                        }
                    },
                    {
                        "idle_right_unarmed_firing",
                        new anim()
                        {
                            w = 16,
                            ticks=punch_time/3,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { -577, -576, -593, -592 },
                                new int[] { -579, -578, -595, -594 },
                                new int[] { -581, -580, -597, -596 },
                            },
                        }
                    },
                    {
                        "idle_down_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(610,1),
                        }
                    },
                    {
                        "idle_left_unarmed_firing",
                        new anim()
                        {
                            w = 16,
                            ticks=punch_time/3,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 576, 577, 592, 593 },
                                new int[] { 578, 579, 594, 595 },
                                new int[] { 580, 581, 596, 597 },
                            },
                        }
                    },


                    {
                        "idle_up_right_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time/2,//how long is each frame shown.
                            frames=create_anim_sequence(608,2),
                        }
                    },
                    {
                        "idle_down_right_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time/6,//how long is each frame shown.
                            frames=create_anim_sequence(582,6),
                        }
                    },
                    {
                        "idle_up_left_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time/2,//how long is each frame shown.
                            frames=create_anim_sequence(608,-2),
                        }
                    },
                    {
                        "idle_down_left_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time/6,//how long is each frame shown.
                            frames=create_anim_sequence(582,-6),
                        }
                    },

                    // SLEEP
                    
                    {
                        "sleeping",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            w = 16,
                            frames= new int[][]
                            {
                                new int[] { 494, 495, 510, 511 },
                            },
                        }
                    },

                    {
                        "waking",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            w = 16,
                            frames= new int[][]
                            {
                                new int[] { 557, 558, 573, 574 },
                            },
                        }
                    },
                };
                */
            #endregion

            #region OLD_ANIM_OVERRIDES
            /*
            // IDLE ARMED
            anims["idle_up_armed"]          = anims["idle_up_unarmed"];
            anims["idle_down_armed"]        = anims["idle_down_unarmed"];
            anims["idle_right_armed"]       = anims["idle_right_unarmed"];
            anims["idle_left_armed"]        = anims["idle_left_unarmed"];
            anims["idle_up_right_armed"]    = anims["idle_up_right_unarmed"];
            anims["idle_up_left_armed"]     = anims["idle_up_left_unarmed"];
            anims["idle_down_right_armed"]  = anims["idle_down_right_unarmed"];
            anims["idle_down_left_armed"]   = anims["idle_down_left_unarmed"];

            // RUN ARMED
            anims["run_up_armed"]           = anims["run_up_unarmed"];
            anims["run_down_armed"]         = anims["run_down_unarmed"];
            anims["run_right_armed"]        = anims["run_right_unarmed"];
            anims["run_left_armed"]         = anims["run_left_unarmed"];
            anims["run_up_right_armed"]     = anims["run_up_right_unarmed"];
            anims["run_up_left_armed"]      = anims["run_up_left_unarmed"];
            anims["run_down_right_armed"]   = anims["run_down_right_unarmed"];
            anims["run_down_left_armed"]    = anims["run_down_left_unarmed"];

            // RUN ARMED FIRING
            anims["run_up_armed_firing"]           = anims["run_up_unarmed"];
            anims["run_down_armed_firing"]         = anims["run_down_unarmed"];
            anims["run_right_armed_firing"]        = anims["run_right_unarmed"];
            anims["run_left_armed_firing"]         = anims["run_left_unarmed"];
            anims["run_up_right_armed_firing"]     = anims["run_up_right_unarmed"];
            anims["run_up_left_armed_firing"]      = anims["run_up_left_unarmed"];
            anims["run_down_right_armed_firing"]   = anims["run_down_right_unarmed"];
            anims["run_down_left_armed_firing"]    = anims["run_down_left_unarmed"];
            */
            #endregion
            //cur_weap = new weap(0, 0, Game);

            anim_prefix = "dog";
            set_anim(anim_prefix + "_idle_down_armed" );
        }

        public override void update()
        {
            base.update();

            // For now, dogs don't patrol. This is mostly so that after finishing searching an area,
            // they go back to sleep.
            if (get_state() == states.patrol && PatrolPoints == null)
            {
                set_state(states.sleeping);
            }
        }

        public override void draw()
        {
            base.draw();
        }

        public override void push_pal()
        {
            Game.apply_pal(Game.get_cur_pal());
        }

        public override void pop_pal()
        {
            Game.pal();
        }

        public override kill_fx generate_kill_fx(obj source)
        {
            kill_fx fx = new kill_dog_fx(x, y, source.dx, source.dy, Game);
            fx.activate();
            return fx;
        }
    }
}
