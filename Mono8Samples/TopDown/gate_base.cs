﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class gate_base : obj
    {
        public int security_level = 0;
        protected string open_anim;

        public gate_base(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            has_power = true;
        }

        public override rect col_box()
        {
            return new rect(x, y, w + 16, h + 16);
        }

        public override void update()
        {
            // For now doors only care about the player.
            player p = Game.game_world.get_player();
            if (p != null)
            {
                bool open_with_card = p.col_box().intersects(col_box()) && p.security_level >= security_level;
                bool open_no_power = !has_power;
                if (open_with_card || open_no_power)
                {
                    set_anim(open_anim);
                    event_on_anim_done += on_door_open_complete;
                }
            }

            base.update();
        }

        public override void draw()
        {
            Game.apply_pal(Game.get_cur_pal());
            base.draw();
            Game.pal();
        }

        public virtual void on_door_open_complete(string anim_name)
        {
        }

        public override void on_switch_toggle()
        {
            base.on_switch_toggle();

            has_power = !has_power;
        }
    }
}
