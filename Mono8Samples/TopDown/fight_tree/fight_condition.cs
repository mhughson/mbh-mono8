﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_condition : sub
    {
        public fight_condition(PicoXGame owner) : base(owner)
        {
        }

        public virtual bool test_condition()
        {
            return true;
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}
