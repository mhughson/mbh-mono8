﻿
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using PicoX;
using Microsoft.Xna.Framework.Audio;
using TiledSharp;
using System.Diagnostics;
using MoonSharp.Interpreter;
#if !WINDOWS_UWP
using Gif.Components;
#endif
using System.IO;

namespace Mono8
{
    /// <summary>
    /// Concrete implementation of Pico8 API built on top of MonoGame.
    /// 
    /// TODO:
    /// 
    /// Save/Load(dget, dset)
    /// 
    /// </summary>
    public class Mono8API : IPico8API
    {
        /// <summary>
        /// Requires for getting information about device (resolution, etc). Could possibly be
        /// replaced with config.
        /// </summary>
        private GraphicsDeviceManager Graphics;

        /// <summary>
        /// Required for the final blit to the screen.
        /// </summary>
        private SpriteBatch Batch;

        /// <summary>
        /// The current Sheet texture converted into an array of color.
        /// TODO: This should probably be part of DrawState now that we have banks.
        /// </summary>
        private int[] SheetData;
        private int SheetDataWidth;

        private class BankData
        {
            public BankData(int[] SheetData, int SheetDataWidth)
            {
                this.SheetData = SheetData;
                this.SheetDataWidth = SheetDataWidth;
                FirstGID = 0;
            }
            public int[] SheetData;
            public int SheetDataWidth;
            public int FirstGID;
        }

        /// <summary>
        /// A list of sprite sheets that can be activated, or deactived on command.
        /// </summary>
        private Dictionary<int, BankData> SheetDataBanks;

        /// <summary>
        /// The texture sheet used for the font.
        /// </summary>
        private Texture2D Font;

        /// <summary>
        /// The font texture converted into an array of color.
        /// </summary>
        private readonly Color[] FontData;

        /// <summary>
        /// The preset list of colors that the engine supports.
        /// </summary>
        public Color[] Colors { get; private set; }

        /// <summary>
        /// Reverse lookup table for going from Color to index into unmodified
        /// color table (does not account for pal calls).
        /// </summary>
        private Dictionary<Color, int> ColorLookup;

        /// <summary>
        /// An array of colors representing the screen. This is what gets draw to, and eventually
        /// gets turned into a texture at the end of the frame.
        /// </summary>
        private Color[] BackBuffer;

        /// <summary>
        /// Stores the last n number of frames for the "record that" behaviour.
        /// </summary>
        //private byte[][] GifBuffer;
        private Color[][] GifBuffer;

        /// <summary>
        /// GifBuffer is a circular buffer, and this is the current index to write to.
        /// </summary>
        private int GifBufferIndex;

        /// <summary>
        /// How many frames should be captures in the GifBuffer.
        /// </summary>
        private int GifMaxLength;

        /// <summary>
        /// The length of the current gif. This is needed because we preallocate the gif buffer to fit
        /// the max running length, but we only want to write the frames that have actually occured.
        /// </summary>
        private int GifRunningLength;

        /// <summary>
        /// How much should the GIF width and height be scaled up from the
        /// native resolution of the engine (not the window size).
        /// </summary>
        public int GifScale;

        /// <summary>
        /// Should the game attempt to capture animated gifs which can have a large memory overhead.
        /// </summary>
        public bool GifCaptureEnabled = true;

        /// <summary>
        /// Modifier used for the final rendering scale of the game. This is different from the resolution as it will cause the
        /// game to be clipped if you scale beyond the default resolution.
        /// </summary>
        public float RenderScale = 1.0f;

        /// <summary>
        /// At the end of each frame, BackBuffer is written into this texture so that
        /// it can be drawn to the screen.
        /// </summary>
        private Texture2D BackBufferTexture;

        /// <summary>
        /// Used for generating random numbers.
        /// </summary>
        private Random Rand;

        /// <summary>
        /// Mapping of Pico 8 Button IDs to MonoGame Buttons/Keys. The whole thing is 
        /// keyed by player id, as that is how Pico 8 looks things up.
        /// </summary>
        private Dictionary<int /*Player #*/, Dictionary<int /*Pico8 Button #*/, Tuple<Buttons /*MonoGame GamePad*/, Keys /*MonoGame Keyboard*/>>> ButtonMap;

        /// <summary>
        /// An array of the state of all gamepads at the end of the previous frame. Used for detecting release.
        /// TODO: Pico8 supports repeats at a rate of 15 frame delay, and then every 4 frames after that.
        /// I don't think any of my games depend on that, so keeping things simple for now.
        /// </summary>
        GamePadState[] LastGamePadState;

        /// <summary>
        /// An array of the sate of all keyboard keys at the end of the previous frame.
        /// </summary>
        KeyboardState LastKeyboardState;

        /// <summary>
        /// The resolution of Pico8 games (not this app, which may be running at 4k).
        /// </summary>
        public Point Resolution;

        /// <summary>
        /// An array of bytes representing the map. Each byte is an index into the sprite sheet.
        /// </summary>
        List<TileData[]> MapData;

        /// <summary>
        /// The size (in tiles) of the current map.
        /// </summary>
        Point MapSize;

        /// <summary>
        /// Name of game used for save game.
        /// </summary>
        string CartDataName;

        /// <summary>
        /// The live cart data.
        /// NOTE: Not used on UWP which accesses setting directly in dget/dset but should
        ///       probably be updated to load into this array like other platforms.
        /// </summary>
        int[] CartData;

        /// <summary>
        /// Pause menu items added by the game.
        /// </summary>
        public Dictionary<int,Tuple<string, Action>> AdditionalPauseMenuEntries { get; private set; }

        /// <summary>
        /// Holds the current draw state of the game.
        /// </summary>
        struct DrawState
        {
            /// <summary>
            /// Current camera position (top left). Used to offset any draw calls.
            /// </summary>
            public Vector2 CameraPos;

            /// <summary>
            /// The current transparent color.
            /// </summary>
            public HashSet<int> ColorKeys;

            /// <summary>
            /// The current mapping of index to color for drawing.
            /// </summary>
            public Color[] DrawColors;

            /// <summary>
            /// The current mapping of index to color for blitting.
            /// </summary>
            public Dictionary<int, Color> ScreenColors;

            /// <summary>
            /// The max number of sprite functions a game can have.
            /// TODO: Handle this more gracefully. It's fixed right now for perf
            /// reasons.
            /// </summary>
            public const int max_sprite_functions = 10;

            /// <summary>
            /// A set of sprite fx functions, indexed by an user supplied id.
            /// </summary>
            public bool[] SpriteFXFunctionsEnabled;
        }

        /// <summary>
        /// The current drawing state.
        /// </summary>
        DrawState CurDrawState;

        /// <summary>
        /// An array of all the sound effects used by the game.
        /// </summary>
        Dictionary<int, SoundEffect> SoundEffects;

        /// <summary>
        /// An array of all the songs in the game.
        /// </summary>
        Dictionary<int, SoundEffectInstance> Music;

        /// <summary>
        /// Mapping of sprite ID to flag. If not in dictionary, assume flag of 0.
        /// </summary>
        Dictionary<int, byte> SpriteFlags;

        Dictionary<int, Func<int, int, int, int>> SpriteFXFunctions;

        public Mono8API(GraphicsDeviceManager Graphics, SpriteBatch Batch, List<Texture2D> Sheets, Texture2D Font, string MapString, Dictionary<int, SoundEffect> SoundEffects, Dictionary<int, SoundEffect> Music, Point Resolution, Texture2D Palette = null)
        {
            this.Graphics = Graphics;
            this.Batch = Batch;
            this.Resolution = Resolution;

            SheetDataBanks = new Dictionary<int, BankData>();

            // Default to bank 0.
            bset(0);

            this.Font = Font;
            FontData = new Color[this.Font.Width * this.Font.Height];
            this.Font.GetData(FontData);

            BackBuffer = new Color[Resolution.X * Resolution.Y];
            for (int i = 0; i < BackBuffer.Length; i++)
            {
                BackBuffer[i] = Color.Black;
            }
            BackBufferTexture = new Texture2D(Graphics.GraphicsDevice, Resolution.X, Resolution.Y);
            GifMaxLength = 60 * 30;
            ResetGifCapture();

            Dictionary<int, Color> ColorsTemp;

            int ColorIndex = 0;
            if (Palette == null)
            {
                ColorsTemp = new Dictionary<int, Color>()
                {
                    { 0, new Color(0,0,0) },
                    { 1, new Color(29,43,83) },    // Dark Blue
                    { 2, new Color(126,37,83) },   // Dark Purple
                    { 3, new Color(0,135,81) },    // Dark Green
                    { 4, new Color(171,82,54) },   // Brown
                    { 5, new Color(95,87,79) },    // Dark Grey
                    { 6, new Color(194,195,199) }, // Light Grey
                    { 7, new Color(255,241,232) }, // White
                    { 8, new Color(255,0,77) },    // Red
                    { 9, new Color(255,163,0) },   // Orange
                    { 10, new Color(255,236,39) },  // Yellow
                    { 11, new Color(0,228,54) },    // Green
                    { 12, new Color(41,173,255) },  // Blue
                    { 13, new Color(131,118,156) }, // Indigo
                    { 14, new Color(255,119,168) }, // Pink
                    { 15, new Color(255,204,170) }  // Peach
                };

                //ColorIndex = Colors.Count;

                //// Add any colors that are not from the Pico-8 Palette.
                //for (int i = 0; i < SheetData.Length; i++)
                //{
                //    if (!Colors.ContainsValue(SheetData[i]))
                //    {
                //        Colors.Add(ColorIndex, SheetData[i]);
                //        ColorIndex++;
                //    }
                //}
            }
            else
            {
                ColorsTemp = new Dictionary<int, Color>();

                Color[] PalData = new Color[Palette.Width * Palette.Height];
                Palette.GetData(PalData);

                // Add any colors that are not from the Pico-8 Palette.
                for (int i = 0; i < PalData.Length; i++)
                {
                    if (!ColorsTemp.ContainsValue(PalData[i]))
                    {
                        ColorsTemp.Add(ColorIndex, PalData[i]);
                        ColorIndex++;
                    }
                }
            }

            //List<Color> Missing = new List<Color>();

            //ColorIndex = ColorsTemp.Count;

            // Add any colors that are not from the Pico-8 Palette.
            // TODO: This should really go through all sprite sheets not just the first.
            //if (SheetData != null)
            //{
            //    for (int i = 0; i < SheetData.Length; i++)
            //    {
            //        if (!ColorsTemp.ContainsValue(SheetData[i]) && !Missing.Contains(SheetData[i]))
            //        {
            //            printh("Missing Color ( " + i + " ): " + SheetData[i]);
            //            Missing.Add(SheetData[i]);
            //            ColorsTemp.Add(ColorIndex, SheetData[i]);
            //            ColorIndex++;
            //        }
            //    }
            //}

            ColorLookup = new Dictionary<Color, int>();
            Colors = new Color[ColorsTemp.Count];
            for (int i = 0; i < ColorsTemp.Count; i++)
            {
                Colors[i] = ColorsTemp[i];
                ColorLookup.Add(Colors[i], i);
            }

            for (int i = 0; i < Sheets.Count; i++)
            {
                Texture2D Sheet = Sheets[i];
                if (Sheets != null)
                {
                    AddBank(i, Sheet);
                }
            }

            Rand = new Random();

            ButtonMap = new Dictionary<int, Dictionary<int, Tuple<Buttons, Keys>>>();

            // Player 1
            ButtonMap.Add(0, new Dictionary<int, Tuple<Buttons, Keys>>());
            ButtonMap[0].Add(0, new Tuple<Buttons, Keys>(Buttons.DPadLeft, Keys.Left));
            ButtonMap[0].Add(1, new Tuple<Buttons, Keys>(Buttons.DPadRight, Keys.Right));
            ButtonMap[0].Add(2, new Tuple<Buttons, Keys>(Buttons.DPadUp, Keys.Up));
            ButtonMap[0].Add(3, new Tuple<Buttons, Keys>(Buttons.DPadDown, Keys.Down));
            ButtonMap[0].Add(4, new Tuple<Buttons, Keys>(Buttons.A, Keys.Z));
            ButtonMap[0].Add(5, new Tuple<Buttons, Keys>(Buttons.B, Keys.X));

            ButtonMap[0].Add(6, new Tuple<Buttons, Keys>(Buttons.Start, Keys.Enter));
#if WINDOWS_UWP
            // UWP maps B to Back which breaks button 7 for us. Instead just map it to Y for now :(
            ButtonMap[0].Add(7, new Tuple<Buttons, Keys>(Buttons.Y, Keys.Escape)); // Extension for Pico8 to support SELECT/BACK button.
#else
            ButtonMap[0].Add(7, new Tuple<Buttons, Keys>(Buttons.Back, Keys.Escape)); // Extension for Pico8 to support SELECT/BACK button.
#endif

            // Player 2
            ButtonMap.Add(1, new Dictionary<int, Tuple<Buttons, Keys>>());
            ButtonMap[1].Add(0, new Tuple<Buttons, Keys>(Buttons.DPadLeft, Keys.S));
            ButtonMap[1].Add(1, new Tuple<Buttons, Keys>(Buttons.DPadRight, Keys.F));
            ButtonMap[1].Add(2, new Tuple<Buttons, Keys>(Buttons.DPadUp, Keys.E));
            ButtonMap[1].Add(3, new Tuple<Buttons, Keys>(Buttons.DPadDown, Keys.D));
            ButtonMap[1].Add(4, new Tuple<Buttons, Keys>(Buttons.A, Keys.Tab));
            ButtonMap[1].Add(5, new Tuple<Buttons, Keys>(Buttons.B, Keys.Q));

            CacheInputStates();

            CurDrawState = new DrawState()
            {
                CameraPos = new Vector2(),
                ColorKeys = new HashSet<int>() { 0 },
                DrawColors = (Color[])Colors.Clone(),
                ScreenColors = new Dictionary<int, Color>(),
                SpriteFXFunctionsEnabled = new bool[DrawState.max_sprite_functions],
            };

            SpriteFlags = new Dictionary<int, byte>();
            SpriteFXFunctions = new Dictionary<int, Func<int, int, int, int>>();

            reloadmap(MapString);

            this.SoundEffects = SoundEffects;

            this.Music = new Dictionary<int, SoundEffectInstance>();
            foreach (var Song in Music)
            {
                this.Music.Add(Song.Key, Song.Value.CreateInstance());
            }

            CartData = new int[64];

            AdditionalPauseMenuEntries = new Dictionary<int, Tuple<string, Action>>();
        }

        public void Shutdown()
        {
            foreach (var Sound in SoundEffects)
            {
                // NOTE: I'm not sure why I was disposing these resources, as this gets called
                //       on "reset" as well, resulting in a freeing off the audio assets which 
                //       are then unusable on subsequent playthroughs.
                //Sound.Value.Dispose();
            }

            foreach (var Song in Music)
            {
                Song.Value.Stop();
                //Song.Value.Dispose();
            }

            Music.Clear();
            SoundEffects.Clear();
        }

        public void CacheInputStates()
        {
            LastGamePadState = new GamePadState[4]
            {
            GamePad.GetState(PlayerIndex.One),
            GamePad.GetState(PlayerIndex.Two),
            GamePad.GetState(PlayerIndex.Three),
            GamePad.GetState(PlayerIndex.Four),
            };
            LastKeyboardState = Keyboard.GetState();
        }

        public uint ToARGB(uint rgba)
        {
            return (rgba << 24) | (rgba >> 8);
        }

        public void EndDraw()
        {
            if (CurDrawState.ScreenColors.Count > 0)
            {
                for (int i = 0; i < BackBuffer.Length; i++)
                {
                    int color_id;
                    if (ColorLookup.TryGetValue(BackBuffer[i], out color_id))
                    {
                        Color new_color;
                        if (CurDrawState.ScreenColors.TryGetValue(color_id, out new_color))
                        {
                            BackBuffer[i] = new_color;
                        }
                    }
                }
            }
               
            // Copy our data block to an actual texture for rendering.
            // TODO: Can the color data be fed into Batch.Draw directly?
            BackBufferTexture.SetData(BackBuffer);
            
            // TODO: Should not be needed, as BackBuffer should cover the screen. Left in for debugging.
            Graphics.GraphicsDevice.Clear(Color.Black);
            Batch.Begin(samplerState: SamplerState.PointClamp);
            int WindowW = Graphics.GraphicsDevice.Viewport.Width;
            int WindowH = Graphics.GraphicsDevice.Viewport.Height;
            float WindowScale = Math.Min(WindowW / Resolution.X, WindowH / Resolution.Y);
            float FinalWidth = WindowScale * Resolution.X;
            float FinalHeight = WindowScale * Resolution.Y;
            // Unlike P8, I center the final scaled image if RenderScale is provided.
            float MiddleX = Resolution.X * 0.5f;
            float MiddleY = Resolution.Y * 0.5f;
            float SourceWidth = Resolution.X / RenderScale;
            float SourceHeight = Resolution.Y / RenderScale;
            Batch.Draw(BackBufferTexture, 
                sourceRectangle: new Rectangle(
                    (int)(MiddleX - SourceWidth * 0.5f), 
                    (int)(MiddleY - SourceHeight * 0.5f),
                    (int)(SourceWidth),
                    (int)(SourceHeight)),
                destinationRectangle: new Rectangle(
                    (WindowW / 2) - (int)(FinalWidth * 0.5f),
                    (WindowH / 2) - (int)(FinalHeight * 0.5f),
                    (int)FinalWidth,
                    (int)FinalHeight),
                color: Color.White);
            Batch.End();
            
            // If we are in the middle of writing out the gif, don't update the byte data or counters.
            if (CaptureCounter != 0 || !GifCaptureEnabled)
            {
                return;
            }

            //GifBuffer.Enqueue((Image[])BackBuffer.Clone());

            //System.Drawing.Bitmap img = new System.Drawing.Bitmap(Resolution.X, Resolution.Y);
            //for (int x = 0; x < Resolution.X; x++)
            //{
            //    for (int y = 0; y < Resolution.Y; y++)
            //    {
            //        Color Pixel = BackBuffer[x + (y * Resolution.X)];
            //        img.SetPixel(x, y, System.Drawing.Color.FromArgb(Pixel.R, Pixel.G, Pixel.B));
            //    }
            //}

            // CHANGE_PRE_ALLOC
            //byte[] rgbIndexes = new byte[Resolution.X * GifScale * Resolution.Y * GifScale];

            //int j = 0;
            //for (int i = 0; i < BackBuffer.Length; i++)
            //{
            //    for (int x = 0; x < Scale; x++)
            //    {
            //        for (int y = 0; y < Scale; y++)
            //        {
            //            BigColors[(i*Scale) + (x + (y * Resolution.X * Scale))] = BackBuffer[i];
            //        }
            //    }
            //}

            //for (int y = 0; y < Resolution.Y * GifScale; y++)
            {
                //for (int x = 0; x < Resolution.X * GifScale; x++)
                {
                    // TODO: Update gif library to support int sized palette.
                    //GifBuffer[GifBufferIndex][ x + (y * (Resolution.X * GifScale))] = (byte)ColorLookup[BackBuffer[(int)(x / GifScale) + ((int)(y / GifScale) * Resolution.X)]];
                    GifBuffer[GifBufferIndex] = (Color[])BackBuffer.Clone();
                }
            }

            //System.Drawing.Bitmap img = new System.Drawing.Bitmap(Resolution.X * Scale, Resolution.Y * Scale);
            //for (int x = 0; x < Resolution.X * Scale; x++)
            //{
            //    for (int y = 0; y < Resolution.Y * Scale; y++)
            //    {
            //        Color Pixel = BigColors[x + (y * Resolution.X * Scale)];
            //        img.SetPixel(x, y, System.Drawing.Color.FromArgb(Pixel.R, Pixel.G, Pixel.B));
            //    }
            //}


            //System.Drawing.Bitmap img = new System.Drawing.Bitmap(Resolution.X * Scale, Resolution.Y * Scale);

            //// Lock the bitmap's bits.  
            //System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, Resolution.X * Scale, Resolution.Y * Scale);
            //System.Drawing.Imaging.BitmapData bmpData =
            //    img.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
            //    img.PixelFormat);

            //// Get the address of the first line.
            //IntPtr ptr = bmpData.Scan0;

            //int bytes = BackBufferTexture.Width * BackBufferTexture.Height * 4;
            //byte[] rgbValues = new byte[bytes];
            //BackBufferTexture.GetData<byte>(rgbValues);

            //int bytes = Resolution.X * GifScale * Resolution.Y * GifScale * 3;
            //byte[] rgbValues = new byte[bytes];
            //int j = 0;
            //byte[] rgbIndexes = new byte[Resolution.X * GifScale * Resolution.Y * GifScale];
            //for (int i = 0; i < BigColors.Length; i++)
            //{
            //    rgbValues[j++] = BigColors[i].R;
            //    rgbValues[j++] = BigColors[i].G;
            //    rgbValues[j++] = BigColors[i].B;
            //    //rgbValues[j++] = BigColors[i].A;

            //    rgbIndexes[i] = ColorLookup[BigColors[i]];

            //    //for (int k=0; k < Colors.Length; k++)
            //    //{
            //    //    if (BigColors[i] == Colors[k])
            //    //    {
            //    //        rgbIndexes[i] = (byte)k;
            //    //    }
            //    //}
            //}

            //// Bitmap expects data in BGRA format, but Texture is in RGBA format.
            //for (int i = 0; i < bytes; i += 4)
            //{
            //    var temp = rgbValues[i + 2];
            //    rgbValues[i + 2] = rgbValues[i];
            //    rgbValues[i] = temp;
            //}

            //// Copy the RGB values back to the bitmap
            //System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            //// Unlock the bits.
            //img.UnlockBits(bmpData);

            // CHANGE_PRE_ALLOC
            //if (CaptureCounter == 0)
            //{
            //    GifBuffer.Enqueue(rgbIndexes);

            //    if (GifBuffer.Count > GifLength)
            //    {
            //        GifBuffer.Dequeue();
            //    }
            //}


            GifBufferIndex++;
            GifRunningLength = (int)min(GifMaxLength, GifRunningLength + 1);
            if (GifBufferIndex >= GifMaxLength)
            {
                GifBufferIndex = 0;
            }
        }
        
        public void ResetGifCapture()
        {
            if (CaptureCounter == 0 && GifCaptureEnabled)
            {
                if (GifBuffer == null)
                {
                    GifBuffer = new Color[GifMaxLength][];
                }
                GifBufferIndex = 0;
                GifRunningLength = 0;
            }
        }

        int CaptureCounter = 0; // atomic
        public void CaptureGif()
        {
            if (!GifCaptureEnabled)
            {
                return;
            }
#if !WINDOWS_UWP
            CaptureCounter++;
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            /* create Gif */
            //you should replace filepath
            System.IO.Directory.CreateDirectory(path + "\\Mono8");
            string uniquefilename = string.Format("{0}{1}{2}", "\\Mono8\\Mono8_", DateTime.Now.Ticks, ".gif");
            String outputFilePath = path + uniquefilename;
            AnimatedGifEncoder e = new AnimatedGifEncoder();
            e.Start();
            e.SetSize(Resolution.X * GifScale, Resolution.Y * GifScale);
            e.SetFrameRate(60);

            byte[] p = new byte[Colors.Length * 3];
            int index = 0;
            for (int i = 0; i < Colors.Length; i++)
            {
                p[index++] = Colors[i].R;
                p[index++] = Colors[i].G;
                p[index++] = Colors[i].B;
            }
            e.SetColorPalette(p);
            //-1:no repeat,0:always repeat
            e.SetRepeat(0);

            // Mod by the length to handle case where gif isn't full length.
            int CurIndex = GifBufferIndex % GifRunningLength; 
            for (int i = 0; i < GifRunningLength; i++)
            {
                Byte[] Frame = new Byte[Resolution.X * GifScale * Resolution.Y * GifScale];
                for (int y = 0; y < Resolution.Y * GifScale; y++)
                {
                    for (int x = 0; x < Resolution.X * GifScale; x++)
                    {
                        Frame[ x + (y * (Resolution.X * GifScale))] = (byte)ColorLookup[GifBuffer[i][(int)(x / GifScale) + ((int)(y / GifScale) * Resolution.X)]];
                        //GifBuffer[GifBufferIndex] = BackBuffer;
                    }
                }
                //Byte[] Frame = GifBuffer[CurIndex];
                e.AddFrame(Frame);

                CurIndex = (CurIndex + 1) % GifRunningLength;
            }
            e.Finish();
            e.Output(outputFilePath);
            CaptureCounter--;

            /*
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //Create new Animated GIF Creator with Path to C:\awesomegif.gif and 33ms delay between frames (=30 fps)
            using (AnimatedGifCreator gifCreator = new AnimatedGifCreator(path + "\\awesomegif.gif", 20))
            {
                //Enumerate through a List<System.Drawing.Image> or List<System.Drawing.Bitmap> for example
                foreach (System.Drawing.Image Screen in GifBuffer)
                {
                    //System.Drawing.Bitmap img = new System.Drawing.Bitmap(Resolution.X, Resolution.Y);
                    //for (int x = 0; x < Resolution.X; x++)
                    //{
                    //    for (int y = 0; y < Resolution.Y; y++)
                    //    {
                    //        Color Pixel = Screen[x + (y * Resolution.X)];
                    //        img.SetPixel(x, y, System.Drawing.Color.FromArgb(Pixel.R, Pixel.G, Pixel.B));
                    //    }
                    //}
                    using (Screen)
                    {
                        //Add the image to gifEncoder with default Quality
                        gifCreator.AddFrame(Screen, GifQuality.Bit8);
                    } //Image disposed
                }
            } // gifCreator.Finish and gifCreator.Dispose is called here
            */
#endif
        }

        public void RegisterLuaFunctions(Script LuaScript)
        {
            // DRAWING
            //

            LuaScript.Globals["cls"] = (Action<int>)cls;
            LuaScript.Globals["pget"] = (Func<int, int, int>)pget;
            LuaScript.Globals["pset"] = (Action<float,float,int>)pset;
            LuaScript.Globals["bset"] = (Action<int>)bset;
            LuaScript.Globals["sspr"] = (Action<int,int,float,float,float,float,float,float,bool,bool>)sspr;
            LuaScript.Globals["spr"] = (Action<int,float,float,float,float,bool,bool>)spr;
            LuaScript.Globals["sget"] = (Func<int,int,int>)sget;
            LuaScript.Globals["mget"] = (Func<int,int,int,int>)mget;
            LuaScript.Globals["mset"] = (Action<int,int,int,int>)mset;
            LuaScript.Globals["fget"] = (Func<int,int,bool>)fget;
            LuaScript.Globals["fset"] = (Action<int,int,bool>)fset;
            LuaScript.Globals["palt"] = (Action<int,bool>)palt;
            LuaScript.Globals["pal"] = (Action<int,int,int>)pal;
            LuaScript.Globals["line"] = (Action<float,float,float,float,int>)line;
            LuaScript.Globals["circfill"] = (Action<float,float,float,int>)circfill;
            LuaScript.Globals["circ"] = (Action<float,float,float,int>)circ;
            LuaScript.Globals["rect"] = (Action<float,float,float,float,int>)rect;
            LuaScript.Globals["rectfill"] = (Action<float,float,float,float,int>)rectfill;
            LuaScript.Globals["print"] = (Action<string,float,float,int>)print;
            LuaScript.Globals["camera"] = (Action<float,float>)camera;
            LuaScript.Globals["map"] = (Action<int,int,float,float,int,int,int,int>)map;


            // AUDIO
            //

            LuaScript.Globals["sfx"] = (Action<int,int,int>)sfx;
            LuaScript.Globals["music"] = (Action<int,int,int>)music;

            // INPUT
            //

            LuaScript.Globals["btn"] = (Func<int,int,bool>)btn;
            LuaScript.Globals["btnp"] = (Func<int,int,bool>)btnp;

            // MATH
            //

            LuaScript.Globals["rnd"] = (Func<float,float>)rnd;
            LuaScript.Globals["sin"] = (Func<float,float>)sin;
            LuaScript.Globals["cos"] = (Func<float,float>)cos;
            LuaScript.Globals["min"] = (Func<float,float,float>)min;
            LuaScript.Globals["max"] = (Func<float,float,float>)max;
            LuaScript.Globals["mid"] = (Func<float, float, float, float>)mid;
            LuaScript.Globals["sqrt"] = (Func<float,float>)sqrt;
            LuaScript.Globals["flr"] = (Func<float,int>)flr;
            LuaScript.Globals["abs"] = (Func<float,float>)abs;

            // FILE IO
            //

            LuaScript.Globals["printh"] = (Action<string,string,bool>)printh;
            LuaScript.Globals["dget"] = (Func<uint,int>)dget;
            LuaScript.Globals["dset"] = (Action<uint,int>)dset;
            LuaScript.Globals["cartdata"] = (Action<string>)cartdata;

        }

        protected void AddBank(int bank_index, Texture2D Sheet)
        {
            if (Sheet != null)
            {
                Color[] NewSheetColorData = new Color[Sheet.Width * Sheet.Height];
                int[] NewSheetData = new int[Sheet.Width * Sheet.Height];

                Sheet.GetData(NewSheetColorData);
                
                for (int i = 0; i < NewSheetColorData.Length; i++)
                {
                    // TODO: Better handle missing colors here (right now it just crashes).
                    // Perhaps it could add it to the color table right now?
                    NewSheetData[i] = ColorLookup[NewSheetColorData[i]];
                }

                BankData NewBank = new BankData(NewSheetData, Sheet.Width);
                SheetDataBanks.Add(bank_index, NewBank);
            }
        }

        private void sspr_internal(int[] SourceData, int RowWidth, int sx, int sy, float sw, float sh, float dx, float dy, float dw = float.MaxValue, float dh = float.MaxValue, bool flip_x = false, bool flip_y = false)
        {
            if (dw == float.MaxValue)
            {
                dw = sw;
            }
            if (dh == float.MaxValue)
            {
                dh = sh;
            }

            int dirx = 1;
            int diry = 1;

            if (flip_x == true)
            {
                dx += dw - 1;
                dirx = -1;
            }
            if (flip_y == true)
            {
                dy += dh - 1;
                diry = -1;
            }

            float delta_w = sw / dw;
            float delta_h = sh / dh;

            // Loop through the destination, stepping by source/dest size.
            for (int y = 0; y < dh; y++)
            {
                for (int x = 0; x < dw; x++)
                {
                    int sourcex = sx + (int)Math.Floor(delta_w * x);
                    int sourcey = sy + (int)Math.Floor(delta_h * y);

                    int ColID = SourceData[sourcex + (sourcey * RowWidth)];

                    // NOTE: Tried rounding instead of casting to int to fix wobble with slow moving pixels.
                    //       Fixes one direction but breaks the other.
                    int fx = (int)dx + (x * dirx);
                    int fy = (int)dy + (y * diry);

                    // Run any and all Sprite FX Functions prior to checking
                    // for transparent pixels, as we want the function to have
                    // the option of canceling the draw operation.
                    for (int i = 0; i < DrawState.max_sprite_functions; i++)
                    {
                        if (CurDrawState.SpriteFXFunctionsEnabled[i])
                        {
                            ColID = SpriteFXFunctions[i](fx, fy, ColID);
                        }
                    }

                    if (!CurDrawState.ColorKeys.Contains(ColID))
                    {
                        // start at (dx + dw) - x
                        // not flip: (int)dx + x
                        pset(fx, fy, ColID);
                    }
                }
            }
        }

        // PICO-8 API BEGIN
        //

        // DRAWING
        //

        public void cls(int c = 0)
        {
            for (int i = 0; i < Resolution.X * Resolution.Y; i++)
            {
                BackBuffer[i] = CurDrawState.DrawColors[c];
            }
        }

        public void pset(float x, float y, int c)
        {
            if (c < CurDrawState.DrawColors.Length)
            {
                pset((int)x, (int)y, CurDrawState.DrawColors[c]);
            }
        }

        private void pset(int x, int y, int c)
        {
            pset(x, y, CurDrawState.DrawColors[c]);
        }

        private void pset(int x, int y, Color c)
        {
            x = x - (int)CurDrawState.CameraPos.X;
            y = y - (int)CurDrawState.CameraPos.Y;

            // TODO: This should ideally be done earlier to avoid huge slowdown just iterating
            //       over pixels that will never be drawn (eg. rectfill(-MAX_INT... MAX_INT)).
            if (x < 0 || y < 0 || x > Resolution.X - 1 || y > Resolution.Y - 1)
            {
                return;
            }

            // PICO_CHANGE: In Pico-8 the transparent color does not affect pset.
            //if (ColorLookup[c] == CurDrawState.ColorKey)
            //{
            //    return;
            //}

            BackBuffer[x + (y * Resolution.X)] = c;
        }

        public int pget(int x, int y)
        {
            x = x - (int)CurDrawState.CameraPos.X;
            y = y - (int)CurDrawState.CameraPos.Y;

            if (x < 0 || y < 0 || x > Resolution.X - 1 || y > Resolution.Y - 1)
            {
                return 0;
            }

            return ColorLookup[BackBuffer[x + (y * Resolution.X)]];
        }

        public void sspr(int sx, int sy, float sw, float sh, float dx, float dy, float dw = float.MaxValue, float dh = float.MaxValue, bool flip_x = false, bool flip_y = false)
        {
            sspr_internal(SheetData, SheetDataWidth, sx, sy, sw, sh, dx, dy, dw, dh, flip_x, flip_y);
        }

        public void spr(int n, float x, float y, float w = 1, float h = 1, bool flip_x = false, bool flip_y = false)
        {
            if (n != 0)
            {
                sspr((n * 8) % SheetDataWidth, ((n * 8) / SheetDataWidth) * 8, w * 8, h * 8, x, y, w * 8, h * 8, flip_x, flip_y);
            }
        }

        public void sprfxset(int fx, bool enabled)
        {
            CurDrawState.SpriteFXFunctionsEnabled[fx] = enabled;
        }

        public void sprfxadd(Func<int, int, int, int> fx_func, int id)
        {
            SpriteFXFunctions.Add(id, fx_func);
        }

        /// <summary>
        /// Updates the currently used graphics bank.
        /// </summary>
        /// <param name="n">The bank to use.</param>
        public void bset(int n)
        {
            if (SheetDataBanks.ContainsKey(n))
            {
                SheetDataWidth = SheetDataBanks[n].SheetDataWidth;
                SheetData = SheetDataBanks[n].SheetData;
            }
        }

        // Gets the color value of a pixel on the sprite sheet.
        public int sget(int x, int y)
        {
            if (x < 0 || y < 0 || x > Resolution.X - 1 || y > Resolution.Y - 1)
            {
                return 0;
            }

            return SheetData[x + (y * SheetDataWidth)];
        }

        public int mget(int celx, int cely, int map_layer = 0)
        {
            int MapWidth = MapSize.X;
            int Index = celx + (cely * MapWidth);

            // Avoid wrapping around to the next level.
            if (celx >= MapSize.X || cely >= MapSize.Y || celx < 0 || cely < 0)
            {
                return 0;
            }

            if (MapData != null && Index < MapData[map_layer].Length && Index >= 0)
            {
                return MapData[map_layer][Index].ID;
            }
            else
            {
                return 0;
            }
        }

        public void mset(int celx, int cely, int snum, int map_layer = 0)
        {
            if (MapData.Count > map_layer)
            {
                int MapWidth = MapSize.X;
                int Index = celx + (cely * MapWidth);
                if (MapData[map_layer].Length > Index && Index >= 0)
                {
                    MapData[map_layer][Index].ID = snum;
                }
            }
        }

        public void msetbank(int celx, int cely, int snum, int bank, int map_layer = 0)
        {
            if (MapData.Count > map_layer)
            {
                int MapWidth = MapSize.X;
                int Index = celx + (cely * MapWidth);
                if (MapData[map_layer].Length > Index && Index >= 0)
                {
                    MapData[map_layer][Index].ID = snum;
                    MapData[map_layer][Index].Bank = bank;
                }
            }
        }

        public bool fget(int n, int f)
        {
            if (SpriteFlags.ContainsKey(n))
            {
                return (SpriteFlags[n] & 1 << f) != 0;
            }
            else
            {
                return false;
            }
        }

        public byte fget(int n)
        {
            if (SpriteFlags.ContainsKey(n))
            {
                return SpriteFlags[n];
            }
            else
            {
                return 0;
            }
        }

        public bool fget(TileData? tile_data, int flags)
        {
            if (tile_data.HasValue)
            {
                return fget(tile_data.Value.ID + SheetDataBanks[tile_data.Value.Bank].FirstGID - 1, flags);
            }
            else
            {
                return false;
            }
        }

        public byte fget(TileData? tile_data)
        {
            if (tile_data.HasValue)
            {
                return fget(tile_data.Value.ID + SheetDataBanks[tile_data.Value.Bank].FirstGID - 1);
            }
            else
            {
                return 0;
            }
        }


        public TileData? mget_tiledata(int celx, int cely, int map_layer = 0)
        {
            int MapWidth = MapSize.X;
            int Index = celx + (cely * MapWidth);

            // Avoid wrapping around to the next level.
            if (celx >= MapSize.X || cely >= MapSize.Y || celx < 0 || cely < 0 || MapData == null)
            {
                return null;
            }

            // Search through all layers until we hit a valid tile.
            if (map_layer == -1)
            {
                foreach (var layer in MapData)
                {
                    if (Index < layer.Length && Index >= 0)
                    {

                        return layer[Index];
                    }
                }

                return null;
            }
            else
            {
                if (MapData != null && Index < MapData[map_layer].Length && Index >= 0)
                {
                    return MapData[map_layer][Index];
                }
                else
                {
                    return null;
                }
            }
        }

        // Grabs a tile on the map and checks if a flag is set. Handles case of multiple
        // sprite sheets.
        public bool mfget(int celx, int cely, int flag, int map_layer = 0)
        {
            TileData? Data = null;

            // check all layers...

            if (map_layer == -1)
            {
                for (int i = 0; i < MapData.Count; i++)
                {
                    Data = mget_tiledata(celx, cely, i);

                    if (fget(Data.Value.ID + SheetDataBanks[Data.Value.Bank].FirstGID - 1, flag))
                    {
                        return true;
                    }
                }

                // didn't find a layer with the requested flag.
                return false;
            }

            // standard check...

            Data = mget_tiledata(celx, cely, map_layer);

            if (Data != null)
            {
                // NOTE: Handling multiple tilesets in the same map/layer. Normal
                //       mget/fget doesn't do this. At some point I need to rethink
                //       the whole banking system to work universally.
                return fget(Data.Value.ID + SheetDataBanks[Data.Value.Bank].FirstGID - 1, flag);
            }
            else
            {
                return false;
            }
        }

        public byte mfgetflags(int celx, int cely, int map_layer = 0)
        {
            TileData? Data = null;

            // check all layers...

            if (map_layer == -1)
            {
                for (int i = 0; i < MapData.Count; i++)
                {
                    Data = mget_tiledata(celx, cely, i);

                    // Find the first tile with flags.
                    byte flags = fget(Data.Value.ID + SheetDataBanks[Data.Value.Bank].FirstGID - 1);
                    if (flags != 0)
                    {
                        return flags;
                    }
                }

                // didn't find a layer with the requested flag.
                return 0;
            }

            // standard check...

            Data = mget_tiledata(celx, cely, map_layer);

            if (Data != null)
            {
                // NOTE: Handling multiple tilesets in the same map/layer. Normal
                //       mget/fget doesn't do this. At some point I need to rethink
                //       the whole banking system to work universally.
                return fget(Data.Value.ID + SheetDataBanks[Data.Value.Bank].FirstGID - 1);
            }
            else
            {
                return 0;
            }
        }

        public void fset(int n, int f, bool v)
        {
            byte flag = (byte)(1 << f);
            if (v)
            {
                SpriteFlags[n] |= flag;
            }
            else
            {
                SpriteFlags[n] &= (byte)~flag;
            }
        }

        public void fset(int n, byte f)
        {
            SpriteFlags[n] = f;
        }

        public void palt(int col, bool t)
        {
            if (!t)
            {
                CurDrawState.ColorKeys.Remove(col);
            }
            else
            {
                CurDrawState.ColorKeys.Add(col);
            }
        }

        public void pal(int c0, int c1, int p = 0)
        {
            if (c0 < CurDrawState.DrawColors.Length && c1 < Colors.Length)
            {
                if (p == 0)
                {
                    CurDrawState.DrawColors[c0] = Colors[c1];
                }
                else if (p == 1)
                {
                    // TODO: Use this when blitting.
                    CurDrawState.ScreenColors[c0] = Colors[c1];
                }
            }
        }

        public void pal()
        {
            for (int i = 0; i < CurDrawState.DrawColors.Length; i++)
            {
                CurDrawState.DrawColors[i] = Colors[i];
            }

            CurDrawState.ScreenColors.Clear();
        }

        public void line(float x0, float y0, float x1, float y1, int col)
        {
            // Implementation based on https://stackoverflow.com/a/11683720/1757884
            //

            float w = x1 - x0;
            float h = y1 - y0;
            float dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = (int)Math.Abs(w);
            int shortest = (int)Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = (int)Math.Abs(h);
                shortest = (int)Math.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                pset(x0, y0, col);
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x0 += dx1;
                    y0 += dy1;
                }
                else
                {
                    x0 += dx2;
                    y0 += dy2;
                }
            }
        }

        private void circ(float x, float y, float r, int col, Action<float, float, float, float, int> PlotCirclePoint)
        {
            // Implementation based on http://www.pracspedia.com/CG/bresenhamcircle.html
            // http://darshangajara.wordpress.com/
            //

            float xc = x;
            float yc = y;

            float pk = 3 - 2 * r;
            x = 0; y = r;
            PlotCirclePoint(x, y, xc, yc, col);
            while (x < y)
            {
                if (pk <= 0)
                {
                    pk = pk + (4 * x) + 6;
                    PlotCirclePoint(++x, y, xc, yc, col);
                }
                else
                {
                    pk = pk + (4 * (x - y)) + 10;
                    PlotCirclePoint(++x, --y, xc, yc, col);
                }
            }
        }

        public void circfill(float x, float y, float r, int col)
        {
            Action<float, float, float, float, int> PlotCircleFill = delegate (float _x, float _y, float _xc, float _yc, int _col)
            {
                line(_x + _xc, _y + _yc, _x + _xc, _yc, _col);
                line(-_x + _xc, _y + _yc, -_x + _xc, _yc, _col);
                line(_x + _xc, -_y + _yc, _x + _xc, _yc, _col);
                line(-_x + _xc, -_y + _yc, -_x + _xc, _yc, _col);
                line(_y + _xc, _x + _yc, _y + _xc, _yc, _col);
                line(_y + _xc, -_x + _yc, _y + _xc, _yc, _col);
                line(-_y + _xc, _x + _yc, -_y + _xc, _yc, _col);
                line(-_y + _xc, -_x + _yc, -_y + _xc, _yc, _col);
            };

            circ(x, y, r, col, PlotCircleFill);
        }

        public void circ(float x, float y, float r, int col)
        {
            // Implementation based on http://www.pracspedia.com/CG/bresenhamcircle.html
            // http://darshangajara.wordpress.com/
            //

            Action<float, float, float, float, int> PlotCirclePoint = delegate (float _x, float _y, float _xc, float _yc, int _col)
            {
                pset(_x + _xc, _y + _yc, _col);
                pset(-_x + _xc, _y + _yc, _col);
                pset(_x + _xc, -_y + _yc, _col);
                pset(-_x + _xc, -_y + _yc, _col);
                pset(_y + _xc, _x + _yc, _col);
                pset(_y + _xc, -_x + _yc, _col);
                pset(-_y + _xc, _x + _yc, _col);
                pset(-_y + _xc, -_x + _yc, _col);
            };

            circ(x, y, r, col, PlotCirclePoint);
        }

        public void rect(float x0, float y0, float x1, float y1, int col)
        {
            line(x0, y0, x1, y0, col);
            line(x1, y0, x1, y1, col);
            line(x1, y1, x0, y1, col);
            line(x0, y1, x0, y0, col);
        }

        public void rectfill(float x0, float y0, float x1, float y1, int col)
        {
            // Ensure x0 is on the left of x1
            if (x0 > x1)
            {
                float temp = x0;
                x0 = x1;
                x1 = temp;
            }

            // Cast to int to match Pico8 behaviour where 0.5 -> 1.4 will draw 2 pixels.
            // Without rounding down the while loop would exit after first line (1.5 > 1.4)
            while ((int)x0 <= (int)x1)
            {
                line(x0, y0, x0, y1, col);
                x0++;
            }
        }

        public void print(string str, float x, float y, int col)
        {
            if (str == null)
            {
                return;
            }

            //str = str.ToLower();
            char[] chars = str.ToCharArray();
            int curx = 0;

            for (int i = 0; i < chars.Length; i++)
            {
                //sspr_internal(FontData, Font.Width, (chars[i] * 8) % Font.Width, (chars[i] * 8) / Font.Width * 8, 8, 8, x + (i * 4), y);

                char chr = chars[i];
                int xinc = 4;

                if (chr >= 'A' && chr <= 'Z')
                {
                    // Jump to special characters.
                    chr += (char)63;
                    xinc = 8;
                }
                else if (chr < 0 || chr > 255)
                {
                    continue;
                }

                int sx = (chr * 8) % Font.Width;
                int sy = (chr * 8) / Font.Width * 8;
                float dx = x + curx;
                float dy = y;
                float sw = 8;
                float sh = 8;
                float dw = 8;
                float dh = 8;

                // Loop through the destination, stepping by source/dest size.
                for (int _y = 0; _y < dh; _y++)
                {
                    for (int _x = 0; _x < dw; _x++)
                    {
                        int sourcex = sx + (int)Math.Floor(((float)sw / (float)dw) * (float)_x);
                        int sourcey = sy + (int)Math.Floor(((float)sh / (float)dh) * (float)_y);

                        Color c = FontData[sourcex + (sourcey * Font.Width)];

                        if (c != Color.Black)
                        {
                            pset(dx + _x, dy + _y, col);
                        }
                    }
                }

                curx += xinc;
            }
        }

        public void camera(float x, float y)
        {
            CurDrawState.CameraPos = new Vector2(x, y);
        }

        public void map(int celx, int cely, float sx, float sy, int celw, int celh, int layer = 0, int map_layer = 0)
        {
            // TODO: Support sprite flag layers

            if (MapData == null || map_layer >= MapData.Count)
            {
                return;
            }

            celx = (int)mid(0, celx, MapSize.X);
            cely = (int)mid(0, cely, MapSize.Y);

            celw = (int)mid(0, celw, MapSize.X - 1);
            celh = (int)mid(0, celh, MapSize.Y - 1);

            int start_x = (int)mid(0, flr(CurDrawState.CameraPos.X / 8), MapSize.X - 1);
            int start_y = (int)mid(0, flr(CurDrawState.CameraPos.Y / 8), MapSize.Y - 1);

            int end_x = start_x + (celw - start_x);
            end_x = (int)mid(0, end_x, min((start_x - (sx/8)) + (Resolution.X/8.0f) + 1, MapSize.X - 1));

            int end_y = start_y + (celh - start_y);
            end_y = (int)mid(0, end_y, min((start_y - (sy/8)) + (Resolution.Y / 8.0f) + 1, MapSize.Y - 1));

            // TODO: It's weird that I did <= here. It makes the calculations above
            // slightly funny I think (adding -1 etc).
            for (int i = start_x; i <= end_x; i++)
            {
                for (int j = start_y; j <= end_y; j++)
                {
                    int index = (celx + i) + ((cely + j) * MapSize.X); // TODO: Support larger maps.
                    var cell = MapData[map_layer][index];

                    // Don't draw tile 0 for maps.
                    if (cell.ID != 0)
                    {
                        var dx = sx + (i * 8);
                        var dy = sy + (j * 8);
                        bset(cell.Bank);
                        spr(cell.ID, dx, dy, 1, 1, cell.FlipX, cell.FlipY);
                    }
                }
            }
        }

        public void set_render_scale(float new_scale = 1.0f)
        {
            RenderScale = new_scale;
        }

        // AUDIO
        //

        public void sfx(int sfx, int channel = 0, int offset = 0)
        {
            // TODO: Support channel and offset.
            // TODO: Support -1 stopping the sound.
            if (SoundEffects.ContainsKey(sfx))
            {
                SoundEffects[sfx].Play();
            }
        }

        public void music(int song, int fadems = 0, int channelmask = 0)
        {
            // TODO: Should probably use Song rather than SoundEffect.
            // TODO: Support fadems and channelmask.

            // Stop all songs.
            foreach (var Song in Music)
            {
                Song.Value.Stop();
            }

            // In the case of song == -1, then all music is stopped and no new
            // music will be started.
            if (Music.ContainsKey(song))
            {
                Music[song].Play();
            }
        }

        // INPUT
        //

        public bool btn(int id, int player = 0)
        {
// This doesn't work because the "back" event is delayed more than a frame, so B button information isn't
// available. Instead, going to remap "btn 7" on UWP.
/*
#if WINDOWS_UWP
            // On UWP the back button is triggered by the RELEASE of the B button... We want to ignore that,
            // so when checking if the BACK button is being pressed, we ensure that the B button wasn't released
            // this frame.
            if (id == 7)
            {
                bool DownLastFrame = LastGamePadState[player].IsButtonDown(ButtonMap[player][5].Item1);
                bool UpThisFrame = GamePad.GetState((PlayerIndex)player).IsButtonUp(ButtonMap[player][5].Item1);
                if (DownLastFrame && UpThisFrame)
                {
                    return false;
                }
            }
#endif
*/
            if (GamePad.GetState((PlayerIndex)player).IsButtonDown(ButtonMap[player][id].Item1))
            {
                return true;
            }
            else if (Keyboard.GetState().IsKeyDown(ButtonMap[player][id].Item2))
            {
                return true;
            }
            return false;
        }

        public bool btnp(int id, int player = 0)
        {
            // TODO: This should really be checking keyboard and gamepad independently, such that
            // while holding the left arrow on the keyboard, I should be able to tap left d-pad 
            // and have btnp(0) return true.
            // Holding on one device should not negate a tap on another.

            // Make sure it is being pressed first.
            if (!btn(id, player))
            {
                return false;
            }

            // Now ensure that it WASN'T pressed last frame.
            if ((!LastGamePadState[player].IsButtonDown(ButtonMap[player][id].Item1)) && (!LastKeyboardState.IsKeyDown(ButtonMap[player][id].Item2)))
            {
                return true;
            }

            return false;
        }

        public bool btnr(int id, int player = 0)
        {
            // TODO: This should really be checking keyboard and gamepad independently, such that
            // while holding the left arrow on the keyboard, I should be able to tap left d-pad 
            // and have btnp(0) return true.
            // Holding on one device should not negate a tap on another.

            // Make sure it is no longer being pressed first.
            if (btn(id, player))
            {
                return false;
            }

            // Now ensure that it WASN'T pressed last frame.
            if ((LastGamePadState[player].IsButtonDown(ButtonMap[player][id].Item1)) || (LastKeyboardState.IsKeyDown(ButtonMap[player][id].Item2)))
            {
                return true;
            }

            return false;
        }

        // MATH
        //

        public void srand(int val)
        {
            Rand = new Random(val);
        }
        public float rnd(float max)
        {
            return (float)Rand.NextDouble() * max;
        }

        public float sin(float a)
        {
            float norm = a * MathHelper.TwoPi;
            return (float)Math.Sin(-norm);
        }

        public float cos(float a)
        {
            float norm = a * MathHelper.TwoPi;
            return (float)Math.Cos(norm);
        }

        public float min(float a, float b)
        {
            return Math.Min(a, b);
        }

        public float max(float a, float b)
        {
            return Math.Max(a, b);
        }

        public float mid(float a, float b, float c)
        {
            return Math.Min(Math.Max(a, b), c);
        }

        public float sqrt(float a)
        {
            return (float)Math.Sqrt(a);
        }

        public int flr(float a)
        {
            return (int)Math.Floor(a);
        }

        public float abs(float a)
        {
            return (float)Math.Abs(a);
        }

        // FILE IO
        //

        public void printh(string str, string filename = "", bool overwrite = false)
        {
            // TODO: Support writing to a file.
#if WINDOWS_UWP
            Debug.WriteLine(str);
#else
            Console.WriteLine(str);
#endif
        }

        public int dget(uint index)
        {
            int val = 0;

            if (!string.IsNullOrEmpty(CartDataName) && index < 64)
            {
#if WINDOWS_UWP
                // https://docs.microsoft.com/en-us/windows/uwp/design/app-settings/store-and-retrieve-app-data
                //                ulong temp = Windows.Storage.ApplicationData.Current.RoamingStorageQuota;
                Windows.Storage.ApplicationDataContainer Settings = Windows.Storage.ApplicationData.Current.RoamingSettings;
                //Windows.Storage.StorageFolder folder =Windows.Storage.ApplicationData.Current.RoamingFolder;
                //                Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                //                Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;


                Windows.Storage.ApplicationDataCompositeValue Composite = (Windows.Storage.ApplicationDataCompositeValue)Settings.Values[CartDataName];

                // TODO: Do I need to make sure object is of type int?
                if (Composite != null && Composite.ContainsKey(index.ToString()))
                {
                    val = (int)(Composite[index.ToString()]);
                }
#else
                return CartData[index];
#endif
            }
            return val;
        }

        public void dset(uint index, int value)
        {
            if (!string.IsNullOrEmpty(CartDataName) && index < 64)
            {
#if WINDOWS_UWP
                // https://docs.microsoft.com/en-us/windows/uwp/design/app-settings/store-and-retrieve-app-data
                //                ulong temp = Windows.Storage.ApplicationData.Current.RoamingStorageQuota;
                Windows.Storage.ApplicationDataContainer Settings = Windows.Storage.ApplicationData.Current.RoamingSettings;
                //Windows.Storage.StorageFolder folder = Windows.Storage.ApplicationData.Current.RoamingFolder;
                //                Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                //                Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

                Windows.Storage.ApplicationDataCompositeValue Composite = (Windows.Storage.ApplicationDataCompositeValue)Settings.Values[CartDataName];
                if (Composite == null)
                {
                    Composite = new Windows.Storage.ApplicationDataCompositeValue();
                }
                Composite[index.ToString()] = value;
                
                // TODO: Merge
                Settings.Values[CartDataName] = Composite;
#else
                CartData[index] = value;

                // TODO: Could this be done on exit or something, to avoid hitting disc 
                //       every time dset is called?
                writecartdata();
#endif
            }
        }

        private void writecartdata()
        {
            using (StreamWriter fs = File.CreateText(CartDataName + ".sav"))
            {
                for (int i = 0; i < CartData.Length; i++)
                {
                    fs.Write(CartData[i].ToString() + " ");
                }
            }
        }

        public void cartdata(string id)
        {
            CartDataName = id;

            CartData = new int[64];

            // When calling this will a new cart name, we should create a 
            // default save (all zeros)
            if (!File.Exists(CartDataName + ".sav"))
            {
                // Write out our zero'd out array.
                writecartdata();
            }

            using (StreamReader sr = File.OpenText(CartDataName + ".sav"))
            {
                string SaveBlock = sr.ReadToEnd();

                if (!String.IsNullOrWhiteSpace(SaveBlock))
                {
                    // For now just assuming each entry is on the same line, delimited
                    // by spaces.
                    string[] Values = SaveBlock.Split(' ');
                    for (int i = 0; i < CartData.Length; i++)
                    {
                        if (Values.Length > i)
                        {
                            CartData[i] = Int32.Parse(Values[i]);
                        }
                    }
                }
            }
        }

        public void reloadmap(string MapString)
        {
            // Were we passed a tmx file path?
            if (string.Compare(Path.GetExtension(MapString), ".tmx", true) == 0)
            {
                // In case this is not the first map to be loaded.
                SpriteFlags.Clear();

                var TmxMapData = new TmxMap(MapString);
                MapSize.X = TmxMapData.Width;
                MapSize.Y = TmxMapData.Height;

                MapData = new List<TileData[]>();

                List<Tuple<int, int>> TileSetInfo = new List<Tuple<int, int>>();

                // Load in the bank data and starting tile id for each tile set.
                foreach (var TileSet in TmxMapData.Tilesets)
                {
                    int Bank = 0;
                    string BankString = "";
                    if (TileSet.Properties.TryGetValue("Bank", out BankString))
                    {
                        Bank = int.Parse(BankString);
                    }
                    TileSetInfo.Add(new Tuple<int, int>(TileSet.FirstGid, Bank));

                    // Update the bank data so that we can reverse look up this info down the road.
                    SheetDataBanks[Bank].FirstGID = TileSet.FirstGid;
                }

                // Put them in reverse order to make algorithm below simpler.
                TileSetInfo.Sort(delegate (Tuple<int, int> x, Tuple<int, int> y)
                {
                    return (x.Item1 > y.Item1) ? -1 : 1;
                });

                for (int Layer = 0; Layer < TmxMapData.Layers.Count; Layer++)
                {
                    MapData.Add(new TileData[TmxMapData.Width * TmxMapData.Height]);

                    // Add a layer so things line up, but leave it empty if it isn't visible.
                    if (TmxMapData.Layers[Layer].Visible)
                    {
                        // Assumes that all layers must have the same number of tiles.
                        for (int i = 0; i < TmxMapData.Layers[0].Tiles.Count; i++)
                        {
                            TmxLayerTile tile = (TmxMapData.Layers[Layer].Tiles[i]);
                            Debug.Assert(!tile.DiagonalFlip);

                            if (tile.Gid == 0)
                            {
                                continue;
                            }

                            // Search through tilesets looking for the bank that this tile 
                            // belongs to so that it can be stored for later.
                            for (int j = 0; j < TileSetInfo.Count; j++)
                            {
                                // We are working with tileset info being reversed, so as soon
                                // as a Gid is greater than the starting Gid for a set, it must
                                // be in it.
                                if (tile.Gid >= TileSetInfo[j].Item1)
                                {
                                    //MapData[Layer][i].BankOffset = TileSetInfo[j].Item1;
                                    MapData[Layer][i].Bank = TileSetInfo[j].Item2;
                                    break;
                                }
                            }
                            // Make the ID 0 based.
                            MapData[Layer][i].ID = (Math.Max(tile.Gid - SheetDataBanks[MapData[Layer][i].Bank].FirstGID, 0));
                            MapData[Layer][i].FlipX = tile.HorizontalFlip;
                            MapData[Layer][i].FlipY = tile.VerticalFlip;
                        }
                    }
                }
                foreach (var TileSet in TmxMapData.Tilesets)
                {
                    foreach (var Tile in TileSet.Tiles)
                    {
#if WINDOWS_UWP
                    if (Tile.Value.Properties.ContainsKey("Flag"))
                    {
                        SpriteFlags.Add(Tile.Value.Id + TileSet.FirstGid - 1, Convert.ToByte(Tile.Value.Properties["Flag"]));
                    }
#else
                        if (Tile.Properties.ContainsKey("Flag"))
                        {
                            SpriteFlags.Add(Tile.Id + TileSet.FirstGid - 1, Convert.ToByte(Tile.Properties["Flag"]));
                        }
#endif
                    }
                }
            }
            else
            {
                // NO LONGER SUPPORTING PASSING IN RAW PICO-8 MAP DATA. USE TMX INSTEAD.
                //
                // Assuming a preprocessed string.
                // MapData = StringToByteArray(MapString);
            }
        }

        public void menuitem(int index, string label, Action callback)
        {
            // pico-8 uses indices 1-5 (not 0 based).
            if (index > 0 && index <= 5)
            {
                AdditionalPauseMenuEntries[index - 1] = new Tuple<string, Action>(label, callback);
            }
        }

        public void menuitem(int index)
        {
            // pico-8 uses indices 1-5 (not 0 based).
            if (index > 0 && index <= 5)
            {
                AdditionalPauseMenuEntries.Remove(index - 1);
            }
        }
    }
}