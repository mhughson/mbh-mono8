﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiledSharp;
using static Mono8Samples.TopDown.TopDown;

namespace Mono8Samples.TopDown
{
    /// <summary>
    /// Manages all the areas in the game.
    /// </summary>
    public class world : sub
    {
        public area cur_area;

        protected Dictionary<string, area> areas;

        // A special area where UI content lives.
        public area ui_area;

        public enum alert_states
        {
            calm,
            low,
            high,
        }

        protected int ticks_in_state;
        protected int ticks_since_last_state_request;
        protected int ticks_to_calm = 120;
        protected int ticks_to_low = 120;
        protected alert_states global_alert_state;

        public world(PicoXGame owner) : base(owner)
        {
            areas = new Dictionary<string, area>();
            cur_area = new area(0,0, Game);
            ui_area = new area(0, 0, Game);
            ticks_since_last_state_request = int.MaxValue;
            set_global_alert_state(alert_states.calm);
        }

        public void clear_loaded_areas()
        {
            areas = new Dictionary<string, area>();
        }

        public void parse_areas(string map_string)
        {
            string MapString = map_string.ToLower();
            var TmxMapData = new TmxMap(MapString);

            // Notes on templates and objects in Tiled:
            // This Tiled Runtime library does not seem to support templates (I checked latest in github).
            // As such, I can't use Templates but I can use something called "Object Types Editor". This
            if (TmxMapData != null)
            {
                area new_area = new area(TmxMapData.Width, TmxMapData.Height, Game);

                // Add now so that recursive calls detect that it is already be processed.
                areas.Add(MapString, new_area);

                if (TmxMapData.Properties.ContainsKey("light_status"))
                {
                    string light_status = TmxMapData.Properties["light_status"];
                    area.lighting_status l = (area.lighting_status)Enum.Parse(typeof(area.lighting_status), light_status);
                    new_area.light_status = l;
                }

                if (TmxMapData.Properties.ContainsKey("game_mode"))
                {
                    string game_mode = TmxMapData.Properties["game_mode"];
                    area.game_modes g = (area.game_modes)Enum.Parse(typeof(area.game_modes), game_mode);
                    new_area.game_mode = g;
                }

                foreach (var group in TmxMapData.ObjectGroups)
                {
                    foreach (var o in group.Objects)
                    {
                        if (string.Compare(o.Type, "spawn_point", true) == 0)
                        {
                            new_area.spawn_point = new Vector2((float)o.X, (float)o.Y);
                            
                            // Spawn points are inferred to be a Point of Interest.
                            new_area.poi.Add(new obj(Game, new_area) { x = (float)o.X, y = (float)o.Y, display_name = o.Name } );
                        }
                        else if (string.Compare(o.Type, "door", true) == 0)
                        {
                            string dest_map;
                            o.Properties.TryGetValue("dest_name", out dest_map);

                            string dest_poi;
                            o.Properties.TryGetValue("dest_poi", out dest_poi);

                            door d = new door((float)o.X + (float)(o.Width * 0.5f), (float)o.Y + (float)(o.Height * 0.5f), (int)o.Width, (int)o.Height,
                                dest_map, dest_poi, Game, new_area)
                            {
                                display_name = o.Name,
                            };

                            string trans_dir_x;
                            if (o.Properties.TryGetValue("trans_dir_x", out trans_dir_x))
                            {
                                d.trans_dir_x = int.Parse(trans_dir_x);
                            }

                            string trans_dir_y;
                            if (o.Properties.TryGetValue("trans_dir_y", out trans_dir_y))
                            {
                                d.trans_dir_y = int.Parse(trans_dir_y);
                            }


                            // Doors are inferred to be a Point of Interest.
                            new_area.poi.Add(d);

                            // RECURSIVE SEARCH TO THE NEXT AREA
                            if (!areas.ContainsKey(d.dest_path.ToLower()))
                            {
                                parse_areas(d.dest_path);
                            }
                        }
                        else if (string.Compare(o.Type, "cam_area", true) == 0)
                        {
                            float width_half = Game.min((float)o.Width * 0.5f, 64.0f);
                            float height_half = Game.min((float)o.Height * 0.5f, 64.0f);
                            new_area.views.Add(new area.view_area()
                            {
                                pos_min = new Vector2((float)o.X, (float)o.Y),
                                pos_max = new Vector2((float)o.X + (float)o.Width, (float)o.Y + (float)o.Height),

                                width = (float)o.Width,
                                height = (float)o.Height,
                            });

                            string pull_threshold;
                            if (o.Properties.TryGetValue("pull_threshold", out pull_threshold))
                            {
                                new_area.pull_threshold = int.Parse(pull_threshold);
                            }

                            int pull_threshold_offset_x = 0;
                            int pull_threshold_offset_y = 0;
                            bool set_offset = false;

                            string _pull_threshold_offset_x;
                            if (o.Properties.TryGetValue("pull_threshold_offset_x", out _pull_threshold_offset_x))
                            {
                                pull_threshold_offset_x = int.Parse(_pull_threshold_offset_x);
                                set_offset = true;
                            }

                            string _pull_threshold_offset_y;
                            if (o.Properties.TryGetValue("pull_threshold_offset_y", out _pull_threshold_offset_y))
                            {
                                pull_threshold_offset_y = int.Parse(_pull_threshold_offset_y);
                                set_offset = true;
                            }

                            if (set_offset)
                            {
                                new_area.pull_threshold_offset = new Vector2(pull_threshold_offset_x, pull_threshold_offset_y);
                            }


                            string allow_backtrack_y;
                            if (o.Properties.TryGetValue("allow_backtrack_y", out allow_backtrack_y))
                            {
                                new_area.allow_backtrack_y = bool.Parse(allow_backtrack_y);
                            }
                        }
                        else if (string.Compare(o.Type, "simple_ai", true) == 0)
                        {
                            string initial_state_string;
                            simple_ai.states inital_state = simple_ai.states.patrol;
                            if (o.Properties.TryGetValue("initial_state", out initial_state_string))
                            {
                                inital_state = (simple_ai.states)Enum.Parse(typeof(simple_ai.states), initial_state_string);
                            }

                            simple_ai ai = new simple_ai(inital_state, Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y - (float)o.Height / 2.0f, // tiles are bottom aligned
                            };
                            string fx_string;
                            if (o.Properties.TryGetValue("fx", out fx_string))
                            {
                                ai.fx = int.Parse(fx_string);
                            }
                            string fy_string;
                            if (o.Properties.TryGetValue("fy", out fy_string))
                            {
                                ai.fy = int.Parse(fy_string);
                            }

                            ai.set_col_box_pos(ai.x, ai.y);
                            weap pistol = weap.create(weap.weap_type.pistol, 0, 0, Game, new_area);
                            pistol.pick_up(ai);

                            string path_name;
                            o.Properties.TryGetValue("path_name", out path_name);
                            ai.path_name = path_name;
                            
                            string can_cloak;
                            if (o.Properties.TryGetValue("can_cloak", out can_cloak))
                            {
                                ai.can_cloak = bool.Parse(can_cloak);
                            }
                            
                        }
                        else if (string.Compare(o.Type, "dog_ai", true) == 0)
                        {
                            dog_ai ai = new dog_ai(Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y - (float)o.Height / 2.0f, // tiles are bottom aligned
                            };
                            weap bite = weap.create(weap.weap_type.melee, 0, 0, Game, new_area);
                            bite.pick_up(ai);

                            string path_name;
                            o.Properties.TryGetValue("path_name", out path_name);
                            ai.path_name = path_name;
                        }
                        else if (string.Compare(o.Type, "npc_ai", true) == 0)
                        {
                            npc_ai ai = new npc_ai(Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y - (float)o.Height / 2.0f, // tiles are bottom aligned
                            };
                            ai.set_col_box_pos(ai.x, ai.y);
                        }
                        else if (string.Compare(o.Type, "pattern_ai", true) == 0)
                        {
                            string type;
                            o.Properties.TryGetValue("pattern_type", out type);
                            pattern_ai.pattern_types t = (pattern_ai.pattern_types)Enum.Parse(typeof(pattern_ai.pattern_types), type);

                            pattern_ai ai = new pattern_ai(t, Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y + (float)o.Height / 2.0f, // now using shape instead of tiles.
                                no_clip = true,
                            };
                            ai.set_col_box_pos(ai.x, ai.y);
                            weap gun = weap.create(weap.weap_type.machine_gun, 0, 0, Game, new_area);
                            gun.pick_up(ai);
                            gun.bullet_speed *= 0.25f; // 0.125f; or 0.25f


                            //string path_name;
                            //o.Properties.TryGetValue("path_name", out path_name);
                            //ai.path_name = path_name;

                            //string can_cloak;
                            //if (o.Properties.TryGetValue("can_cloak", out can_cloak))
                            //{
                            //    ai.can_cloak = bool.Parse(can_cloak);
                            //}
                        }
                        else if (string.Compare(o.Type, "hummer_ai", true) == 0)
                        {
                            hummer_ai g = new hummer_ai(Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y + (float)o.Height / 2.0f,
                            };

                            // TODO: Can this be done at a higher level (eg. after each object is read, apply this logic)?
                            if (o.ObjectType == TmxObjectType.Tile)
                            {
                                g.y -= (float)o.Height;
                            }
                        }
                        else if (string.Compare(o.Type, "path", true) == 0)
                        {
                            if (o.ObjectType == TmxObjectType.Polyline)
                            {
                                path new_path = new path();

                                new_path.name = o.Name;

                                foreach (var p in o.Points)
                                {
                                    path_point pp = new path_point();
                                    pp.position = new Vector2((float)p.X + (float)o.X, (float)p.Y + (float)o.Y);
                                    string fx;
                                    if (o.Properties.TryGetValue("fx", out fx))
                                    {
                                        pp.fx = int.Parse(fx);
                                    }
                                    string fy;
                                    if (o.Properties.TryGetValue("fy", out fy))
                                    {
                                        pp.fy = int.Parse(fy);
                                    }
                                    new_path.points.Add(pp);
                                }

                                // Assume that the path was looped, causing a duplicate at the first and last entry of the list.
                                new_path.points.RemoveAt(new_path.points.Count - 1);

                                new_area.paths.Add(new_path);
                            }
                        }
                        else if (string.Compare(o.Type, "weap", true) == 0)
                        {
                            string type;
                            o.Properties.TryGetValue("type", out type);
                            weap.weap_type t = (weap.weap_type) Enum.Parse(typeof(weap.weap_type), type);
                            weap.create(t, (float)o.X + (float)(o.Width * 0.5f), (float)o.Y - (float)(o.Height * 0.5f), Game, new_area); // tiles are bottom aligned.
                        }
                        else if (string.Compare(o.Type, "gear", true) == 0)
                        {
                            string type;
                            o.Properties.TryGetValue("type", out type);
                            gear.gear_types t = (gear.gear_types)Enum.Parse(typeof(gear.gear_types), type);

                            if (t == gear.gear_types.key_card)
                            {
                                int security_level_value = 0;
                                string security_level;
                                if (o.Properties.TryGetValue("security_level", out security_level))
                                {
                                    security_level_value = int.Parse(security_level);
                                }
                                new gear_keycard(t, (float)o.X + (float)(o.Width * 0.5f), (float)o.Y - (float)(o.Height * 0.5f), security_level_value, Game, new_area); // tiles are bottom aligned.
                            }
                            else
                            {
                                new gear(t, (float)o.X + (float)(o.Width * 0.5f), (float)o.Y - (float)(o.Height * 0.5f), Game, new_area); // tiles are bottom aligned.
                            }
                        }
                        else if (string.Compare(o.Type, "col", true) == 0)
                        {
                            area.collision_flag flag = area.collision_flag.all;
                            if (o.Properties.ContainsKey("collision_flag"))
                            {
                                int f = Convert.ToInt32(o.Properties["collision_flag"]);
                                if (f != 0)
                                {
                                    flag = (area.collision_flag)f;
                                }
                            }
                            int start_x = Game.flr((float)(o.X / 8.0));
                            int start_y = Game.flr((float)(o.Y / 8.0));
                            int end_x = start_x + Game.flr((float)(o.Width / 8.0));
                            int end_y = start_y + Game.flr((float)(o.Height / 8.0));

                            for (int y = start_y; y < end_y; y++)
                            {
                                for (int x = start_x; x < end_x; x++)
                                {
                                    if (x >= 0 && y >= 0 && x < TmxMapData.Width && y < TmxMapData.Height)
                                    {
                                        new_area.col_data[(x % TmxMapData.Width) + (y * TmxMapData.Width)].flag = flag;
                                    }
                                }
                            }
                        }
                        else if (string.Compare(o.Type, "gate_top", true) == 0)
                        {
                            int security_level_value = 0;
                            string security_level;
                            if (o.Properties.TryGetValue("security_level", out security_level))
                            {
                                security_level_value = int.Parse(security_level);
                            }

                            string door_type_num_string;
                            if (!o.Properties.TryGetValue("door_type_num", out door_type_num_string))
                            {
                                door_type_num_string = "01";
                            }

                            gate_top g = new gate_top(door_type_num_string, Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y, // tiles are bottom aligned
                                security_level = security_level_value,
                                id_name = o.Name,
                            };

                        }
                        else if (string.Compare(o.Type, "gate_side", true) == 0)
                        {
                            int security_level_value = 0;
                            string security_level;
                            if (o.Properties.TryGetValue("security_level", out security_level))
                            {
                                security_level_value = int.Parse(security_level);
                            }

                            gate_side g = new gate_side(Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y + 4, // tiles are bottom aligned
                                security_level = security_level_value,
                                id_name = o.Name,
                            };

                        }
                        else if (string.Compare(o.Type, "vent_top", true) == 0)
                        {
                            int security_level_value = 0;
                            string security_level;
                            if (o.Properties.TryGetValue("security_level", out security_level))
                            {
                                security_level_value = int.Parse(security_level);
                            }

                            string door_type_num_string;
                            if (!o.Properties.TryGetValue("door_type_num", out door_type_num_string))
                            {
                                door_type_num_string = "01";
                            }

                            vent_top g = new vent_top(door_type_num_string, Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y - 4, // tiles are bottom aligned
                                security_level = security_level_value,
                            };

                        }
                        else if (string.Compare(o.Type, "destructable", true) == 0)
                        {
                            destructable g = new destructable(Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y + (float)o.Height / 2.0f,
                            };

                            // TODO: Can this be done at a higher level (eg. after each object is read, apply this logic)?
                            if (o.ObjectType == TmxObjectType.Tile)
                            {
                                g.y -= (float)o.Height;
                            }
                        }
                        else if (string.Compare(o.Type, "switch_control", true) == 0)
                        {
                            switch_control g = new switch_control(Game, new_area)
                            {
                                x = (float)o.X + (float)o.Width / 2.0f,
                                y = (float)o.Y - (float)o.Height / 2.0f, // tiles are bottom aligned
                            };

                            string type;
                            o.Properties.TryGetValue("type", out type);
                            switch_control.switch_types t = (switch_control.switch_types)Enum.Parse(typeof(switch_control.switch_types), type);
                            g.switch_type = t;

                            string targets;
                            if (o.Properties.TryGetValue("targets", out targets))
                            {
                                targets = targets.Replace(" ", "");
                                string[] target_list = targets.Split(',');

                                g.targets = target_list.ToList();
                            }

                        }
                        else if (string.Compare(o.Type, "laser", true) == 0)
                        {
                            if (o.ObjectType == TmxObjectType.Polyline)
                            {
                                laser new_laser = new laser(Game, new_area);
                                new_laser.x = (float)o.X + (float)o.Points[0].X;
                                new_laser.y = (float)o.Y + (float)o.Points[0].Y;
                                new_laser.end_point.X = (float)o.X + (float)o.Points[1].X;
                                new_laser.end_point.Y = (float)o.Y + (float)o.Points[1].Y;
                                
                                // Poly lines move in increments of 4, so a line across a single tile,
                                // actually ends in the first pixel of the next tile. This is to correct
                                // for that, but only in the case where the line actually has a length>0.
                                if (new_laser.end_point.X != new_laser.x)
                                    new_laser.end_point.X -= 1.0f;
                                if (new_laser.end_point.Y != new_laser.y)
                                    new_laser.end_point.Y -= 1.0f;

                                // TODO: Allow for lasers of different orientations.
                                new_laser.w = (int)Game.abs(new_laser.end_point.X - new_laser.x);

                                new_laser.id_name = o.Name;
                            }
                        }
                    }
                }
                
                foreach (simple_ai o in new_area.objs_queue.OfType<simple_ai>())
                {
                    if (!string.IsNullOrWhiteSpace(o.path_name))
                    {
                        foreach (var p in new_area.paths)
                        {
                            if (string.Compare(p.name, o.path_name, true) == 0)
                            {
                                o.set_path(p);
                            }
                        }
                    }
                }
            }
        }

        public area parse_and_get_area(string area_name)
        {
            area_name = area_name.ToLower();

            // Handle the case where this area was not found in a link from the initial map.
            if (!areas.ContainsKey(area_name))
            {
                parse_areas(area_name);
            }

            return areas[area_name];
        }

        public void transition_to_area(string area_name)
        {
            area found;
            area_name = area_name.ToLower();

            // Handle the case where this area was not found in a link from the initial map.
            if (!areas.ContainsKey(area_name))
            {
                parse_areas(area_name);
            }

            if (areas.TryGetValue(area_name, out found))
            {
                // TODO: Not sure if it makes sense to put anything back into the active list
                //       right away, since it may be far off camera in the new area.
                //
                for (int i = cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    obj o = cur_area.objs_active[i];
                    if (o.is_persistent)
                    {
                        cur_area.objs_active.Remove(o);
                        found.objs_active.Add(o);
                        o.owning_area = found;
                    }
                }
                for (int i = cur_area.objs_queue.Count - 1; i >= 0; i--)
                {
                    obj o = cur_area.objs_queue[i];
                    if (o.is_persistent)
                    {
                        cur_area.objs_queue.Remove(o);
                        found.objs_queue.Add(o);
                        o.owning_area = found;
                    }
                }

                cur_area = found;

                // MOVE PLAYER HERE
                // We need to move the player before the load happens, because the post_load function
                // in door needs to check if the player is inside the trigger volume.
                if (Game.queued_spawn_point.HasValue) // Did the load request include a spawn point?
                {
                    player p = Game.game_world.get_player();
                    p.set_col_box_pos(Game.queued_spawn_point.Value.X, Game.queued_spawn_point.Value.Y);
                }
                else if (cur_area.spawn_point != null) // Does the map have a default spawn point?
                {
                    {
                        player p = Game.game_world.get_player();
                        p?.set_col_box_pos(cur_area.spawn_point.X, cur_area.spawn_point.Y);
                    }

                    // For player_fight support.
                    //{
                    //    foreach (obj p in cur_area.objs_active)
                    //    {
                    //        if ((p.type_flag & type_flags.Player) == type_flags.Player)
                    //        {
                    //            p.set_col_box_pos(cur_area.spawn_point.X, cur_area.spawn_point.Y);
                    //        }
                    //    }
                    //}
                }

                // We just loaded an area and all its views, so do an initial updating of the current
                // view, prior to trying to set it.
                cur_area.update_cur_view();
                if (cur_area.cur_view != null)
                {
                    Game.game_cam.pos_min = cur_area.cur_view.pos_min;
                    Game.game_cam.pos_max = cur_area.cur_view.pos_max;
                    Game.game_cam.pull_threshold = cur_area.pull_threshold;
                    Game.game_cam.pull_threshold_offset = cur_area.pull_threshold_offset;
                    Game.game_cam.allow_backtrack_y = cur_area.allow_backtrack_y;
                }
                // Now that all the view information is loaded, jump to the player.
                Game.game_cam.jump_to_target();

                foreach (obj o in cur_area.objs_queue)
                {
                    o.post_load();
                }
                foreach (obj o in cur_area.objs_active)
                {
                    o.post_load();
                }

                switch(cur_area.game_mode)
                {
                    case area.game_modes.action:
                        {
                            player p = get_player();
                            if (p != null)
                            {
                                p.run_speed = 0.5f;
                            }
                            break;
                        }

                    case area.game_modes.stealth:
                        {
                            player p = get_player();
                            if (p != null)
                            {
                                p.run_speed = 0.75f;
                            }
                            break;
                        }
                }
            }
        }

        public void update(int delta_ticks = 1)
        {
            ticks_in_state = (int)Game.min(int.MaxValue, ticks_in_state + delta_ticks);
            ticks_since_last_state_request = (int)Game.min(int.MaxValue, ticks_since_last_state_request + delta_ticks);
            
            if (cur_area.game_mode == area.game_modes.action)
            {
                // For arcade we just always stay at high.
                set_global_alert_state(alert_states.high);
            }
            else
            {
                switch (global_alert_state)
                {
                    case alert_states.low:
                        {
                            if (ticks_in_state > ticks_to_calm)
                            {
                                set_global_alert_state(alert_states.calm);
                            }
                            break;
                        }
                    case alert_states.high:
                        {
                            if (ticks_since_last_state_request > ticks_to_low)
                            {
                                set_global_alert_state(alert_states.low);
                            }
                            break;
                        }
                }
            }

            // Clean up any objects ready to be destroyed. We wait till now, so that it isn't done while
            // iterating over these lists (causing indexing problems).
            for (int i = cur_area.objs_active.Count - 1; i >= 0; i--)
            {
                if (cur_area.objs_active[i].pending_destroy)
                {
                    cur_area.objs_active.Remove(cur_area.objs_active[i]);
                }
            }

            for (int i = ui_area.objs_active.Count - 1; i >= 0; i--)
            {
                if (ui_area.objs_active[i].pending_destroy)
                {
                    ui_area.objs_active.Remove(ui_area.objs_active[i]);
                }
            }
        }

        public alert_states get_global_alert_state()
        {
            return global_alert_state;
        }

        public void set_global_alert_state(alert_states new_state)
        {
            if (new_state != global_alert_state)
            {
                global_alert_state = new_state;
                ticks_in_state = 0;
            }
            ticks_since_last_state_request = 0;
        }

        internal void draw_alert_state()
        {
            if (cur_area.game_mode == area.game_modes.action)
            {
                return;
            }

            string state = "";
            string state_detail = "";
            int col_fg = 0;
            int col_bg = 31;

            switch (global_alert_state)
            {
                case alert_states.calm:
                    {
                        state += "calm";
                        break;
                    }

                case alert_states.low:
                    {
                        state += "low " + (ticks_to_calm - ticks_in_state);
                        state_detail += ticks_to_calm - ticks_in_state;
                        col_fg = 16;
                        break;
                    }

                case alert_states.high:
                    {
                        state += (char)Game.rnd(64) + "high" + (char)Game.rnd(64);
                        state_detail += (char)Game.rnd(64) + "" + (char)Game.rnd(64); //ticks_to_low - ticks_since_last_state_request;
                        col_fg = 15;
                        break;
                    }
            }

            Game.printc(state, 144, 6, col_fg, col_bg, 0);
            //Game.printc(state_detail, 144, 12, col_fg, col_bg, 0);
        }

        public player get_player()
        {
            foreach(obj p in cur_area.objs_active)
            {
                if ((p.type_flag & type_flags.Player) == type_flags.Player)
                {
                    return p as player;
                }
            }

            return null;
        }
    }
}
