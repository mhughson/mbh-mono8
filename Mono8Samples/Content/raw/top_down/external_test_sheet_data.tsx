<?xml version="1.0" encoding="UTF-8"?>
<tileset name="top_down_map_sheet" tilewidth="8" tileheight="8" tilecount="3328" columns="16">
 <image source="top_down_gfx_bank_0.png" trans="7cc84c" width="128" height="1664"/>
 <tile id="2">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="38">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="67">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="68">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="71">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="79">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="89">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="90">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="91">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="92">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="93">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="98">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="99">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="100">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="101">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="103">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="104">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="106">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="107">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="108">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="109">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="110">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="111">
  <properties>
   <property name="Flag" type="int" value="1"/>
  </properties>
 </tile>
</tileset>
