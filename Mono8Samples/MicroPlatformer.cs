﻿using System;
using System.Collections.Generic;
using PicoX;

namespace Mono8Samples
{
    class MicroPlatformer : PicoXGame
    {
        //player
        class Player
        {
            //position
            public float x = 72;
            public float y = 16;
            //velocity
            public float dx = 0;
            public float dy = 0;

            //is the player standing on
            //the ground. used to determine
            //if they can jump.
            public bool isgrounded = false;

            //tuning constants

            public float jumpvel = 2.4f;
        };

        Player p1;

        float grav = 0.1f; // gravity per frame

        public override void _init()
        {
            p1 = new Player();
        }

        public override void _update60()
        {
            //remember where we started
            var startx = p1.x;

            //jump 
            //

            //if on the ground and the
            //user presses x,c,or,up...
            if ((btnp(2) || btnp(4) || btnp(5)) && p1.isgrounded)
            {
                //launch the player upwards
                p1.dy = -p1.jumpvel;

            }

            //walk
            //

            p1.dx = 0;

            if (btn(0)) //left
            {
                p1.dx = -1;

            }

            if (btn(1)) //right
            {
                p1.dx = 1;

            }

            //move the player left/right
            p1.x += p1.dx;

            //hit side walls
            //

            //check for walls in the
            //direction we are moving.
            var xoffset = 0;

            if (p1.dx > 0) xoffset = 7;

            //look for a wall
            var h = mget(((int)p1.x + xoffset) / 8, ((int)p1.y + 7) / 8);

            if (fget(h, 0))
            {
                //they hit a wall so move them
                //back to their original pos.
                //it should really move them to
                //the edge of the wall but this
                //mostly works and is simpler.
                p1.x = startx;

            }

            //accumulate gravity
            p1.dy += grav;

            //fall
            p1.y += p1.dy;

            //hit floor
            //

            //check bottom center of the
            //player.
            var v = mget(((int)p1.x + 4) / 8, ((int)p1.y + 8) / 8);

            //assume they are floating 
            //until we determine otherwise
            p1.isgrounded = false;

            //only check for floors when
            //moving downward
            if (p1.dy >= 0)
            {
                //look for a solid tile
                if (fget(v, 0))
                {
                    //place p1 on top of tile
                    p1.y = flr((p1.y) / 8) * 8;
                    //halt velocity
                    p1.dy = 0;
                    //allow jumping again
                    p1.isgrounded = true;

                }
            }

            //hit ceiling
            //

            //check top center of p1
            v = mget(((int)p1.x + 4) / 8, ((int)p1.y) / 8);

            //only check for ceilings when
            //moving up
            if (p1.dy <= 0)
            {
                //look for solid tile
                if (fget(v, 0))
                {
                    //position p1 right below
                    //ceiling
                    p1.y = flr((p1.y + 8) / 8) * 8;
                    //halt upward velocity
                    p1.dy = 0;

                }
            }
        }

        public override void _draw()
        {
            cls(); //clear the screen
            map(0, 0, 0, 0, 128, 64); //draw map

            spr(1, p1.x, p1.y); //draw player

            print("v1.0 2016 - @matthughson", 14, 0, 1);
        }

        public override string GetMapString()

        {
            return "Content/raw/micro_map.tmx";
        }

        public override Dictionary<int, string> GetMusicPaths()
        {
            return new Dictionary<int, string>();
        }

        public override List<string> GetSheetPath()
        {
            return new List<string>() { "raw\\micro_sheet" };
        }

        public override Dictionary<int, string> GetSoundEffectPaths()
        {
            return new Dictionary<int, string>();
        }

        public override Dictionary<string, object> GetScriptFunctions()
        {
            return new Dictionary<string, object>();
        }

        public override string GetPalTextureString()
        {
            return "";
        }
    }
}
