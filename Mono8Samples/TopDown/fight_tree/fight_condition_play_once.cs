﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    class fight_condition_play_once : fight_condition
    {
        int play_count = 0;

        public fight_condition_play_once(PicoXGame owner) : base(owner)
        {
        }

        public override bool test_condition()
        {
            if (play_count <= 0)
            {
                play_count = 1;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
