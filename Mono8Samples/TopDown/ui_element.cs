﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    public class ui_element : obj
    {
        public ui_element(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
        }

        public virtual bool HandleInput()
        {
            return false;
        }
    }
}
