﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    class ui_selector<T>
    {
        List<Tuple<string /*label*/,T/*value*/>> data_set;
        int cur_index;
        bool wrap;
        Action on_execute;
        string title;

        public ui_selector(string title, List<Tuple<string /*label*/, T/*value*/>> data_set, int starting_index, bool wrap, Action on_execute)
        {
            this.data_set = data_set;
            this.cur_index = starting_index;
            this.wrap = wrap;
            this.on_execute = on_execute;
            this.title = title;
        }

        public void inc_index(int amount)
        {
            cur_index += amount;

            if (wrap)
            {
                // Add count to support wrapping from negative nums.
                cur_index = (cur_index + data_set.Count) % data_set.Count;
            }
        }

        public Tuple<string,T> get_cur_data()
        {
            return data_set[cur_index];
        }

        public string get_title()
        {
            return title;
        }

        public string get_cur_label()
        {
            return data_set[cur_index].Item1;
        }

        public T get_cur_value()
        {
            return data_set[cur_index].Item2;
        }

        public int get_data_set_size()
        {
            return data_set.Count;
        }

        public void execute()
        {
            on_execute();
        }
    }
}
