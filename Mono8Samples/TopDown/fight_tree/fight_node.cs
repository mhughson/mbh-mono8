﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_node : fight_branch
    {
        public int ticks = 0;

        public List<fight_track> tracks { get; protected set; }

        public fight_node(List<fight_track> tracks, List<fight_condition> conditions, List<fight_branch> children, string name, PicoXGame owner) : base(conditions, children, name, owner)
        {
            this.tracks = tracks;
        }

        public void add_track(fight_track new_track)
        {
            tracks.Add(new_track);
        }

        public void remove_track(fight_track old_track)
        {
            tracks.Remove(old_track);
        }

        public virtual void play()
        {
            ticks = 0;

            foreach (fight_track track in tracks)
            {
                track.reset();
            }
        }

        public virtual bool advance(int frames = 1)
        {
            bool complete = true;
            foreach(fight_track track in tracks)
            {
                if ((/*track.start == null || */track.start <= ticks) && (track.length == null || track.start + track.length >= ticks))
                {
                    if (!track.is_complete)
                    {
                        bool res = track.advance(frames);
                        track.is_complete = res;

                        // If any master track is not complete than the nodes is not complete.
                        if (track.is_master && !track.is_complete)
                        {
                            complete = false;
                        }
                        //complete |= track.is_complete || !track.is_master;
                    }
                }
            }

            // TODO: This is a little backwards. We should instead advance the ticks before updating
            //       tracks, and pass in 0 for frames in the first update.
            ticks += frames;
            return complete;
        }
    }
}
