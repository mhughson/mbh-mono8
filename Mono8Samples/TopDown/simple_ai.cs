﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;
using static Mono8Samples.TopDown.TopDown;
using RoyT.AStar;

namespace Mono8Samples.TopDown
{
    public class simple_ai : base_ai
    {
        int shoot_delay = 30;
        int cur_shoot_delay = 0;

        int patrol_delay = 60;
        int cur_patrol_delay = 0;

        int spotted_time = 0;
        int min_spotted_time = 60;

        public Queue<path_point> PatrolPoints { get; private set; }
        path_point Destination;

        public string path_name;

        Vector2 target_last_known_position = Vector2.Zero;
        obj cur_target = null;

        int search_count = 0;
        float cur_min_dist_to_last_known_pos = 0;

        int time_in_state = 0;

        public enum states
        {
            patrol = 0,
            go_to_last_known,
            search_pos,
            attack,
            alert,
            sleeping,
            waking,
            pissing,
            reading,
            dead,
        }

        states cur_state;

        public simple_ai(states initial_state, PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            type_flag = type_flags.Enemy;
            attack_target_types = type_flags.Player;

            acc = 1;
            dcc = 0;
            walk_speed = 0.25f;
            run_speed = 0.5f;
            set_state(initial_state);
        }

        public override void on_death(obj source)
        {
            base.on_death(source);

            set_state(states.dead);
        }

        public override void update()
        {
            if (Game.game_world.get_global_alert_state() == world.alert_states.high)
            {
                cur_movement_type = movement_types.run;
                if (cur_state == states.patrol)
                {
                    player p = Game.game_world.get_player();
                    if (p != null && p.is_alive)
                    {
                        target_last_known_position = new Vector2(p.col_box().x, p.col_box().y);
                        cur_target = p;
                        set_state(states.alert);
                    }
                }
            }
            else
            {
                cur_movement_type = movement_types.walk;
            }

            // NOTE: Intentionally allowed to go below zero for "hold fire" behaviour.
            cur_shoot_delay--;

            time_in_state++;

            switch (cur_state)
            {
                case states.sleeping:
                    {
                        break;
                    }
                case states.patrol:
                case states.pissing:
                case states.reading:
                    {
                        // First check if we can see any targets.
                        obj target;
                        float angle;
                        if (find_target(out target, out angle))
                        {
                            spotted_time++;
                            target_last_known_position = new Vector2(target.col_box().x, target.col_box().y);
                            cur_target = target;

                            // Has the player been in view for long enough.
                            if (spotted_time >= min_spotted_time)
                            {
                                set_state(states.alert);
                            }
                        }
                        else
                        {
                            // Was the player previously in view?
                            if (spotted_time > 0)
                            {
                                // Keep incrementing the spotted time until we
                                // hit the min time, so that there is always
                                // a little delay before they begin pursuit.
                                spotted_time++;
                                if (spotted_time >= min_spotted_time)
                                {
                                    // Pursue the player.
                                    set_state(states.go_to_last_known);
                                }
                            }
                            else if (Destination != null && cur_patrol_delay <= 0 && PatrolPoints != null)
                            {
                                
                                // How far are we from the Destination (the next patrol point).
                                var dx = Destination.position.X - col_box().x;
                                var dy = Destination.position.Y - col_box().y;
                                var dist = Game.sqrt((dx * dx) + (dy * dy));

                                // Have we arrived?
                                // TODO: Should this be < max_speed?
                                if (Game.abs(dist) < 1)
                                {
                                    if (Destination.fx != 0 || Destination.fy != 0)
                                    {
                                        fx = Destination.fx;
                                        fy = Destination.fy;
                                    }

                                    PatrolPoints.Enqueue(Destination);
                                    Destination = PatrolPoints.Dequeue();
                                    cur_patrol_delay = patrol_delay;
                                    btn_left = false;
                                    btn_right = false;
                                    btn_up = false;
                                    btn_down = false;
                                }
                                else
                                {
                                    move_to_pos(Destination.position.X, Destination.position.Y);
                                }
                            }

                            cur_patrol_delay = (int)Game.max(0, cur_patrol_delay - 1);
                        }
                        break;
                    }

                case states.attack:
                    {
                        obj target;
                        float angle;
                        if (find_target(out target, out angle))
                        {
                            Game.game_world.set_global_alert_state(world.alert_states.high);

                            target_last_known_position = new Vector2(target.col_box().x, target.col_box().y);

                            // Don't use collision center since the bullet has an offset collision point.
                            Vector2 to_other = new Vector2(target.x - x, target.y - y);

                            bool in_range = cur_weap != null ? to_other.Length() < cur_weap.attack_range : false;

                            if (in_range)
                            {
                                to_other.Normalize();
                                fx = wx = to_other.X;
                                fy = wx = to_other.Y;

                                // Stop and wait to see if enough time has passed before we can shoot again.
                                btn_left = false;
                                btn_right = false;
                                btn_up = false;
                                btn_down = false;
                                btn_attack = false;

                                if (cur_shoot_delay <= 0)
                                {
                                    btn_attack = true;

                                    // hold of a bit
                                    if (cur_shoot_delay < -15)
                                    {
                                        cur_shoot_delay = shoot_delay;
                                    }
                                }
                            }
                            else
                            {
                                // Try to get closer.
                                move_to_pos(target_last_known_position.X, target_last_known_position.Y);
                            }
                        }
                        else
                        {
                            // We lost track of the target. Start searching...
                            set_state(states.go_to_last_known);
                        }
                        break;
                    }

                case states.go_to_last_known:
                    {
                        // Only keep setting to high state if we are there already.
                        // Prevent going to last known from ENTERING high; it should
                        // only maintain.
                        if (Game.game_world.get_global_alert_state() == world.alert_states.high)
                        {
                            Game.game_world.set_global_alert_state(world.alert_states.high);
                        }

                        if (time_in_state < 120)
                        {
                            target_last_known_position = new Vector2(cur_target.col_box().x, cur_target.col_box().y);
                        }

                        // First check if we can see any targets.
                        obj target;
                        float angle;
                        if (find_target(out target, out angle))
                        {
                            target_last_known_position = new Vector2(target.col_box().x, target.col_box().y);
                            set_state(states.attack);
                        }
                        else if (move_to_pos(target_last_known_position.X, target_last_known_position.Y))
                        {
                            set_state(states.search_pos);
                        }
                        else
                        {
                            Vector2 pos = new Vector2(col_box().x, col_box().y);
                            float d = Vector2.DistanceSquared(pos, target_last_known_position);

                            if (d < cur_min_dist_to_last_known_pos)
                            {
                                set_state(states.search_pos);
                            }
                        }
                        break;
                    }

                case states.search_pos:
                    {
                        btn_left = false;
                        btn_right = false;
                        btn_up = false;
                        btn_down = false;
                        btn_attack = false;

                        obj target;
                        float angle;
                        if (find_target(out target, out angle))
                        {
                            target_last_known_position = new Vector2(target.col_box().x, target.col_box().y);
                            set_state(states.attack);
                        }
                        else
                        {
                            const int time_to_look = 120 * 4;
                            const int time_per_state = time_to_look / 4;

                            if (time_in_state < time_per_state)
                            {
                                fx = wx = -1;
                            }
                            else if (time_in_state < time_per_state * 2)
                            {
                                fy = wy = -1;
                            }
                            else if (time_in_state < time_per_state * 3)
                            {
                                fx = wx = 1;
                            }
                            else if (time_in_state < time_per_state * 4)
                            {
                                fy = wy = 1;
                            }
                            else if (search_count < 2 && false) // disabled for now, as it can lead to bad locations.
                            {
                                Vector2 old = target_last_known_position;
                                for (int i = 0; i < 10; i++)
                                {
                                    int dir_x = Game.flr(Game.rnd(3)) - 1;
                                    int dir_y = Game.flr(Game.rnd(3)) - 1;

                                    int new_x = (int)(target_last_known_position.X / 8) + dir_x;
                                    int new_y = (int)(target_last_known_position.Y / 8) + dir_y;

                                    if (!Game.game_world.cur_area.fget(new_x, new_y, area.collision_flag.all) && new_x < Game.map_grid.DimX && new_x >= 0 && new_y < Game.map_grid.DimY && new_y >= 0)
                                    {
                                        target_last_known_position += new Vector2(dir_x * 8, dir_y * 8);
                                    }
                                }

                                if (target_last_known_position != old)
                                {
                                    set_state(states.go_to_last_known);
                                }
                                else
                                {
                                    set_state(states.patrol);
                                }
                            }
                            else
                            {
                                set_state(states.patrol);
                            }
                        }

                        break;
                    }

                case states.alert:
                case states.waking:
                    {
                        btn_left = false;
                        btn_right = false;
                        btn_up = false;
                        btn_down = false;
                        btn_attack = false;
                        
                        Vector2 to_other = new Vector2(target_last_known_position.X - x, target_last_known_position.Y - y);
                        to_other.Normalize();
                        fx = wx = to_other.X;
                        fy = wx = to_other.Y;

                        if (time_in_state > 30)
                        {
                            set_state(states.attack);
                        }
                        break;
                    }

                case states.dead:
                    {
                        btn_left = false;
                        btn_right = false;
                        btn_up = false;
                        btn_down = false;
                        btn_attack = false;
                        btn_dodge = false;

                        dx = 0;
                        dy = 0;

                        break;
                    }
            }

            // Little hack to avoid agents overlapping. Works by timing out the path finding,
            // but seems like a bad way of doing things. Should instead build a "tile reservation"
            // system where agents can reserve a spot they are heading to, and other agents will try
            // not to pick the same spot.
            //float old_x = col_box().x;
            //float old_y = col_box().y;
            base.update();

            //Game.map_grid.SetCellCost(new Position((int)(old_x / 8.0f), (int)(old_y / 8.0f)), 1.0f);
            //Game.map_grid.SetCellCost(new Position((int)(col_box().x / 8.0f), (int)(col_box().y / 8.0f)), 100.0f);
        }

        public override void set_anim(string anim_name)
        {
            if (cur_state == states.sleeping)
            {
                base.set_anim(anim_prefix + "_sleeping");
            }
            else if (cur_state == states.waking)
            {
                base.set_anim(anim_prefix + "_waking");
            }
            else if (cur_state == states.pissing)
            {
                base.set_anim(anim_prefix + "_pissing");
            }
            else if (cur_state == states.reading)
            {
                base.set_anim(anim_prefix + "_reading_magazine");
            }
            else
            {
                base.set_anim(anim_name);
            }
        }

        public override void on_sense(sense_emit source)
        {
            if (cur_state != states.attack)
            {
                if ((source.creator.type_flag & type_flags.Enemy) != type_flags.Enemy)
                {
                    target_last_known_position = new Vector2(source.x, source.y);
                    cur_target = source.creator;
                    if (cur_state == states.sleeping)
                    {
                        set_state(states.waking);
                    }
                    else if (cur_state == states.go_to_last_known)
                    {
                        // In the event that they are already on their way to find me, just go back into
                        // attack mode, which will update the position they are moving towards.
                        set_state(states.attack);
                    }
                    else
                    {
                        set_state(states.alert);
                    }
                }
                // Transfer information to other AI
                //else if (source.creator.type_flag == type_flags.Enemy)
                //{
                //    simple_ai ai = source.creator as simple_ai;

                //    if (ai != null)
                //    {
                //        target_last_known_position = ai.target_last_known_position;
                //        cur_target = ai.cur_target;
                //        set_state(states.alert);
                //    }
                //}
            }

            base.on_sense(source);
        }

        private bool move_to_pos(float dest_x, float dest_y)
        {
            btn_right = btn_left = btn_up = btn_down = false;

            Vector2 col_center = new Vector2(col_box().x, col_box().y);
            Position[] path = Game.map_grid.GetPath(new Position((int)(col_center.X / 8), (int)(col_center.Y / 8)), new Position((int)(dest_x / 8), (int)(dest_y / 8)), MovementPatterns.Full, 1000);

            if (path.Count() <= 0)
            {
                // We failed to find a path to the target.
                // THIS NEVER SEEMS TO FIRE.
                return true;
            }

#if DEBUG
            foreach (Position pos in path)
            {
                Game.rect_d(pos.X * 8, pos.Y * 8, pos.X * 8 + 8, pos.Y * 8 + 8, 23);
            }
#endif

            // Path finder includes the cell we are in, in the solution. So we need to account for the case where
            // we are right at the center of this cell, and thus, should actually move to the next point in the solution.
            int path_index = 0;
            while (path_index < path.Count() - 1 && path[path_index].X == (int)(col_center.X / 8) && path[path_index].Y == (int)(col_center.Y / 8))
            {
                path_index++;
            }

            // The delta between the next point we need to travel and our current position.
            float delta_x = ((path[path_index].X * 8) + 4) - col_center.X;
            float delta_y = ((path[path_index].Y * 8) + 4) - col_center.Y;


            // Limit the AI to only try to move in a direction that is 
            // greater than the move speed.
            // This prevents the AI from bouncing back and forth between
            // directions, as it overshoots the middle of the destination.
            if (Game.abs(delta_y) < cur_move_speed)
            {
                delta_y = 0;
            }
            if (Game.abs(delta_x) < cur_move_speed)
            {
                delta_x = 0;
            }

            // Normalize the direction for use with movement code.
            float dist = Game.sqrt((delta_x * delta_x) + (delta_y * delta_y));

            // Is the distance to the target shorter than the amount we move in a frame.
            if (dist <= cur_move_speed)
            {
                // They have reached the last point in the path.
                if (path.Count() <= 1)
                {
                    // We have reached the target.
                    return true;
                }
                else
                {
                    // We have reached the next point in the path.
                    return false;
                }
            }

            delta_x /= dist;
            delta_y /= dist;

            // Force movement to 8 directions.
            fx = wx = (float)Math.Round(delta_x);
            fy = wy = (float)Math.Round(delta_y);

            // Can't use wx/wy because they get stuck on edges.
            btn_right = delta_x > 0;
            btn_left = delta_x < 0;
            btn_up = delta_y < 0;
            btn_down = delta_y > 0;

            return false;
        }

        public override void draw()
        {
            base.draw();

            if (cur_state == states.alert || cur_state == states.waking)
            {
                Game.printc("!", (int)x, (int)y - 16, 0, 31, 0);
            }
            else if (cur_state == states.search_pos || ((spotted_time > 0 || cur_state == states.go_to_last_known) && Game.game_world.get_global_alert_state() != world.alert_states.high))
            {
                Game.printc("?", (int)x, (int)y - 16, 0, 31, 0);
            }
            else if (cur_state == states.sleeping)
            {
                Game.printc("z", (int)(x - Game.sin(time_in_state * 0.005f) * 4), (int)(y - (time_in_state * 0.1) % 8), 0, 31, 0);
                Game.printc("z", (int)(x - Game.cos(time_in_state * 0.005f) * 4), (int)(y - ((time_in_state * 0.1) + 4) % 8), 0, 31, 0);
            }

            //Game.rect_d(target_last_known_position.X - 4, target_last_known_position.Y - 4, target_last_known_position.X + 4, target_last_known_position.Y + 4, 21);
        }

        public void set_path(path p)
        {
            if (p.points.Count > 0)
            {
                PatrolPoints = new Queue<path_point>();
                foreach (var point in p.points)
                {
                    PatrolPoints.Enqueue(point);
                }
                Destination = PatrolPoints.Dequeue();
            }
        }

        protected states get_state()
        {
            return cur_state;
        }

        protected void set_state(states new_state)
        {
            // Don't allow re-entry. Death is forever.
            if (new_state == cur_state || cur_state == states.dead)
            {
                return;
            }

            switch (cur_state)
            {
                case states.patrol:
                    {
                        spotted_time = 0;
                        break;
                    }
                case states.attack:
                    {
                        search_count=0;
                        break;
                    }
            }

            cur_state = new_state;

            switch (cur_state)
            {
                case states.patrol:
                    {
                        spotted_time = 0;
                        break;
                    }
                case states.search_pos:
                    {
                        search_count++;
                        break;
                    }

                case states.attack:
                    {
                        Game.game_world.set_global_alert_state(world.alert_states.high);
                        break;
                    }

                case states.go_to_last_known:
                    {
                        // Allow for some variance in how close the AI tries to get to the last know position before entering
                        // the search state.
                        cur_min_dist_to_last_known_pos = Game.rnd(16.0f * 16.0f) + (8.0f * 8.0f);
                        break;
                    }
                case states.pissing:
                    {
                        fy = -1;
                        fx = 0;
                        break;
                    }
                case states.reading:
                    {
                        fx = 0;
                        fy = 1;
                        break;
                    }
            }

            time_in_state = 0;
        }
    }
}
