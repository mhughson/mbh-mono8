# WHAT IS MONO8 #

Mono8 meant to assist in porting Pico 8 games to MonoGame. It makes the porting process easier by implementing most of the Pico 8 API as a layer on top of MonoGame, such that all the Pico 8 functions continue to work. For example cls(7) still clears the screen White, but it now does it through MonoGame calls under the hood.

# WHAT ISN'T MONO8 #

Mono8 is not a completely faithful recreating of Pico 8. The expectation is that you want to build beyond the limitations of Pico 8, and that is why you are porting to a new framework, and so it wouldn't make sense to maintain functions related to memory (peek, poke, etc). The memory will not be laid out in the same way.

It also doesn't read in *.p8 files, but instead relies on the client to provide the data required (an exporting sprite sheet, a string of map data, etc). Again, this is because Mono8 intends to allow you to expand beyond the limitations of Pico 8, so limiting you to (for example) the 8 waveforms Pico 8 uses for audio, wouldn't make sense.

# PICOX #

You may notice this project includes a small sub-library: PicoX.

This library provides the platform/framework agnostic interface that Mono8 implements. If you desire to port Pico 8 to any other platforms or frameworks, use PicoX as a starting point.