﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_condition_btn : fight_condition
    {
        int btn;

        public fight_condition_btn(int btn, PicoXGame owner) : base(owner)
        {
            this.btn = btn;
        }

        public override bool test_condition()
        {
            return Game.btn(btn);
        }

        public override string ToString()
        {
            string res = base.ToString();

            res += "( btn: " + btn + " )";

            return res;
        }
    }
}
