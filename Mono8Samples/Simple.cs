﻿using System;
using System.Collections.Generic;
using PicoX;

namespace Mono8Samples
{
    class Simple : PicoXGame
    {
        public override void _init()
        {
            mset(2, 0, mget(1, 0));
            fset(1, 2, true);
            fset(1, 2, false);
            fset(7, 3, true);
            fset(8, 15);
        }
        public override void _draw()
        {
            cls(7);

            rectfill(0, 8, 0 + 15, 8 + 15, 1);

            circfill(8, 16, 8, 8);

            for (int i = 0; i < 8; i++)
            {
                rectfill(i * 8, 0, i * 8 + 8, 8, i + 8);
            }

            map(0, 0, 0, 64, 16, 16);

            for(int i=0; i < 8; i++)
            {
                byte flags = fget(mget(i, 0));
                print(flags.ToString(), i * 8, 64 + 8 + (i % 2 * 8), 1 + i % 2);
                print(fget(mget(i, 0), 3) == true ? "y" : "n", i * 8, 64 + 8 + 16 + (i % 2 * 8), 1 + i % 2);
            }
        }

        public override string GetMapString()

        {
            return "Content/raw/simple_map.tmx";
        }

        public override Dictionary<int, string> GetMusicPaths()
        {
            return new Dictionary<int, string>();
        }

        public override List<string> GetSheetPath()
        {
            return new List<string>() { "raw\\simple_sheet" };
        }

        public override Dictionary<int, string> GetSoundEffectPaths()
        {
            return new Dictionary<int, string>();
        }

        public override Dictionary<string, object> GetScriptFunctions()
        {
            return new Dictionary<string, object>();
        }

        public override string GetPalTextureString()
        {
            return "";
        }
    }
}
