﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class simple_fx : obj
    {
        public List<Tuple<int, int>> pal;

        public simple_fx(float x, float y, PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            this.x = x;
            this.y = y;
            event_on_anim_done += delegate (string anim_name)
            {
                destroy();
            };
            pal = new List<Tuple<int, int>>();
        }

        public override void draw()
        {
            int[] p = Game.get_cur_pal();
            Game.apply_pal(p);

            foreach (var t in pal)
            {
                Game.pal(t.Item1, p[t.Item2]);
            }
            base.draw();

            Game.pal();
        }
    }

    public class bullet_hit_fx : simple_fx
    {
        public bullet_hit_fx(float x, float y, PicoXGame owner, area owning_area = null) : base(x, y, owner, owning_area)
        {
            anims = new Dictionary<string, anim>()
                {
                    {
                        "idle",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= new int[][] { new int[] { 17 } },//what frames are shown.
                        }
                    }
                };
            set_anim("idle");
        }
    }

    public class muzzle_fire_fx : simple_fx
    {
        // Track the last flip setting so that every fx flips the other way giving a
        // more pleasant look.
        static bool last_flip_x = false;

        public muzzle_fire_fx(float x, float y, PicoXGame owner, area owning_area = null) : base(x, y, owner, owning_area)
        {
            anims = new Dictionary<string, anim>()
                {
                    {
                        "idle",
                        new anim()
                        {
                            ticks=2,//how long is each frame shown.
                            frames= new int[][] { new int[] { 1 } },//what frames are shown.
                        }
                    }
                };
            set_anim("idle");

            last_flip_x = !last_flip_x;
            flipx = last_flip_x;
            //flipy = Game.rnd(2.0f) < 1.0f;
        }
    }

    public class explosion_fx : simple_fx
    {
        public explosion_fx(float x, float y, PicoXGame owner, area owning_area = null) : base(x, y, owner, owning_area)
        {
            w = 8;
            h = 16;
            gfx_bank = 1;

            set_anim("fx_small_explosion");
        }
    }

    public class kill_fx : simple_fx
    {
        public Dictionary<int, Dictionary<int, int[][]>> anim_lookup = new Dictionary<int, Dictionary<int, int[][]>>()
        {
            {
                -1, new Dictionary<int, int[][]>
                {
                    { -1, agent.create_anim_sequence(426, -5) },
                    {  0, agent.create_anim_sequence(212, -4) },
                    {  1, agent.create_anim_sequence(421, -5) },
                }
            },
            {
                0, new Dictionary<int, int[][]>
                {
                    { -1, agent.create_anim_sequence(416,  5) },
                    {  0, agent.create_anim_sequence(416,  5) }, // INVALID
                    {  1, agent.create_anim_sequence(220,  4) },
                }
            },
            {
                1, new Dictionary<int, int[][]>
                {
                    { -1, agent.create_anim_sequence(426,  5) },
                    {  0, agent.create_anim_sequence(212,  4) },
                    {  1, agent.create_anim_sequence(421,  5) },
                }
            },
        };

        public kill_fx(float x, float y, float dx, float dy, PicoXGame owner, area owning_area = null) : base(x, y, owner, owning_area)
        {
            gfx_bank = 1;

            this.dx = dx;
            this.dy = dy;

            h = 16;

            // TODO:
            pal.Add(new Tuple<int, int>(30, 21));
            pal.Add(new Tuple<int, int>(7, 20));

            anims = new Dictionary<string, anim>()
                {
                    {
                        "idle",
                        new anim()
                        {
                            loop=false,
                            ticks=10,//how long is each frame shown.
                            frames=anim_lookup[(int)Math.Round(dx)][(int)Math.Round(dy)],
                        }
                    }
                };


            set_anim("idle");

            event_on_anim_done = null;
        }

        public override void update()
        {
            // Prevent this from flipping after it has come to a stand still.
            if (Game.abs(dx) > 0.1f || Game.abs(dy) > 0.1f)
            {
                if (Game.hit_right(this) || Game.hit_left(this))
                {
                    dx *= -0.7f;
                    flipx = !flipx;
                }
                if (Game.hit_top(this) || Game.hit_bottom(this))
                {
                    dy *= -0.7f;
                    //flipy = !flipy; // busted
                }

                dx *= 0.9f;
                x += dx;
                dy *= 0.9f;
                y += dy;
            }
            base.update();
        }
    }

    public class kill_dog_fx : kill_fx
    {
        public kill_dog_fx(float x, float y, float dx, float dy, PicoXGame owner, area owning_area = null) : base(x, y, dx, dy, owner, owning_area)
        {
            this.dx = dx;
            this.dy = dy;
            w = 16;
            h = 16;
            pal = new List<Tuple<int, int>>();
            anims = new Dictionary<string, anim>()
                {
                    {
                        "idle",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            loop = false,
                            frames= new int[][]
                            {
                                new int[] { 549, 550, 565, 566, },
                                new int[] { 551, 552, 567, 568, },
                                new int[] { 553, 554, 569, 570, },
                                new int[] { 555, 556, 571, 572, },
                            },
                        }
                    }
                };
            set_anim("idle");
            event_on_anim_done = null;
        }
    }

    public class kill_drowned_fx : kill_fx
    {
        public kill_drowned_fx(float x, float y, float dx, float dy, PicoXGame owner, area owning_area = null) : base(x, y, dx, dy, owner, owning_area)
        {
            this.dx = dx;
            this.dy = dy;
            w = 16;
            h = 16;
            pal = new List<Tuple<int, int>>();
            set_anim("soldier_drowned");
            event_on_anim_done = null;
        }

        public override void update()
        {
            base.update();
            
            y += Game.sin(Game.tick * 0.001f) * 0.01f;
        }
    }

    public class floating_text : ui_element
    {
        struct state_info
        {
            public float start_x;
            public float delta;
            public int ticks;
            public int length;
        };

        Queue<state_info> states;
        state_info state;

        string text;

        public float target_y;
        
        public floating_text(string text, PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            this.text = text;


            float start_x = text.Length * -4;
            float end_x = 2;

            states = new Queue<state_info>();
            states.Enqueue(
                new state_info()
                {
                    start_x = start_x,
                    delta = end_x - start_x,
                    ticks = 0,
                    length = 60,
                }
            );
            states.Enqueue(
                new state_info()
                {
                    start_x = end_x,
                    delta = 0,
                    ticks = 0,
                    length = 60,
                }
            );
            states.Enqueue(
                new state_info()
                {
                    start_x = end_x,
                    delta = start_x - end_x,
                    ticks = 0,
                    length = 60,
                }
            );
            target_y = y = 127 - 6;

            state = states.Dequeue();

            for (int i = Game.game_world.ui_area.objs_active.Count - 1; i >= 0; i--)
            {
                floating_text ft = Game.game_world.ui_area.objs_active[i] as floating_text;
                if (ft != null)
                {
                    ft.target_y -= 8;
                }
            }
        }

        public override void update()
        {
            state.ticks++;

            if (state.ticks >= state.length)
            {
                if (states.Count == 0)
                {
                    destroy();
                }
                else
                {
                    state = states.Dequeue();
                }
            }

            x = Game.easeinoutquint((float)state.ticks, state.start_x, state.delta, state.length);

            y += (target_y - y) * 0.1f;

            base.update();
        }

        public override void draw()
        {
            Game.rectfill(0, y-2, x + text.Length * 4, y-2+8, 31);
            Game.printo(text, x, y, 0, 29);
        }

        public override bool HandleInput()
        {
            return false;
        }
    }
}
