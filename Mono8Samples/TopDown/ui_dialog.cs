﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class ui_dialog : ui_element
    {
        int dialog_box_height = 34;
        int display_string_start_x = 16 + 3 + 4;
        string display_string;
        obj portrait;
        List<string> display_string_lines;
        int visible_lines = 5;
        int cur_character = 0;

        bool done_showing = false;
        int ticks_in_state = 0;
        float yscroll = 0;
        
        public Action on_closed_completed;

        public ui_dialog(string text, string portrait_anim, PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            display_string = text;

            // TODO: Add to area with parenting information?
            portrait = new obj(Game)
            {
                x = 8 + 3,
                y = 0, // set every frame.
            };
            
            portrait.set_anim(portrait_anim);

            display_string_lines = new List<string>();

            // Width of the screen divided by the widget of a character.
            int chars_per_line = (127 - display_string_start_x) / 4;

            string[] words = text.Split(' ');
            string line = "";

            for(int i = 0; i < words.Length; i++)
            {
                // TODO: if a single word is longer than the single line, then add hyphen and move to new line.
                if(words[i].Length + line.Length < chars_per_line)
                {
                    line += words[i] + " ";
                }
                else
                {
                    display_string_lines.Add(line);
                    line = words[i] + " ";
                }
            }

            // Add the last line.
            // TODO: Handle case where the last word triggered an Add already.
            display_string_lines.Add(line);
        }

        public override void update()
        {
            cur_character++;

            if (Game.btnp(4) || Game.btnp(5))
            {
                if (!done_showing)
                {
                    // Skip to the end of this section, by setting the current character to something
                    // large enough to for sure include all characters.
                    cur_character = 9999;
                    done_showing = true;
                }
                else
                {
                    // The user pressed a button after this section of dialog has be shown fully.
                    // Remove all of this text, and move on to the next section.
                    display_string_lines.RemoveRange(0, (int)Game.min(visible_lines, display_string_lines.Count));
                    cur_character = 0;
                    done_showing = false;
                }
            }

            // Is there nothing left to display? Hide the dialog box.
            // NOTE: This adds a buffer in to make sure our input isn't read by the player. If we remove this
            //       we may need to change this to "on release" or something.
            if (display_string_lines.Count==0)
            {
                ticks_in_state++;
                yscroll = Game.easeinoutquint(ticks_in_state/64.0f, 0, dialog_box_height, 1.0f);

                // Has the animation completed? If so, destroy the dialog box.
                if (ticks_in_state > 64)
                {
                    destroy();
                    on_closed_completed?.Invoke();
                }
            }
        }

        public override void draw()
        {
            // Background.
            Game.rectfill(0 , 127 - dialog_box_height + yscroll, 127, 127 + yscroll, 31);
            // Border.
            Game.rect(1, 127 - dialog_box_height + 1 + yscroll, 126, 126 + yscroll, 0);

            // Character portrait.
            Game.bset(4);
            portrait.y = 127 - dialog_box_height + 12 + 3 + yscroll;
            portrait.draw();
            Game.bset(0);

            int count = 0;
            bool is_done = true;
            for(int i = 0; i < visible_lines && i < display_string_lines.Count; i++)
            {
                if (display_string_lines[i].Length < cur_character - count)
                {
                    Game.print(display_string_lines[i], display_string_start_x, 127 - dialog_box_height + 3 + (i * 6), 0);
                    count += display_string_lines[i].Length;
                }
                else
                {
                    Game.print(display_string_lines[i].Substring(0, cur_character - count), display_string_start_x, 127 - dialog_box_height + 3 + (i * 6), 0);
                    is_done = false;
                    break;
                }
            }

            done_showing = is_done;
        }

        public override bool HandleInput()
        {
            return true;
        }
    }
}
