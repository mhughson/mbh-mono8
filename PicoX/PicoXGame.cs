﻿using System;
using System.Collections.Generic;

namespace PicoX
{
    // TODO: Should an additional interface be created for the Pico8 Game itself, rather
    // than it deriving from Mono8Game? This interface would define _init, etc, as well
    // as expose the Pico8GameAPI. Perhaps when creating a Mono8Game, you would pass in
    // the Pico8 game to it, where it would initialize everything needed.
    // This would have the added benifit of hidding away things like MonoGame's Draw function
    // to avoid confusion.
    public abstract class PicoXGame : IPico8GameAPI
    {
        /// <summary>
        /// Provides an implementation of the Game API this class implements.
        /// </summary>
        public IPico8GameAPI P8API { private get; set; }

        public abstract string GetMapString();
        public abstract List<string> GetSheetPath();
        public abstract Dictionary<int, string> GetSoundEffectPaths();
        public abstract Dictionary<int, string> GetMusicPaths();
        public abstract Dictionary<string, object> GetScriptFunctions();
        public abstract string GetPalTextureString();
        public virtual Tuple<int, int> GetResolution() { return new Tuple<int, int>(128, 128); }
        public virtual int GetGifScale() { return 4; }
        public virtual bool GetGifCaptureEnabled() { return true; }
        public virtual bool GetPauseMenuEnabled() { return true; }

        // OPTIONAL GAME FUNCTIONS
        //

        public virtual void _init() { }

        public virtual void _update60() { }

        public virtual void _draw() { }

        // PICO 8 API
        //

        public void cls(int c = 0)
        {
            P8API.cls(c);
        }

        public void pset(float x, float y, int c)
        {
            P8API.pset(x, y, c);
        }

        public int pget(int x, int y)
        {
            return P8API.pget(x, y);
        }

        public void sspr(int sx, int sy, float sw, float sh, float dx, float dy, float dw = float.MaxValue, float dh = float.MaxValue, bool flip_x = false, bool flip_y = false)
        {
            P8API.sspr(sx, sy, sw, sh, dx, dy, dw, dh, flip_x, flip_y);
        }

        public void spr(int n, float x, float y, float w = 1, float h = 1, bool flip_x = false, bool flip_y = false)
        {
            P8API.spr(n, x, y, w, h, flip_x, flip_y);
        }

        public void sprfxset(int fx, bool enabled)
        {
            P8API.sprfxset(fx, enabled);
        }

        public void sprfxadd(Func<int, int, int, int> fx_func, int id)
        {
            P8API.sprfxadd(fx_func, id);
        }

        public void bset(int n)
        {
            P8API.bset(n);
        }

        public int sget(int x, int y)
        {
            return P8API.sget(x, y);
        }

        public int mget(int celx, int cely, int map_layer = 0)
        {
            return P8API.mget(celx, cely, map_layer);
        }

        public TileData? mget_tiledata(int celx, int cely, int map_layer = 0)
        {
            return P8API.mget_tiledata(celx, cely, map_layer);
        }

        public bool mfget(int celx, int cely, int flag, int map_layer = 0)
        {
            return P8API.mfget(celx, cely, flag, map_layer);
        }

        public byte mfgetflags(int celx, int cely, int map_layer = 0)
        {
            return P8API.mfgetflags(celx, cely, map_layer);
        }

        public void mset(int celx, int cely, int snum, int map_layer = 0)
        {
            P8API.mset(celx, cely, snum, map_layer);
        }

        public void msetbank(int celx, int cely, int snum, int bank, int map_layer = 0)
        {
            P8API.msetbank(celx, cely, snum, bank, map_layer);
        }

        public bool fget(int n, int f)
        {
            return P8API.fget(n, f);
        }

        public byte fget(int n)
        {
            return P8API.fget(n);
        }

        public bool fget(TileData? tile_data, int flags)
        {
            return P8API.fget(tile_data, flags);
        }

        public byte fget(TileData? tile_data)
        {
            return P8API.fget(tile_data);
        }

        public void fset(int n, int f, bool v)
        {
            P8API.fset(n, f, v);
        }

        public void fset(int n, byte f)
        {
            P8API.fset(n, f);
        }

        public void palt(int col, bool t)
        {
            P8API.palt(col, t);
        }

        public void pal(int c0, int c1, int p = 0)
        {
            P8API.pal(c0, c1, p);
        }

        public void pal()
        {
            P8API.pal();
        }

        public void line(float x0, float y0, float x1, float y1, int col)
        {
            P8API.line(x0, y0, x1, y1, col);
        }

        public void circfill(float x, float y, float r, int col)
        {
            P8API.circfill(x, y, r, col);
        }

        public void circ(float x, float y, float r, int col)
        {
            P8API.circ(x, y, r, col);
        }

        public void rect(float x0, float y0, float x1, float y1, int col)
        {
            P8API.rect(x0, y0, x1, y1, col);
        }

        public void rectfill(float x0, float y0, float x1, float y1, int col)
        {
            P8API.rectfill(x0, y0, x1, y1, col);
        }

        public void print(string str, float x, float y, int col)
        {
            P8API.print(str, x, y, col);
        }

        public void camera(float x, float y)
        {
            P8API.camera(x, y);
        }

        public void map(int celx, int cely, float sx, float sy, int celw, int celh, int layer = 0, int map_layer = 0)
        {
            P8API.map(celx, cely, sx, sy, celw, celh, layer, map_layer);
        }

        public void set_render_scale(float new_scale = 1.0f)
        {
            P8API.set_render_scale(new_scale);
        }

        public bool btn(int id, int player = 0)
        {
            return P8API.btn(id, player);
        }

        public bool btnp(int id, int player = 0)
        {
            return P8API.btnp(id, player);
        }

        public bool btnr(int id, int player = 0)
        {
            return P8API.btnr(id, player);
        }

        public void srand(int val)
        {
            P8API.srand(val);
        }

        public float rnd(float max)
        {
            return P8API.rnd(max);
        }

        public float sin(float a)
        {
            return P8API.sin(a);
        }

        public float cos(float a)
        {
            return P8API.cos(a);
        }

        public float min(float a, float b)
        {
            return P8API.min(a, b);
        }

        public float max(float a, float b)
        {
            return P8API.max(a, b);
        }

        public float mid(float a, float b, float c)
        {
            return P8API.mid(a, b, c);
        }

        public float sqrt(float a)
        {
            return P8API.sqrt(a);
        }

        public int flr(float a)
        {
            return P8API.flr(a);
        }

        public float abs(float a)
        {
            return P8API.abs(a);
        }

        public void printh(string str, string filename = "", bool overwrite = false)
        {
            P8API.printh(str, filename, overwrite);
        }

        public void sfx(int sfx, int channel = 0, int offset = 0)
        {
            P8API.sfx(sfx, channel, offset);
        }

        public void music(int song, int fadems = 0, int channelmask = 0)
        {
            P8API.music(song, fadems, channelmask);
        }

        public int dget(uint index)
        {
            return P8API.dget(index);
        }

        public void dset(uint index, int value)
        {
            P8API.dset(index, value);
        }

        public void cartdata(string id)
        {
            P8API.cartdata(id);
        }

        public void reloadmap(string map_string)
        {
            P8API.reloadmap(map_string);
        }

        public void menuitem(int index, string label, Action callback)
        {
            P8API.menuitem(index, label, callback);
        }

        public void menuitem(int index)
        {
            P8API.menuitem(index);
        }
    }
}
