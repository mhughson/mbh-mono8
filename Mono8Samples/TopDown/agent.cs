﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class agent : obj
    {
        public float fx = 0;
        public float fy = 1;

        protected float wx = 0;
        protected float wy = 1;

        protected int sp = 128;

        // TODO: Make near and far angles.
        float view_angle = 45.0f;

        public weap cur_weap { get; protected set; }

        protected int cur_dodge_time = 0;
        protected int dodge_time = 30;
        protected int cur_punch_time = 0;
        protected int punch_time = 10;

        protected bool btn_left;
        protected bool btn_right;
        protected bool btn_up;
        protected bool btn_down;
        protected bool btn_attack;
        protected bool btn_dodge;

        // How long has the dodge button been held. Used to determine
        // if it should trigger crawl.
        protected int btn_dodge_held_count = 0;

        protected float crawl_speed = 0.25f;
        protected float walk_speed = 0.25f; // Only used by AI to determine speed when patroling vs pursuing.
        public float run_speed = 0.75f;

        // The default speed of the player and the current move speed of AI.
        public float cur_move_speed
        {
            get
            {
                switch(cur_movement_type)
                {
                    case movement_types.crawl:
                        {
                            return crawl_speed;
                        }
                    case movement_types.run:
                        {
                            return run_speed;
                        }
                    case movement_types.walk:
                        {
                            return walk_speed;
                        }
                    default:
                        {
                            return 0;
                        }
                }
            }
        }

        protected float acc = 999.0f; //0.1f; // instant speed up
        protected float dcc = 0.0f; //0.8f; // instant slow down

        public enum movement_types
        {
            walk,
            run,
            crawl,
        }

        public movement_types cur_movement_type { get; protected set; }

        public bool is_crawling
        {
            get
            {
                return cur_movement_type == movement_types.crawl;
            }
        }

        protected type_flags attack_target_types = type_flags.None;

        public List<pickup> inventory { get; protected set; }

        public string anim_prefix = "soldier";

        public bool no_clip = false;

        protected bool in_water = false;
        float o2_levels = 1.0f;
        float o2_drop_speed = 1.0f / (60.0f * 10.0f);
        float o2_fill_speed = 1.0f / (60.0f * 2.5f);
        const float o2_min = -0.2f;

        public int security_level { get; set; }

        public agent(PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            w = 8;
            h = 16;
            killable = true;
            inventory = new List<pickup>();
            gfx_bank = 1;
            is_alive = true;

            #region OLD_ANIMS
            /*
            anims = new Dictionary<string, anim>()
                {
                    // IDLE

                
                    {
                        "soldier_idle_up_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(153,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_right_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(152,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_down_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(151,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_left_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(152,-1),//what frames are shown.
                        }
                    },


                    {
                        "soldier_idle_up_right_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(155,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_down_right_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(154,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_up_left_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(155,-1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_down_left_armed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(154,-1),
                        }
                    },

                    {
                        "soldier_idle_up_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(397,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_right_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(396,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_down_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(395,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_left_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(396,-1),//what frames are shown.
                        }
                    },


                    {
                        "soldier_idle_up_right_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(399,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_down_right_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(398,1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_up_left_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(399,-1),//what frames are shown.
                        }
                    },
                    {
                        "soldier_idle_down_left_unarmed",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= create_anim_sequence(398,-1),
                        }
                    },

                    // WALKING

                    {
                        "soldier_walk_up",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(112,4),
                        }
                    },
                    {
                        "soldier_walk_right",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(184,4),
                        }
                    },
                    {
                        "soldier_walk_down",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(118,4),
                        }
                    },
                    {
                        "soldier_walk_left",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(184,-4),
                        }
                    },

                    {
                        "soldier_walk_up_right",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(125,3),
                        }
                    },
                    {
                        "soldier_walk_down_right",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames= create_anim_sequence(122,3),
                        }
                    },
                    {
                        "soldier_walk_up_left",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames=  create_anim_sequence(125,-3),
                        }
                    },
                    {
                        "soldier_walk_down_left",
                        new anim()
                        {
                            ticks=10,//how long is each frame shown.
                            frames=create_anim_sequence(122,-3),
                        }
                    },


                    // RUNNING

                    {
                        "soldier_run_up_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(112,4),
                        }
                    },
                    {
                        "soldier_run_right_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(156,3),
                        }
                    },
                    {
                        "soldier_run_down_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(118,4),
                        }
                    },
                    {
                        "soldier_run_left_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(156,-3),
                        }
                    },

                    {
                        "soldier_run_up_right_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(125,3),
                        }
                    },
                    {
                        "soldier_run_down_right_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(122,3),
                        }
                    },
                    {
                        "soldier_run_up_left_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames=  create_anim_sequence(125,-3),
                        }
                    },
                    {
                        "soldier_run_down_left_armed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(122,-3),
                        }
                    },

                    // FIRING

                    {
                        "soldier_run_up_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(180,4),
                        }
                    },
                    {
                        "soldier_run_right_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(448,3),
                        }
                    },
                    {
                        "soldier_run_down_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(176,4),
                        }
                    },
                    {
                        "soldier_run_left_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(448,-3),
                        }
                    },

                    {
                        "soldier_run_up_right_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(329,3),
                        }
                    },
                    {
                        "soldier_run_down_right_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(361,3),
                        }
                    },
                    {
                        "soldier_run_up_left_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames=  create_anim_sequence(329,-3),
                        }
                    },
                    {
                        "soldier_run_down_left_armed_firing",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(361,-3),
                        }
                    },

                    {
                        "soldier_run_up_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(288,4),
                        }
                    },
                    {
                        "soldier_run_right_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(384,3),
                        }
                    },
                    {
                        "soldier_run_down_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(256,4),
                        }
                    },
                    {
                        "soldier_run_left_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(384,-3),
                        }
                    },

                    {
                        "soldier_run_up_right_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(352,3),
                        }
                    },
                    {
                        "soldier_run_down_right_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames= create_anim_sequence(320,3),
                        }
                    },
                    {
                        "soldier_run_up_left_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames=  create_anim_sequence(352,-3),
                        }
                    },
                    {
                        "soldier_run_down_left_unarmed",
                        new anim()
                        {
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(320,-3),
                        }
                    },

                    // PUNCH
                    {
                        "soldier_idle_up_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(318,1),
                        }
                    },
                    {
                        "soldier_idle_right_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(285,1),
                        }
                    },
                    {
                        "soldier_idle_down_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(286,1),
                        }
                    },
                    {
                        "soldier_idle_left_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(285,-1),
                        }
                    },


                    {
                        "soldier_idle_up_right_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(317,1),
                        }
                    },
                    {
                        "soldier_idle_down_right_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(350,1),
                        }
                    },
                    {
                        "soldier_idle_up_left_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(317,-1),
                        }
                    },
                    {
                        "soldier_idle_down_left_unarmed_firing",
                        new anim()
                        {
                            ticks=punch_time,//how long is each frame shown.
                            frames=create_anim_sequence(350,-1),
                        }
                    },

                    // ROLL
                    {
                        "soldier_roll_up",
                        new anim()
                        {
                            ticks=dodge_time/5,//how long is each frame shown.
                            frames=create_anim_sequence(293,5),
                        }
                    },
                    {
                        "soldier_roll_right",
                        new anim()
                        {
                            ticks=dodge_time/6,//how long is each frame shown.
                            frames=create_anim_sequence(388,6),
                        }
                    },
                    {
                        "soldier_roll_down",
                        new anim()
                        {
                            ticks=dodge_time/5,//how long is each frame shown.
                            frames=create_anim_sequence(261,5),
                        }
                    },
                    {
                        "soldier_roll_left",
                        new anim()
                        {
                            ticks=dodge_time/6,//how long is each frame shown.
                            frames=create_anim_sequence(388,-6),
                        }
                    },


                    {
                        "soldier_roll_up_right",
                        new anim()
                        {
                            ticks=dodge_time/5,//how long is each frame shown.
                            frames=create_anim_sequence(356,5),
                        }
                    },
                    {
                        "soldier_roll_down_right",
                        new anim()
                        {
                            ticks=dodge_time/5,//how long is each frame shown.
                            frames=create_anim_sequence(324,5),
                        }
                    },
                    {
                        "soldier_roll_up_left",
                        new anim()
                        {
                            ticks=dodge_time/5,//how long is each frame shown.
                            frames=create_anim_sequence(356,-5),
                        }
                    },
                    {
                        "soldier_roll_down_left",
                        new anim()
                        {
                            ticks=dodge_time/5,//how long is each frame shown.
                            frames=create_anim_sequence(324,-5),
                        }
                    },

                    // crawl
                    {
                        "soldier_crawl_up",
                        new anim()
                        {
                            h=24,
                            ticks=15,//how long is each frame shown.
                            frames=create_anim_sequence(451,4,1),
                        }
                    },
                    {
                        "soldier_crawl_right",
                        new anim()
                        {
                            w = 16,
                            h = 16,
                            ticks=15,//how long is each frame shown.
                            frames=new int[][]
                            {
                                new int[] { 0, 0, 500, 501, },
                                new int[] { 0, 0, 502, 503, },
                                new int[] { 0, 0, 504, 505, },
                                new int[] { 0, 0, 506, 507, },
                                new int[] { 0, 0, 508, 509, },
                            },
                        }
                    },
                    {
                        "soldier_crawl_down",
                        new anim()
                        {
                            h = 24,
                            ticks=15,//how long is each frame shown.
                            frames=create_anim_sequence(455,4,1),
                        }
                    },
                    {
                        "soldier_crawl_left",
                        new anim()
                        {
                            w = 16,
                            h = 16,
                            ticks=15,//how long is each frame shown.
                            frames=new int[][]
                            {
                                new int[] { 0, 0, -501, -500, },
                                new int[] { 0, 0, -503, -502, },
                                new int[] { 0, 0, -505, -504, },
                                new int[] { 0, 0, -507, -506, },
                                new int[] { 0, 0, -509, -508, },
                            },
                        }
                    },


                    {
                        "soldier_crawl_up_right",
                        new anim()
                        {
                            h = 24,
                            ticks=15,//how long is each frame shown.
                            frames=create_anim_sequence(480,-4, 1),
                        }
                    },
                    {
                        "soldier_crawl_down_right",
                        new anim()
                        {
                            h = 24,
                            ticks=15,//how long is each frame shown.
                            frames=create_anim_sequence(459,-4, 1),
                        }
                    },
                    {
                        "soldier_crawl_up_left",
                        new anim()
                        {
                            h = 24,
                            ticks=15,//how long is each frame shown.
                            frames=create_anim_sequence(480,4,1),
                        }
                    },
                    {
                        "soldier_crawl_down_left",
                        new anim()
                        {
                            h=24,
                            ticks=15,//how long is each frame shown.
                            frames=create_anim_sequence(459,4,1),
                        }
                    },

                    // idle crawl
                    {
                        "soldier_idle_crawl_up",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(451,1,1),
                        }
                    },
                    {
                        "soldier_idle_crawl_right",
                        new anim()
                        {
                            w = 16,
                            h = 16,
                            ticks=5,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, 500, 501, },
                            },
                        }
                    },
                    {
                        "soldier_idle_crawl_down",
                        new anim()
                        {
                            h = 24,
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(455,1,1),
                        }
                    },
                    {
                        "soldier_idle_crawl_left",
                        new anim()
                        {
                            w = 16,
                            h = 16,
                            ticks=5,//how long is each frame shown.
                            frames= new int[][]
                            {
                                new int[] { 0, 0, -501, -500, },
                            },
                        }
                    },


                    {
                        "soldier_idle_crawl_up_right",
                        new anim()
                        {
                            h = 24,
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(480,-1, 1),
                        }
                    },
                    {
                        "soldier_idle_crawl_down_right",
                        new anim()
                        {
                            h = 24,
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(459,-1,1),
                        }
                    },
                    {
                        "soldier_idle_crawl_up_left",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(480,1,1),
                        }
                    },
                    {
                        "soldier_idle_crawl_down_left",
                        new anim()
                        {
                            h=24,
                            ticks=5,//how long is each frame shown.
                            frames=create_anim_sequence(459,1,1),
                        }
                    },
                };
                */
            #endregion
            //cur_weap = new weap(0, 0, Game);

            set_anim(anim_prefix + "_standing_atease_rifle_s");
        }
        static public int[][] create_anim_sequence(int start, int count, int top_buffer_count = 0)
        {
            int dir = 1;
            if (count < 0)
            {
                dir = -1;
                count = -count;
            }
            int[][] frames = new int[count][];

            for (int i = 0; i < count; i++)
            {
                frames[i] = new int[2 + top_buffer_count];
                int j = 0;
                for (; j < top_buffer_count; j++)
                {
                    frames[i][j] = 0;
                }
                frames[i][j] = (start + i) * dir;
                frames[i][j+1] = (start + i + 16) * dir;
                //frames[i] = new int[] { (start + i) * dir, (start + i + 16) * dir };
            }

            return frames;
        }

        public override void update()
        {
            if (!is_alive)
            {
                // Water ripples.
                if (in_water)
                {
                    // TODO: Don't duplicate this.
                    new line_particle(x + Game.rnd(8) - 4, y + Game.rnd(8) - 4, dx * 0.3f, dy * 0.3f, 0, 3, 0, 1, Game) { lifetime = 30 }.activate();
                    //new line_particle(x + Game.rnd(8) - 4, y, 0, 0, 0, 3, 0, 1, Game) { lifetime = 30 }.activate();
                }

                return;
            }

            var self = this;
            float old_x = x;
            float old_y = y;
            var move_x = false;

            cur_dodge_time = (int)Game.max(cur_dodge_time - 1, 0);
            cur_punch_time = (int)Game.max(cur_punch_time - 1, 0);

            if (cur_dodge_time <= 0 && cur_punch_time <= 0)
            {
                if (btn_left)
                {
                    if (!Game.first_person_view_enabled)
                    {
                        dx = Game.max(-cur_move_speed, dx - acc);

                        move_x = true;
                        if (!btn_attack)
                        {
                            fx = -1;
                        }
                        wx = -1;
                    }
                    else
                    {
                        Vector2 res = Game.rot_vec(fx, fy, 0.01f);
                        fx = wx = res.X;
                        fy = wy = res.Y;
                    }
                }
                if (btn_right)
                {
                    if (!Game.first_person_view_enabled)
                    {
                        dx = Game.min(cur_move_speed, dx + acc);

                        move_x = true;

                        if (!btn_attack)
                        {
                            fx = 1;
                        }
                        wx = 1;
                    }
                    else
                    {
                        Vector2 res = Game.rot_vec(fx, fy, -0.01f);
                        fx = wx = res.X;
                        fy = wy = res.Y;
                    }
                }
            }

            if (!move_x && cur_dodge_time <= 0)
            {
                dx *= dcc;
            }


            var move_y = false;

            if (cur_dodge_time <= 0 && cur_punch_time <= 0)
            {
                if (btn_up)
                {
                    if (!Game.first_person_view_enabled)
                    {
                        dy = Game.max(-cur_move_speed, dy - acc);

                        move_y = true;
                        if (!btn_attack)
                        {
                            fy = -1;
                        }
                        wy = -1;
                    }
                    else
                    {
                        Vector2 forward = new Vector2(fx, fy);
                        forward.Normalize();

                        dx += forward.X * acc;
                        dy += forward.Y * acc;

                        Vector2 vel = new Vector2(dx, dy);
                        if (vel.Length() > cur_move_speed)
                        {
                            vel.Normalize();
                            vel *= cur_move_speed;
                            dx = vel.X;
                            dy = vel.Y;
                        }
                    }
                }
                if (btn_down)
                {
                    if (!Game.first_person_view_enabled)
                    {
                        dy = Game.min(cur_move_speed, dy + acc);

                        move_y = true;
                        if (!btn_attack)
                        {
                            fy = 1;
                        }
                        wy = 1;
                    }

                    else
                    {
                        Vector2 forward = new Vector2(fx, fy);
                        forward.Normalize();

                        dx += forward.X * -acc;
                        dy += forward.Y * -acc;

                        Vector2 vel = new Vector2(dx, dy);
                        if (vel.Length() > cur_move_speed)
                        {
                            vel.Normalize();
                            vel *= cur_move_speed;
                            dx = vel.X;
                            dy = vel.Y;
                        }
                    }
                }
            }

            if (!move_y && cur_dodge_time <= 0)
            {
                dy *= dcc;
            }

            bool can_stand = false;
            if (Game.game_world.cur_area.fget((int)(col_box().x / 8.0f), (int)(col_box().y / 8.0f)) == area.collision_flag.none)
            {
                can_stand = true;
            }

            int roll_time_req = dodge_time;
            if (btn_dodge && cur_cover == cover_dir.none)
            {
                // On dodge button PRESSED.
                if (cur_dodge_time <= 0 && btn_dodge_held_count == 0)
                {
                    // If already crawling, stand up right away.
                    if (is_crawling)
                    {
                        if (can_stand)
                        {
                            // TODO: This won't work for agents which change speed (like AI).
                            cur_movement_type = movement_types.run;
                        }
                    }
                    else
                    {
                        // Start dodge roll right away. This will 
                        // transition into crawl if button is held.
                        cur_dodge_time = dodge_time;
                        // Uncomment to change facing direction with roll.
                        dx = /*fx = */wx;
                        dy = /*fy = */wy;
                    }
                }

                // On dodge button HELD for long period.
                if (btn_dodge_held_count == roll_time_req)
                {
                    // Stop dodging.
                    cur_dodge_time = 0;

                    // Change speeds.
                    if (!is_crawling)
                    {
                        cur_movement_type = movement_types.crawl;
                    }
                    else if (is_crawling)
                    {
                        if (can_stand)
                        {
                            cur_movement_type = movement_types.run;
                        }
                    }
                }

                // Keep track of how long the button has been held.
                btn_dodge_held_count++;
            }
            else if (btn_dodge_held_count > 0)
            {
                btn_dodge_held_count = 0;
            }

            if (move_x && !move_y)
            {
                if (!btn_attack)
                {
                    fy = 0;
                }
                wy = 0;
            }
            else if (move_y && !move_x)
            {
                if (!btn_attack)
                {
                    fx = 0;
                }
                wx = 0;
            }

            x += dx;
            y += dy;

            // before performing collision, update which views are visible, so that the
            // player will not get stuck in a view.
            if (type_flag == type_flags.Player)
            {
                Game.game_world.cur_area.update_cur_view();
            }

            bool hl = false;
            bool hr = false;
            bool ht = false;
            bool hb = false;

            if (!no_clip)
            {
                area.collision_flag flag = is_crawling ? area.collision_flag.low : area.collision_flag.all;

                Vector2 correction_pos = Vector2.Zero;
                Vector2 unused = Vector2.Zero;
                hl = Game.hit_left(self, ref unused, ref correction_pos, flag);
                hr = Game.hit_right(self, ref unused, ref correction_pos, flag);
                if (hl || hr)
                {
                    dx = 0;
                    x = correction_pos.X;
                }

                ht = Game.hit_top(self, ref unused, ref correction_pos, flag);
                hb = Game.hit_bottom(self, ref unused, ref correction_pos, flag);
                if (ht || hb)
                {
                    dy = 0;
                    y = correction_pos.Y;
                }
                post_resolve_wall_collisions(hl, hr, ht, hb);
            }

            // Interact with objects before firing a weapon.
            bool interaction_performed = false;
            if (btn_attack)
            {
                for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    obj o = Game.game_world.cur_area.objs_active[i];
                    if (o != this && o.can_interact(this))
                    {
                        if (o.col_box().intersects(col_box()))
                        {
                            o.on_interact();
                            interaction_performed = true;
                        }
                    }
                }
            }

            bool fired_this_frame = false;
            if (cur_weap != null && !interaction_performed && cur_cover == cover_dir.none)
            {
                fired_this_frame = cur_weap.start_fire(btn_attack && cur_dodge_time <= 0, this, fx, fy, attack_target_types);
                if (fired_this_frame)
                {
                    if (cur_weap.weapon_type == weap.weap_type.melee)
                    {
                        cur_punch_time = punch_time;
                        dx = 0;
                        dy = 0;
                        x = old_x;
                        y = old_y;
                    }
                }
            }

            // FORMAT:  character_bodystate_weaponstate_weapontype_direction
            //          soldier_standing_atease_pistol_s
            //          

            // ANIM_BODYSTATE
            //

            string state = anim_prefix + "_standing";
            bool show_weap = true;
            // TODO: Show firing when attacking with something other than melee.
            bool show_firing = false;
            bool moving = Game.abs(dx) > 0.1f || Game.abs(dy) > 0.1f;

            if (cur_dodge_time > 0)
            {
                state = anim_prefix + "_rolling";
                show_weap = false;
            }
            else if (cur_punch_time > 0)
            {
                show_weap = true;
                show_firing = true;
            }
            else if (is_crawling)
            {
                if (!moving)
                {
                    state = anim_prefix + "_prone";
                }
                else
                {
                    state = anim_prefix + "_crawling";
                }
                show_weap = false;
                show_firing = false;
            }
            else if (cur_move_speed >= 0.5f && moving)//Game.abs(dx) > 0.25f || Game.abs(dy) > 0.25f)
            {
                state = anim_prefix + "_running";
                show_weap = true;
                show_firing = true;
                if (Game.tick % 20 == 0)
                {
                    new sense_emit(Game)
                    {
                        x = col_box().x,
                        y = col_box().y,
                        radius = 16,
                        radial_velocity = 0,
                        lifetime = 4,
                        creator = this,
                    }.activate();
                }
            }
            else if (cur_move_speed >= 0.25f && moving) // Game.abs(dx) > 0.1f || Game.abs(dy) > 0.1f)
            {
                state = anim_prefix + "_walking";
                show_weap = false;
            }
            else if (!moving && cur_weap != null && cur_weap.weapon_type != weap.weap_type.melee && this.type_flag != type_flags.Player  && Game.game_world.get_global_alert_state() != world.alert_states.calm)
            {
                state = anim_prefix + "_crouched";
                show_weap = true;
                show_firing = true;
            }

            float round_fx = (int)Math.Round(fx);
            float round_fy = (int)Math.Round(fy);



            //soldier_incover_atease_melee_s

            bool cover_set = false;
            // Bit of a hack to override the animation when pressed against a wall.
            if ((hl && btn_left && !btn_up && !btn_down && btn_dodge == true) || (cur_cover == cover_dir.left && !btn_right && !btn_up && !btn_down))
            {
                cover_set = true;
                cur_cover = cover_dir.left;
                state = anim_prefix + "_incover";
            }

            if ((hr && btn_right && !btn_up && !btn_down && btn_dodge == true) || (cur_cover == cover_dir.right && !btn_left && !btn_up && !btn_down))
            {
                cover_set = true;
                cur_cover = cover_dir.right;
                state = anim_prefix + "_incover";
            }

            if ((ht && btn_up && !btn_left && !btn_right && btn_dodge == true) || (cur_cover == cover_dir.up && !btn_down && !btn_left && !btn_right))
            {
                cover_set = true;
                cur_cover = cover_dir.up;
                state = anim_prefix + "_incover";
            }

            if ((hb && btn_down && !btn_left && !btn_right && btn_dodge == true) || (cur_cover == cover_dir.down && !btn_up && !btn_left && !btn_right))
            {
                cover_set = true;
                cur_cover = cover_dir.down;
                state = anim_prefix + "_incover";
            }

            // ANIM_WEAPONSTATE
            // ANIM_WEAPONTYPE
            //

            //if (show_weap)
            {
                if (cur_weap == null)
                {
                    state += "atease_melee";
                }
                else
                {
                    if (show_firing && cur_weap.bullet_delay < 10)
                    {
                        state += "_firing";
                    }
                    else
                    {
                        state += "_atease";
                    }

                    if (cur_weap.weapon_type == weap.weap_type.melee)
                    {
                        state += "_melee";
                    }
                    else if (cur_weap.weapon_type == weap.weap_type.machine_gun)
                    {
                        state += "_rifle";
                    }
                    else
                    {
                        state += "_pistol";
                    }
                }
            }

            if (!cover_set)
            {
                cur_cover = cover_dir.none;
            }
            else
            {
                cur_dodge_time = 0;
            }

            // ANIM_DIRECTION

            if (cover_set)
            {
                switch(cur_cover)
                {
                    case cover_dir.down:
                        {
                            state += "_s";
                            break;
                        }
                    case cover_dir.up:
                        {
                            state += "_n";
                            break;
                        }
                    case cover_dir.right:
                        {
                            state += "_e";
                            break;
                        }
                    case cover_dir.left:
                        {
                            state += "_w";
                            break;
                        }
                }
            }
            else if (round_fx < 0 && round_fy < 0) state += "_nw";
            else if (round_fx > 0 && round_fy < 0) state += "_ne";
            else if (round_fx > 0 && round_fy > 0) state += "_se";
            else if (round_fx < 0 && round_fy > 0) state += "_sw";
            else if (round_fx < 0) state += "_w";
            else if (round_fx > 0) state += "_e";
            else if (round_fy < 0) state += "_n";
            else if (round_fy > 0) state += "_s";
            else state += "_s"; // edge case of no direction.


            // ANIM_SET
            //

            set_anim(state);

            // Now that the animation is set, we can spawn the bullets with the proper offsets.
            if (fired_this_frame)
            {
                cur_weap.complete_fire(this, fx, fy, attack_target_types);
            }

            // Water ripples.
            if (in_water)
            {
                new line_particle(x + Game.rnd(8) - 4, y + Game.rnd(8) - 4, dx * 0.3f, dy * 0.3f, 0, 3, 0, 1, Game) { lifetime = 30 }.activate();
                //new line_particle(x + Game.rnd(8) - 4, y, 0, 0, 0, 3, 0, 1, Game) { lifetime = 30 }.activate();
            }

            float submerge_amount = 8;
            if (!in_water)
            {
                //float old_y = y;
                int tile = Game.mget(Game.flr(col_box().x / 8.0f), Game.flr(col_box().y / 8.0f));
                switch (tile)
                {
                    case 322:
                    case 355:
                    case 356:
                    case 357:
                    case 358:
                        {
                            y += submerge_amount;
                            sunken_amount = submerge_amount;
                            in_water = true;
                            break;
                        }
                }
            }
            else
            {
                // Check where we WILL be if we leave water.
                int tile = Game.mget(Game.flr(col_box().x / 8.0f), Game.flr((col_box().y - submerge_amount) / 8.0f));
                switch (tile)
                {
                    case 322:
                    case 355:
                    case 356:
                    case 357:
                    case 358:
                        {
                            break;
                        }
                    default:
                        {

                            in_water = false;
                            y -= 8;
                            sunken_amount = 0;
                        }
                        break;
                }

                if (in_water)
                {
                    if (is_crawling)
                    {
                        sunken_amount = 16;
                    }
                    else
                    {
                        sunken_amount = 8;
                    }
                }
            }

            if (in_water && is_crawling) // can't breath
            {
                // Go below zero to allow make the player feel better if they die.
                o2_levels = Game.max(o2_min, o2_levels - o2_drop_speed);
                if (o2_levels <= o2_min)
                {
                    on_death(this);
                }
            }
            else
            {
                // Note: Jumps right up to 0 when previous < 0. This is intentional.
                o2_levels = Game.mid(0.0f, o2_levels + o2_fill_speed, 1.0f);
            }

            base.update();
        }

        protected enum cover_dir
        {
            none = -1,
            left = 0,
            right = 1,
            up = 2,
            down = 3,
        }
        protected cover_dir cur_cover = cover_dir.none;

        public override void draw()
        {
            #region OLD
            //var self = this;
            ////Game.camera(x - 64, y - 64);

            //bool flip_x = false;
            //if (Game.abs(dx) > 0.1 || Game.abs(dy) > 0.1)
            //{
            //    flip_x = Game.tick % 10 < 5;
            //}

            /*
            obj o;
            float a;
            if (find_target(out o, out a))
            {
                Game.print_d("spotted", x, y, 0);
            }
            */

            //Game.print_d(curanim, x, y, 0);

            //Game.spr(sp, x, y, 1, 2, flip_x, false);


            //for (int i = 0; i < 100; i++)
            //{
            //    Game.pal(i, 15+4);
            //}

            //var old_x = x;
            //var old_y = y;

            //this.x = old_x;
            //this.y = old_y - 1;
            //base.draw();

            //this.x = old_x;
            //this.y = old_y + 1;
            //base.draw();

            //this.x = old_x - 1;
            //this.y = old_y;
            //base.draw();

            //this.x = old_x + 1;
            //this.y = old_y;
            //base.draw();

            //this.x = old_x;
            //this.y = old_y;

            //for (int i = 0; i < 100; i++)
            //{
            //    Game.pal(i, i);
            //}
            #endregion
            if (is_alive)
            {
                base.draw();

                if (o2_levels < 1.0f && o2_levels > o2_min)
                {
                    int col = 24;
                    if (o2_levels <= 0.0f)
                    {
                        col = Game.tick % 3 == 0 ? 15 : 14;
                    }
                    else if (o2_levels < 0.1f)
                    {
                        col = 15;
                    }
                    else if (o2_levels < 0.2f)
                    {
                        col = 16;
                    }
                    else if(o2_levels < 0.3f)
                    {
                        col = 17;
                    }

                    const float full_bar_width = 16.0f;
                    const float full_bar_width_half = full_bar_width * 0.5f;
                    float line_width = Game.max(0.0f, (float)Math.Round(o2_levels * full_bar_width) - 1); // - 1 to account of line drawing
                    float line_width_half = line_width * 0.5f;
                    Game.rectfill(x - full_bar_width_half - 1, y - 12, x + full_bar_width_half, y - 10, 31);
                    Game.line(x - full_bar_width_half, y - 11, x - full_bar_width_half + line_width, y - 11, col);
                }
            }

            // Draw the field of view.
            float angle = view_angle / 360.0f;
            Vector2 v1 = Game.rot_vec(fx, fy, angle);
            Vector2 v2 = Game.rot_vec(fx, fy, -angle);
            Game.line_d(x, y, x + (v1.X * 128), y + (v1.Y * 128), 0);
            //Game.line_d(x, y, x + (fx * 128), y + (fy * 128), 0);
            Game.line_d(x, y, x + (v2.X * 128), y + (v2.Y * 128), 0);

            /*
            offset o = bullet_offsets[(int)fx][(int)fy];
            Game.pset(x + o.x, y + o.y, 14);
            */
        }

        public override rect col_box()

        {
            return new rect(
                x, 
                y + 5, 
                w - 2,
                6);
        }

        public override void on_death(obj source)
        {
            base.on_death(source);
            generate_kill_fx(source);
            if (source as bullet != null)
            {
                generate_blood_explosion(source);
            }
            is_alive = false;

            if (cur_weap != null)
            {
                //cur_weap.drop();
            }
        }

        public virtual kill_fx generate_kill_fx(obj source)
        {
            kill_fx fx;
            if (!in_water)
            {
                fx = new kill_fx(x, y, source.dx, source.dy, Game);
            }
            else
            {
                fx = new kill_drowned_fx(x, y, source.dx, source.dy, Game);
            }

            fx.activate();
            return fx;
        }

        public virtual void on_sense(sense_emit source)
        {
            
        }

        public override bool is_killable_now()
        {
            return base.is_killable_now() && is_alive && cur_dodge_time <= 0;
        }

        public bool add_to_inventory(pickup new_pickup)
        {
            // If we are picking up a weapon, check if we have that weapon already, and if so,
            // grab the ammo, not the weapon itself.
            weap new_weap = new_pickup as weap;
            if (new_weap != null)
            {
                foreach (var item in inventory)
                {
                    weap existing_weap = item as weap;
                    if (existing_weap != null)
                    {
                        // Are they the same type?
                        if (existing_weap.weapon_type == new_weap.weapon_type)
                        {
                            // Now if this weapon doesn't use ammo, there is nothing to do, and we should,
                            // just destroy the pickup.
                            if (!new_weap.uses_ammo)
                            {
                                new_weap.destroy();
                                return false;
                            }
                            else
                            {
                                int space = existing_weap.ammo_max - existing_weap.ammo_remaining;
                                int take_ammount = (int)Game.min(space, new_weap.ammo_remaining);
                                existing_weap.ammo_remaining = (int)Game.min(existing_weap.ammo_max, existing_weap.ammo_remaining + take_ammount);
                                if (existing_weap.cur_mag <= 0 && existing_weap.ammo_remaining > 0)
                                {
                                    existing_weap.cur_mag = (int)Game.min(existing_weap.ammo_remaining, existing_weap.magazine_size);
                                }
                                new_weap.ammo_remaining = (int)Game.max(0, new_weap.ammo_remaining - take_ammount);
                                if (new_weap.ammo_remaining <= 0)
                                {
                                    new_weap.destroy();
                                }
                                return false;
                            }
                        }
                    }
                }
            }

            inventory.Add(new_pickup);

            if (Game.game_world.cur_area.game_mode == area.game_modes.action)
            {
                if(new_pickup.GetType() == typeof(weap))
                {
                    if (cur_weap == null)
                    {
                        cur_weap = new_pickup as weap;
                    }
                    else
                    {
                        //cur_weap.drop();
                        //remove_from_inventory(cur_weap);
                        activate_weap(new_pickup as weap);
                    }
                }
            }

            if (cur_weap == null && new_pickup.GetType() == typeof(weap))
            {
                cur_weap = new_pickup as weap;
            }

            return true;
        }

        public void remove_from_inventory(pickup p)
        {
            inventory.Remove(p);
            if (p == cur_weap)
            {
                cur_weap = null;
            }
        }

        public void activate_weap(weap new_weap)
        {
            if (new_weap != null)
            {
                cur_weap = new_weap;
            }
        }

        public override void on_destroy()
        {
            // If we are still holding a weapon at this point, death has not resulting in dropping
            // it, so we need to just destroy it so that it doesn't get left in a persistent state.
            if (cur_weap != null)
            {
                cur_weap.destroy();
            }
            base.on_destroy();
        }

        public bool find_target(out obj target, out float angle)
        {
            Vector2 forward = new Vector2(fx, fy);
            forward.Normalize();
            //Game.line_d(x, y, x + forward.X * 16, y + forward.Y * 16, 15);

            if (GetType() == typeof(player) || Game.game_world.cur_area.light_status == area.lighting_status.off)
            {
                target = null;
                angle = 0;
                return false;
            }
            for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
            {
                obj o = Game.game_world.cur_area.objs_active[i];
                if (o != this && /*o.is_killable_now() &&*/ o.is_alive && o.type_flag != type_flags.None && attack_target_types.HasFlag(o.type_flag) && o.is_detectable(this))
                {
                    //walk_tiles_to_target(o);
                    float a = angle_to_target(forward, o);

                    if (Game.abs(a) < view_angle)
                    {
                        // Better algorithm which uses line drawing algo, but makes checking for 
                        // space which bullet can fit through tough.
                        #region line_check
                        //// Agent.
                        //float x0 = (float)Math.Round(col_box().x / 8.0f);
                        //float y0 = (float)Math.Round(col_box().y / 8.0f);
                        //// Target.
                        //float x1 = (float)Math.Round(o.col_box().x / 8.0f);
                        //float y1 = (float)Math.Round(o.col_box().y / 8.0f);

                        //float w = x1 - x0;
                        //float h = y1 - y0;
                        //float dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
                        //if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
                        //if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
                        //if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
                        //int longest = (int)Math.Abs(w);
                        //int shortest = (int)Math.Abs(h);
                        //if (!(longest > shortest))
                        //{
                        //    longest = (int)Math.Abs(h);
                        //    shortest = (int)Math.Abs(w);
                        //    if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                        //    dx2 = 0;
                        //}
                        //int numerator = longest >> 1;
                        //bool spotted = true;
                        //for (int j = 0; j <= longest; j++)
                        //{
                        //    //pset(x0, y0, col);
                        //    Game.rect_d(x0 * 8, y0 * 8, x0 * 8 + 7, y0 * 8 + 7, 19);

                        //    // COLLISION CHECK GOES HERE
                        //    if (Game.game_world.cur_area.fget((int)(x0), (int)(y0), area.collision_flag.high))
                        //    {
                        //        Game.rect_d(x0 * 8, y0 * 8, x0 * 8 + 7, y0 * 8 + 7, 15);
                        //        spotted = false;
                        //        break;
                        //    }

                        //    numerator += shortest;
                        //    if (!(numerator < longest))
                        //    {
                        //        numerator -= longest;
                        //        x0 += dx1;
                        //        y0 += dy1;
                        //    }
                        //    else
                        //    {
                        //        x0 += dx2;
                        //        y0 += dy2;
                        //    }
                        //}
                        #endregion

                        Vector2 other_pos = new Vector2(o.col_box().x, o.col_box().y);
                        Vector2 check_pos = new Vector2(col_box().x, col_box().y);
                        Vector2 to_other = new Vector2(other_pos.X - check_pos.X, other_pos.Y - check_pos.Y);
                        to_other.Normalize();

                        bool spotted = true;
                        while (Vector2.Distance(other_pos, check_pos) > 8.0f)
                        {
                            check_pos += to_other * 8;
                            //Game.rect_d(check_pos.X - 4, check_pos.Y - 4, check_pos.X + 4, check_pos.Y + 4, 23);

                            // Make sure there is enough room for bullets.
                            for (int dx = -3; dx <= 3; dx++)
                            {
                                for (int dy = -3; dy <= 3; dy++)
                                {
                                    if (Game.game_world.cur_area.fget((int)(check_pos.X + dx) / 8, (int)(check_pos.Y + dy) / 8, area.collision_flag.high))
                                    {
                                        //Game.rect_d(check_pos.X - 4, check_pos.Y - 4, check_pos.X + 4, check_pos.Y + 4, 15);
                                        spotted = false;
                                        break;
                                    }
                                }
                            }
                        }

                        if (spotted)
                        {
                            target = o;
                            angle = a;
                            return true;
                        }
                    }
                }
            }

            target = null;
            angle = float.MaxValue;
            return false;
        }

        public float angle_to_target(Vector2 forward, obj target)
        {
            //Game.line_d(x, y, target.x, target.y, 8);
            Vector2 to_other = new Vector2(target.x - x, target.y - y);
            to_other.Normalize();
            //float angle = MathHelper.ToDegrees((float)Math.Atan2(forward.Y, forward.X) - (float)Math.Atan2(to_other.Y, to_other.X));
            float dot = MathHelper.ToDegrees((float)Math.Acos(MathHelper.Clamp(Vector2.Dot(to_other, forward), -1.0f, 1.0f)));
            Game.print_d(dot.ToString(), x + 4, y - 8, 0);
            return dot;
        }

        public void walk_tiles_to_target(obj target)
        {
            // Start at the center of the tile the object is on.
            float x0 = (float)Math.Round(x / 8.0f) * 8.0f + 4.0f;
            float y0 = (float)Math.Round(y / 8.0f) * 8.0f + 4.0f;

            float x1 = target.x;
            float y1 = target.y;

            float w = x1 - x0;
            float h = y1 - y0;

            float step = 8;

            float dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -step; else if (w > 0) dx1 = step;
            if (h < 0) dy1 = -step; else if (h > 0) dy1 = step;
            if (w < 0) dx2 = -step; else if (w > 0) dx2 = step;
            int longest = (int)Math.Abs(w);
            int shortest = (int)Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = (int)Math.Abs(h);
                shortest = (int)Math.Abs(w);
                if (h < 0) dy2 = -step; else if (h > 0) dy2 = step;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i+=(int)step)
            {
                Game.pset(x0, y0, 7);
                Game.rect(x0 - 4, y0 - 4, x0 + 4, y0 + 4, 7);
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x0 += dx1;
                    y0 += dy1;
                }
                else
                {
                    x0 += dx2;
                    y0 += dy2;
                }
            }
        }

        public void generate_blood_explosion(obj source)
        {
            //how long to make the streaks.
            var h = 2;

            ////legs
            //new line_particle(x, y,
            //    -dx, -dy, 9, h, 0.1f, 1, Game).activate();
            //new line_particle(x, y,
            //    -dx, -dy - 2, 9, h, 0.2f, 1, Game).activate();
            //new line_particle(x, y,
            //    dx, -dy, 9, h, 0.2f, 1, Game).activate();
            //new line_particle(x, y,
            //    dx, -dy - 2, 9, h, 0.1f, 1, Game).activate();

            ////head
            //new line_particle(x, y,
            //    -dx + 1, -dy, 12, h, 0.2f, 1, Game).activate();
            //new line_particle(x, y,
            //    dx + 1, -dy, 12, h, 0.1f, 1, Game).activate();

            ////arms
            //new line_particle(x, y,
            //    -dx, -dy - 1, 7, h, 0.1f, 1, Game).activate();
            //new line_particle(x, y,
            //    -dx + 2, -dy - 1, 7, h, 0.2f, 1, Game).activate();
            //new line_particle(x, y,
            //    dx, -dy - 1, 7, h, 0.2f, 1, Game).activate();
            //new line_particle(x, y,
            //    dx + 2, -dy - 1, 7, h, 0.1f, 1, Game).activate();

            //flying blood
            int life = 30;
            int col1 = 15;
            int col2 = 14;
            new line_particle(source.x, source.y,
                source.dx, source.dy, col1, h, 0.05f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();
            new line_particle(source.x, source.y,
                source.dx * 2, source.dy + Game.rnd(2) - 1, col1, h, 0.05f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();
            new line_particle(source.x, source.y,
                source.dx + Game.rnd(2) - 1, source.dy * 2, col2, h * 4, 0.1f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();
            new line_particle(source.x, source.y,
                source.dx * 2 + Game.rnd(2) - 1, source.dy * 2 + Game.rnd(2) - 1, col2, h * 2, 0.05f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();

            //blood
            //new line_particle(x, y, dx, dy, 8,
            //    100, 0, 0.95f, Game).activate();
            //new line_particle(x, y, dx * 0.1f, dy, 8,
            //    100, 0, 0.9f, Game).activate();
            //new line_particle(x, y, dx, dy - 1, 8,
            //    100, 0, 0.9f, Game).activate();
        }

        protected virtual void post_resolve_wall_collisions(bool hit_left, bool hit_right, bool hit_top, bool hit_bottom) { }

        protected void clear_btns()
        {
            btn_left = false;
            btn_right = false;
            btn_up = false;
            btn_down = false;
            btn_attack = false;
            btn_dodge = false;
        }

        public override bool is_detectable(agent detector)
        {
            return !is_crawling || !in_water;
        }

        public Vector2? get_bullet_spawn_point()
        {
            if (!string.IsNullOrEmpty(curanim))
            {
                var dfanim = Game.game_anims.get_anim(curanim);
                if (dfanim != null)
                {
                    var c = dfanim.cells[curframe];
                    {
                        // Handle case where frame has no sprites in it (eg. door opening)
                        if (c.sprites != null)
                        {
                            foreach (var sp in c.sprites)
                            {
                                var s = Game.game_anims.get_sprite(sp.name);
                                if (s != null && s.name.StartsWith("slot_fire_point"))
                                {
                                    var final_w_half = Game.flr(s.w * 0.5f);
                                    var final_h_half = Game.flr(s.h * 0.5f);

                                    //var start_x = x + s.x - (final_w_half);
                                    //var start_y = y + s.y - (final_h_half);

                                    // note: this seems like it should be offset by half the dimensions of the sprite, but
                                    //       for some reason that spawn the bullet on the right pixel. Probably a rounding
                                    //       issue somewhere.
                                    return new Vector2(sp.x + final_w_half, sp.y + final_h_half); 
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }
    }
}
