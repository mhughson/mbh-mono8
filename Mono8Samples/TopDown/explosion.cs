﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;

namespace Mono8Samples.TopDown
{
    class explosion : obj
    {
        obj source;

        int radius;

        public bool is_primary = false;

        public explosion(int radius, PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            this.source = this; // temp. source isn't really needed anymore.
            this.radius = radius;
        }

        public override void on_activated()
        {
            base.on_activated();

            if (is_primary)
            {
                generate_particles(source);
            }

            // +1 to draw on top.
            new explosion_fx(x, y, Game) { draw_layer = 1 }.activate();

            Game.mset((int)(x / 8), (int)((y + 4) / 8), 284, (int)TopDown.map_layers.shadow);

            destroy();
        }

        public void generate_particles(obj source)
        {
            //how long to make the streaks.
            var h = 2;

            //
            int life = 15;
            int col1 = 0;
            int col2 = 16;
            int col3 = 18;

            new line_particle(source.x, source.y,
                source.dx, source.dy, col1, h, 0.05f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();
            new line_particle(source.x, source.y,
                source.dx * 2, source.dy + Game.rnd(2) - 1, col1, h, 0.05f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();

            new line_particle(source.x, source.y,
                source.dx + Game.rnd(2) - 1, source.dy * 2, col2, h * 4, 0.1f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();
            new line_particle(source.x, source.y,
                source.dx * 2 + Game.rnd(2) - 1, source.dy * 2 + Game.rnd(2) - 1, col2, h * 2, 0.05f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();

            new line_particle(source.x, source.y,
                source.dx + Game.rnd(2) - 1, source.dy * 3, col3, h * 4, 0.1f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();
            new line_particle(source.x, source.y,
                source.dx * 3 + Game.rnd(2) - 1, source.dy * 3 + Game.rnd(4) - 1, col3, h * 2, 0.05f, 1, Game)
            { lifetime = (int)Game.rnd(life) + life }.activate();

            //blood
            //new line_particle(x, y, dx, dy, 8,
            //    100, 0, 0.95f, Game).activate();
            //new line_particle(x, y, dx * 0.1f, dy, 8,
            //    100, 0, 0.9f, Game).activate();
            //new line_particle(x, y, dx, dy - 1, 8,
            //    100, 0, 0.9f, Game).activate();
        }
    }
}
