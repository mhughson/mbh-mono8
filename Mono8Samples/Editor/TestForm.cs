﻿using Mono8;
using Mono8Samples.TopDown;
using PicoX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mono8Samples
{
    public partial class TestForm<PicoXGameType> : Form
        where PicoXGameType : PicoXGame, new()
    {
        Mono8Game<PicoXGameType> OwningGame;
        obj selected_obj;

        public void SetOwningGame(Mono8Game<PicoXGameType> OwningGame)
        {
            this.OwningGame = OwningGame;
            this.OwningGame.event_post_init += on_game_init;
            this.OwningGame.event_post_update += on_game_update;
        }

        void on_game_update()
        {
            if (selected_obj != null)
            {
                update_tree(selected_obj.motion_tree_root, treeView1.Nodes, selected_obj, 0);
            }
        }

        bool update_tree(TopDown.fight_tree.fight_branch cur_branch, TreeNodeCollection cur_tree, obj target, int index)
        {
            if (cur_branch == null)
            {
                return false;
            }

            TreeNode next_tree_node = cur_tree[index];
            next_tree_node.BackColor = Color.Transparent;

            if (cur_branch.children != null)
            {
                //xpos += 4;

                int count = 0;
                foreach (TopDown.fight_tree.fight_branch child in cur_branch.children)
                {
                    //ypos += 6;
                    if (update_tree(child, next_tree_node.Nodes, target, count))
                    {
                        next_tree_node.BackColor = Color.Yellow;
                    }
                    count++;
                }
            }

            if (cur_branch == target.motion_tree_active_node)
            {
                next_tree_node.BackColor = Color.Yellow;
                return true;
            }

            return false;
        }

        private void on_game_init(Mono8Game<PicoXGameType> game)
        {
            TopDown.TopDown t = game.PXGame as TopDown.TopDown;
            if (t != null)
            {
                t.event_on_obj_selected += on_obj_selected;
            }
        }

        public TestForm()
        {
            InitializeComponent();
        }

        void on_obj_selected(obj o)
        {
            selected_obj = o;
            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();
            populate_tree(o.motion_tree_root, treeView1.Nodes, o);
            treeView1.ExpandAll();
            treeView1.EndUpdate();
        }

        void populate_tree(TopDown.fight_tree.fight_branch cur_branch, TreeNodeCollection cur_tree, obj target)
        {
            if (cur_branch == null)
            {
                return;
            }

            string display = cur_branch.name;
            //if (cur_branch.conditions != null)
            //{
            //    display += "(";

            //    foreach (var con in cur_branch.conditions)
            //    {
            //        display += con.GetType().Name;
            //    }

            //    display += ")";
            //}

            //Game.print_d(display.ToLower(), xpos, ypos, col);
            TreeNode next_tree_node = cur_tree.Add(cur_branch.name);
            next_tree_node.Tag = cur_branch;
            
            if (cur_branch as TopDown.fight_tree.fight_node != null)
            {
                next_tree_node.ImageIndex = 1;
                next_tree_node.SelectedImageIndex = 1;
            }
            else
            {
                next_tree_node.ImageIndex = 0;
                next_tree_node.SelectedImageIndex = 0;
            }
            //if (cur_branch == target.motion_tree_active_node)
            //{
            //    next_tree_node.BackColor = Color.Yellow;
            //}

            if (cur_branch.children != null)
            {
                //xpos += 4;

                foreach (TopDown.fight_tree.fight_branch child in cur_branch.children)
                {
                    //ypos += 6;
                    populate_tree(child, next_tree_node.Nodes, target);
                }
            }
        }

        private void treeView1_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            //e.Cancel = true;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();

            TopDown.fight_tree.fight_branch cur_branch = e.Node.Tag as TopDown.fight_tree.fight_branch;
            if (cur_branch != null)
            {
                if (cur_branch.conditions != null)
                {
                    foreach (var con in cur_branch.conditions)
                    {
                        listBox1.Items.Add(con.ToString());
                    }
                }

                TopDown.fight_tree.fight_node node = cur_branch as TopDown.fight_tree.fight_node;
                if (node != null && node.tracks != null)
                {
                    foreach(var track in node.tracks)
                    {
                        listBox2.Items.Add(track.ToString());
                    }
                }
            }
        }
    }
}
