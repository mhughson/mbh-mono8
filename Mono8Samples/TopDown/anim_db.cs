﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkFunctionDotNet.AnimationSet;
using DarkFunctionDotNet.SpriteSheet;

namespace Mono8Samples.TopDown
{
    public class anim_db
    {
        Dictionary<string, DarkFunctionAnim> anims;
        Dictionary<string, DarkFunctionDotNet.SpriteSheet.DarkFunctionSpr> sprites;

        // When T2 is requested, use T1 instead.
        Dictionary<string, string> anim_overrides;

        public anim_db()
        {
            anims = new Dictionary<string, DarkFunctionAnim>();
            sprites = new Dictionary<string, DarkFunctionDotNet.SpriteSheet.DarkFunctionSpr>();
            anim_overrides = new Dictionary<string, string>();
        }

        public void add(DarkFunctionAnimations DFAnims, DarkFunctionImg DFImg)
        {
            foreach (var a in DFAnims.anims)
            {
                anims.Add(a.name, a);
            }

            parse_sprite_dir(DFImg.def.sub_dir[0], DFImg.def.sub_dir[0].name);
        }

        protected void parse_sprite_dir(DarkFunctionDir sub_dir, string path)
        {
            if (sub_dir == null)
            {
                return;
            }

            if (sub_dir.sprites != null)
            {
                foreach (var s in sub_dir.sprites)
                {
                    sprites.Add(path + s.name, s);
                }
            }

            if (sub_dir.sub_dir != null)
            {
                foreach (var d in sub_dir.sub_dir)
                {
                    parse_sprite_dir(d, path + d.name + "/");
                }
            }
        }

        public DarkFunctionAnim get_anim(string anim_name)
        {
            if (anims.ContainsKey(anim_name))
            {
                return anims[anim_name];
            }
            else if (anim_overrides.ContainsKey(anim_name))
            {
                // todo: ensure the override is in the anims list.
                return anims[anim_overrides[anim_name]];
            }
            {
                return null;
            }
        }

        public DarkFunctionDotNet.SpriteSheet.DarkFunctionSpr get_sprite(string sprite_path)
        {
            if (sprites.ContainsKey(sprite_path))
            {
                return sprites[sprite_path];
            }
            else
            {
                return null;
            }
        }

        // if using wildcards, strings must have matching formats. 
        // default character animation format is:
        //  character_bodystate_weaponstate_weapontype_direction
        // existing comes in as: character_bodystate_weaponstate_weapontype
        // override comes in as: weapontype
        // creates an override for that weapon in all directions.
        // if override is null is will override all weapons in all directions.
        public void add_anim_override(string existing_anim_name, string override_weapon_name = null)
        {
            // This order must match the anim naming contract.
            string[][] anim_keys = new string[][]
            {
                new string[] { "soldier", "dog" },
                new string[] { "standing", "walking", "running", "crouched", "crawling", "rolling", "incover" },
                new string[] { "atease", "aiming", "firing" },
                new string[] { "melee", "pistol", "rifle" },
                new string[] { "e", "ne", "n", "nw", "w", "sw", "s", "se" },
            };

            if (!anim_overrides.ContainsKey(existing_anim_name))
            {
                int index = existing_anim_name.LastIndexOf("_");
                if (index > 0)
                {
                    string weapon_name = existing_anim_name.Substring(index + 1);
                    string anim_base_name = existing_anim_name.Substring(0, index + 1); // or index + 1 to keep _

                    if (string.IsNullOrWhiteSpace(override_weapon_name))
                    {
                        foreach(string weapon in anim_keys[3])
                        {
                            if (weapon != weapon_name)
                            {
                                override_weapon_anim(existing_anim_name, weapon_name, weapon, anim_base_name, anim_keys[4]);
                            }
                        }
                    }
                    else
                    {
                        string weapon = override_weapon_name;
                        override_weapon_anim(existing_anim_name, weapon_name, weapon, anim_base_name, anim_keys[4]);
                    }
                }

                #region OLD
                /*

                string[] existing_tokens = existing_anim_name.Split('_');
                string[] override_tokens = override_anim_name.Split('_');

                if (existing_tokens.Length != override_tokens.Length)
                {
                    throw new ArgumentException("anim names do have same formating.");
                }
                else
                {
                    // The anim names may contain wildcards, which we want to replace with 
                    // real data.
                    string final_existing_anim_name = "";
                    string final_override_anim_name = "";

                    // Walk through each element of the string, mapping the override to the
                    // existing anim name.
                    for (int i = 0; i < existing_tokens.Length; i++)
                    {
                        // hmmm... it seems like we will need to know about all possible values
                        // for each token, so that we can iterate over the possibilities.
                        if (existing_tokens[i] == "*")
                        {
                            // TODO: Looks like we may want to make this a recursive function to 
                            //       build up all the different versions of the anim name.
                            //       Might need to build a tree.
                            //       Alternatively, we only support * in existing name for directions
                            //       and handle that as a special case.
                            //final_existing_anim_name += anim_keys[i]
                        }
                    }
                }
                */
                #endregion OLD
            }
        }

        private void override_weapon_anim(string existing_anim_name, string weapon_name, string override_weapon_name, string anim_base_name, string[] anim_keys)
        {
            string weapon = override_weapon_name;

            // don't override the existing animation
            if (weapon != weapon_name)
            {
                string anim_override_weap = anim_base_name;
                anim_override_weap += weapon + "_";

                foreach (string direction in anim_keys)
                {
                    string anim_override_final = anim_override_weap;
                    anim_override_final += direction;

                    anim_overrides.Add(anim_override_final, existing_anim_name + "_" + direction);
                }
            }
        }
    }
}
