﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples
{
    // hello world
    // by zep
    // ported by mhughson
    class Hello : PicoXGame
    {
        float t = 0;

        public Hello() : base()
        {
        }

        public override void _init()
        {
            music(0);
        }

        public override void _update60()
        {
            t += 0.5f;
        }

        public override void _draw()
        {
            cls();

            for (int i = 1; i <= 11; i++)
            {
                for (int j0 = 0; j0 <= 7; j0++)
                {
                    var j = 7 - j0;
                    var col = 7 + j;
                    var t1 = t + i * 4 - j * 2;
                    var x = 0; // cos(t0) * 5; // bug in original - t0 doesn't exist.
                    var y = 38 + j + cos(t1 / 50.0f) * 5;
                    pal(7, col);
                    spr(16 + i, 8 + i * 8 + x, y);
                }
            }

            print("this is",
              37, 70, 14); //8+(t/4)%8)

            print("        mono-8", 37, 70, flr((t%4) + 7)); //8+(t/4)%8)

            print("nice to meet you",
               34, 80, 12); //8+(t/4)%8)

            spr(1, 64 - 4, 90);
        }

        public override string GetMapString()
        {
            return "";
        }

        public override Dictionary<int, string> GetMusicPaths()
        {
            var Dic = new Dictionary<int, string>();
            Dic[0] = "raw/hello_mus_0";
            return Dic;
        }

        public override List<string> GetSheetPath()
        {
            return new List<string>() { "raw/sheet_hello" };
        }

        public override Dictionary<int, string> GetSoundEffectPaths()
        {
            return new Dictionary<int, string>();
        }

        public override Dictionary<string, object> GetScriptFunctions()
        {
            return new Dictionary<string, object>();
        }

        public override string GetPalTextureString()
        {
            return "";
        }
    }
}
