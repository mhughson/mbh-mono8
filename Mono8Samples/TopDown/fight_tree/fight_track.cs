﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_track : sub
    {
        public bool is_master = false;

        public bool is_complete = false;

        public int start = 0;
        public int? length;

        protected obj owning_obj
        {
            get
            {
                if (owning_obj_object == null)
                {
                    owning_obj_object = Game.game_world.cur_area.find_obj_by_id_name(owning_obj_id);

                    if (owning_obj_object != null)
                    {
                        on_owning_object_found?.Invoke();
                    }
                }
                return owning_obj_object;
            }
            set
            {
                owning_obj_object = value;
            }
        }

        protected Action on_owning_object_found;

        // Internally the owing object my be just a string, or a direct reference to the object.
        private obj owning_obj_object;
        private string owning_obj_id;

        protected int ticks { get; private set; }

        public fight_track(obj owning_obj, PicoXGame owner) : base(owner)
        {
            this.owning_obj = owning_obj;
            reset();
        }

        public fight_track(string owning_obj_id, PicoXGame owner) : base(owner)
        {
            this.owning_obj_id = owning_obj_id;
            reset();
        }

        public virtual void reset()
        {
            ticks = 0;
        }

        public virtual bool advance(int frames)
        {
            ticks += frames;

            // return true when the track is finished.
            return true;
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}
