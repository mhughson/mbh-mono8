﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    class fight_track_destroy_object : fight_track
    {
        public fight_track_destroy_object(obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {

        }

        public fight_track_destroy_object(string owning_obj_id, PicoXGame owner) : base(owning_obj_id, owner)
        {
        }

        public override bool advance(int frames)
        {
            owning_obj.destroy();
            return true;
        }
    }
}
