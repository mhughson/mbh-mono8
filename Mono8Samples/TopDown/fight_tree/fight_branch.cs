﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_branch : sub
    {
        public string name { get; protected set; }

        public List<fight_condition> conditions { get; protected set; }

        public List<fight_branch> children { get; private set; }

        //public fight_branch()
        //{
        //    conditions = new List<fight_condition>();
        //    children = new List<fight_branch>();
        //    name = "unnamed";
        //}

        public fight_branch(List<fight_condition> conditions, List<fight_branch> children, string name, PicoXGame owner) : base(owner)
        {
            this.conditions = conditions;
            this.children = children;
            this.name = name;
        }

        public void add_condition(fight_condition new_condition)
        {
            conditions.Add(new_condition);
        }

        public void add_child(fight_branch new_child)
        {
            children.Add(new_child);
        }

        public virtual bool test_conditions()
        {
            if (conditions != null)
            {
                foreach (fight_condition con in conditions)
                {
                    if (!con.test_condition())
                    {
                        // If any condition is false, then return false.
                        // TODO: Add support for OR.
                        return false;
                    }
                }
            }

            // All the conditions are true.
            return true;
        }
    }
}
