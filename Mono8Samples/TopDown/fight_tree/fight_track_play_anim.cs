﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    public class fight_track_play_anim : fight_track
    {
        string anim_to_play;
        bool anim_complete;
        int bank_num;

        public fight_track_play_anim(string anim_to_play, int bank_num, obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {
            reset();
            this.anim_to_play = anim_to_play;
            owning_obj.event_on_anim_done += on_anim_done;
            owning_obj.gfx_bank = bank_num;
        }

        public fight_track_play_anim(string anim_to_play, int bank_num, string owning_obj_id_name, PicoXGame owner) : base(owning_obj_id_name, owner)
        {
            reset();
            this.anim_to_play = anim_to_play;
            this.bank_num = bank_num;
            on_owning_object_found += new Action(() => 
            {
                owning_obj.event_on_anim_done += on_anim_done;
                owning_obj.gfx_bank = bank_num;
            });
        }

        private void on_anim_done(string anim_name)
        {
            if (anim_name == anim_to_play)
            {
                anim_complete = true;
            }
        }

        public override void reset()
        {
            base.reset();
            anim_complete = false;
        }

        public override bool advance(int frames)
        {
            owning_obj.set_anim(anim_to_play);
            return anim_complete;
        }

        public override string ToString()
        {
            string res = base.ToString();
            res += "( anim: " + anim_to_play + " )";
            return res;
        }
    }
}
