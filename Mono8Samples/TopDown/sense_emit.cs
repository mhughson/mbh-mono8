﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;

namespace Mono8Samples.TopDown
{
    /// <summary>
    /// Object which emits waves which AI agents can sense, and will try to investigate.
    /// </summary>
    public class sense_emit : obj
    {
        public float radius;
        public float radial_velocity;
        public int lifetime = -1; // -1 == forever
        public List<sense_emit> next_emitter;
        public obj creator;

        protected List<agent> visitors = new List<agent>();

        public sense_emit(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
        }

        public bool mark_visited(agent visiter)
        {
            if (visitors.Contains(visiter))
            {
                return false;
            }
            else
            {
                visitors.Add(visiter);
                return true;
            }
        }

        public override void update()
        {
            radius += radial_velocity;

            if (lifetime != -1)
            {
                lifetime = (int)Game.max(0, lifetime - 1);

                if (lifetime <= 0)
                {
                    destroy(); // return?
                }
            }

            for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
            {
                agent o = Game.game_world.cur_area.objs_active[i] as agent;
                if (o != null && o != creator && !visitors.Contains(o))
                {
                    if (Vector2.DistanceSquared(new Vector2(col_box().x, col_box().y), new Vector2(o.col_box().x, o.col_box().y)) < radius * radius)
                    {
                        visitors.Add(o);
                        o.on_sense(this);
                    }
                }
            }

            base.update();
        }

        public override void draw()
        {
            // Don't draw anything.

            Game.circ_d(x, y, radius, 25);

            //base.draw();
        }
    }
}
