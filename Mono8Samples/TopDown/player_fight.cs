﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class player_fight : agent_fight
    {
        //int push_x_time = 0;
        //int push_y_time = 0;

        public int trans_dir_x = 0;
        public int trans_dir_y = 0;

        public player_fight(PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            type_flag = type_flags.Player;
            //attack_target_types = type_flags.Enemy | type_flags.Destructable;
            //cur_weap.bullet_speed = 3;
            is_persistent = true;

            gfx_bank = 1;
            //set_anim("soldier_idle_down_unarmed");

            //motion_tree_root = new fight_tree.fight_branch();

            //fight_tree.fight_track_test track = new fight_tree.fight_track_test();
            //fight_tree.fight_node node = new fight_tree.fight_node();
            //node.add_track(track);
            //motion_tree_root.add_child(node);

            #region motion_tree
            motion_tree_root = new fight_tree.fight_branch
            (
                conditions:
                null,

                children:
                new List<fight_tree.fight_branch>()
                {
                    new fight_tree.fight_branch
                    (
                        conditions:
                        null,

                        children:
                        new List<fight_tree.fight_branch>()
                        {
                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_up_left_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(-1, -1, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(0, Game),
                                    new fight_tree.fight_condition_btn(2, Game),
                                },

                                children:
                                null,

                                name:
                                "move up left",

                                owner:
                                Game
                            ),
                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_up_right_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(1, -1, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(1, Game),
                                    new fight_tree.fight_condition_btn(2, Game),
                                },

                                children:
                                null,

                                name:
                                "move up right",

                                owner:
                                Game
                            ),
                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_down_left_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(-1, 1, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(3, Game),
                                    new fight_tree.fight_condition_btn(0, Game),
                                },

                                children:
                                null,

                                name:
                                "move down left",

                                owner:
                                Game
                            ),
                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_down_right_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(1, 1, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(3, Game),
                                    new fight_tree.fight_condition_btn(1, Game),
                                },

                                children:
                                null,

                                name:
                                "move down right",

                                owner:
                                Game
                            ),

                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_left_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(-1, 0, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(0, Game),
                                },

                                children:
                                null,

                                name:
                                "move left",

                                owner:
                                Game
                            ),


                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_right_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(1, 0, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(1, Game),
                                },

                                children:
                                null,

                                name:
                                "move right",

                                owner:
                                Game
                            ),


                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_up_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(0, -1, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(2, Game),
                                },

                                children:
                                null,

                                name:
                                "move up",

                                owner:
                                Game
                            ),

                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_run_down_unarmed", 1, this, Game),
                                    new fight_tree.fight_track_move_in_dir(0, 1, this, Game),
                                },

                                conditions:
                                new List<fight_tree.fight_condition>()
                                {
                                    new fight_tree.fight_condition_btn(3, Game),
                                },

                                children:
                                null,

                                name:
                                "move down",

                                owner:
                                Game
                            ),

                            new fight_tree.fight_node
                            (
                                tracks:
                                new List<fight_tree.fight_track>()
                                {
                                    new fight_tree.fight_track_play_anim("soldier_idle_down_unarmed", 1, this, Game),
                                },

                                conditions:
                                null,

                                children:
                                null,

                                name:
                                "standing",

                                owner:
                                Game
                            ),
                        },

                        name:
                        "movement",

                        owner:
                        Game
                    ),
                },

                name:
                "root",

                owner:
                Game
            );
            #endregion
        }

        public override void update()
        {
            base.update();
        }

        public override void draw()
        {
            if (Game.ir_vision_enabled)
            {
                // Make the character a solid color.
                for (int i = 0; i < 32; i++)
                {
                    Game.pal(i, 16);
                }
            }
            else if (Game.night_vision_enabled || Game.game_world.cur_area.light_status == area.lighting_status.on)
            {
                int[] p = Game.get_cur_pal();
                Game.apply_pal(p);
            }
            base.draw();
            Game.pal();
        }
    }
}
