﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class door : obj
    {
        protected string dest_map;
        protected string dest_poi; // destination point of interest.
        public int trans_dir_x = 0;
        public int trans_dir_y = 0;

        protected HashSet<obj> colliding_objs_at_load;

        public door(float x, float y, int w, int h, string dest_map, string dest_poi, PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.dest_map = dest_map;
            this.dest_poi = dest_poi;

            colliding_objs_at_load = new HashSet<obj>();
        }

        public override void post_load()
        {
            base.post_load();

            for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
            {
                obj o = Game.game_world.cur_area.objs_active[i];
                if (o != this)
                {
                    //if (o.col_box().intersects(col_box()))
                    {
                        colliding_objs_at_load.Add(o);
                    }
                }
            }
        }

        public override void update()
        {
            //// Keep adding objects while we are transitioning in.
            //if (Game.get_game_state() == TopDown.game_states.door_trans_in)
            //{
            //    for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
            //    {
            //        obj o = Game.game_world.cur_area.objs_active[i];
            //        if (o != this)
            //        {
            //            if (o.col_box().intersects(col_box()))
            //            {
            //                colliding_objs_at_load.Add(o);
            //            }
            //        }
            //    }
            //}
            //else
            {
                // Clean up the post_load list.
                for (int i = colliding_objs_at_load.Count - 1; i >= 0; i--)
                {
                    obj o = colliding_objs_at_load.ElementAt(i);
                    if (!o.col_box().intersects(col_box()))
                    {
                        colliding_objs_at_load.Remove(o);
                    }
                }
            }

            // TODO: This should be triggered when pressing against the back of 
            //       trigger, rather than just entering it.
            // TODO: Teleport the player such that their col_box() lines up
            //       with the destination box, not their center point.
            for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
            {
                obj o = Game.game_world.cur_area.objs_active[i];
                if (o != this)
                {
                    if (o.col_box().intersects(col_box()))
                    {
                        if (!colliding_objs_at_load.Contains(o))
                        {
                            // For now doors only care about the player.
                            if (o.type_flag == type_flags.Player)
                            {
                                obj poi = Game.game_world.parse_and_get_area(dest_path).poi.Find((x) => { return x.display_name == dest_poi; });
                                if (poi != null)
                                {
                                    (o as player).trans_dir_x = trans_dir_x;
                                    (o as player).trans_dir_y = trans_dir_y;
                                    (o as player).dx = 0;
                                    (o as player).dy = 0;

                                    int offset = 4;
                                    Game.StartDoorTransition( new Action(() => 
                                    {
                                        Game.QueueLoadMap(dest_path, new Microsoft.Xna.Framework.Vector2(poi.x + (offset * -trans_dir_x), poi.y +  (offset * -trans_dir_y)));
                                    } ));
                                }
                                break;
                            }
                        }
                    }
                }
            }

            base.update();
        }

        public string dest_path { get { return "Content/raw/top_down/" + dest_map + ".tmx"; } }
    }
}
