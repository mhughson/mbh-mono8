﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class cam : obj
    {
        // NOTE: x,y is the CENTER of the camera view.

        // The obj being targeted. Camera will attempt to focus on this object.
        public obj tar;

        public float pull_threshold;
        public Vector2 pull_threshold_offset;

        // The top left of the camera view, without any offsets.
        public Vector2 pos_min;

        // the bottom right of the camera view, without any offsets.
        public Vector2 pos_max;
        public bool allow_backtrack_y;

        // Distance beyond the visible area at which point 
        public float spawm_threshold;

        // Offset from the target. Used for things like peek-a-boo cam.
        public Vector2 offset;

        public cam(obj target, PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            tar = target;//target to follow.
            x = target.x + target.w_half();
            y = target.y + target.h_half();

            //how far from center of screen target must
            //be before camera starts following.

            //allows for movement in center without camera
            //constantly moving.
            pull_threshold = 16;
            pull_threshold_offset = Vector2.Zero;

            allow_backtrack_y = true;

            //min and max positions of camera.

            //the edges of the level.
            pos_min = new Vector2(0, 0);
            pos_max = new Vector2(512, 512);

            offset = Vector2.Zero;

            is_persistent = true;
        }

        public override void update()
        {
            var self = this;
            base.update();

            // WIP_VIEWS
            // TODO: Make more robust and support x direction.
            if (!allow_backtrack_y)
            {
                Game.game_world.cur_area.back_track_max_y = y + 64;
            }
            Game.game_world.cur_area.update_cur_view();
            
            pos_min = Game.game_world.cur_area.cur_view.pos_min;
            pos_max = Game.game_world.cur_area.cur_view.pos_max;
            pull_threshold = Game.game_world.cur_area.pull_threshold;
            pull_threshold_offset = Game.game_world.cur_area.pull_threshold_offset;
            allow_backtrack_y = Game.game_world.cur_area.allow_backtrack_y;

            float initial_y = y;
            float max_cam_speed = 1.0f;

            //follow target outside of
            //pull range.
            if (pull_max_x() < tar.x)
            {
                x += Game.min(tar.x - pull_max_x(), max_cam_speed);
            }
            if (pull_min_x() > tar.x)
            {
                x += Game.max((tar.x - pull_min_x()), -max_cam_speed);
            }
            if (pull_max_y() < tar.y)
            {
                y += Game.min(tar.y - pull_max_y(), max_cam_speed);
            }
            if (pull_min_y() > tar.y)
            {
                y += Game.max((tar.y - pull_min_y()), -max_cam_speed);
            }

            //lock to edge

            // TODO: Should account of smaller views than 128x128
            float offset_x = Game.min(64, Game.game_world.cur_area.cur_view.width * 0.5f);
            x = Game.mid(pos_min.X + offset_x, x, pos_max.X - offset_x);

            float offset_y = Game.min(64, Game.game_world.cur_area.cur_view.height * 0.5f);
            y = Game.mid(pos_min.Y + offset_y, y, pos_max.Y - offset_y);

            // TODO: Mono8 Port
            //self:cull_objs()

            activate_objs();

        }

        public override void draw()
        {
            // Do nothing.
            //base.draw();

            /*
            Game.rect(pull_min_x(), pull_min_y(), pull_max_x()-1, pull_max_y()-1, 3);
            Game.print("player limit", pull_min_x(), pull_min_y() - 6, 3);

            Game.rect(pos_min.X, pos_min.Y, pos_max.X-1, pos_max.Y-1, 12);
            Game.print("map limit", pos_min.X, pos_min.Y - 6, 12);
            */

            /*
            Game.rect(spawn_rect().Left, spawn_rect().Top, spawn_rect().Right, spawn_rect().Bottom, 12);
            Game.rect(x-32, y-32, x+32, y+32, 8);
            Game.print("activation zone", cam_pos().X + 2, cam_pos().Y + 3, 0);
            Game.print("activation zone", cam_pos().X + 2, cam_pos().Y + 2, 12);
            */
        }

        public Vector2 spawn_chunk_center_pos()
        {
            int chunk_size = 128;
            int cam_center = 64;
            float chunk_x = ((float)Math.Round(cam_pos().X / chunk_size) /*+ 1*/) * chunk_size;
            float chunk_y = ((float)Math.Round(cam_pos().Y / chunk_size) /*+ 1*/) * chunk_size;
            return new Vector2(chunk_x + cam_center, chunk_y + cam_center);
        }

        public Rectangle spawn_rect()
        {
            Vector2 center = spawn_chunk_center_pos();
            int spawn_range = 256;
            return new Rectangle((int)(center.X - spawn_range / 2), (int)(center.Y - spawn_range / 2), spawn_range, spawn_range);
        }

        public Vector2 cam_pos()
        {
            //calculate camera shake.
            //          local shk=m_vec(0,0)
            //			if shake_remaining>0 then
            //              shk.x=rnd(shake_force)-(shake_force/2)
            //              shk.y=rnd(shake_force)-(shake_force/2)
            //          end
            //			return pos.x-64+shk.x,pos.y-64+shk.y

            float fx = MathHelper.Clamp(x + offset.X, pos_min.X, pos_max.X);
            float fy = MathHelper.Clamp(y + offset.Y, pos_min.Y, pos_max.Y);
            return new Vector2(fx - 64, fy - 64);
        }

        public float pull_max_x()
        {
            return x + pull_threshold + pull_threshold_offset.X;
        }

        public float pull_min_x()
        {
            return x - pull_threshold + pull_threshold_offset.X;
        }

        public float pull_max_y()
        {
            return y + pull_threshold + pull_threshold_offset.Y;
        }

        public float pull_min_y()
        {
            return y - pull_threshold + pull_threshold_offset.Y;
        }

        public void activate_objs()
        {
            for (int i = Game.game_world.cur_area.objs_queue.Count - 1; i >= 0; i--)
            {
                obj v = Game.game_world.cur_area.objs_queue[i];

                Rectangle area = spawn_rect();
                if (v.x <= (area.Right + v.w_half()) && v.x >= (area.Left - v.w_half()) && v.y <= (area.Bottom + v.h_half()) && v.y >= (area.Top - v.h_half()))
                //if (v.x < x)
                {
                    // move to active list.
                    v.activate();
                }
            }
        }

        public void jump_to_target()
        {
            x = tar.x;
            y = tar.y;

            //lock to edge
            x = Game.mid(pos_min.X, x, pos_max.X);
            y = Game.mid(pos_min.Y, y, pos_max.Y);
        }
    }
}
