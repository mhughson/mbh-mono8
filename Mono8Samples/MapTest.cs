﻿using System;
using System.Collections.Generic;
using PicoX;

namespace Mono8Samples
{
    class MapTest : PicoXGame
    {
        public override void _init()
        {
            //mset(2, 0, mget(1, 0));
            //fset(1, 2, true);
            //fset(1, 2, false);
            //fset(7, 3, true);
            //fset(8, 15);
        }
        public override void _update60()
        {
            count++;
            int num_tests = 6;
            if (btnp(1))
            {
                test = (test + 1) % num_tests;
            }
            if (btnp(0))
            {
                test = (test - 1 + num_tests) % num_tests;
            }
            base._update60();
        }
        int count = 0;
        float t = 0;
        int test = 0;
        public override void _draw()
        {
            cls(7);

            switch (test)
            {
                case 0:
                    {
                        camera(0, 0);
                        map(-2, 0, 0, 0, 8, 8);
                        break;
                    }

                case 1:
                    {
                        camera(-8, 8);
                        map(0, 0, 0, 0, 8, 8);
                        break;
                    }
                case 2:
                    {
                        camera(0, 0);
                        map(0, 0, 0, 0, 9999, 99999);
                        break;
                    }
                case 3:
                    {
                        camera(sin(t) * 128, 0);
                        map(0, 0, 0, 0, 9999, 99999);
                        break;
                    }
                case 4:
                    {
                        camera(0, 0);
                        map(0, 0, sin(t) * 128, 0, 9999, 99999);
                        break;
                    }
                case 5:
                    {
                        camera(0, 0);
                        map(0, 0, sin(t) * 128, sin(t) * 128, 8, 8);
                        break;
                    }
            }

            t += 0.005f;

            if (count > 1)
            {
                print("missed frame: " + (count - 1), 0, 0, 0);
            }

            count = 0;

            return;

            rectfill(0, 8, 0 + 15, 8 + 15, 1);

            circfill(8, 16, 8, 8);

            for (int i = 0; i < 8; i++)
            {
                rectfill(i * 8, 0, i * 8 + 8, 8, i + 8);
            }

            map(0, 0, 0, 64, 16, 16);

            for (int i = 0; i < 8; i++)
            {
                byte flags = fget(mget(i, 0));
                print(flags.ToString(), i * 8, 64 + 8 + (i % 2 * 8), 1 + i % 2);
                print(fget(mget(i, 0), 3) == true ? "y" : "n", i * 8, 64 + 8 + 16 + (i % 2 * 8), 1 + i % 2);
            }
        }

        public override string GetMapString()

        {
            return "Content/raw/simple_map.tmx";
        }

        public override Dictionary<int, string> GetMusicPaths()
        {
            return new Dictionary<int, string>();
        }

        public override List<string> GetSheetPath()
        {
            return new List<string>() { "raw\\simple_sheet" };
        }

        public override Dictionary<int, string> GetSoundEffectPaths()
        {
            return new Dictionary<int, string>();
        }

        public override Dictionary<string, object> GetScriptFunctions()
        {
            return new Dictionary<string, object>();
        }

        public override string GetPalTextureString()
        {
            return "";
        }
    }
}
