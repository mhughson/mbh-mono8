﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    class fight_track_custom : fight_track
    {
        Action on_start;
        Func<bool> on_advance;

        public fight_track_custom(Action on_start, Func<bool> on_advance, obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {
            this.on_start = on_start;
            this.on_advance = on_advance;
        }

        public override bool advance(int frames)
        {
            if (ticks == 0)
            {
                on_start?.Invoke();
            }
            bool res = base.advance(frames);

            if (on_advance != null)
            {
                return on_advance.Invoke();
            }
            else
            {
                return res;
            }
        }
    }
}
