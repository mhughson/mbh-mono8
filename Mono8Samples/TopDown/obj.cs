﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public struct anim
    {
        public int ticks;
        public int[][] frames;
        public bool? loop;
        public int? w;
        public int? h;
    }
    
    [Flags]
    public enum type_flags
    {
        None = 0,
        Player = 1,
        Enemy = 2,
        Destructable = 4,
    }

    public class obj : sub
    {
        public float x = 0;
        public float y = 0;
        public float dx = 0;
        public float dy = 0;

        public bool flipx = false;
        public bool flipy = false;

        public int w = 8;
        public int h = 8;

        public int ticks = 0;

        // Should this object live on beyond the life of a level.
        public bool is_persistent { get; protected set; } = false;

        protected bool killable = false;
        public virtual bool is_killable_now() { return killable; }

        public bool is_alive { get; protected set; } = false;

        public virtual bool is_detectable(agent detector)
        {
            return true;
        }

        public type_flags type_flag = type_flags.None;

        public string display_name = "not set";
        public string id_name = "not set";

        public int? gfx_bank { get; set; }

        public int w_half() { return Game.flr(w * 0.5f); }
        public int h_half() { return Game.flr(h * 0.5f); }

        public virtual rect col_box() { return new rect(x, y, w, h); }

        // TODO: Probably move this to some sort of "powered device" class.
        public bool has_power = true;

        public int draw_layer = 0;

        //animation definitions.
        //use with set_anim()
        public Dictionary<string, anim> anims = new Dictionary<string, anim>()
            {
                {
                    "idle",
                    new anim()
                    {
                        ticks=1,//how long is each frame shown.
                        frames= new int[][] { new int[] { 1 } },//what frames are shown.
                    }
                }
            };

        public string curanim { get; private set; } //currently playing animation
        public int curframe { get; private set; } = 0;        //curent frame of animation.
        int animtick = 0;        //ticks until next frame should show.

        public delegate void on_anim_done_delegate(string anim_name);
        public on_anim_done_delegate event_on_anim_done;

        public area owning_area { get; set; }

        public fight_tree.fight_branch motion_tree_root { get; protected set; }
        public fight_tree.fight_node motion_tree_active_node { get; protected set; }
        protected fight_tree.fight_node motion_tree_sequenced_node;

        public obj(PicoXGame owner, area owning_area = null) : base(owner)
        {
            // If an area isn't specified, assume that they wish to add the object to the current area.
            if (owning_area == null)
            {
                this.owning_area = Game.game_world.cur_area;
            }
            else
            {
                this.owning_area = owning_area;
            }
            deactivate();
        }

        //request new animation to play.
        public virtual void set_anim(string anim_name)
        {
            var self = this;
            if (anim_name == curanim) return; //early out.

            var dfanim = Game.game_anims.get_anim(anim_name);
            if (dfanim != null)
            {
                animtick = dfanim.cells[0].delay;
                curanim = anim_name;
                curframe = 0;
                return;
            }

            if (anims.ContainsKey(anim_name))
            {
                var a = anims[anim_name];

                animtick = a.ticks; //ticks count down.
                curanim = anim_name;
                curframe = 0;
                return;
            }

            Game.printh("unknown anim: " + anim_name);
        }

        void update_anim()
        {
            //anim tick
            var self = this;

            if (curanim == null || (!anims.ContainsKey(curanim) && Game.game_anims.get_anim(curanim) == null))
            {
                return;
            }

            animtick -= 1;
            if (animtick <= 0)
            {
                curframe += 1;

                var dfanim = Game.game_anims.get_anim(curanim);

                if (dfanim != null)
                {
                    // Have we reached the last frame?
                    if (curframe >= dfanim.cells.Count())
                    {
                        // For now 0 means loop forever, and anything else means don't loop.
                        if (dfanim.loops != 0)
                        {
                            // TODO: Support counting the number of loops.
                            // Go back to the final frame.
                            event_on_anim_done?.Invoke(curanim);
                            curframe--;
                        }
                        else
                        {
                            // TODO: Was it intentional that this is only called when looping?
                            event_on_anim_done?.Invoke(curanim);
                            curframe = 0;
                        }
                    }
                    animtick = dfanim.cells[curframe].delay;
                }
                else
                {
                    var a = anims[curanim];
                    animtick = a.ticks; //reset timer
                    if (curframe >= a.frames.Length)
                    {
                        if (a.loop == false)
                        {
                            event_on_anim_done?.Invoke(curanim);
                            curframe--; // back up the frame counter
                        }
                        else
                        {
                            // TODO: Was it intentional that this is only called when looping?
                            event_on_anim_done?.Invoke(curanim);
                            curframe = 0; //loop
                        }
                    }
                }
            }
        }

        public virtual void post_load() { }

        public virtual void update()
        {
            ticks++;

            update_anim();

            if (motion_tree_sequenced_node != motion_tree_active_node)
            {
                motion_tree_active_node = motion_tree_sequenced_node;

                if (motion_tree_active_node != null)
                {
                    motion_tree_active_node.play();
                }
            }

            if (motion_tree_root != null && motion_tree_active_node == null)
            {
                motion_tree_active_node = motion_tree_sequenced_node = find_new_node(motion_tree_root);
                if (motion_tree_active_node != null)
                {
                    motion_tree_active_node.play();
                }
            }

            if (motion_tree_active_node != null)
            {
                if (motion_tree_active_node.advance())
                {
                    // For the tree to re-evaluate.
                    motion_tree_sequenced_node = null;
                }
            }
        }

        fight_tree.fight_node find_new_node(fight_tree.fight_branch cur_branch)
        {
            if (cur_branch.test_conditions())
            {
                fight_tree.fight_node node = cur_branch as fight_tree.fight_node;
                if (node != null)
                {
                    return node;
                }
                else
                {
                    // we found a branch that has conditions that pass, but it is not
                    // a fight_node so we treat is a bank, and iterate its children if
                    // it has any. If it doesn't then this bank is pretty pointless
                    // and we just skip over it.

                    if (cur_branch.children != null)
                    {
                        foreach (fight_tree.fight_branch child in cur_branch.children)
                        {
                            fight_tree.fight_node found_node = find_new_node(child);

                            if (found_node != null)
                            {
                                return found_node;
                            }
                        }
                    }
                }
            }

            return null;
        }

        protected float sunken_amount = 0;

        public virtual void draw()
        {
            var self = this;

            if (!string.IsNullOrEmpty(curanim))
            {
                var dfanim = Game.game_anims.get_anim(curanim);
                if (dfanim != null)
                {
                    var c = dfanim.cells[curframe];
                    {
                        // Handle case where frame has no sprites in it (eg. door opening)
                        if (c.sprites != null)
                        {
                            foreach (var sp in c.sprites)
                            {
                                var s = Game.game_anims.get_sprite(sp.name);
                                if (s != null && !s.name.StartsWith("slot_"))
                                {
                                    var final_w_half = Game.flr(s.w * 0.5f);
                                    var final_h_half = Game.flr(s.h * 0.5f);

                                    var start_x = x - (final_w_half);
                                    var start_y = y - (final_h_half);
                                    Game.sspr(s.x, s.y, s.w, s.h - sunken_amount, start_x + sp.x, start_y + sp.y, s.w, s.h - (int)sunken_amount, sp.flipH != 0, sp.flipV != 0);
                                }
                            }
                        }
                    }
                }
                else if (anims.ContainsKey(curanim))
                {
                    var a = anims[curanim];
                    int[] frame = a.frames[curframe];

                    // TODO: Mono8 Port
                    //if (pal) push_pal(pal)

                    // Mono8 Port: Starting with table only style.
                    //if type(frame) == "table" then
                    {
                        var final_w = a.w ?? w;
                        var final_h = a.h ?? h;
                        var final_w_half = Game.flr(final_w * 0.5f);
                        var final_h_half = Game.flr(final_h * 0.5f);

                        var start_x = x - (final_w_half);
                        var start_y = y - (final_h_half);

                        var count = 0;

                        var num_vert = Game.flr(final_h / 8);
                        var num_horz = Game.flr(final_w / 8);

                        var inc_x = 8;
                        var inc_y = 8;

                        if (flipx)
                        {
                            start_x = start_x + ((num_horz - 1) * 8);
                            inc_x = -8;
                        }


                        if (flipy)
                        {
                            start_y = start_y + ((num_vert - 1) * 8);
                            inc_y = -8;
                        }

                        var y2 = start_y;

                        for (int v_count = 0; v_count < num_vert; v_count++)
                        {
                            var x2 = start_x;

                            for (int h_count = 0; h_count < num_horz; h_count++)
                            {
                                //draw in frame order, but from
                                // right to left.
                                var f = frame[count];

                                // Don't draw sprite 0. This allows us to use that as a special 
                                // sprite in our animation data.
                                if (f != 0)
                                {

                                    var flipx2 = flipx;

                                    var flipy2 = flipy;

                                    // Mono8 Port: frame is an int can can't be null.
                                    //if (f != null)
                                    {
                                        // TODO: This doesn't properly support flipping collections of tiles (eg. turn a 3 tile high 
                                        // sprite upside down. it will flip each tile independently).
                                        if (f < 0)
                                        {
                                            f = (int)Game.abs(f);

                                            flipx2 = !flipx2;
                                        }

                                        // Hack to allow flipping Y. Add 512 to your sprite id.
                                        if (f >= 9999)
                                        {
                                            f -= 9999;

                                            flipy2 = !flipy2;
                                        }

                                        Game.sspr((f * 8) % 128, Game.flr((f / 16)) * 8, 8, 8,
                                            x2, y2, 8, 8,
                                            flipx2, flipy2);

                                    }
                                }
                                count += 1;

                                x2 += inc_x;

                            }
                            y2 += inc_y;

                        }
                        #region Mono8Port
                        // Mono8 Port: Starting with table only style.
                        //else
                        //	var flipx = flipx

                        //             var flipy = flipy

                        //             if frame < 0 then
                        //                   flipx = not flipx
                        //                     frame = abs(frame)

                        //             end

                        //             if frame >= 256 then
                        //                   frame-= 256

                        //                 flipy = true

                        //             end

                        //             sspr((frame * 8) % 128, flr((frame / 16)) * 8, w, h,
                        //                 x - (self:w_half()), y + (self:h_half())-(h / scaley),  //-(h / 4 / 2), 
                        //		w, h / scaley, flipx, flipy )
                        //end

                        // TODO: Mono8 Port
                        //if (pal) pop_pal()

                        // TODO: Mono8 Port
                        //add(debug_points, { x,y})
                        //var x, y, w, h = self:col_box()
                        //rect(x - w, y - h, x + w, y + h, 9)
                        #endregion
                    }
                }
            }

            Game.pset_d(x, y, 24);
            rect r = col_box();
            Game.rect_d(r.left, r.top, r.right - 1, r.bottom - 1, 25);
        }

        public virtual void on_death(obj source) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns>True if the obj is killed.</returns>
        public virtual bool on_take_damage(obj source, int amount = 1) { return true; }

        public virtual void on_activated() { }

        public void activate()
        {
            if (owning_area.objs_active.Contains(this) == false)
            {
                if (owning_area.objs_queue.Contains(this))
                {
                    owning_area.objs_queue.Remove(this);
                }

                owning_area.objs_active.Add(this);

                on_activated();

                // Objects get added at the end of the frame update, so we want to force a call to 
                // update() on this object, so that when it draws things frame, it isn't in an uninitialized
                // state.
                // If this becomes to heavy handed, we could move this to on_activated of certain objects.
                // We could also change the camera to do a pre_update and post_update, so that objects have a chance
                // to activate/deactivate before the updating of all the active objects happens.
                update();
            }
        }

        public void deactivate()
        {
            if (owning_area.objs_queue.Contains(this) == false)
            {
                if (owning_area.objs_active.Contains(this))
                {
                    owning_area.objs_active.Remove(this);
                }

                owning_area.objs_queue.Add(this);
            }
        }

        public bool pending_destroy { get; protected set; } = false;
        public void destroy()
        {
            if (owning_area.objs_active.Contains(this))
            {
                on_destroy();
                pending_destroy = true; // don't remove it right away, just flag it for cleanup.
            }
        }

        public virtual void on_destroy()
        {

        }

        public virtual void on_interact()
        {

        }

        public virtual bool can_interact(obj target)
        {
            return false;
        }

        public virtual void on_switch_toggle()
        {

        }

        public void set_col_box_pos(float x, float y)
        {
            // Set the position offset by the delta to the center of col_box
            this.x = x + (this.x - col_box().x);
            this.y = y + (this.y - col_box().y);
        }
    }
}
