﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;

namespace Mono8Samples.TopDown
{
    public class destructable : obj
    {
        protected int hp = 6;
        int ticks_since_last_hit = 9999;
        int ticks_since_dead = 0;
        obj source_of_death;
        protected area.collision_flag collision_type = area.collision_flag.all;
        public string husk_anim;
        bool is_husk = false;

        public destructable(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            w = 8;
            h = 16;

            type_flag = type_flags.Destructable;

            anims = new Dictionary<string, anim>()
                {
                    {
                        "idle",
                        new anim()
                        {
                            ticks=1,//how long is each frame shown.
                            frames= new int[][] { new int[] { 539, 555 } },//what frames are shown.
                        }
                    },
                };

            is_alive = true;

            set_anim("idle");
        }

        public override rect col_box()
        {
            return new rect(x, y + 4, w, h / 2);
        }

        public override void post_load()
        {
            base.post_load();

            set_collision_area(collision_type);

            //Game.game_world.cur_area.fset((int)(x / 8), (int)((y + 4) / 8), area.collision_flag.all);
        }

        protected void set_collision_area(area.collision_flag flag)
        {
            for (int j = -h_half() + 8; j < h_half(); j++)
            {
                for (int i = -w_half(); i < w_half(); i++)
                {
                    Game.game_world.cur_area.fset((int)(x / 8) + (int)(i / 8), (int)(y / 8) + (int)(j / 8), flag);
                }
            }
        }

        public override bool is_killable_now()
        {
            return is_alive;
        }

        public override bool on_take_damage(obj source, int amount)
        {
            ticks_since_last_hit = 0;
            hp-=amount;
            if (hp <= 0)
            {
                is_alive = false;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void on_death(obj source)
        {
            base.on_death(source);

            source_of_death = source;

            if (string.IsNullOrWhiteSpace(husk_anim))
            {
                set_collision_area(area.collision_flag.none);
            }
            //Game.game_world.cur_area.fset((int)(x / 8), (int)((y + 4) / 8), area.collision_flag.none);

            new explosion_series(20, 0, h, h_half(), Game) { x = col_box().x, y = col_box().y }.activate();

            // destroy() in update.
            //destroy();
        }

        public override void update()
        {
            ticks_since_last_hit++;

            if (!is_alive && !is_husk)
            {
                ticks_since_dead++;

                //new explosion_fx(x + Game.rnd(16) - 8, y + Game.rnd(16) - 8, Game).activate();
                //new explosion(source_of_death, Game) { x = x + Game.rnd(16) - 8, y = y + Game.rnd(16) - 8 }.activate();
                
                if (ticks_since_dead >= 20)
                {
                    is_husk = true;
                    if (string.IsNullOrWhiteSpace(husk_anim))
                    {
                        // now clean up this object.
                        destroy();
                    }
                    else
                    {
                        set_anim(husk_anim);
                    }
                }
            }

            base.update();
        }

        public override void draw()
        {
            if (ticks_since_last_hit < 5)
            {
                Game.apply_pal( Game.damage_pal);
            }

            if (ticks_since_dead > 0 && Game.tick % 2 == 0 && !is_husk)
            {
                Game.apply_pal(Game.ir_vision_pal);
            }

            base.draw();
            Game.apply_pal(Game.get_cur_pal());
        }
    }
}
