﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;

namespace Mono8Samples.TopDown
{
    class laser : obj
    {
        public Vector2 end_point;

        float height_offset = -4.0f;

        bool triggered = false;

        public laser(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            h = 2;
        }

        public override void update()
        {
            // NOTE: Removed because this means you won't trigger new laser when running around in 
            //       high alert which is confusing to the player.
            //if (Game.game_world.get_global_alert_state() != world.alert_states.high)
            if (has_power)
            {
                for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    agent o = Game.game_world.cur_area.objs_active[i] as agent;
                    if (o != null && o.type_flag == type_flags.Player && !o.is_crawling)
                    {
                        // TODO: This should be converted to "point on line" check to allow for
                        //       lasers with arbitrary orientations.
                        if (o.col_box().intersects(col_box()))
                        {
                            triggered = true;

                            new sense_emit(Game)
                            {
                                x = o.col_box().x,
                                y = o.col_box().y,
                                radius = 0,
                                radial_velocity = 5,
                                lifetime = 30,
                                creator = o,
                            }.activate();
                        }
                    }
                }
            }

            base.update();
        }

        public override void draw()
        {
            if ((Game.ir_vision_enabled || triggered) && Game.tick % 3 == 0 && has_power)
            {
                // Intentionally not using cur_pal as I want these things to POP!
                Game.line(x, y, end_point.X, end_point.Y, 30);
                Game.line(x, y + height_offset, end_point.X, end_point.Y + height_offset, 15);
            }

            base.draw();
        }

        public override rect col_box()
        {
            return new rect(x + w_half(), y + h_half(), w, h);
        }

        public override void on_switch_toggle()
        {
            base.on_switch_toggle();

            has_power = !has_power;
        }
    }
}
