﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples
{
    class Talos : PicoXGame
    {
        //camera position
        float camx = 0;
        float camy = 0;

        //offset from player.
        float camoffset = 32;

        //last position the path was 
        //generated at. if screen goes
        //past this, a new path node 
        //will be needed.
        float lasty = 128;
        //how much space between each
        //point in the path.
        float yinc = 32;
        //max distance of walls from 
        //center of path.
        float xwide = 32;
        //max distance of walls from 
        //center of path.
        float xwide_min = 8;
        //current speed modification.
        //increases as player progresses
        //to increase challenge.
        float speedmod = 1;

        //the player.
        public player p;

        //start with a path down the 
        //center with super wide space.
        //create look of cave opening.
        List<int[]> path;

        //the speed modifier when player
        //as the player passed thresholds.
        //{speedmod, ypos in meters}
        float[][] speeds;

        //clear object lists.
        List<doodad> doodads;
        List<particle> parts;
        List<streak> streaks;

        //main menu.
        int state;

        //reset score.
        int hiscore;
        int score;

        bool gothiscore;

        int t = 0;
        int tsincedead = 0;

        public Talos() : base()
        {
            //talos descent
            //@matthughson

            //thanks to ultrabrite for the
            //improve line drawing.

            /* 

            //todo//

            -must-

            -nice-

            up thrust (not sure)
            music (tried... haven't landed)
            intro (would be nice)
            player speed streak
            branching center line
            fade transition

            */
        }

        ////////////////////////////

        //convert ypos to meters
        float ytom(float y)
        {
            return y * 0.3f;
        }

        //convert meters to ypos
        float mtoy(float m)
        {
            return m / 0.3f;
        }

        ////////////////////////////

        //clamp a number to a range.
        //same as mid
        float clamp(float n, float low, float high)
        {
            return max(min(n, high), low);
        }

        ////////////////////////////

        public class sub
        {
            public Talos Game;

            public sub(PicoXGame owner)
            {
                Game = owner as Talos;
            }
        }

        ////////////////////////////

        public class vec : sub
        {
            public vec(float x, float y, PicoXGame owner) : base(owner)
            {
                this.x = x;
                this.y = y;
            }
            public float x;
	        public float y;
	        //get the length of the vector
	        public float getlength()
            {
                return Game.sqrt((float)Math.Pow(x,2) + (float)Math.Pow(y,2));
            }
            //get the normal of the vector
            public vec getnorm()
            {
                var l = getlength();
                return new vec(x / l, y / l, Game);
            }
        }

        ////////////////////////////

        public class lineseg : sub
        {
            public lineseg(float x1, float y1, float x2, float y2, PicoXGame owner) : base(owner)
            {
                a = new vec(x1, y1, Game);
                b = new vec(x2, y2, Game);
            }
            public vec a;
            public vec b;

            public void draw(int col)
            {
                //thanks ultrabrite
                var a = this.a;
                var b = this.b;
                var ly = b.y - a.y;
                var x = a.x;
                var dx = (b.x - a.x) / ly;
                var bx = 1.0f;
                var bdx = 1.0f / ly;
                for (var y = a.y; y <= b.y; y++)
                {
                    //Game.printh((x - bx) + "," + (x + bx) + "," + y);
                    Game.rectfill(x - bx, y, x + bx, y, col);
                    x += dx;
                    bx -= bdx;
                }
                //Game.printh("--");
            }

            //helper to draw closest point
            //on a line to an object
            public void drawnear(player o, int col)
            {
                var lp = Game.closestpointalongline(a, b, new vec(o.x, o.y, Game));
                Game.circ(lp.x, lp.y, 2, col);
            }

            //finds the closest point on a
            //line to an object.
            public vec closest(player o)
            {
                return Game.closestpointalongline(a, b, new vec(o.x, o.y, Game));
            }
        }

        ////////////////////////////

        //make a random doodad.
        //the stuff that is in the dirt
        //allow the sides, like skulls.
        public class doodad : sub
        {
            public doodad(float _x, float _y, PicoXGame owner) : base(owner)
            {
                var _fx = (Game.flr(Game.rnd(2)) == 0);
                var _fy = (Game.flr(Game.rnd(2)) == 0);
                var _sp = Game.flr(Game.rnd(4)) + 4;
                x = _x;
                y = _y;
                sp = _sp;
                fx = _fx;
                fy = _fy;

                Game.doodads.Add(this);
            }

            public float x;
            public float y;
            public int sp;
            public bool fx;
            public bool fy;
        }

        ////////////////////////////

        //make a pixel particle.
        public class particle : sub
        {
            public particle(float _x, float _y, float _tmin, float _tmax, int _col, PicoXGame owner) : base(owner)
            {
                //position
                x = _x;
                y = _y;

                //velocity
                dx = (Game.rnd(2) - 4) * 0.1f;
                dy = (Game.rnd(3) - 1) * 0.1f;

                //gravity
                g = 0;

                //lifetime
                t = Game.rnd(_tmax - _tmin) + _tmin;

                //color
                col = _col;

                Game.parts.Add(this);
            }

            public float x;
            public float y;
            public float dx;

            public float dy;

            public float g;
            public float t;
            public int col;

            //update
            public void u()
            {
                var o = this;
                o.t -= 1;
                o.dy += o.g;
                o.x += o.dx;
                o.y += o.dy;

                if (o.t <= 0)
                {
                    Game.parts.Remove(o);
                }
            }

            //draw
            public void d()
            {
                Game.pset(x, y, col);
            }
        }

        ////////////////////////////

        //make an air jet particle.
        public class jetpart : particle
        {
            public jetpart(float d, PicoXGame owner) : base((owner as Talos).p.x - d, (owner as Talos).p.y - 1, 10, 20, 6, owner)
            {
                dy += Game.p.dy * Game.speedmod;
                dx *= d;
            }
        }

        ////////////////////////////

        //make a streak; a line particle
        public class streak : sub
        {
            public streak(float _x, float _y, float _dx, float _dy, int _col, int _maxhist, float _g, float _dec, PicoXGame owner) : base(owner)
            {
                x = _x;
                y = _y;
                dx = _dx;
                dy = _dy;
                decel = _dec;
                g = _g;
                hist = new List<vec>();
                maxhist = _maxhist;
                col = _col;
                stopped = false;
                lifetime = -1;

                hist.Add(new vec(x, y, Game));

                Game.streaks.Add(this);
            }

            //position
            public float x;
            public float y;
            //velocity
            public float dx;
            public float dy;
            //deceleration
            public float decel;
            //gravity
            public float g;
            //history of positions
            public List<vec> hist;
            //Game.max length of history to record
            public int maxhist;
            //color
            public int col;
            //has it slowed to a stop
            public bool stopped;
            //how long should it live
            public int lifetime;

            //update
            public void u()
            {
                //once stopped, do nothing.
                if (stopped)
                {
                    return;
                }

                //died of old age?
                if (lifetime != -1)
                {
                    lifetime -= 1;


                    if (lifetime <= 0)
                    {
                        Game.streaks.Remove(this);
                        return;
                    }
                }

                //fall
                dy += g;

                //slow down
                dx *= decel;
                dy *= decel;

                //detect slow enough to stop.
                if (Game.abs(dx) <= 0.2f && Game.abs(dy) <= 0.2f && decel != 1.0f)
                {
                    stopped = true;
                    return;
                }

                //move
                x += dx;
                y += dy;

                //save history of position
                hist.Add(new vec(x, y, Game));

                if (hist.Count > maxhist)
                {
                    hist.RemoveAt(0);
                }

                //clean up when off bottom.
                //todo: detect offscreen in 
                //general.
                if (hist[0].y > Game.camy + 128)
                {
                    Game.streaks.Remove(this);
                    return;
                }
            }

            //draw
            public void d()
            {
                //draw a line between each 
                //position in history, creating
                //a kind of curve.
                if (hist.Count > 1)
                {
                    for (int i = 0; i < hist.Count - 1; i++)
                    {
                        var p1 = hist[i];
                        var p2 = hist[i + 1];
                        Game.line(p1.x, p1.y, p2.x, p2.y, col);
                        //				Game.pset(p1.x,p1.y,col)			
                    }
                }
            }

        }

        ////////////////////////////

        //find the point on a line 
        //closest to an object.
        public vec closestpointalongline(vec linefrom, vec lineto, vec c)
        {

            Tuple<vec, float> temp = tounitvec(linefrom, lineto);
            var u = temp.Item1;
            var len = temp.Item2;
            var v = new vec(c.x - linefrom.x, c.y - linefrom.y, this);
            var w = dotproduct(u, v);
            if (w < -len)
            {
                w = -len;
            }
            else if (w > 1)
            {
                w = 1;
            }

            var r = new vec((u.x * w), (u.y * w), this);

            r.x += linefrom.x;
            r.y += linefrom.y;
            return r;
        }

        ////////////////////////////

        //normalize a vector.
        public Tuple<vec, float> tounitvec(vec c1, vec c2)
        {
            var cx = (c1.x - c2.x);
            var cy = (c1.y - c2.y);
            var c = new vec(cx, cy, this);
            var l = c.getlength();
            return new Tuple<vec, float>(new vec(c.x / l, c.y / l, this), l);
        }

        ////////////////////////////

        //dot product of 2 vectors
        public float dotproduct(vec v1, vec v2)
        {
            return v1.x * v2.x + v1.y * v2.y;
        }

        ////////////////////////////

        ////////////////////////////

        //save the highscore to disc.
        //call at end of run.
        void updatehiscore()
        {
            // TODO_M8
            var oldscore = dget(0);
            if (oldscore < score)
            {
                dset(0, score);
                hiscore = score;
                
                gothiscore = true;
                sfx(8);
            }
        }

        ////////////////////////////

        //on the death of the player.
        void ondeath()
        {
            //create a bunch of streaks to 
            //represent to parts of the 
            //player exploding.

            //how long to make the streaks.
            var h = 2;

            //legs
            new streak(p.x, p.y,
                -p.dx, -p.dy, 9, h, 0.1f, 1, this);
            new streak(p.x, p.y,
                -p.dx, -p.dy - 2, 9, h, 0.2f, 1, this);
            new streak(p.x, p.y,
                p.dx, -p.dy, 9, h, 0.2f, 1, this);
            new streak(p.x, p.y,
                p.dx, -p.dy - 2, 9, h, 0.1f, 1, this);

            //head
            new streak(p.x, p.y,
                -p.dx + 1, -p.dy, 12, h, 0.2f, 1, this);
            new streak(p.x, p.y,
                p.dx + 1, -p.dy, 12, h, 0.1f, 1, this);

            //arms
            new streak(p.x, p.y,
                -p.dx, -p.dy - 1, 7, h, 0.1f, 1, this);
            new streak(p.x, p.y,
                -p.dx + 2, -p.dy - 1, 7, h, 0.2f, 1, this);
            new streak(p.x, p.y,
                p.dx, -p.dy - 1, 7, h, 0.2f, 1, this);
            new streak(p.x, p.y,
                p.dx + 2, -p.dy - 1, 7, h, 0.1f, 1, this);

            //flying blood
            new streak(p.x, p.y,
                -p.dx, -p.dy, 8, h, 0.05f, 1, this);
            new streak(p.x, p.y,
                -p.dx + 1, -p.dy, 8, h, 0.05f, 1, this);
            new streak(p.x, p.y,
                p.dx, -p.dy * 2, 8, h * 4, 0.1f, 1, this);
            new streak(p.x, p.y,
                p.dx + 1, -p.dy, 8, h * 2, 0.05f, 1, this);

            //blood
            new streak(p.x, p.y, p.dx, p.dy, 8,
                100, 0, 0.95f, this);
            new streak(p.x, p.y, p.dx * 0.1f, p.dy, 8,
                100, 0, 0.9f, this);
            new streak(p.x, p.y, p.dx, p.dy - 1, 8,
                100, 0, 0.9f, this);

            //player has died so update the 
            //highscore.
            updatehiscore();

            //reset the time since died.
            tsincedead = 0;

            //sound.
            sfx(2);
            sfx(3);
            music(9);

            //gameover state.
            state = 2;
        }

        ////////////////////////////

        //print string with outline.
        void printo(string str, float startx, float starty, int col, int col_bg)
        {
            print(str, startx + 1, starty, col_bg);
            print(str, startx - 1, starty, col_bg);
            print(str, startx, starty + 1, col_bg);
            print(str, startx, starty - 1, col_bg);
            print(str, startx + 1, starty - 1, col_bg);
            print(str, startx - 1, starty - 1, col_bg);
            print(str, startx - 1, starty + 1, col_bg);
            print(str, startx + 1, starty + 1, col_bg);
            print(str, startx, starty, col);
        }

        //print string centered with 
        //outline.
        void printc(string str, float x, float y, int col, int col_bg, int special_chars)
        {
            var len = (str.Length * 4) + (special_chars * 3);
            var startx = x - (len / 2);
            var starty = y - 2;
            printo(str, startx, starty, col, col_bg);
        }

        //print string with sin wave 
        //bobbing with outline.
        void printsin(string str, float x, float y, int col, int col_bg, int special_chars)
        {
            var len = (str.Length * 4) + (special_chars * 3);
            var startx = x - (len / 2);
            var starty = y - 2;

            var fx = startx;
            for (int i = 0; i < str.Length; i++)
            {
                var s = str.Substring(i, 1);
                printo(s, fx, starty + (sin((fx + t) * 0.015f) * 2), col, col_bg);
                fx += 4;
                if (s == "X")
                {
                    fx += 3;
                }
            }
        }

        //print string with cosine wave 
        //bobbing with outline.
        //good when paired with sin.
        void printcos(string str, float x, float y, int col, int col_bg, int special_chars)
        {
            var len = (str.Length * 4) + (special_chars * 3);
            var startx = x - (len / 2);
            var starty = y - 2;

            var fx = startx;
            for (int i = 0; i < str.Length; i++)
            {
                var s = str.Substring(i, 1);
                printo(s, fx, starty + (cos((fx + t) * 0.015f) * 2), col, col_bg);
                fx += 4;
                if (s == "X")
                {
                    fx += 3;
                }
            }
        }

        public override void _init()
        {
            printh("==new run==");

            //name of save game
            cartdata("mbh_talos");

            music(5);
            reset();
        }

        ////////////////////////////

        public class player : sub
        {
            //position
            public float x;
            public float y;
            //velocity
            public float dx;
            public float dy;
            //sprite
            public int sp;
            //flip
            public bool flipx;

            public player(PicoXGame owner) : base(owner)
            {
            }
        }

        void reset()
        { 
            //camera position
            camx = 0;
            camy = 0;

            //offset from player.
            camoffset = 32;

            //last position the path was 
            //generated at. if screen goes
            //past this, a new path node 
            //will be needed.
            lasty = 128;
            //how much space between each
            //point in the path.
            yinc = 32;
            //max distance of walls from 
            //center of path.
            xwide = 32;
            //max distance of walls from 
            //center of path.
            xwide_min = 8;
            //current speed modification.
            //increases as player progresses
            //to increase challenge.
            speedmod = 1;

            //the player.
            p = new player(this)
            {
                //position
                x = 64,
                y = -10,//start offscreen.
                        //velocity
                dx = 0,
                dy = 1,
                //sprite
                sp = 3,
                //flip
                flipx = false,
            };

            //start with a path down the 
            //center with super wide space.
            //create look of cave opening.
            path = new List<int[]>
            {
                new int [] {64,60,64,64},
                new int [] {64,64,56,48},
                new int [] {64,96,48,32},
                new int [] {64,128,(int)xwide,(int)xwide},
            };

            //the speed modifier when player
            //as the player passed thresholds.
            //{speedmod, ypos in meters}
            speeds = new float[][]
            {
                new float[]{1,200},
                new float[]{1.5f,500},
                new float[]{2,1000},
            };

            //clear object lists.
            doodads = new List<doodad>();
            parts = new List<particle>();
            streaks = new List<streak>();

            //main menu.
            state = 0;

            //reset score.
            score = 0;

            //load hiscore.
            hiscore=dget(0);
            gothiscore = false;
        }
        public override void _update60()
        {
            t += 1;

            //update particles.
            for (int i = 0; i < parts.Count; i++)
            {
                parts[i].u();
            }

            //update streaks.
            for (int i = 0; i < streaks.Count; i++)
            {
                streaks[i].u();
            }

            //main menu.
            if (state == 0)
            {

                //start game.
                if (btnp(5) || btnp(4))
                {
                    state = 1;
                    music(-1, 3000);
                    sfx(12);
                }

                return;
            }
            //game over.
            else if (state == 2)
            {
                tsincedead += 1;

                //restart game.
                if (btnp(5) || btnp(4))
                {
                    sfx(12);
                    reset();
                    state = 1;
                    music(-1, 3000);
                }
                return;
            }

            //check if player has passed
            //a speed threshold and speed
            //should be increased.
            foreach (var v in speeds)
            {
                if (ytom(p.y) < v[1])
                {
                    if (speedmod != v[0])
                    {
                        speedmod = v[0];
                        sfx(11);
                        //hack for heartbeat.
                        if (speedmod <= 1.5f)
                        {
                            music(3);
                        }
                        else if (speedmod <= 2)
                        {
                            music(4);
                        }
                    }
                    break;
                }
            }

            //mod the players speed.
            var pspd = 0.05f * speedmod;
            Action postmove = null;
            if (btn(0))
            {
                p.dx -= pspd;
                p.dx = max(p.dx, -1);
                p.flipx = true;
                // Pico8 seems to play sounds at 30fps, causing this to sound
                // spamming when run at 60fps.
                if (t % 2 == 0)
                {
                    sfx(0);
                }
                postmove = () =>
                {
                    new jetpart(-1, this);
                };
            }

            if (btn(1))
            {
                p.dx += pspd;
                p.dx = min(p.dx, 1);
                p.flipx = false;
                if (t % 2 == 0)
                {
                    sfx(0);
                }
                postmove = () => { new jetpart(1, this); };
            }

            p.x += p.dx;
            p.y += p.dy * speedmod;
            postmove?.Invoke();
            camy = max(p.y - camoffset, 0);


            //the streak
            if (speedmod > 1)
            {
                var c = new int[] { 12, 9, 7, 4 };
                for (int i = -1; i <= 1; i++)
                {
                    var s = new streak(p.x + i, p.y, -p.dx, -p.dy, c[flr(rnd(c.Length))], 5, 0, 1, this);
                    s.lifetime = (int)rnd(5.0f * speedmod);
                }
            }

            score = flr(ytom(p.y - camoffset));

            //is it time for a new point
            //on the path?
            if (camy + 128 > lasty && path.Count > 0)
            {
                //start with the previous x
                var newx = path[path.Count - 1][0];
                //deviate from that pos
                newx += (int)rnd(32) - 16;
                //avoid getting to close to
                //the edges
                newx = (int)clamp(newx, xwide, 127 - xwide);
                //move downward
                lasty += yinc;
                path.Add(new int[] { newx, (int)lasty, (int)rnd(xwide - xwide_min) + (int)xwide_min, (int)rnd(xwide - xwide_min) + (int)xwide_min });

                //place a doodad.
                if (path.Count > 1)
                {
                    var i = path.Count - 1;
                    var p1 = path[i];
                    var p2 = path[i - 1];

                    //left
                    var l = new lineseg(
                        p1[0] - p1[2], p1[1],
                        p2[0] - p2[2], p2[1], this);
                    var sml = l.a.x;
                    if (l.a.x > l.b.x) { sml = l.b.x; }
                    var _x = rnd(sml - 8);
                    var _y = rnd(l.a.y - l.b.y) + l.b.y;
                    new doodad(_x, _y, this);

                    //right
                    l = new lineseg(
                        p1[0] + p1[3], p1[1],
                        p2[0] + p2[3], p2[1], this);
                    var lrg = l.a.x;
                    if (l.a.x < l.b.x) { lrg = l.b.x; }
                    _x = rnd(128 - lrg) + lrg;
                    _y = rnd(l.a.y - l.b.y) + l.b.y;
                    new doodad(_x, _y, this);
                }
            }

            //clean up paths offscreen.
            for (int i = 0; i < path.Count; i++)
            {
                if (path[i][1] < camy - yinc)
                {
                    path.RemoveAt(i);
                }
            }

            //clean up doodads offscreen.
            for(int i = 0; i < doodads.Count; i++)
            {
                if (doodads[i].y < camy - 8)
                {
                    doodads.RemoveAt(i);
                }
            }

            //check for player hitting wall.
            for (int i = 0; i < path.Count - 1; i++)
            {
                var p1 = path[i];
                var p2 = path[i + 1];

                //left
                var l = new lineseg(
                    p1[0] - p1[2], p1[1],
                    p2[0] - p2[2], p2[1], this);
                var c = l.closest(p);
                var d = new vec(p.x - c.x, p.y - c.y, this);
                var len = d.getlength();

                //blue reflection when close
                //to wall.
                if (len < 8)
                {
                    var s = new streak(c.x, c.y, 0, 0, 12, 2, 0, 1, this);
                    s.lifetime = 5;
                }
                //white reflection when really
                //close to wall.
                if (len < 6)
                {
                    var s = new streak(c.x, c.y, 0, 0, 7, 2, 0, 1, this);
                    s.lifetime = 5;
                }

                //dead if within 2 pixels of 
                //wall.
                if (len < 2)
                {
                    ondeath();
                }

                //right		
                l = new lineseg(
                    p1[0] + p1[3], p1[1],
                    p2[0] + p2[3], p2[1], this);
                c = l.closest(p);
                d = new vec(p.x - c.x, p.y - c.y, this);
                len = d.getlength();
                if (len < 8)
                {
                    var s = new streak(c.x, c.y, 0, 0, 12, 2, 0, 1, this);
                    s.lifetime = 5;
                }
                if (len < 6)
                {
                    var s = new streak(c.x, c.y, 0, 0, 7, 2, 0, 1, this);
                    s.lifetime = 5;
                }
                if (len < 2)
                {
                    ondeath();
                }
            }
        }

        public override void _draw()
        {
            cls(0);

            //rect(0.9f, 0.9f, 1.9f, 1.9f, 7);

            //var myline = new line(32, 32, 42, 64, this);
            //myline.draw(7);
            //return;

            //sky at the start of game.
            var c1 = 12;
            var c2 = 1;
            camera(camx, camy * 0.5f);
            rectfill(0, 0, 128, 28, c1);
            rectfill(0, 29, 128, 56, c2);
            for (int i = 28; i <= 36; i += 2)
            {
                line(0, i, 128, i, c1);
            }
            for (int i = 56; i <= 64; i += 2)
            {
                line(0, i, 128, i, c2);
            }

            //calculate camera shake.
            var shk = new vec(0, 0, this);
            if (state == 2 && tsincedead < 10)
            {
                if (t % 2 == 0)
                {
                    shk.x = rnd(4) - 2;
                    shk.y = rnd(4) - 2;
                }
            }

            camera(camx + shk.x, camy + shk.y);

            //draw speed up thresholds.
            for (int i = 0; i < speeds.Length; i++)
            {
                var s = speeds[i];
                var v = new vec(64, mtoy(s[1]), this);

                if (v.y > camy && v.y < camy + 128)
                {
                    //todo! too expensive
                    printcos("", v.x, v.y, 1, 1, 0);
                    printsin("", v.x, v.y, 1, 0, 0);
                }
            }

            //thanks ultrabrite
            for (int i = 0; i < path.Count - 1; i++)
            {
                var p1 = path[i];
                var p2 = path[i + 1];

                //center line.
                var l = new lineseg(
                    p1[0], p1[1],
                    p2[0], p2[1], this);

                //avoid paralax  
                if (l.a.y > mtoy(50))
                {
                    l.draw(1);
                }
                else if (l.a.y <= mtoy(50) && l.b.y >= mtoy(50))
                {
                    l = new lineseg(p1[0] - p1[2], p1[1], p2[0], p2[1], this);
                    l.draw(1);
                    l = new lineseg(p1[0] + p1[3], p1[1], p2[0], p2[1], this);
                    l.draw(1);
                }

                //left
                l = new lineseg(p1[0] - p1[2], p1[1], p2[0] - p2[2], p2[1], this);

                var x = l.a.x;
                var dx = (l.b.x - l.a.x) / (l.b.y - l.a.y);
                for (float y = l.a.y; y <= l.b.y; y++)
                {
                    rectfill(x, y, 0, y, 1);
                    x += dx;
                }
                l.draw(13);

                //right    
                l = new lineseg(p1[0] + p1[3], p1[1], p2[0] + p2[3], p2[1], this);

                x = l.a.x;
                dx = (l.b.x - l.a.x) / (l.b.y - l.a.y);
                for (float y = l.a.y; y <= l.b.y; y++)
                {
                    rectfill(x, y, 127, y, 1);
                    x += dx;
                }
                l.draw(13);
            }

            foreach (doodad v in doodads)
            {
                spr(v.sp, v.x, v.y, 1, 1, v.fx, v.fy);
            }

            foreach (particle v in parts)
            {
                v.d();
            }

            foreach (streak v in streaks)
            {
                v.d();
            }

            //draw the player
            if (state != 2)
            {
                spr(p.sp, p.x - 4, p.y - 4, 1, 1, p.flipx, false);
            }

            //hud

            camera(0, 0);


            printo("score:" + score + "m", 2, 2, 7, 0);

            var str = "best:" + hiscore + "m";
            var len = str.Length * 4;
            var startx = 128 - len - 1;

            printo(str, startx, 2, 7, 0);

            //print(stat(1),0,16)
            //print(stat(0),0,24)

            if (state == 0)
            {

                map(0, 0, 28, 32, 9, 4);


                printcos("press X to start", 64, 80, 1, 1, 1);

                printsin("press X to start", 64, 80, 7, 0, 1);
            }
            else if (state == 2)
            {

                printcos("press X to play again", 64, 80, 1, 1, 1);

                printsin("press X to play again", 64, 80, 7, 0, 1);

                printc("game over", 64, 32, 7, 0, 0);

                //hiscore
                var col = 7;
                if (gothiscore)
                {
                    col = (int)(t * 0.25f) % 2 + 9;
                }
                str = score + "m";
                if (gothiscore)
                {

                    printcos(str, 64, 48, 7, 7, 0);

                    printsin(str, 64, 48, col, 0, 0);
                }
                else
                {

                    printc(str, 64, 40, col, 0, 0);
                }
            }
        }

        public override string GetMapString()
        {
            return "Content/raw/talos_map.tmx";
            /*
            return
            "0008090a0b0c0d0e0f0f00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "1718191a1b1c1d1e1f0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "2728292a2b2c2d2e2f0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "3738393a3b3c3d3e000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            */
        }

        public override List<string> GetSheetPath()
        {
            return new List<string>() { "raw/sheet_talos" };
        }

        public override Dictionary<int, string> GetSoundEffectPaths()
        {
            Dictionary<int, string> paths = new Dictionary<int, string>(64);

            for (int i = 0; i < 64; i++)
            {
                paths[i] = "raw/talos_sfx_" + i;
            }

            return paths;
        }

        public override Dictionary<int, string> GetMusicPaths()
        {
            Dictionary<int, string> Paths = new Dictionary<int, string>(64);

            Paths[3] = "raw/talos_mus_3";
            Paths[4] = "raw/talos_mus_4";
            Paths[5] = "raw/talos_mus_5";
            Paths[9] = "raw/talos_mus_9";

            return Paths;
        }

        public override Dictionary<string, object> GetScriptFunctions()
        {
            return new Dictionary<string, object>();
        }

        public override string GetPalTextureString()
        {
            return "";
        }
    }
}
