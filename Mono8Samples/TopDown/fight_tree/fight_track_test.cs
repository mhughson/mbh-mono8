﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    class fight_track_test : fight_track
    {
        public fight_track_test(obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {
        }

        public override bool advance(int frames)
        {
            bool base_res = base.advance(frames);

            //if (ticks < 30)
            //{
            //    return false;
            //}
            //else
            {
                return base_res;
            }
        }
    }
}
