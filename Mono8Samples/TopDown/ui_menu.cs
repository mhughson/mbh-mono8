﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    class ui_menu<T> : ui_element
    {
        List<ui_selector<T>> menu_items;
        int cur_index;
        bool wrap;

        public ui_menu(List<ui_selector<T>> menu_items, int starting_index, bool wrap, PicoXGame owner, area owning_area) : base(owner, owning_area)
        {
            this.menu_items = menu_items;
            cur_index = starting_index;
            this.wrap = wrap;
        }

        public override void update()
        {
            base.update();

            if (Game.btnp(3))
            {
                inc_index(-1);
            }
            if (Game.btnp(2))
            {
                inc_index(1);
            }
            if (Game.btnp(0))
            {
                get_cur_data().inc_index(-1);
            }
            if (Game.btnp(1))
            {
                get_cur_data().inc_index(1);
            }
            if (Game.btnp(4) || Game.btnp(5))
            {
                execute();
            }
        }

        public override void draw()
        {
            int mx = 64;
            int c2 = 30;
            int my = 32 + 16 + 32 + 32;

            foreach (var item in get_menu_items())
            {
                int new_c1 = 25;
                if (item == get_cur_data())
                {
                    new_c1 = 17;
                }
                //printc(item.get_title() + " (" + item.get_data_set_size() + "):", x, y, c1, c2, 0);
                //y += 8;
                Game.printc(item.get_cur_label(), mx, my, new_c1, c2, 0);
                my += 8;
            }

            base.draw();
        }

        public void inc_index(int amount)
        {
            cur_index += amount;

            if (wrap)
            {
                // Add count to support wrapping from negative nums.
                cur_index = (cur_index + menu_items.Count) % menu_items.Count;
            }
        }

        public List<ui_selector<T>> get_menu_items()
        {
            return menu_items;
        }

        public ui_selector<T> get_cur_data()
        {
            return menu_items[cur_index];
        }

        public string get_cur_label()
        {
            return menu_items[cur_index].get_cur_label();
        }

        public T get_cur_value()
        {
            return menu_items[cur_index].get_cur_value();
        }

        public int get_num_menu_items()
        {
            return menu_items.Count;
        }

        public void execute()
        {
            menu_items[cur_index].execute();
        }

        public override bool HandleInput()
        {
            return true;
        }
    }
}
