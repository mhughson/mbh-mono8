﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class ui_letterbox : ui_element
    {
        int last_ticks_active = 0;

        public ui_letterbox(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
        }

        public override void draw()
        {
            base.draw();

            int ticks_active = 0;
            int len = 30;

            // Find the NIS which has been active the longest, and use its active timer to determine
            // how visible the letter box should be.
            foreach(var o in Game.game_world.cur_area.objs_active)
            {
                var n = o as nis.nis_scene_test;
                if (n != null)
                {
                    ticks_active = (int)Game.min(len, Game.max(n.ticks, ticks_active));
                }
            }

            if (ticks_active <= 0 && last_ticks_active > 0)
            {
                // time to transition out.
                last_ticks_active--;
                ticks_active = last_ticks_active;
            }

            float height = 8;
            float lerp = Game.min(ticks_active, len) / (float)len;
            Game.rectfill(0, 0, 127, height * lerp, 31);
            Game.rectfill(0, 127 - height * lerp, 127, 128, 31);

            last_ticks_active = ticks_active;
        }
    }
}
