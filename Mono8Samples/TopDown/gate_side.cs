﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class gate_side : gate_base
    {

        public gate_side(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            w = 8;
            h = 24;

            open_anim = "side_door_02_opening";

            set_anim("side_door_02_closed");
        }

        public override void on_door_open_complete(string anim_name)
        {
            if (anim_name == open_anim)
            {
                Game.game_world.cur_area.fset((int)(x / 8), (int)((y) / 8), area.collision_flag.none);
                Game.game_world.cur_area.fset((int)(x / 8), (int)((y) / 8) + 1, area.collision_flag.none);
            }
        }
    }
}
