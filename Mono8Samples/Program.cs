﻿#if !WINDOWS_UAP
using Mono8;
using System;

namespace Mono8Samples
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string [] args)
        {
            // Load the command line args.
            CommandLineManager.pInstance.pArgs = args;

            using (var game = new Mono8Game<MapTest>())
            {
                //if (!string.IsNullOrWhiteSpace(CommandLineManager.pInstance["editor"]))
                //{
                //    TestForm<TopDown.TopDown> t = new TestForm<TopDown.TopDown>();
                //    t.SetOwningGame(game);
                //    t.Show();
                //}
                game.Run();
            }
        }
    }
}
#endif