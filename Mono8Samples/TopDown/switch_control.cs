﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using RoyT.AStar;

namespace Mono8Samples.TopDown
{
    class switch_control : obj
    {
        public enum switch_types
        {
            object_toggle,
            light_switch,
        }

        public switch_types switch_type;

        public List<string> targets = new List<string>();

        public switch_control(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            set_anim("switch_control");
        }

        public override void draw()
        {
            Game.apply_pal(Game.get_cur_pal());
            base.draw();
            Game.pal();

            if (switch_type == switch_types.object_toggle)
            {
                foreach (string s in targets)
                {
                    // TODO: Maybe do this on init (or at least try) if we don't think objects will trade names at runtime.
                    obj found = Game.game_world.cur_area.find_obj_by_id_name(s);

                    if (found != null)
                    {
                        //Game.line(x, y, found.x, found.y, 15);                       

                        Position[] path = Game.map_grid.GetPath(new Position((int)(x / 8), (int)(y / 8) + 1), new Position((int)(found.x / 8), (int)(found.y / 8)), MovementPatterns.Full, 1000);

                        for(int i = 1; i < path.Length; i++)
                        {
                            int col = 13;
                            if (found.has_power)
                            {
                                if (Game.tick % (path.Length * 2) == i)
                                {
                                    col += 2;
                                }
                            }
                            Game.line(path[i - 1].X * 8, path[i - 1].Y * 8, path[i].X * 8, path[i].Y * 8, col);
                        }
                    }
                }
            }
        }

        public override void on_interact()
        {
            base.on_interact();

            switch(switch_type)
            {
                case switch_types.light_switch:
                    {
                        Game.game_world.cur_area.toggle_lights();
                        break;
                    }

                case switch_types.object_toggle:
                    {
                        foreach (string s in targets)
                        {
                            // TODO: Cache at start.
                            obj found = Game.game_world.cur_area.find_obj_by_id_name(s);

                            if (found != null)
                            {
                                found.on_switch_toggle();
                            }
                        }
                        break;
                    }
            }
        }

        public override bool can_interact(obj target)
        {
            // TODO: When AI can intentionally trigger switches, we can expand this check to include
            //       all agents.
            if (target.type_flag == type_flags.Player)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override rect col_box()
        {
            return new rect(x, y + 4, 8, 16);
        }
    }
}
