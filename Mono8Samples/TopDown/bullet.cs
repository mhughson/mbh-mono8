﻿using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class bullet : obj
    {

        public int s = 0;
        public float speed = 0;
        public int t = 0;
        public obj source = null;
        public int life_time = 120;

        public type_flags collision_mask;

        public bullet(float _x, float _y, float _dx, float _dy, obj _source, type_flags _collision_mask, PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
            x = _x;
            y = _y;

            dx = _dx;
            dy = _dy;

            s = 16;
            speed = 3;
            t = 0;

            source = _source;

            collision_mask = _collision_mask;
        }

        public override void update()
        {
            var self = this;
            t += 1;
            if (t > life_time)
            {
                destroy();
                new bullet_hit_fx(x, y, Game).activate();
                return;
            }

            float old_x = x;

            float old_y = y;
            x += dx * speed;
            y += dy * speed;

            if (Game.hit_top(self, area.collision_flag.high) || Game.hit_bottom(self, area.collision_flag.high) || Game.hit_left(self, area.collision_flag.high) || Game.hit_right(self, area.collision_flag.high))
            {
                destroy();
                new bullet_hit_fx(x, y, Game).activate();
            }

            for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
            {
                obj o = Game.game_world.cur_area.objs_active[i];
                if (o != this && o != source && o.is_killable_now() && o.type_flag != type_flags.None && collision_mask.HasFlag(o.type_flag))
                {
                    // Check both the collision box and the center point to make things make sense in all directions.
                    if (o.col_box().intersects(col_box()) || o.col_box().intersects(new rect(x, y, 2, 2))) 
                    {
                        if (o.on_take_damage(this))
                        {
                            o.on_death(this);
                        }

                        //o.destroy();
                        destroy();
                        new bullet_hit_fx(x, y, Game).activate();
                        break;
                    }
                }
            }

            base.update();
        }

        public override void draw()
        {
            Game.apply_pal(Game.get_cur_pal());
            base.draw();
            Game.pal();

            float old_x = x;
            float old_y = y;
            x = col_box().x;
            y = col_box().y;
            Game.sprfxset(0, true);
            base.draw();
            Game.sprfxset(0, false);
            x = old_x;
            y = old_y;
        }

        public override rect col_box()
        {
            return new rect(x, y + 8, 2, 2);
        }
    }
}
