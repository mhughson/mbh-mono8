﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown.fight_tree
{
    class fight_track_npc_dialog : fight_track
    {
        ui_dialog dialog;

        public fight_track_npc_dialog(ui_dialog dialog, obj owning_obj, PicoXGame owner) : base(owning_obj, owner)
        {
            this.dialog = dialog;
            this.dialog.on_closed_completed += new Action(() => { is_complete = true; }); // wait for the user to close the dialog.
        }

        public override bool advance(int frames)
        {
            if (ticks == 0)
            {
                dialog.activate();
            }

            base.advance(frames);

            return false;
        }
    }
}
