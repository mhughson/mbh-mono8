﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;
using Microsoft.Xna.Framework;

namespace Mono8Samples.TopDown
{
    class ui_minimap : ui_element
    {
        public ui_minimap(PicoXGame owner, area owning_area = null) : base(owner, owning_area)
        {
        }

        public override void draw()
        {
            // MAP
            int map_start_x = 128;
            int map_start_y = 12;
            int map_width = 32;
            int map_height = 32;
            Game.rectfill(map_start_x, map_start_y, map_start_x + map_width - 1, map_start_y + map_height - 1, 21);

            Vector2 cam_pos_in_tiles = Game.game_cam.cam_pos() / new Vector2(8, 8);
            cam_pos_in_tiles += new Vector2(8, 8); // center it
            cam_pos_in_tiles -= new Vector2(map_width / 2, map_height / 2); // offset for the size of the hud map.
            Point cpos_fixed = new Point((int)Math.Round(cam_pos_in_tiles.X), (int)Math.Round(cam_pos_in_tiles.Y));

            float map_width_half = Game.min((Game.game_world.cur_area.cur_view.width / 8.0f) * 0.5f, 8.0f);
            float map_height_half = Game.min((Game.game_world.cur_area.cur_view.height / 8.0f) * 0.5f, 8.0f);

            for (int y = (int)cpos_fixed.Y; y < (int)cpos_fixed.Y + map_height; y++)
            {
                for (int x = (int)cpos_fixed.X; x < (int)cpos_fixed.X + map_width; x++)
                {
                    if (x < (Game.game_cam.pos_min.X / 8)
                     || y < (Game.game_cam.pos_min.Y / 8)
                     || x >= (Game.game_cam.pos_max.X / 8)
                     || y >= (Game.game_cam.pos_max.Y / 8))
                    {
                        Game.pset(map_start_x + x - (int)cpos_fixed.X, map_start_y + y - (int)cpos_fixed.Y, (int)Game.rnd(2) + 30);
                    }
                    else if (Game.game_world.cur_area.fget(x, y) != area.collision_flag.none)
                    {
                        Game.pset(map_start_x + x - (int)cpos_fixed.X, map_start_y + y - (int)cpos_fixed.Y, 20);
                    }
                }
            }

            Action<obj> draw_obj = delegate (obj o)
            {
                if (o.type_flag == type_flags.Enemy)
                {
                    int col = 26;
                    //if (tick % 4 > 1)
                    //{
                    //    col = 28;
                    //}
                    if (!o.is_alive)
                    {
                        col = 30;
                    }

                    rect c = o.col_box();

                    float x = (float)Game.flr(c.x / 8.0f);
                    float y = (float)Game.flr(c.y / 8.0f);

                    if (x > (Game.game_cam.pos_min.X / 8 - map_width_half)
                     && y > (Game.game_cam.pos_min.Y / 8 - map_height_half)
                     && x <= (Game.game_cam.pos_max.X / 8 + map_width_half)
                     && y <= (Game.game_cam.pos_max.Y / 8 + map_height_half))
                    {
                        Game.pset(map_start_x + x - cpos_fixed.X, map_start_y + y - cpos_fixed.Y, col);
                    }
                }
            };

            for (int i = Game.game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
            {
                obj o = Game.game_world.cur_area.objs_active[i];
                draw_obj(o);
            }

            foreach (obj o in Game.game_world.cur_area.objs_queue)
            //for (int i = game_world.cur_area.objs_queue.Count - 1; i >= 0; i--)
            {
                draw_obj(o);
            }

            {
                player p = Game.game_world.get_player();
                if (p != null)
                {
                    int col = 24;
                    if (Game.tick % 60 > 30)
                    {
                        col = 22;
                    }
                    rect c = p.col_box();
                    float x = map_start_x + (float)Game.flr(c.x / 8.0f) - cpos_fixed.X;
                    float y = map_start_y + (float)Game.flr(c.y / 8.0f) - cpos_fixed.Y;
                    Game.pset(x, y, col);
                }
            }
        }
    }
}
