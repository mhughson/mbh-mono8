﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using Mono8;
using RoyT.AStar;
using System.IO;
using System.Xml.Serialization;
using DarkFunctionDotNet.SpriteSheet;
using DarkFunctionDotNet.AnimationSet;

namespace Mono8Samples.TopDown
{
    /* 
     * IDEAS:
     *  - Battletoads-esque first person boss.
     *  - Parachute intro
     *  - Winners Enlist "FBI" warning.
     *  - Stealth levels (knife only) - cutscene coming out of the water at night with face camo. (http://mentalfloss.com/article/91593/apocalypse-now-video-game-hits-kickstarter-cooperation-francis-ford-coppola)
     *  - Firing squad level.
     *      - Rows of soliders watch you as you approach the target.
     *      - Maybe dictator is giving speach as you do.
     *          - On stage on maybe splitscreen with Ninja gaiden cutscenes while you play.
     *  - Night vision/Infred googles (pallete swap).
     *  - Sniper rifle switches to Ray Casting Engine!
     *  - Tripping out level: https://twitter.com/matthughson/status/1022344668594429952
     *      - Maybe include glitches tiles on map (like maybe use wrong bank or something)
     *  - For large vehicles like the Jeep and Tank, do 2d 3d rotation: http://www.like100bears.com/writing/2d-3d-in-gamemaker-studio
     *  - River of dead bodies... play dead.
     *  - "Speeder bike" level using ray casting.
     *  - XP items hidden around the world to make exploration more meaningful
     *      - XP is used to upgrade stats on the player
     *      - Each XP item could let the player chose between 2 options (like XCOM)
     *      - Could upgrade weapons, equipment too.
     *  
     * CRAZY BOSSES
     *  - Find command line argument in the world, launch game again with it to play another game to advance the story.
     *      - Maybe find a URL to a PICO-8 cart or something.
     *  - Boss that requires palette swap to see.
     *      - Infered googles or something.
     *  - Something in the game manual as an anti-piracy measure.
     *  - Mech Boss Fight
     *      - Agile mech chase you as you drive away in a jeep: https://youtu.be/rxqHVoZ0fzc?t=33
     *      - Lead him to smash into walls, bridges, and trees in an attempt to break his armor.
     *  - Jet boss fight.
     *  
     * STORY:
     *  - Metal Gear meets Commando
     *  - Start the game as the OP "Hero" but get captured at the end.
     *  - Level 2 starts with you in control on an enemy, who has to execute the Hero.
     *  - After executing him, you have second thoughts, and run away.
     *  - You find a letter on the body, and learn he has a family and make it your goal to go
     *    and tell them what happened.
     *  - Most of the game is you travelling the war torn country side, fighting.
     *  - Eventually you arrive, and are mistaken for the hero! You take his place, and rais
     *    his family.
     *  - Epiloge... your old partner comes looking for you in a final showdown.
     *  - You defeat him, but having learned of your deceipt, you are murdered by your new wife.
     *  
     *  TECH DEBT:
     *   - Level transitions should teleport you to the center of the door that takes you out.
     *   
     *  DONT FORGET FEATURES EXIST:
     *   - slot_ for firing point.
     *   - switches for doors.
     *   
     */

    /*
    character animation breakdown:

    character <soldier,dog>

    body state <standing, walk, run, crouched, crawl, roll, in cover, hit, climb>

    weapon state <at ease, aim, firing>

    gun type <knife, pistol, rifle>

    direction <0, 45, 90, 135, 180, 225, 270, 315> or <e, ne, n, nw, w, sw, s, se>

    *frame <0,1,2,3...> *(NOT NEEDED IN ANIM NAME - this is at the sprite level)

    RESULT: ~1296 different animations and ~5184 sprites (assuming 4 frames per animation on average)

    Examples:

    soldier_standing_relaxed_knife_e_0
    dog_rolling_firing_pistol_se_0


    Thoughts:

            it would be nice if there was a fallback mechanism. so things like roll, don't need to be defined for
            every weapon, and every weapon state, since they are all the same.

            could it be a kind of tree, where is node is a part defined above, and based on the previous
            nodes may or may not exist and can be skipped.

            so soldier->rolling->aiming->pistol->e->0 would somehow return just "soldier_rolling_e_0" (omit aiming and pistol)

            this assumes a clear hierarchy of information: eg. aiming being omitted based on weapon is not possible.

            alternatively, every animation may be defined in code, with a mapping to a real animation:

            eg. "soldier->rolling->aiming->pistol->e->0" would map to "soldier_rolling_e_0" but so would 
                "solider->rolling->aiming->rifle->e->0".

            This is nice because there is no dependency, but does require every possible animation be defined.

            perhaps the default is a 1:1 mapping, and only overrides need be defined.

            maybe a helper function which does a kind of find and replace (map all rolling knife animations to rolling pistol).

            or at least, override "for all directions"

            // when param2 is requested use param1
            
            // map all directions of 1 weapon to another.
            mapall("soldier_rolling_relaxed_pistol_*", "soldier_rolling_aiming_rifle_*")

            // map all weapon weapon states and directions to another.
            mapall("soldier_rolling_relaxed_pistol_*", "solder_rolling_*_*_*")
    */

    public class TopDown : PicoXGame
    {
        public int tick = 0;
        int cur_pal = 0;
        int[] test = { -2, 2 };
        int col_offset = 1;
        int[][] pals = { new int[] { 0, 8, 11, 10 }, new int[] { 0, 14, 12, 7 }, };

        public cam game_cam;

        public world game_world;

        private string next_area;
        public Vector2? queued_spawn_point { get; private set; }

        public Grid map_grid;

        public class path_point
        {
            // facing directions.
            public float fx;
            public float fy;
            public Vector2 position;
        }

        public class path
        {
            public string name;
            public List<path_point> points = new List<path_point>();
        };

        public bool debug_enabled = false;
        public bool night_vision_enabled = false;
        public bool ir_vision_enabled = false;
        public bool los_enabled = false;
        public bool first_person_view_enabled;
        public bool god_mode_enabled = false;

        BufferedKey EnableDebugKey;
        BufferedKey EnablePalDisplayKey;
        BufferedKey EnableCollisionDisplayKey;
        BufferedKey KillAllKey;
        BufferedKey NightVisionKey;
        BufferedKey IRVisionKey;
        BufferedKey LightsKey;
        BufferedKey LOSKey;
        public BufferedKey FirstPersonKey;
        BufferedKey NISKey;

        List<Action> DebugDrawQueue = new List<Action>();

        public anim_db game_anims { get; private set; }

        ui_menu<string> menu_root;

        public enum map_layers
        {
            low = 0,
            decal = 1,
            shadow = 2,
            high = 3,
            ducts = 4,
        }

        public int[][] FadeTable =
        {
            new int[] { 0, 25,  5,  6,  7, 30, 31, 31},
            new int[] { 1,  2,  3,  4, 30, 31, 31, 31},
            new int[] { 2,  3,  4, 30, 31, 31, 31, 31},
            new int[] { 3,  4, 30, 31, 31, 31, 31, 31},
            new int[] { 4, 30, 31, 31, 31, 31, 31, 31},
            new int[] { 5,  6,  7, 30, 31, 31, 31, 31},
            new int[] { 6,  7, 30, 31, 31, 31, 31, 31},
            new int[] { 7, 30, 31, 31, 31, 31, 31, 31},
            new int[] { 8,  9, 10, 11, 12, 13, 30, 31},
            new int[] { 9, 10, 11, 12, 13, 30, 31, 31},
            new int[] {10, 11, 12, 13, 30, 31, 31, 31},
            new int[] {11, 12, 13, 30, 31, 31, 31, 31},
            new int[] {12, 13, 30, 31, 31, 31, 31, 31},
            new int[] {13, 30, 31, 31, 31, 31, 31, 31},
            new int[] {14, 13, 30, 31, 31, 31, 31, 31},
            new int[] {15, 14, 13, 30, 31, 31, 31, 31},
            new int[] {16, 15, 14, 13, 30, 31, 31, 31},
            new int[] {17, 16, 15, 14, 13, 30, 31, 31},
            new int[] {18, 17, 16, 15, 14, 13, 30, 31},
            new int[] {19, 20, 21, 30, 31, 31, 31, 31},
            new int[] {20, 21, 30, 31, 31, 31, 31, 31},
            new int[] {21, 30, 31, 31, 31, 31, 31, 31},
            new int[] {22, 29, 30, 31, 31, 31, 31, 31},
            new int[] {23, 22, 29, 30, 31, 31, 31, 31},
            new int[] {24, 23, 22, 29, 30, 31, 31, 31},
            new int[] {25, 26, 27, 28, 29, 30, 31, 31},
            new int[] {26, 27, 28, 29, 30, 31, 31, 31},
            new int[] {27, 28, 29, 30, 31, 31, 31, 31},
            new int[] {28, 29, 30, 31, 31, 31, 31, 31},
            new int[] {29, 30, 31, 31, 31, 31, 31, 31},
            new int[] {30, 31, 31, 31, 31, 31, 31, 31},
            new int[] {31, 31, 31, 31, 31, 31, 31, 31},
        };

        public int[] default_pal = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, };

        public int[] night_vision_pal = new int[]
        {
            19,
            20,
            20,
            21,
            31,
            19,
            20,
            31,
            19,
            19,
            20,//10
            20,
            21,
            21,
            20,
            19,
            21,
            20,
            19,
            0,
            19,//20
            20,
            21,
            20,
            19,
            20,
            20,
            21,
            21, // floor
            31,
            31,
            31,
        };

        public int[] night_vision_lights_pal = new int[]
        {
            0,
            0,
            0,
            0,
            19,
            0,
            0,
            19,
            0,
            0,
            0,//10
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,//20
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0, // floor
            19,
            19,
            19,
        };

        public int[] lights_out_pal =
        {
            6,
            4,
            30,
            31,
            31,
            30,
            31,
            31,
            11,
            12,
            13,
            30,
            31,
            31,
            31,
            30,
            13,
            14,
            15,
            30,
            31,
            31,
            31,
            30,
            29,
            28,
            29,
            30,
            31,
            31,
            31,
            31,
        };


        public int[] ir_vision_pal = new int[]
        {
            18,
            16,
            15,
            14,
            13,
            15,
            14,
            13,
            18,
            17,
            16,//10 - skin
            15,
            14,
            13,
            14,
            15,
            16,
            17,
            18,
            17,
            15,//20
            13,
            15,
            17,
            18,
            18,
            17,
            16,
            15, // floor
            14,
            13,
            31,
        };

        public int[] ir_vision_lights_pal =
        {
            14,
            13,
            13,
            31,
            31,
            13,
            31,
            31,
            15,
            14,
            13, // 10
            13,
            31,
            31,
            31,
            13,
            13,
            14,
            15,
            13,
            31, // 20
            31,
            31,
            13,
            14,
            15,
            14,
            13,
            31,
            31,
            31, // 30
            31,
            31
        };

        public int[] damage_pal =
        {
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            
            0,
            0,

        };

        public enum game_states
        {
            none,

            main_menu,
            gameplay,
            door_trans_out,
            door_trans_in,
            inventory,
            game_over,
        }

        int ticks_in_state = 0;
        bool menu_done = false;
        game_states game_state;

        public delegate void on_obj_selected(obj o);
        public on_obj_selected event_on_obj_selected;

        public save_game save_data;

        public bool hit_left(obj self, area.collision_flag flag = area.collision_flag.all)
        {
            Vector2 temp = Vector2.Zero;
            return hit_left(self, ref temp, ref temp, flag);
        }

        public bool hit_left(obj self, ref Vector2 hit_point, ref Vector2 correction_pos, area.collision_flag flag = area.collision_flag.all)
        {
            if (game_state == game_states.door_trans_out || game_state == game_states.door_trans_in)
            {
                return false;
            }
            if (self.dx < 0)
            {
                rect col_box = self.col_box();
                Vector2[] test = new Vector2[]
                {
                    new Vector2(col_box.left, col_box.top + col_offset),
                    new Vector2(col_box.left, col_box.bottom - col_offset),
                };
                foreach (Vector2 t in test)
                {
                    if (game_world.cur_area.fget((int)t.X / 8, (int)t.Y / 8, flag))
                    {
                        hit_point = new Vector2(((int)(t.X / 8.0f) * 8.0f) + 8.0f, t.Y);
                        correction_pos = new Vector2(hit_point.X + (self.col_box().w * 0.5f) + (self.x - self.col_box().x), hit_point.Y);
                        return true;
                    }
                    else if (t.X < game_cam.pos_min.X)
                    {
                        hit_point = new Vector2(game_cam.pos_min.X, t.Y);
                        correction_pos = new Vector2(hit_point.X + (self.col_box().w * 0.5f) + (self.x - self.col_box().x), hit_point.Y);
                        return true;
                    }
                }

            }
            return false;
        }

        public bool hit_right(obj self, area.collision_flag flag = area.collision_flag.all)
        {
            Vector2 temp = Vector2.Zero;
            return hit_right(self, ref temp, ref temp, flag);
        }

        public bool hit_right(obj self, ref Vector2 hit_point, ref Vector2 correction_pos, area.collision_flag flag = area.collision_flag.all)
        {
            if (game_state == game_states.door_trans_out || game_state == game_states.door_trans_in)
            {
                return false;
            }
            if (self.dx > 0)
            {
                rect col_box = self.col_box();
                Vector2[] test = new Vector2[]
                {
                    new Vector2(col_box.right, col_box.top + col_offset),
                    new Vector2(col_box.right, col_box.bottom - col_offset),
                };
                foreach (Vector2 t in test)
                {
                    if (game_world.cur_area.fget((int)t.X / 8, (int)t.Y / 8, flag))
                    {
                        hit_point = new Vector2(((int)(t.X / 8.0f) * 8.0f), t.Y);
                        correction_pos = new Vector2(hit_point.X - (self.col_box().w * 0.5f) + (self.x - self.col_box().x), hit_point.Y);
                        return true;
                    }
                    else if (t.X > game_cam.pos_max.X)
                    {
                        hit_point = new Vector2(game_cam.pos_max.X, t.Y);
                        correction_pos = new Vector2(hit_point.X - (self.col_box().w * 0.5f) + (self.x - self.col_box().x), hit_point.Y);
                        return true;
                    }
                }

            }
            return false;
        }

        public bool hit_top(obj self, area.collision_flag flag = area.collision_flag.all)
        {
            Vector2 temp = Vector2.Zero;
            return hit_top(self, ref temp, ref temp, flag);
        }

        public bool hit_top(obj self, ref Vector2 hit_point, ref Vector2 correction_pos, area.collision_flag flag = area.collision_flag.all)
        {
            if (game_state == game_states.door_trans_out || game_state == game_states.door_trans_in)
            {
                return false;
            }

            if (self.dy < 0)
            {
                rect col_box = self.col_box();
                Vector2[] test = new Vector2[]
                {
                    new Vector2(col_box.left + col_offset, col_box.top),
                    new Vector2(col_box.right - col_offset, col_box.top),
                };

                foreach (Vector2 t in test)
                {
                    if (game_world.cur_area.fget((int)t.X / 8, (int)t.Y / 8, flag))
                    {
                        hit_point = new Vector2(t.X, ((int)(t.Y / 8.0f) * 8.0f) + 8.0f);
                        correction_pos = new Vector2(hit_point.X, hit_point.Y + (self.col_box().h * 0.5f) + (self.y - self.col_box().y));
                        return true;
                    }
                    else if (t.Y < game_cam.pos_min.Y) // WIP_VIEWS - Don't lock to cam view. 
                    {
                        hit_point = new Vector2(t.X, game_cam.pos_min.Y);
                        correction_pos = new Vector2(hit_point.X, hit_point.Y + (self.col_box().h * 0.5f) + (self.y - self.col_box().y));
                        return true;
                    }
                }
            }
            return false;
        }

        public bool hit_bottom(obj self, area.collision_flag flag = area.collision_flag.all)
        {
            Vector2 temp = Vector2.Zero;
            return hit_bottom(self, ref temp, ref temp, flag);
        }

        public bool hit_bottom(obj self, ref Vector2 hit_point, ref Vector2 correction_pos, area.collision_flag flag = area.collision_flag.all)
        {
            if (game_state == game_states.door_trans_out || game_state == game_states.door_trans_in)
            {
                return false;
            }
            if (self.dy > 0)
            {
                rect col_box = self.col_box();
                Vector2[] test = new Vector2[]
                {
                    new Vector2(col_box.left + col_offset, col_box.bottom),
                    new Vector2(col_box.right - col_offset, col_box.bottom),
                };
                foreach (Vector2 t in test)
                {
                    if (game_world.cur_area.fget(((int)t.X) / 8, ((int)t.Y) / 8, flag))
                    {
                        hit_point = new Vector2(t.X, ((int)(t.Y / 8.0f) * 8.0f));
                        correction_pos = new Vector2(hit_point.X, hit_point.Y - (self.col_box().h * 0.5f) + (self.y - self.col_box().y));
                        return true;
                    }
                    else if (t.Y > game_cam.pos_max.Y)
                    {
                        hit_point = new Vector2(t.X, game_cam.pos_max.Y);
                        correction_pos = new Vector2(hit_point.X, hit_point.Y - (self.col_box().h * 0.5f) + (self.y - self.col_box().y));
                        return true;
                    }
                }

            }
            return false;
        }

        public void reset()
        {
            // TODO: Reload all the levels so that the object lists get reset.

            // Go to main menu regardless of commandline because level loading is done
            // in the process of going from main_menu -> gameplay.
            set_game_state(game_states.main_menu);

            if (!string.IsNullOrWhiteSpace(CommandLineManager.pInstance["level"]))
            {
                set_game_state(game_states.gameplay);
            }
        }

        public game_states get_game_state()
        {
            return game_state;
        }

        public void set_game_state(game_states new_state)
        {
            if (new_state == game_state)
            {
                return;
            }

            switch (game_state)
            {
                case game_states.main_menu:
                    {
                        if (new_state == game_states.gameplay)
                        {
                            string map_name = menu_root.get_cur_data().get_cur_value();// "Content/raw/" + "top_down_map" + ".tmx";

                            if (!string.IsNullOrWhiteSpace(CommandLineManager.pInstance["level"]))
                            {
                                map_name = CommandLineManager.pInstance["level"];
                            }

                            //game_world = new world(this);
                            //game_world.parse_areas(map_name);

                            //player_fight p = new player_fight(this, game_world.cur_area);
                            player p = new player(this, game_world.cur_area);
                            weap punch = weap.create(weap.weap_type.melee, 0, 0, this, game_world.cur_area);
                            punch.pick_up(p);
                            p.activate();

                            event_on_obj_selected?.Invoke(p);

                            game_cam = new cam(p, this, game_world.cur_area);
                            game_cam.activate();

                            new ui_minimap(this, game_world.ui_area).activate();
                            new ui_inventory_item(this, game_world.ui_area)
                            {
                                x = 144,
                                y = 45,
                            }.activate();
                            new ui_letterbox(this, game_world.ui_area).activate(); // for nis

                            //LoadMap(map_name);
                            QueueLoadMap(map_name);
                        }
                        menu_root.destroy();
                        break;
                    }
                case game_states.game_over:
                    {
                        if (new_state == game_states.gameplay)
                        {
                            // TODO: Copy over player inventory etc.
                            game_world.get_player().destroy();
                            game_world.clear_loaded_areas();
                            game_world.set_global_alert_state(world.alert_states.calm);

                            player p = new player(this, game_world.cur_area);
                            weap punch = weap.create(weap.weap_type.melee, 0, 0, this, game_world.cur_area);
                            punch.pick_up(p);
                            p.activate();

                            event_on_obj_selected?.Invoke(p);

                            // TODO: I think this leaves the cam 
                            //       not in the list of active objects.
                            //       Should maybe call activate?
                            game_cam.tar = p;
                        }
                        break;
                    }
            }

            game_state = new_state;
            ticks_in_state = 0;

            switch (game_state)
            {
                case game_states.main_menu:
                    {
                        menu_done = false;
                        game_world = new world(this);
                        game_cam = new cam(new obj(this), this, game_world.cur_area);
                        game_cam.activate();
                        
                        menu_root = new ui_menu<string>(new List<ui_selector<string>>()
                        {
                            new ui_selector<string>("vr missions", new List<Tuple<string, string>>() { new Tuple<string, string>("vr missions", "vr_basic") }, 0, true, () => { set_game_state(game_states.gameplay); }),
                            populate_map_list(),
                        }, 0, true, this, game_world.ui_area);
                        menu_root.activate();
                        break;
                    }

                case game_states.inventory:
                    {
                        player p = game_world.get_player();

                        if (p != null)
                        {
                            inventory_count = p.inventory.Count;

                            for (int i = 0; i < inventory_count; i++)
                            {
                                if (p.cur_weap == p.inventory[i])
                                {
                                    inventory_index = i;
                                    break;
                                }
                            }
                        }
                        break;
                    }
            }
        }

        public void process_map_data()
        {
            // TODO: Weapons via objects

            //for (int x = 0; x < 128; x++)
            //{
            //    for (int y = 0; y < 64; y++)
            //    {
            //        // offset to the center of the tile.
            //        int pos_x = x * 8 + 4;
            //        int pos_y = y * 8 + 4;
            //        byte empty = 75;
            //        switch (mget(x, y))
            //        {
            //            // machine gun
            //            case 9:
            //                {
            //                    mset(x, y, empty);
            //                    weap.create(weap.weap_type.machine_gun, pos_x, pos_y, this);
            //                    break;
            //                }
            //            // pistol
            //            case 25:
            //                {
            //                    mset(x, y, empty);
            //                    weap.create(weap.weap_type.pistol, pos_x, pos_y, this);
            //                    break;
            //                }
            //            // enemy
            //            case 128:
            //                {
            //                    mset(x, y, empty);
            //                    simple_ai ai = new simple_ai(this)
            //                    {
            //                        x = pos_x,
            //                        y = pos_y,
            //                    };
            //                    weap pistol = weap.create(weap.weap_type.pistol, pos_x, pos_y, this);
            //                    pistol.pick_up(ai);
            //                    break;
            //                }
            //        }
            //    }
            //}
        }

        void toggle_pal()
        {
            cur_pal += 1;
            if (cur_pal >= pals.Length) cur_pal = 0;
            var new_pal = pals[cur_pal];
            for (int i = 0; i < new_pal.Length; i++)
            {
                pal(pals[0][i], new_pal[i]);
            }
        }

        // print string with outline.
        public void printo(string str, float startx, float starty, int col, int col_bg)
        {
            print(str, startx + 1, starty, col_bg);
            print(str, startx - 1, starty, col_bg);
            print(str, startx, starty + 1, col_bg);
            print(str, startx, starty - 1, col_bg);
            print(str, startx + 1, starty - 1, col_bg);
            print(str, startx - 1, starty - 1, col_bg);
            print(str, startx - 1, starty + 1, col_bg);
            print(str, startx + 1, starty + 1, col_bg);
            print(str, startx, starty, col);
        }

        // print string centered with 
        // outline.
        public void printc(string str, float x, float y, int col, int col_bg, int special_chars)
        {
            var len = (str.Length * 4) + (special_chars * 3);
            var startx = x - (len / 2);
            var starty = y - 2;

            printo(str, startx, starty, col, col_bg);
        }

        string gfx_file_name_base = "top_down_gfx_bank_";
        int gfx_anim_bank_count = 5;

#if WINDOWS_UAP
        public async void load_dark_function_async()
        {

            game_anims = new anim_db();
            for (int i = 0; i < gfx_anim_bank_count; i++)
            {
                DarkFunctionImg SpriteDB;
                DarkFunctionAnimations AnimDB;

                Windows.Storage.StorageFile file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync("\\Content\\raw\\top_down\\" + gfx_file_name_base + i.ToString() + "_sprites.sprites");
                Stream stream = await file.OpenStreamForReadAsync();
                XmlSerializer reader = new XmlSerializer(typeof(DarkFunctionImg));
                using (StreamReader streamreader = new StreamReader(stream))
                {
                    SpriteDB = (DarkFunctionImg)reader.Deserialize(streamreader);
                }

                file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync("\\Content\\raw\\top_down\\" + gfx_file_name_base + i.ToString() + "_anims.anim");
                stream = await file.OpenStreamForReadAsync();
                reader = new XmlSerializer(typeof(DarkFunctionAnimations));
                using (StreamReader streamreader = new StreamReader(stream))
                {
                    AnimDB = (DarkFunctionAnimations)reader.Deserialize(streamreader);
                }

                game_anims.add(AnimDB, SpriteDB);
            }
        }
#endif

        public override void _init()
        {
            printh("--init--");

#if WINDOWS_UAP
            load_dark_function_async();
#else
            game_anims = new anim_db();

            // TODO: These aren't being done on UWP!
            game_anims.add_anim_override("soldier_prone_atease_melee");
            game_anims.add_anim_override("soldier_crawling_atease_melee");
            game_anims.add_anim_override("soldier_rolling_atease_melee");
            game_anims.add_anim_override("soldier_incover_atease_melee");
            game_anims.add_anim_override("soldier_crouched_atease_rifle", "pistol");
            game_anims.add_anim_override("soldier_crouched_firing_rifle", "pistol");
            //game_anims.add_anim_override("soldier_standing_atease_pistol", "melee");

            for (int i = 0; i < gfx_anim_bank_count; i++)
            {
                DarkFunctionImg SpriteDB;
                DarkFunctionAnimations AnimDB;

                string filepath = Directory.GetCurrentDirectory() + "/Content/raw/top_down/" + gfx_file_name_base + i.ToString() + "_sprites.sprites";
                XmlSerializer reader = new XmlSerializer(typeof(DarkFunctionImg));
                using (StreamReader file = new StreamReader(filepath))
                {
                    SpriteDB = (DarkFunctionImg)reader.Deserialize(file);
                }

                filepath = Directory.GetCurrentDirectory() + "/Content/raw/top_down/" + gfx_file_name_base + i.ToString() + "_anims.anim";
                reader = new XmlSerializer(typeof(DarkFunctionAnimations));
                using (StreamReader file = new StreamReader(filepath))
                {
                    AnimDB = (DarkFunctionAnimations)reader.Deserialize(file);
                }

                game_anims.add(AnimDB, SpriteDB);
            }
#endif

            save_data = new save_game();
            // Mono8_Port
            //menuitem(1, "swap palatte", toggle_pal)

            EnableDebugKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.F1);
            EnablePalDisplayKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.F2);
            EnableCollisionDisplayKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.F3);
            KillAllKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.K);
            NightVisionKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.N);
            IRVisionKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.I);
            LightsKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.L);
            LOSKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.S);
            FirstPersonKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.F4);
            NISKey = new BufferedKey(Microsoft.Xna.Framework.Input.Keys.C);

            // Initialize our sprite effects.
            sprfxadd(sprfx_camo_warp, 0);

            reset();
        }

        ui_selector<string> populate_map_list()
        {
            string targetDirectory = Directory.GetCurrentDirectory() + "/Content/raw/top_down/";

            // Process the list of files found in the directory.
            string[] map_file_list = Directory.GetFiles(targetDirectory, "*.tmx");

            List<Tuple<string /*label*/, string/*value*/>> data_set = new List<Tuple<string, string>>();
            int count = 1;
            foreach (string s in map_file_list)
            {
                string file = Path.GetFileNameWithoutExtension(s);
                data_set.Add(new Tuple<string, string>(file + " (" + count++ + "/" + map_file_list.Length + ")", file));
            }

            return new ui_selector<string>("level select", data_set, 0, true, () => { set_game_state(game_states.gameplay); });
        }

        public int sprfx_camo_warp(int x, int y, int c)
        {
            // Don't do anything with transparent pixels.
            // TODO: Check if c == palt value.
            if (c == 19) return c;

            // Dither:
            //if (x % 2 == 0 && y % 2 == 0 || x % 2 == 1 && y % 2 == 1)
            //{
            //    return 19;
            //}

            // Look at the color already at this location and use that color
            // but faded out slightly.
            return FadeTable[pget(x, y)][1];
        }

        int inventory_index = 0;
        int inventory_count = 0;


        public override void _update60()
        {
            ticks_in_state += 1;
            tick += 1;

            if (!string.IsNullOrWhiteSpace(next_area))
            {
                LoadMap(next_area);
                next_area = "";
            }

#if DEBUG
            if (EnableDebugKey.Update())
            {
                debug_enabled = !debug_enabled;
            }

            EnablePalDisplayKey.Update();
            EnableCollisionDisplayKey.Update();
            if (KillAllKey.Update())
            {
                for (int i = game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    if (game_world.cur_area.objs_active[i].type_flag == type_flags.Enemy)
                    {
                        game_world.cur_area.objs_active[i].destroy();
                    }
                }
            }

            if (NightVisionKey.Update())
            {
                night_vision_enabled = !night_vision_enabled;
            }
            if (IRVisionKey.Update())
            {
                ir_vision_enabled = !ir_vision_enabled;
            }

            if (LightsKey.Update())
            {
                game_world.cur_area.toggle_lights();
            }

            if (LOSKey.Update())
            {
                los_enabled = !los_enabled;
            }

            if (NISKey.Update())
            {
                new nis.nis_scene_test(this);
            }
#endif

            if (game_state == game_states.main_menu)
            {
                //if (btnp(3))
                //{
                //    menu_root.inc_index(-1);
                //}
                //if (btnp(2))
                //{
                //    menu_root.inc_index(1);
                //}
                //if (btnp(0))
                //{
                //    menu_root.get_cur_data().inc_index(-1);
                //}
                //if (btnp(1))
                //{
                //    menu_root.get_cur_data().inc_index(1);
                //}
                //if (btnp(4) || btnp(5))
                //{
                //    if (!menu_done)
                //    {
                //        menu_done = true;
                //    }
                //    else
                //    {
                //        menu_root.execute();
                //    }
                //}
            }
            else if (game_state == game_states.gameplay)
            {
                if (btnp(7))
                {
                    set_game_state(game_states.inventory);
                }

                for (int i = game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    game_world.cur_area.objs_active[i].update();
                }

                // UPDATE FPS VIEW
                if (FirstPersonKey.Update())
                {
                    first_person_view_enabled = !first_person_view_enabled;
                }

                game_world.update();

                game_cam.update();
            }
            else if (game_state == game_states.door_trans_out)
            {
                if (ticks_in_state > ticks_for_door_trans)
                {
                    on_door_trans_complete();
                    set_game_state(game_states.door_trans_in);
                }

                // Only the player gets updated.
                game_world.get_player().update();

                game_world.update();

                game_cam.update();
            }
            else if (game_state == game_states.door_trans_in)
            {
                if (ticks_in_state > ticks_for_door_trans)
                {
                    set_game_state(game_states.gameplay);
                }

                // Only the player gets updated.
                game_world.get_player().update();

                game_world.update();

                game_cam.update();
            }
            else if (game_state == game_states.inventory)
            {
                if (btnp(7))
                {
                    set_game_state(game_states.gameplay);
                }

                if (btnp(4) || btnp(5))
                {
                    player p = game_world.get_player();
                    weap w = p.inventory[inventory_index] as weap;
                    if (w != null)
                    {
                        p.activate_weap(w);
                    }

                    gear g = p.inventory[inventory_index] as gear;
                    if (g != null)
                    {
                        g.toggle();
                    }
                }
                if (btnp(2))
                {
                    inventory_index = (int)max(0, inventory_index - 1);
                }
                else if (btnp(3))
                {
                    inventory_index = (int)min(inventory_count - 1, inventory_index + 1);
                }

                // update with time stopped.
                game_world.update(0);
            }
            else if(game_state == game_states.game_over)
            {
                if (btnp(4) || btnp(5))
                {
                    set_game_state(game_states.gameplay);
                    QueueLoadMap(save_data.checkpoint_map_name);
                }
                for (int i = game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    game_world.cur_area.objs_active[i].update();
                }

                game_world.update();

                game_cam.update();
            }

            // Update to top most ui.
            // TODO: Update everything but bubble input from the top-most.
            //int cur_ui = game_world.ui_area.objs_active.Count - 1;
            //if (cur_ui >= 0)
            //{
            //    game_world.ui_area.objs_active[cur_ui].update();
            //}
            for (int i = game_world.ui_area.objs_active.Count - 1; i >= 0; i--)
            {
                game_world.ui_area.objs_active[i].update();
            }
        }

        public Vector2 rot_vec(float x, float y, float rot)
        {
            float _x = x * (float)cos(rot) - y * (float)sin(rot);

            float _y = x * (float)sin(rot) + y * (float)cos(rot);

            return new Vector2(_x, _y);
        }

        public void apply_pal(int[] p)
        {
            for (int i = 0; i < 32; i++)
            {
                pal(i, p[i]);
            }
        }

        float first_person_view_height = 0;

        public void _draw_fps_view()
        {
            //  //printh("//////////////-")
            //  cls()

            camera(0, 0);

            for (int i = 0; i <= 15; i++)
            {
                // TODO
                //pal(i, fadetable[i][light])
            }

            // TOP_DOWN_CHANGE
            player p = game_world.get_player();

            float view_change_speed = 4;
            if (p.is_crawling)
            {
                first_person_view_height = max(-64, first_person_view_height - view_change_speed);
            }
            else
            {
                first_person_view_height = min(0, first_person_view_height + view_change_speed);
            }

            rectfill(0, 0, 127, 63 /*+ (int)first_person_view_height*/, 22);
            rectfill(0, 63 /*+ (int)first_person_view_height*/, 127, 127, 28);

            for (int x = 0; x <= 127; x++)
            {

                //put camera into - 1 to 1 space

                float camerax = 2.0f * x / 128.0f - 1.0f;

                // TOP_DOWN_CHANGE
                float posx = p.col_box().x / 8;
                float posy = p.col_box().y / 8;

                float rayposx = posx;
                float rayposy = posy;

                //// TOP_DOWN_CHANGE
                //// Should be based on player facing.
                float dirx = p.fx;
                float diry = p.fy;

                Vector2 dir = new Vector2(dirx, diry);
                dir.Normalize();
                float planex = 0.66f * -dir.Y;
                float planey = 0.66f * dir.X;

                float raydirx = dirx + planex * camerax;
                //      raydirx = 1

                float raydiry = diry + planey * camerax;
                //      raydiry = 1


                int mapx = (int)Math.Floor(rayposx);

                int mapy = (int)Math.Floor(rayposy);


                float sidedistx = 0;

                float sidedisty = 0;


                float deltadistx = (float)Math.Sqrt(1 + ((raydiry * raydiry) / (raydirx * raydirx)));

                float deltadisty = (float)Math.Sqrt(1 + ((raydirx * raydirx) / (raydiry * raydiry)));

                //if(ray dir approaches zero,
                //it will have causes lua to
                //go over Math.Max num when calc
                //deltadist(divide by near 0).

                //to account for this, detect
                //&& treat as a very large
                //deltadist.
                if (Math.Abs(raydirx) < 0.01)
                {

                    deltadistx = 9999;

                }
                if (Math.Abs(raydiry) < 0.01)
                {

                    deltadisty = 9999;

                }

                if (false && (Math.Abs(raydiry) < 0.006 || Math.Abs(raydirx) < 0.006))
                {

                    //printh("error!!!!!!")

                    //printh("sqrt:"..Math.Sqrt(1 / 0.1))

                    //printh(raydirx..","..raydiry)

                    //printh(deltadistx..","..deltadisty)

                }


                float perpwalldist = 0;

                int stepx = 0;

                int stepy = 0;

                float side = -1;

                if (raydirx < 0)
                {

                    stepx = -1;

                    sidedistx = (rayposx - mapx) * deltadistx;
                }
                else if (raydirx > 0)
                {

                    stepx = 1;

                    sidedistx = (mapx + 1 - rayposx) * deltadistx;

                }

                if (raydiry < 0)
                {

                    stepy = -1;

                    sidedisty = (rayposy - mapy) * deltadisty;
                }
                else if (raydiry > 0)
                {

                    stepy = 1;

                    sidedisty = (mapy + 1 - rayposy) * deltadisty;

                }

                draw_fps_view_recurse(sidedistx,  sidedisty,  deltadistx,  deltadisty,  mapx,  mapy, side,  stepx,  stepy,  perpwalldist,  rayposx,  rayposy,  raydirx,  raydiry,  x);
            }
        }

        void draw_fps_view_recurse(float sidedistx, float sidedisty, float deltadistx, float deltadisty, int mapx, int mapy, 
            float side, int stepx, int stepy, float perpwalldist, float rayposx, float rayposy, float raydirx, float raydiry, int x)
        {
            bool hit = false;

            //int col = 0;
            area.collision_flag flag = area.collision_flag.none;
            while (!hit)
            {

                if (sidedistx < sidedisty)
                {

                    sidedistx += deltadistx;

                    mapx += stepx;

                    side = 0;
                }
                else
                {
                    sidedisty += deltadisty;

                    mapy += stepy;

                    side = 1;

                }

                // TOP_DOWN_CHANGE
                flag = game_world.cur_area.fget(mapx, mapy);
                int sprite_id = mget(mapx, mapy);
                //col = sget(mapx, mapy);

                // TOP_DOWN_CHANGE
                if (mapx >= game_world.cur_area.width || mapy >= game_world.cur_area.height || mapy < 0 || mapx < 0)
                    hit = true;

                if ((flag & area.collision_flag.all) != area.collision_flag.none)
                {
                    hit = true;
                }

                if (hit)
                {
                    if (side == 0)
                    {
                        perpwalldist = (mapx - rayposx + (1 - stepx) / 2) / raydirx;
                    }
                    else
                    {
                        perpwalldist = (mapy - rayposy + (1 - stepy) / 2) / raydiry;
                    }


                    int lineheight = (int)Math.Floor(128.0f / perpwalldist);

                    float drawstart = Math.Max(0, -lineheight / 2 + 127 / 2);

                    int color = 30;

                    if (flag == area.collision_flag.low)
                    {
                        drawstart = 63;
                        lineheight /= 2;
                        color = 3;

                        draw_fps_view_recurse(sidedistx, sidedisty, deltadistx, deltadisty, mapx, mapy, side, stepx, stepy, perpwalldist, rayposx, rayposy, raydirx, raydiry, x);
                    }
                    else if (flag == area.collision_flag.high)
                    {
                        lineheight /= 2;

                        draw_fps_view_recurse(sidedistx, sidedisty, deltadistx, deltadisty, mapx, mapy, side, stepx, stepy, perpwalldist, rayposx, rayposy, raydirx, raydiry, x);
                    }
                    else
                    {
                        // full wall.
                    }

                    drawstart += (int)Math.Floor(first_person_view_height / perpwalldist);


                    float wallx = 0;
                    if (side == 0)
                    {
                        wallx = rayposy + perpwalldist * raydiry;
                    }
                    else
                    {
                        wallx = rayposx + perpwalldist * raydirx;
                    }

                    //printh("!!!")
                    //printh("side:"..side)
                    //printh("rayposx:"..rayposx)
                    //printh("perpwalldist:"..perpwalldist)
                    //printh("raydirx:"..raydirx)
                    //printh("wallx:"..wallx)
                    //printh("wallx:"..rayposx + (perwalldist * raydirx))
                    //printh("wallx:"..22 + 4.618 * 1)
                    //printh("!!!")

                    wallx = wallx - (float)Math.Floor(wallx);

                    //u coord of texture

                    float texx = (float)Math.Floor((wallx * 8));

                    if (side == 0 && raydirx > 0)
                    {
                        texx = 8 - texx - 1;
                    }
                    else if (side == 1 && raydiry < 0)
                    {
                        texx = 8 - texx - 1;
                    }

                    // TOP_DOWN_CHANGE
                    bool dump = false;
                    if (dump == true)
                    {
                        //printh("//-")
                        //printh("x:"..x)
                        //printh("lineheight:"..lineheight)
                        if (lineheight == 0)
                        {
                            //printh("error error error")
                        }
                        //printh("map:["..mapx..","..mapy.."]")
                        //printh("perpwalldist:"..perpwalldist)
                        //printh("side:"..side)
                        //printh("rayposx:"..rayposx)
                        //printh("rayposy:"..rayposy)
                        //printh("raydirx:"..raydirx)
                        //printh("raydiry:"..raydiry)
                        //printh("deltadistx:"..deltadistx)
                        //printh("deltadisty:"..deltadisty)
                        //printh("sidedistx:"..sidedistx)
                        //printh("sidedisty:"..sidedisty)
                        //printh("wallx:"..wallx)
                        //printh("texx:"..texx)
                    }


                    // TOP_DOWN_CHANGE
                    //float sx = col * 8 + texx;
                    //sspr((int)sx, (int)side * 8,
                    //    1, 8,
                    //    x, (int)drawstart,
                    //    1, lineheight,
                    //    false, false);

                    bool draw_textures = true;

                    if (!draw_textures)
                    {
                        if (color != 31)
                        {
                            if (side == 1)
                            {
                                line(x, (int)drawstart, x, (int)(drawstart + lineheight), color - 1);
                            }
                            else
                            {
                                line(x, (int)drawstart, x, (int)(drawstart + lineheight), color);
                            }
                        }
                    }
                    else
                    {
                        int sprite_x = sprite_id % 16;
                        int sprite_y = flr(sprite_id / 16);
                        float sx = sprite_x * 8 + texx;
                        float sy = sprite_y * 8;
                        sspr((int)sx, (int)sy,
                            1, 8,
                            x, (int)drawstart,
                            1, lineheight,
                            false, false);
                    }
                }
            }
        }

        public override void _draw()
        {
            // SCREEN CLEARING
            //

            pal();

            palt(0, false);
            palt(19, true); // Sprites use 19 for transparent.

            if (first_person_view_enabled)
            {
                _draw_fps_view();
                return;
            }

            if (!ir_vision_enabled)
            {
                cls(31);
            }
            else
            {
                // When IR is on, we don't clear the screen so that we can apply a dynamic
                // dithering effect to clear the screen.
                // See: https://trasevol.dog/2017/02/01/doodle-insights-2-procedural-dithering-part-1/
                for (int i = 0; i < 1000; i++)
                {
                    int x = (int)Math.Round(rnd(127));
                    int y = (int)Math.Round(rnd(127));
                    int c = pget(x, y);
                    c = FadeTable[c][1]; // fade one shade darker... eventually stabilize on 31.
                    circ(x, y, 1, c);
                }
            }

            // CAMERA
            //

            camera(game_cam.cam_pos().X, game_cam.cam_pos().Y);

            // MAP LOWER LAYER
            //

            apply_pal(get_cur_pal());

            // When IR is enabled we turn th floor color into the clear color, so that the floor
            // doesn't get drawn. Obviously this has pitfalls (anything else that color will not
            // get drawn too and our traditional color key will be drawn).
            if (ir_vision_enabled)
            {
                palt(28, true);
            }

            bool in_duct = false;
            if (map_grid != null)
            {
                player p = game_world.get_player();
                if (p != null)
                {
                    int px = (int)(p.col_box().x / 8.0f);
                    int py = (int)(p.col_box().y / 8.0f);

                    if (px >= 0 && py >= 0 && px < map_grid.DimX && py < map_grid.DimY)
                    {
                        in_duct = (game_world.cur_area.fget(px, py) == area.collision_flag.high);
                    }
                }
            }

            if (!in_duct)
            {
                // Map uses 0 for transparent.
                //palt(0, true);
                //palt(16, false);
                bset(0);
                map(0, 0, 0, 0, 128, 128, 0, (int)map_layers.low);
                map(0, 0, 0, 0, 128, 128, 0, (int)map_layers.decal);
                sprfxset(0, true);
                // TODO: This could be done after drawing objects, and looks pretty nice,
                //       the problem is some areas are not covered in shadow even though
                //       they should be (eg. walls at top).
                map(0, 0, 0, 0, 128, 128, 0, (int)map_layers.shadow);
                sprfxset(0, false);
            }
            pal();

            // OBJECT RENDERING
            //

            // Sort objs based on the bottom of their sprite.
            game_world.cur_area.objs_active.Sort(delegate (obj lhs, obj rhs)
            {
                var ly = lhs.y + lhs.h_half();
                var ry = rhs.y + rhs.h_half();
                if (lhs.draw_layer < rhs.draw_layer)
                {
                    return 1;
                }
                else if (lhs.draw_layer > rhs.draw_layer)
                {
                    return -1;
                }
                else if (ly == ry)
                {
                    return 0;
                }
                else if (ly > ry)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            });

            // Restore the floor (in the case the IR was enabled).
            palt(28, false);

            // TODO: Should probably check if the object is in duct too.
            if (!in_duct)
            {
                for (int i = game_world.cur_area.objs_active.Count - 1; i >= 0; i--)
                {
                    obj o = game_world.cur_area.objs_active[i];
                    if (o.gfx_bank.HasValue)
                    {
                        bset(o.gfx_bank.Value);
                    }
                    else
                    {
                        bset(0);
                    }

                    o.draw();
                }
            }

            // MAP UPPER LAYER
            //

            apply_pal(get_cur_pal());

            // Don't draw the floor.
            if (ir_vision_enabled)
            {
                palt(28, true);
            }
            bset(0);
            if (!in_duct)
            {
                map(0, 0, 0, 0, 128, 128, 0, (int)map_layers.high);
            }
            pal();

            // Restore the floor.
            palt(28, false);

            /*
            //first_person_view_enabled = false;
            if (map_grid != null)
            {
                player p = game_world.get_player();
                if (p != null)
                {
                    int px = (int)(p.col_box().x / 8.0f);
                    int py = (int)(p.col_box().y / 8.0f);

                    if (px >= 0 && py >= 0 && px < map_grid.DimX && py < map_grid.DimY)
                    {
                        if (game_world.cur_area.fget(px, py) == area.collision_flag.high)
                        {
                            for (int x = (int)(game_cam.x / 8.0f) - 8; x < (int)(game_cam.x / 8.0f) + 8; x++)
                            {
                                for (int y = (int)(game_cam.y / 8.0f) - 8; y < (int)(game_cam.y / 8.0f) + 8; y++)
                                {
                                    if (x >= 0 && y >= 0 && x < map_grid.DimX && y < map_grid.DimY)
                                    {
                                        if (game_world.cur_area.fget(x, y) == area.collision_flag.high)
                                        {
                                            //rectfill(x * 8, y * 8, x * 8 + 7, y * 8 + 7, 30);
                                        }
                                    }
                                }
                            }

                            //sprfxset(0, false);
                            map(0, 0, 0, 0, 128, 128, 0, (int)map_layers.ducts);

                            //sprfxset(0, true);
                            bset(1);
                            p.draw();
                            bset(0);
                            //sprfxset(0, false);

                            //first_person_view_enabled = true;
                        }
                    }
                }
            }
            */

            if (in_duct)
            {
                map(0, 0, 0, 0, 128, 128, 0, (int)map_layers.ducts);

                bset(1);
                game_world.get_player()?.draw();
                bset(0);
            }

            if (los_enabled)
            {
                List<Tuple<int, int>> Visited = new List<Tuple<int, int>>();
                if (map_grid != null && game_world.get_player() != null)
                {
                    int px = flr(game_world.get_player().col_box().x / 8.0f);
                    int py = flr(game_world.get_player().col_box().y / 8.0f);

                    for (int x = (int)(game_cam.x / 8.0f) - 8; x < (int)(game_cam.x / 8.0f) + 8; x++)
                    {
                        for (int y = (int)(game_cam.y / 8.0f) - 8; y < (int)(game_cam.y / 8.0f) + 8; y++)
                        {
#if false
                        Vector2 other_pos = new Vector2(x + 4, y + 4);
                        Vector2 check_pos = new Vector2(px + 4, py + 4);
                        Vector2 to_other = new Vector2(other_pos.X - check_pos.X, other_pos.Y - check_pos.Y);
                        to_other.Normalize();

                        bool found = false;
                        while (Vector2.Distance(other_pos, check_pos) > 1.0f)
                        {
                            check_pos += to_other;
                            //Game.rect_d(check_pos.X - 4, check_pos.Y - 4, check_pos.X + 4, check_pos.Y + 4, 23);

                            // Make sure there is enough room for bullets.
                            //for (int dx = -3; dx <= 3; dx++)
                            //{
                            //    for (int dy = -3; dy <= 3; dy++)
                            //    {
                                    if (game_world.cur_area.fget((int)(check_pos.X), (int)(check_pos.Y), area.collision_flag.all))
                                    {
                                        found = true;
                                        break;
                                    }
                            //    }
                            //}
                        }

                        if (found)
                        {
                            rectfill(other_pos.X * 8 - 4, other_pos.Y * 8 - 4, other_pos.X * 8 + 4, other_pos.Y * 8 + 4, 31);
                        }
#endif

                            float x0 = px; //(float)Math.Round(game_world.get_player().x / 8.0f);
                            float y0 = py; //(float)Math.Round(game_world.get_player().y / 8.0f);
                            float x1 = x;
                            float y1 = y;

                            float w = x1 - x0;
                            float h = y1 - y0;
                            float dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
                            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
                            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
                            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
                            int longest = (int)Math.Abs(w);
                            int shortest = (int)Math.Abs(h);
                            if (!(longest > shortest))
                            {
                                longest = (int)Math.Abs(h);
                                shortest = (int)Math.Abs(w);
                                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                                dx2 = 0;
                            }
                            bool found = false;
                            int numerator = longest >> 1;

                            for (int i = 0; i <= longest; i++)
                            {
                                //pset(x0 * 8, y0 * 8, 15);
                                // CHECK TILE TYPE HERE
                                if (found || game_world.cur_area.fget((int)(x0), (int)(y0), area.collision_flag.high))
                                {
                                    if (!Visited.Exists(item => item.Item1 == x0 && item.Item2 == y0))
                                    {
                                        for (int a = 0; a < 8; a++)
                                        {
                                            for (int b = 0; b < 8; b++)
                                            {
                                                float sx = ((x0 * 8) + a);// - game_cam.cam_pos().X;
                                                float sy = ((y0 * 8) + b);// - game_cam.cam_pos().Y;
                                                pset(sx, sy, FadeTable[pget((int)sx, (int)sy)][3]);
                                            }
                                        }

                                        Visited.Add(new Tuple<int, int>((int)x0, (int)y0));
                                    }

                                    //rectfill(x0 * 8, y0 * 8, x0 * 8 + 7, y0 * 8 + 7, 31);
                                    //pset(x0 * 8, y0 * 8, 31);
                                    found = true;
                                }

                                numerator += shortest;
                                if (!(numerator < longest))
                                {
                                    numerator -= longest;
                                    x0 += dx1;
                                    y0 += dy1;
                                }
                                else
                                {
                                    x0 += dx2;
                                    y0 += dy2;
                                }
                            }
                        }
                    }
                }
            }

            // IN WORLD DEBUG
            //
#if DEBUG

            foreach (var p in game_world.cur_area.paths)
            {
                int i = 0;
                for (; i < p.points.Count - 1; i++)
                {
                    line_d(p.points[i].position.X, p.points[i].position.Y, p.points[i + 1].position.X, p.points[i + 1].position.Y, 30);
                    print_d(i.ToString(), p.points[i].position.X, p.points[i].position.Y - 4, 26);
                }

                // Print the loop
                line_d(p.points[i].position.X, p.points[i].position.Y, p.points[0].position.X, p.points[0].position.Y, 30);
                print_d(i.ToString(), p.points[i].position.X, p.points[i].position.Y - 4, 26);
            }

            //sprfxset(0, true);

            if (map_grid != null && EnableCollisionDisplayKey.IsPressed)
            {
                for (int x = 0; x < map_grid.DimX; x++)
                {
                    for (int y = 0; y < map_grid.DimY; y++)
                    {
                        float cost = map_grid.GetCellCost(new Position(x, y));

                        if (game_world.cur_area.fget(x, y) == area.collision_flag.high)
                        {
                            rect_d(x * 8, y * 8, x * 8 + 7, y * 8 + 7, 17);
                        }
                        else if (game_world.cur_area.fget(x, y) == area.collision_flag.low)
                        {
                            rect_d(x * 8, y * 8, x * 8 + 7, y * 8 + 7, 16);
                        }
                        else if (game_world.cur_area.fget(x, y) == area.collision_flag.door)
                        {
                            rect_d(x * 8, y * 8, x * 8 + 7, y * 8 + 7, 9);
                        }
                        else if (game_world.cur_area.fget(x, y) == area.collision_flag.all)
                        {
                            rect_d(x * 8, y * 8, x * 8 + 7, y * 8 + 7, 15);
                        }
                    }
                }
            }
            foreach (var l in DebugDrawQueue)
            {
                l();
            }


            //sprfxset(0, false);
#endif

            DebugDrawQueue.Clear();

            // HUD CAMERA
            //

            camera(0, 0);

            // POST PROCESS
            //

            if (night_vision_enabled || ir_vision_enabled)
            {
                // vignette
                for (int x = 0; x < 128; x++)
                {
                    for (int y = 0; y < 128; y++)
                    {
                        player p = game_world.get_player();
                        if (p != null)
                        {
                            float dist_rnd = 16;
                            if (game_world.cur_area.light_status == area.lighting_status.on)
                            {
                                dist_rnd = 64;
                            }
                            Vector2 v = new Vector2(p.x - game_cam.x + 64 + (rnd(dist_rnd) - (dist_rnd * 0.5f)), p.y - game_cam.y + 64 + (rnd(dist_rnd) - (dist_rnd * 0.5f)));
                            //Vector2 v = new Vector2(63 + (rnd(16) - 8), 63 + (rnd(16) - 8));
                            float dist = Vector2.Distance(v, new Vector2(x, y));
                            int fade_index = (int)MathHelper.SmoothStep(0, 8, dist / 300.0f);
                            if (rnd(100) < 75.0f)
                            {
                                fade_index = MathHelper.Clamp(fade_index + ((int)rnd(3) - 1), 0, 7);
                            }
                            pset(x, y, FadeTable[pget(x, y)][fade_index]);
                        }
                    }
                }

                // scanline
                int min_y = (int)(((tick * 0.5f) % 128.0f));

                int max_y = min_y + 3;

                for (int y = min_y; y < max_y; y++)
                {
                    float y_final = y % 128;

                    float d = sin(((y_final * cos(tick * 0.1f)) + tick * 2) * 0.01f) * 127;

                    for (int x = (int)abs(d); x > 0; x--)
                    {
                        float x_samp = x - cos(tick * 0.1f);
                        pset(x, y_final, pget((int)x_samp, (int)y_final));
                    }
                }
            }
            //else
            //{
            //    // default noise
            //    for (int x = 0; x < 128; x++)
            //    {
            //        for (int y = 0; y < 128; y++)
            //        {
            //            player p = game_world.get_player();
            //            if (p != null)
            //            {
            //                int fade_index = 0;
            //                if (rnd(100) < 50.0f)
            //                {
            //                    fade_index = MathHelper.Clamp(fade_index + ((int)rnd(2)), 0, 7);
            //                }
            //                pset(x, y, FadeTable[pget(x, y)][fade_index]);
            //            }
            //        }
            //    }
            //}

            // HUD RENDERING
            //

            switch (game_state)
            {
                case game_states.main_menu:
                    {
                        // Based on: https://twitter.com/voxeledphoton/status/1023774248538701824
                        //int m = 64;
                        //srand(1);
                        //for (int i = 0; i < 20; i++)
                        //{
                        //    var a = rnd(1);
                        //    var z = (rnd(176) + (ticks_in_state * 0.016f) * m) % 176;
                        //    var d = cos(a) * z;
                        //    var e = sin(a) * z;
                        //    var f = cos(a + 0.1f) * z;
                        //    var g = sin(a + 0.1f) * z;
                        //    var u = m + d / 2.0f;
                        //    var v = m + e / 2.0f;
                        //    var w = m + f / 2.0f;
                        //    var x = m + g / 2.0f;
                        //    var c = (int)((1.0f - (z / 176.0f)) * 7) + 24;
                        //    line(m + d, m + e, m + f, m + g, c);
                        //    line(u, v, w, x, c);
                        //    line(m + d, m + e, u, v, c);
                        //    line(m + f, m + g, w, x, c);
                        //}

                        float yscroll = easeinoutquint(max(0, min(ticks_in_state, 128)), 440, (512 - 440), 128);
                        if (menu_done)
                        {
                            yscroll = 512;
                        }
                        else if ((int)yscroll >= 512)
                        {
                            menu_done = true;
                        }

                        // TODO: Move to Map, and possibly new bank.
                        sspr(0, (int)yscroll, 128, 128, 0, 0);

                        if (menu_done)
                        {
                            int mx = 64;
                            int my = 32;
                            int c1 = 25;
                            int c2 = 30;
                            printc("project metal solider", mx, my, c1, c2, 0);
                        }

                        break;
                    }
                case game_states.door_trans_out:
                    {
                        // SIDE PANEL
                        rectfill(128, 0, 159, 127, 31);
                        float amount = (float)ticks_in_state / (float)ticks_for_door_trans;
                        int fade_index = (int)min((int)(amount * FadeTable[0].Length), FadeTable[0].Length - 1);
                        for (int i = 0; i < FadeTable.Length; i++)
                        {
                            pal(i, FadeTable[i][fade_index], 1);
                        }
                        break;
                    }
                case game_states.door_trans_in:
                    {
                        // SIDE PANEL
                        rectfill(128, 0, 159, 127, 31);
                        float amount = 1.0f - ((float)ticks_in_state / (float)ticks_for_door_trans);
                        int fade_index = (int)min((int)(amount * FadeTable[0].Length), FadeTable[0].Length - 1);
                        for (int i = 0; i < FadeTable.Length; i++)
                        {
                            pal(i, FadeTable[i][fade_index], 1);
                        }
                        break;
                    }
                case game_states.game_over:
                    {
                        // SIDE PANEL
                        rectfill(128, 0, 159, 127, 31);

                        int ticks_delay_on_death = 120;
                        int ticks_fade_on_death = 120;

                        if (ticks_in_state > ticks_delay_on_death + ticks_fade_on_death)
                        {
                            rectfill(0, 0, 127, 127, 31);
                            printc("game over", 64, 64, 0, 30, 0);
                        }
                        else if (ticks_in_state > ticks_delay_on_death)
                        {
                            int adjusted_ticks = ticks_in_state - ticks_delay_on_death;

                            float amount = (float)adjusted_ticks / (float)ticks_fade_on_death;
                            int fade_index = (int)min((int)(amount * FadeTable[0].Length), FadeTable[0].Length - 1);
                            for (int i = 0; i < FadeTable.Length; i++)
                            {
                                pal(i, FadeTable[i][fade_index], 1);
                            }
                        }
                        break;
                    }
                case game_states.gameplay:
                    {
                        // SIDE PANEL
                        rectfill(128, 0, 159, 127, 31);
                        game_world.draw_alert_state();

                        break;
                    }
                case game_states.inventory:
                    {
                        //float final_y = max(128 - (ticks_in_state * 4), 2);
                        //rectfill(2, final_y, 125, final_y + 123, 31);

                        for (int y = 0; y < 128; y++)
                        {
                            for (int x = 0; x < 128; x++)
                            {
                                int fade_amount = (int)min(ticks_in_state, 3);
                                pset(x, y, FadeTable[pget(x, y)][fade_amount]);
                            }
                        }

                        player p = game_world.get_player();

                        if (p != null)
                        {
                            int y_pos = 8;
                            for (int i = 0; i < p.inventory.Count; i++)
                            {
                                obj o = p.inventory[i];
                                int col = 0;
                                int col_bg = 30;
                                string display = o.display_name;
                                if (p.cur_weap == o)
                                {
                                    display += " [e]";
                                }
                                gear g = o as gear;
                                if (g != null)
                                {
                                    if (g.enabled)
                                    {
                                        display += " [on]";
                                    }
                                    else
                                    {
                                        display += " [off]";
                                    }
                                }
                                weap w = o as weap;
                                if (w != null && w.uses_ammo)
                                {
                                    display += "[" + w.ammo_remaining + "]";
                                }
                                if (i == inventory_index)
                                {
                                    col = 18;
                                    col_bg = 29;
                                }
                                printo(display, 6, y_pos, col, col_bg);
                                y_pos += 8;
                            }
                        }

                        break;
                    }
            }

            for (int i = 0; i < game_world.ui_area.objs_active.Count; i++)
            {
                obj o = game_world.ui_area.objs_active[i];
                if (o.gfx_bank.HasValue)
                {
                    bset(o.gfx_bank.Value);
                }
                else
                {
                    bset(0);
                }

                o.draw();
            }
            
            // HUD DEBUG
            //

#if DEBUG
            print_d("obj:" + game_world.cur_area.objs_active.Count, 2, 3, 31);
            print_d("obj:" + game_world.cur_area.objs_active.Count, 2, 2, 0);

            if (EnablePalDisplayKey.IsPressed)
            {
                int c = 0;
                for (int y = 0; y < 16; y++)
                {
                    for (int x = 0; x < 16; x++)
                    {
                        rectfill_d(x * 8, y * 8, x * 8 + 7, y * 8 + 7, c);
                        print_d((c % 100).ToString(), x * 8, y * 8, c + 1);
                        c++;
                    }
                }

                int size = 4;
                for (int p = 0; p < 32; p++)
                {
                    for (int col = 0; col < 8; col++)
                    {
                        int fcol = FadeTable[p][col];
                        int x = col * size + (flr(p / 16) * 32);
                        int y = (32 + p * size) - (flr(p / 16) * (size * 16));
                        rectfill_d(x, y, x + (size - 1), y + (size - 1), fcol);
                        //print_d((fcol).ToString(), col * size, p * size, (fcol + 1) % 32);
                    }
                }
            }

            foreach (var l in DebugDrawQueue)
            {
                l();
            }
#endif

            DebugDrawQueue.Clear();
        }

        public void QueueLoadMap(string area_name, Vector2? spawn_point = null)
        {
            if (area_name == null)
            {
                return;
            }

            // Make the calling pattern a little simpler to use.
            if (!area_name.StartsWith("content/raw/top_down/", StringComparison.CurrentCultureIgnoreCase))
            {
                area_name = "content/raw/top_down/" + area_name;
            }
            if (!area_name.EndsWith(".tmx", StringComparison.CurrentCultureIgnoreCase))
            {
                area_name += ".tmx";
            }

            next_area = area_name;

            queued_spawn_point = spawn_point;
        }

        Action on_door_trans_complete;
        int ticks_for_door_trans = 16; // time it takes for the player to move the offset set in door / speed player moves.

        public void StartDoorTransition(Action on_complete)
        {
            set_game_state(game_states.door_trans_out);
            on_door_trans_complete = on_complete;
        }

        private void LoadMap(string map_string)
        {
            save_data.checkpoint_map_name = map_string;

            game_world.transition_to_area(map_string);
            reloadmap(map_string);

            // Create a new grid and let each cell have a default traversal cost of 1.0
            map_grid = new Grid(game_world.cur_area.width, game_world.cur_area.height, 1.0f);

            for (int x = 0; x < game_world.cur_area.width; x++)
            {
                for (int y = 0; y < game_world.cur_area.height; y++)
                {
                    if (game_world.cur_area.fget(x, y, area.collision_flag.all))
                    {
                        map_grid.BlockCell(new Position(x, y));
                    }
                }
            }
        }

        public override string GetMapString()
        {
            return "";
        }

        public override Dictionary<int, string> GetMusicPaths()
        {
            return new Dictionary<int, string>();
        }

        public override List<string> GetSheetPath()
        {
            return new List<string>() { @"raw\top_down\top_down_gfx_bank_0", @"raw\top_down\top_down_gfx_bank_1", @"raw\top_down\top_down_gfx_bank_2", @"raw\top_down\top_down_gfx_bank_3", @"raw\top_down\top_down_gfx_bank_4", @"raw\top_down\top_down_gfx_bank_5" };
        }

        public override Dictionary<int, string> GetSoundEffectPaths()
        {
            return new Dictionary<int, string>();
        }

        public override Dictionary<string, object> GetScriptFunctions()
        {
            Dictionary<string, object> Funcs = new Dictionary<string, object>();
            Funcs.Add("queueloadmap", (Action<string, Vector2?>)QueueLoadMap);
            Funcs.Add("open", (Action<string, Vector2?>)QueueLoadMap);
            Action toggle_god = new Action(( ) => { god_mode_enabled = !god_mode_enabled;  });
            Funcs.Add("god", toggle_god);
            return Funcs;
            // TODO
        }

        public override string GetPalTextureString()
        {
            return "raw/top_down/top_down_pal";
        }

        public override Tuple<int, int> GetResolution()
        {
            return new Tuple<int, int>(160, 128);
        }

        public int[] get_cur_pal()
        {
            if (night_vision_enabled)
            {
                if (game_world.cur_area.light_status == area.lighting_status.off)
                {
                    return night_vision_pal;
                }
                else
                {
                    return night_vision_lights_pal;
                }
            }
            else if (ir_vision_enabled)
            {
                if (game_world.cur_area.light_status == area.lighting_status.off)
                {
                    return ir_vision_lights_pal;
                }
                else
                {
                    return ir_vision_pal;
                }
            }
            else if (game_world.cur_area.light_status == area.lighting_status.off)
            {
                return lights_out_pal;
            }
            else
            {
                return default_pal;
            }
        }

        public bool is_gameplay_input_enabled()
        {
            int cur_ui = game_world.ui_area.objs_active.Count - 1;
            if (cur_ui >= 0)
            {
                ui_element e = game_world.ui_area.objs_active[cur_ui] as ui_element;

                if (e != null)
                {
                    return !e.HandleInput();
                }
            }

            return game_world.ui_area.objs_active.Count == 0;
        }

        // debug wrappers for draw functions.
        //

        public void line_d(float x0, float y0, float x1, float y1, int col)
        {
#if DEBUG
            if (debug_enabled)
            {
                DebugDrawQueue.Add(() => line(x0, y0, x1, y1, col));
            }
#endif
        }

        public void rect_d(float x0, float y0, float x1, float y1, int col)
        {
#if DEBUG
            if (debug_enabled)
            {
                DebugDrawQueue.Add(() => rect(x0, y0, x1, y1, col));
            }
#endif
        }

        public void rectfill_d(float x0, float y0, float x1, float y1, int col)
        {
#if DEBUG
            if (debug_enabled)
            {
                DebugDrawQueue.Add(() => rectfill(x0, y0, x1, y1, col));
            }
#endif
        }

        public void circ_d(float x, float y, float r, int col)
        {
#if DEBUG
            if (debug_enabled)
            {
                DebugDrawQueue.Add(() => circ(x, y, r, col));
            }
#endif
        }

        public void print_d(string str, float x, float y, int col)
        {
#if DEBUG
            if (debug_enabled)
            {
                DebugDrawQueue.Add(() => print(str, x, y, col));
            }
#endif
        }

        public void pset_d(float x, float y, int c)
        {
#if DEBUG
            if (debug_enabled)
            {
                DebugDrawQueue.Add(() => pset(x, y, c));
            }
#endif
        }

        public float easeinquart(float t, float b, float c, float d)
        {
            t /= d;
            var ts = (t) * t;
            var tc = ts * t;

            return b + c * (-11.3475f * tc * ts + 19.4475f * ts * ts + -7.8f * tc + 0.8f * ts + -0.2f * t);
        }

        public float easeoutquint(float t, float b, float c, float d)
        {
            t /= d;
            t -= 1;
            return c * (t * t * t * t * t + 1) + b;
        }
        public float easeoutelastic(float t, float b, float c, float d)
        {
            t /= d;
            var ts = (t) * t;
            var tc = ts * t;
            return b + c * (33.0f * tc * ts + -106.0f * ts * ts + 126.0f * tc + -67.0f * ts + 15.0f * t);
        }

        public float easeinoutquint(float t, float b, float c, float d)
        {
            var ts = (t /= d) * t;
            var tc = ts * t;
            return b + c * (6 * tc * ts + -15 * ts * ts + 10 * tc);
        }


        

    }
}
