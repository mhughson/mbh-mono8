﻿using Microsoft.Xna.Framework;
using PicoX;
using RoyT.AStar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono8Samples.TopDown;

namespace Mono8Samples.TopDown
{
    /// <summary>
    /// Manages a single level of the game. All the objects and such.
    /// </summary>
    public class area
    {
        public class view_area
        {
            public Vector2 pos_min;
            public Vector2 pos_max;

            public float width;
            public float height;

            public bool unlocked; // has the player entered here yet.
        }
        public List<obj> objs_active { get; private set; }
        public List<obj> objs_queue { get; private set; }
        public List<TopDown.path> paths;
        public List<obj> poi;
        public List<view_area> views;

        public float pull_threshold = 16;
        public Vector2 pull_threshold_offset = Vector2.Zero;
        public bool allow_backtrack_y = true;

        public Vector2 spawn_point;

        public enum collision_flag
        {
            none = 0,
            low = 1 << 0,
            high = 1 << 1,

            door = 1 << 2,

            all = low | high,
        }

        public enum lighting_status
        {
            off,
            on,
        }

        public enum game_modes
        {
            action,
            stealth,
        }

        public struct collision_data
        {
            public collision_flag flag;
        }
        public int width;
        public int height;
        public collision_data[] col_data;
        public lighting_status light_status = lighting_status.on;
        public game_modes game_mode = game_modes.stealth;
        TopDown Game;

        public area(int width, int height, PicoXGame owner)
        {
            Game = owner as TopDown;
            objs_active = new List<obj>();
            objs_queue = new List<obj>();
            paths = new List<TopDown.path>();
            poi = new List<obj>();
            views = new List<view_area>();
            this.width = width;
            this.height = height;
            col_data = new collision_data[width * height];
        }

        public collision_flag fget(int x, int y)
        {
            if (x < 0 || y < 0 || x >= width || y >= height)
            {
                return collision_flag.none;
            }

            return (col_data[x % width + y * width].flag);
        }

        public bool fget(int x, int y, collision_flag flag)
        {
            return (fget(x, y) & flag) != collision_flag.none;
        }

        public void fset(int x, int y, collision_flag flag)
        {
            if (x < 0 || y < 0 || x >= width || y >= height)
            {
                return;
            }

            col_data[(x % width) + (y * width)].flag = flag;

            if (fget(x, y, collision_flag.all))
            {
                // This tile is now blocked.
                Game.map_grid?.BlockCell(new Position(x, y));
            }
            else
            {
                // This tile is now clear.
                Game.map_grid?.UnblockCell(new Position(x, y));
            }
        }

        public lighting_status toggle_lights()
        {
            switch (light_status)
            {
                case lighting_status.off:
                    {
                        light_status = lighting_status.on;
                        break;
                    }

                case lighting_status.on:
                    {
                        light_status = lighting_status.off;
                        break;
                    }
            }

            return light_status;
        }

        public obj find_obj_by_id_name(string id_name)
        {
            obj found = objs_active.Find((obj o) => { return (o.id_name == id_name); });

            if (found != null)
            {
                return found;
            }

            return objs_queue.Find((obj o) => { return (o.id_name == id_name); });
        }

        public void update_cur_view()
        {
            player p = Game.game_world.get_player();

            if (p == null || views.Count <= 0)
            {
                cur_view = new view_area();
            }
            else
            {
                Vector2 group_min = new Vector2(float.MaxValue, float.MaxValue);
                Vector2 group_max = group_min * -1;


                for (int i = 0; i < views.Count; i++)
                {
                    view_area v = views[i];

                    // If this area isn't unlocked yet, see if the player is inside of it.
                    if (!v.unlocked)
                    {
                        //int px = (int)p.col_box().x;
                        //int py = (int)p.col_box().y;

                        //if (px >= v.pos_min.X && px <= v.pos_max.X &&
                        //    py >= v.pos_min.Y && py <= v.pos_max.Y)
                        //{
                        //    v.unlocked = true;
                        //}

                        rect temp = new rect(v.pos_min.X + v.width * 0.5f, v.pos_min.Y + v.height * 0.5f, v.width, v.height);

                        if (temp.intersects(p.col_box()))
                        {
                            v.unlocked = true;
                        }
                    }

                    if (v.unlocked)
                    {
                        // Take the one that is further away from the "center"
                        group_min.X = group_min.X > v.pos_min.X ? v.pos_min.X : group_min.X;
                        group_min.Y = group_min.Y > v.pos_min.Y ? v.pos_min.Y : group_min.Y;
                        group_max.X = group_max.X < v.pos_max.X ? v.pos_max.X : group_max.X;
                        group_max.Y = group_max.Y < v.pos_max.Y ? v.pos_max.Y : group_max.Y;
                    }
                }

                // Clamp for maps that don't allow back tracking.
                if (!Game.game_cam.allow_backtrack_y && group_max.Y > back_track_max_y)
                {
                    group_max.Y = back_track_max_y;
                }

                // Create a new cur_area based on the the min and max of all unlocked areas.
                cur_view = new view_area()
                {
                    pos_min = new Vector2(group_min.X, group_min.Y),
                    pos_max = new Vector2(group_max.X, group_max.Y),

                    width = (group_max.X - group_min.X),
                    height = (group_max.Y - group_min.Y),
                };
            }
        }
        
        // A little hack to prevent going backwards on some levels.
        public float back_track_max_y = float.MaxValue;
        private view_area _cur_view;

        // TODO: Support multiple views.
        public view_area cur_view
        {
            set
            {
                _cur_view = value;
            }
            get
            {
                return _cur_view;// views.Count > 0 ? views[0] : new view_area();
                
                // WIP_VIEWS
                /*
                player p = Game.game_world.get_player();

                if (p == null || views.Count <= 0)
                {
                    return new view_area();
                }
                else
                {
                    Vector2 group_min = Vector2.Zero;
                    Vector2 group_max = Vector2.Zero;

                    for(int i = 0; i < views.Count; i++)
                    {
                        view_area v = views[i];

                        int px = (int)p.col_box().x;
                        int py = (int)p.col_box().y;

                        float view_width_half = v.width * 0.5f;
                        float view_height_half = v.height * 0.5f;

                        if (px >= v.pos_min.X - view_width_half && px <= v.pos_max.X + view_width_half && 
                            py >= v.pos_min.Y - view_height_half && py <= v.pos_max.Y + view_height_half)
                        {
                            //return v;
                            v.unlocked = true;
                        }

                        group_min.X = Game.min(group_min.X, v.pos_min.X);
                        group_min.Y = Game.min(group_min.Y, v.pos_min.Y);

                        group_max.X = Game.max(group_max.X, v.pos_max.X);
                        group_max.Y = Game.max(group_max.Y, v.pos_max.Y);
                    }

                    float width_half = Game.min((float)(group_max.X - group_min.X) * 0.5f, 64.0f);
                    float height_half = Game.min((float)(group_max.Y - group_min.Y) * 0.5f, 64.0f);
                    view_area new_area = new view_area()
                    {
                        pos_min = new Vector2((float)o.X + width_half, (float)o.Y + height_half),
                        pos_max = new Vector2((float)o.X + (float)o.Width - width_half, (float)o.Y + (float)o.Height - height_half),

                        width = (float)o.Width,
                        height = (float)o.Height,
                    });
                }
                */
            }
        }
    }
}
