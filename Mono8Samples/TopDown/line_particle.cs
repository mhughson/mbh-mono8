﻿using Microsoft.Xna.Framework;
using PicoX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono8Samples.TopDown
{
    public class line_particle : obj
    {
        public line_particle(float _x, float _y, float _dx, float _dy, int _col, int _maxhist, float _g, float _dec, PicoXGame owner) : base(owner)
        {
            x = _x;
            y = _y;
            dx = _dx;
            dy = _dy;
            decel = _dec;
            g = _g;
            hist = new List<Vector2>();
            maxhist = _maxhist;
            col = _col;
            stopped = false;
            lifetime = -1;

            hist.Add(new Vector2(x, y));
        }

        //deceleration
        public float decel;
        //gravity
        public float g;
        //history of positions
        public List<Vector2> hist;
        //Game.max length of history to record
        public int maxhist;
        //color
        public int col;
        //has it slowed to a stop
        public bool stopped;
        //how long should it live
        public int lifetime;

        //update
        public override void update()
        {
            //once stopped, do nothing.
            if (stopped)
            {
                return;
            }

            //died of old age?
            if (lifetime != -1)
            {
                lifetime -= 1;


                if (lifetime <= 0)
                {
                    destroy();
                    return;
                }
            }

            //fall
            dy += g;

            //slow down
            dx *= decel;
            dy *= decel;

            //detect slow enough to stop.
            if (Game.abs(dx) <= 0.2f && Game.abs(dy) <= 0.2f && decel != 1.0f)
            {
                stopped = true;
                return;
            }

            //move
            x += dx;
            y += dy;

            //save history of position
            hist.Add(new Vector2(x, y));

            if (hist.Count > maxhist)
            {
                hist.RemoveAt(0);
            }

            //clean up when off bottom.
            //todo: detect offscreen in 
            //general.
            //if (hist[0].y > Game.camy + 128)
            //{
            //destroy();
            //    return;
            //}

            //base.update();
        }

        //draw
        public override void draw()
        {
            Game.apply_pal(Game.get_cur_pal());
            //draw a line between each 
            //position in history, creating
            //a kind of curve.
            if (hist.Count > 1)
            {
                for (int i = 0; i < hist.Count - 1; i++)
                {
                    var p1 = hist[i];
                    var p2 = hist[i + 1];
                    Game.line(p1.X, p1.Y, p2.X, p2.Y, col);
                    //				Game.pset(p1.x,p1.y,col)			
                }
            }
            Game.pal();

            //base.draw();
        }
    }
}
