﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PicoX;

namespace Mono8Samples.TopDown
{
    class gear_keycard : gear
    {
        int security_level;

        public gear_keycard(gear_types t, float pos_x, float pos_y, int security_level, PicoXGame owner, area owning_area = null) : base(t, pos_x, pos_y, owner, owning_area)
        {
            this.security_level = security_level;

            anims = new Dictionary<string, anim>()
            {
                {
                    "on_ground",
                    new anim()
                    {
                        ticks=1,//how long is each frame shown.
                        frames= new int[][] { new int[] { 60 } },//what frames are shown.
                    }
                },
            };

            set_anim("on_ground");
            display_name = "k.card (l." + security_level + ")";
            on_toggle = () =>
            {
                /* TODO: set security level */
                int new_level = 0;
                if (enabled)
                {
                    new_level = security_level;
                }
                Game.game_world.get_player().security_level = new_level;
            };
        }

        public override void draw()
        {
            if (owner != null)
            {
                return;
            }
            

            base.draw();

            int[] p = Game.get_cur_pal();
            
            float _y = y + Game.sin((Game.tick * 0.005f)) * 4.0f - 7.0f;

            Game.print(security_level.ToString(), x-4, _y, p[25]);

            Game.pal();
        }
    }
}
